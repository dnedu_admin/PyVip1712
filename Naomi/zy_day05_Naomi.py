# Coding = utf-8
# Author: Naomi-Lau (qq:843047623)
# File: zy_day05_Naomi.py
# Time: 2018/1/25 11:08

# 1.自己写一个字典:name_dict = {'name':'ben','age':22,'sex':'男'} 添加,删除,更新,清空操作

# name_dict = {'name':'ben','age':22,'sex':'男'}
# name_dict['addr'] = '北京'
# print(name_dict.pop('age'))
# print(name_dict)
# add_dict = {'other':'kity'}
# name_dict.update(add_dict)
# print(name_dict, add_dict)
# name_dict.clear()
# print(name_dict)


# 写两个集合:num1_set = {3,5,1,2,7} num2_set = {1,2,3,11} 并分别进行&,|,^,-运算

# num1_set = {3,5,1,2,7}
# num2_set = {1,2,3,11}
# print(num1_set & num2_set)
# print(num1_set | num2_set)
# print(num1_set - num2_set)
# print(num1_set ^ num2_set)
# print(num2_set - num1_set)

# 3.整数字典:num_dict = {'a':13,'b':22,'c':18,'d':24} 按照dict中value从小到大的顺序排序

# num_dict = {'a':13,'b':22,'c':18,'d':24}
# num_order = dict(sorted(num_dict.items(), key = lambda item:item[1]))
# num_dict = num_order
# print(num_dict)

