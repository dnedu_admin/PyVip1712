# Coding = utf-8
# Author: Naomi-Lau (qq:843047623)
# File: zy_day04_Naomi.py
# Time: 2018/1/24 07:52

# 第一题:
# num_list = [[1,2],[‘tom’,’jim’],(3,4),[‘ben’]]
#  1. 在’ben’后面添加’kity’
num_list = [[1,2],['tom','jim'],(3,4),['ben']]
num_list.append("kity")
print(num_list)

#  2. 获取包含’ben’的list的元素
print(num_list[-2])

#  3. 把’jim’修改为’lucy’
num_list[1] = ['tom','lucy']
print(num_list)

#  4. 尝试修改3为5,看看
print(type(num_list[2]))
# 元组不可改变

#  5. 把[6,7]添加到[‘tom’,’jim’]中作为第三个元素
#做法一：这个print不出来，是正确的吗？
# num_list_redef = list(['tom','jim'])
# num_list_redef_append = num_list_redef.append("[6,7]")
# num_list_pop = num_list.pop(1)
# num_list = num_list.insert(1,num_list_redef)

#做法二
# num_list_redef = list(['tom','jim'])
# num_list_redef_append = num_list_redef.append("[6,7]")
# num_list_pop = num_list.pop(1)
# num_list[1] = num_list_redef

#  6.把num_list切片操作: num_list[-1::-1]
print(num_list[-1::-1])

# 第二题:
# numbers = [1,3,5,7,8,25,4,20,29];
# 1.对list所有的元素按从小到大的顺序排序
numbers = [1,3,5,7,8,25,4,20,29]
print(sorted(numbers))

# 2.求list所有元素之和
print(sum(numbers))

# 3.将所有元素倒序排列
print(sorted(numbers,reverse=True))








