"""
一、简答题
1. 列表list_1 = [[1,2,3,4,5],[{“name”:”张三”,”age”:28},{“name”:”James”:”age:30}]] ,获取James的年龄

"""
# list_1 = [[1,2,3,4,5],[{"name":"张三","age":28},{"name":"James","age":30}]]
# James=list_1[1][1]
# print(James["age"])


'''
2. 名词解释: 可迭代对象、迭代器、生成器、装饰器
可迭代对象：可以直接作用于 for 循环的对象统称为可迭代对象：Iteration
迭代器：可以被 next（） 函数调用并不断返回下一个值得对象称为迭代器 Iterator
生成器：不但可以作用于 for 循环，还可以被next（）函数不断调用，直到抛出StopIteration错误
装饰器：在代码运行期间动态增加功能，在函数调用前后自动打印日志，但又不修改函数的定义

'''

'''
3. 名词解释：面向对象编程中“类”中有几种方法？分别解释各种方法的用途。
实例方法：第一个参数名是self，代表实例本身，用于绑定各种属性在实例上。
静态函数方法：调用静态方法可以无需创建对象，当它运行时不需要实例和类参与就能直接调用。
类方法：将类本身作为对象进行操作的方法。

'''

# 4. 输出以下代码运行结果，解释变量作用域LEGB
# x = 2 #Global
# z = 3
# def func_outer():
#     y = 9
#     x = 0 #Enclosing
#     global z
#     def func_inner():
#         nonlocal x #local
#         x += 5
#         z = 6
#         print("func_inner",max(x,z),max(x,y))
#     z += 3
#     x += 2
#     print(x,y,z)
#     return func_inner
# func_outer()()
`
'''
5. 解释什么是GIL(全局解释器锁)
在调用外部代码时，GIL将会被锁定，保证同一时刻只有一个线程在运行，知道这个函数结束为止。

6. 什么是线程、协程、进程
进程：一个程序执行的总和
线程：线程运行在进程空间内，是操作系统能够进行运算调度的最小单元，同一进程所产生的线程共享同一内存空间。
协程：分解一个线程成为多个“微线程”，在一个线程中规定某个代码块的执行顺序。

7. 什么时候该用多线程、什么情况该用多进程
如果代码时CPU密集型，使用多进程
如果代码时IO密集型，使用多线程

8. 多个进程间如何通信？
管道pipe，进程队列queue，共享数据manage

'''
