# 1.请写出一段Python代码实现删除一个list里面的重复元素
# a = [1,2,4,5,5,5,6,7,8,8]
# b = list(set(a))
# print(b)


# 2.如何用Python来进行查询和替换一个文本字符串？
# st_a = 'qwertyuiop'
# a = ('qwe')  # 要查询的字符串
# b = ('asd')  # 要替换的字符串
# st_b = st_a.replace(a, b)
# print(st_b)


# 3.a=1, b=2, 不用中间变量交换a和b的值
# a = 1
# b = 2
# a, b = b, a
# print(a,b)


# 4.写一个函数, 输入一个字符串, 返回倒序排列的结果:?如: string_reverse(‘abcdef’), 返回: ‘fedcba’
# 5种方法:
# 1. 简单的步长为-1, 即字符串的翻转;
# def re(text=input("请输入字符串")):
#     return text[::-1]
# print(re())

# 2. 交换前后字母的位置;
# def re(text = input("请输入字符")):
#     L=list(text)
#     L.reverse()
#     return ''.join(L)
# print(re())

# 3. 递归的方式;
# def re(text = input("请输入字符")):
#     if len(text) <=1:
#         return text
#     else:
#         return text[-1]+re(text[:-1])
# print(re())

# 4. 使用extendleft()函数;
# from collections import deque
# def re(text = input("请输入字符")):
#     d = deque()
#     d.extendleft(text)
#     return ''.join(d)
# print(re())

# 5. 使用for循环;
# def re(text=input("请输入字符串")):
#     new_t=[]
#     for i in range(1,len(text)+1):
#         new_t.append(text[-i])
#     return ''.join(new_t)
# print(re())


# 5.请用自己的算法, 按升序合并如下两个list, 并去除重复的元素
# 合并链表, 递归的快速排序, 去重;
# L_a = [1, 2, 3, 1, 8]
# L_b = [2, 5, 6, 8, 2]
# L = set(L_a + L_b)
# print(L)
