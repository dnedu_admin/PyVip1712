#coding=utf-8
# # 1.自己动手写一个学生类,创建属性名字,年龄,性别,完成
#      __new__,__init__,__str__,__del__，__call__.
class Student():
     def __init__(self,name,age,sex):#第二调用初始化函数
          self.name=name
          self.age=age
          self.sex=sex
          print(name,age,sex)
     def __del__(self):#最后调用析构函数
          print("最后调用析构函数")
     def get_info(self):
          print('name is %s'%self.name)
     def __new__(cls, *args, **kwargs):#优先调用构造函数
          print("优先调用构造函数")
          return super().__new__(cls)
     def __str__(self):
          print('2')
     def __call__(self, *args, **kwargs):
          print("魔法")
          # return self.name
student=Student('lmin',29,'man')
student.get_info()
student()#魔法方法 调用本身
student.__str__()#实例调用__str__



# 2.创建父类Person,属性name,函数eat,run,创建子类Student,学生类
#   继承Person,属性name,age,id,函数eat,study,全局定义一个函数
#   get_name,传入一个Person类的实例参数,分别传入Person和Student
#   两个类的实例对象进行调用.
# class Person():
#      def __init__(self,name):
#           self.name=name
#           # print(self.name)
#      def eat(self):
#           print("2")
#      def run(self):
#           print("333")
# class Student(Person):
#      def __init__(self,name,age,id):
#           self.name=name
#           self.age=age
#           self.id=id
#           # print(self.age)
#      def eat(self):
#           print("4444")
#      def study(self):
#           print("8888")
# p1=Person('lmin')
# s1=Student('lmin',22,44)
# def get_name(person):
#      person.eat()
# get_name(p1)
# get_name(s1)


# 3.用代码完成python多态例子和鸭子模型

# class father():#多态例子
#      def eat(self):
#           print("I am baba")
# class son(father):
#      def eat(self):
#           print("I am son")
# f1=father()
# s1=son()
# def get_all(p):
#      p.eat()
# get_all(f1)
# get_all(s1)


# class father():#鸭子模型
#      def eat(self):
#           print("I am baba")
# class son():
#      def eat(self):
#           print("I am son")
# f1 = father()
# s1 = son()
# def get_all(p):
#      p.eat()
# get_all(f1)
# get_all(s1)

