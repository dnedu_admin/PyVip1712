#coding=utf-8
#使用函数缓存
from functools import lru_cache
@lru_cache(maxsize=10,typed=False)
def get_mouth(a):
    for i in range(1,a):
        math1=pow(i,20)
    print(math1)
#***********************************************
#性能分析器的使用
import  cProfile
def get_mouth_1(a):
    for i in range(1,a):
        math2=pow(i,20)
    print(math2)

from line_profiler import LineProfiler
def get_mouth_2(a):
    for i in range(1,a):
        math3=pow(i,20)
    print(math3)
pro=LineProfiler(get_mouth_2)
pro.enable()
get_mouth_2(1000007)
pro.disable()
#******************************************************
# 内存分析器的使用
from memory_profiler import profile
@profile
def get_mouth_3():
    y=9*(11**6**3)
    x=8*(10**5**3)
    del y
    del x
from time import time
if __name__=="__main__":
    time1=time()
    get_mouth(1000007)
    time2=time()
    print(time2-time1)
    print('程序已经运行完毕共用时间%s'%str(time2-time1))
#*******************************************************
    time3=time()
    cProfile.run('get_mouth_1(1000007)')
    time4=time()
    print(time4-time3)
#*******************************************************
    pro=LineProfiler(get_mouth_2)
    pro.enable()
    get_mouth_2(1000007)
    pro.disable()
#*******************************************************
    get_mouth_3()