#coding=utf-8
# 1.使用pickle,json(注意:两个模块)
# 把字典a_dict = [1,2,3,[4,5,6]]
# 序列化与反序列化,序列化,保存到文件名为serialize.txt文件里面,然后大开,并读取数据
# 反序列化为python对象加载到内存,并打印输出
# 尝试序列化/反序列非文件中,直接操作
# import pickle
from io import BytesIO
# a_list=[1,2,3,[4,5,6]]
# f=open('serialize.txt','wb+')
# pickle.dump(a_list,f,0)
# f.close()
#
# f1=open("serialize.txt","rb+")
# b_list=pickle.load(f1)
# print(b_list)
# f1.close()
import  json
from io import StringIO
# c_list=[1,2,3,[4,5,6]]
# f2=open('serialize11.txt','w')
# json.dump(c_list,f2)
# f.close()

# f3=open("serialize11.txt","r")
# d_list=json.load(f3)
# print(d_list)
# f3.close()


# 2.自己写代码对比深浅拷贝的区别,写完代码然后看看运行结果,然后分析原因
#    比如：a_list=[1,2,3,4,[8,9,0]]
# from copy import copy
# from copy import deepcopy
# a_list=[1,2,3,4,[8,9,[5,6,6,7],0]]
# b_list=list(a_list)
# c_list=copy(b_list)
# e_list=deepcopy(c_list)
# b_list[4][2][2]=9999
# print(a_list)
# print(b_list)
# print(c_list)
# print(e_list)



# 3.分别写代码完成内存缓冲区文本数据和二进制数据的读/写操作

# from io import StringIO
# wenb=StringIO()
# wenb.write('good good study day day up\nMy heart will go on')
# wenb.write('python is a simple programm language\n')
# print(wenb.getvalue())
#
# wenb2=StringIO('good good study day day up\nMy heart will go on\npython is a simple programm language')
# # print(wenb2.readlines())
# while 1:
#     s=wenb2.readline()
#     if len(s)!=0:
#         print(s)
#     else:
#         break
#
from io import BytesIO
wenb2=BytesIO()
wenb2.write('good good study day day up\nMy heart will go on\n'.encode("utf-8"))
wenb2.write('python is a simple programm language\n'.encode("utf-8"))
print(wenb2.getvalue().decode("utf-8"))
# #
wenb2=BytesIO('good good study day day up\nMy heart will go on\npython is a simple programm language'.encode("Utf-8"))
print(wenb2.readlines())###打印的结果为什么会带b'
# while 1:
#     s=wenb2.readline()
#     if len(s)!=0:
#         print(s)
#     else:
#         break
#
