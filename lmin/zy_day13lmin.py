#coding=utf-8
#1.自己写代码对比2.x深度优先和3.x广度优先的继承顺序
# class Mater():
#     def teach(self):
#         print("teach language")
# class S1():
#     # def teach(self):
#     #     print("I am a learner")
#     pass
# class S2():
#     def teach(self):
#         print("I am a learner too")
# class S3(S1,Mater):
#     pass
# class S4(S3,Mater):
#     pass
# s=S4()
# s.teach()

# 2.写一个类MyClass,使用__slots__属性,定义属性(id,name),创建该类实例
#     后,动态附加属性age,然后执行代码看看.
class Myclass():
    __slots__ = ("id","name")
p1=Myclass()
p1.name='lmin'
print(p1.name)
p1.age='22'
print(p1.age)


# 3.使用type创建一个类,属性name,age,sex,方法sleep,run并创建此类的
#     实例对象
# def sleep(self):
#     print("睡觉中")
# def run(self):
#     print("奔跑中")
# Girl=type('Girl',(),{"name":'name',"age":'age',"sex":'sex',"sleep":sleep,"run":run})
# girl=Girl()
# print(girl.name)
# girl.sleep()
# girl.run()

#4.写例子完成属性包装,包括获取属性,设置属性,删除属性
# class New():
#     @property
#     def name(self):
#         return self._name
#     @name.setter
#     def name(self,name):
#         self._name=name
#     @name.deleter
#     def name(self):
#         del self._name
# n=New()
# n.name="lmin"
# print(n.name)
# del n.name
# print(n.name)