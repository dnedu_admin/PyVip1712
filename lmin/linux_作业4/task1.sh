#!/bin/bash
#求和，计算1+2+3+...+100的值for i in {1..100}
sum=0
for i in ` seq 1 100 `
do
	sum=$((i+$sum))
	i=$(($i+1))
done
echo "$sum"
