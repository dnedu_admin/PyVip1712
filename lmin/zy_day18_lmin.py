#coding=utf-8
# 1.计算range(1,99999)的总和,采取三种方式,第一:重复10次计算总时间;
#  第二:开启十个子线程去计算;第三:开启十个进程去计算,然后对比耗时
#
# import time
# print("第一种:重复10次计算总时间")
# def jisuan():
#     time1 = time.time()
#     for i in range(1,11):
#         j=sum(range(1,999999))
#         # print(j)
#     time2 = time.time()
#     print(time2-time1)
# jisuan()



# print("第二种:开启十个子线程去计算")
# from multiprocessing import Process
# def jisuan1():
#     j1=sum(range(1,999999))
#     # print(j1)
# if __name__=='__main__':
#     time3 = time.time()
#     print("time1为",time3)
#     pro=Process(target=jisuan1(),args=())
#     pro = Process(target=jisuan1(), args=())
#     pro = Process(target=jisuan1(), args=())
#     pro = Process(target=jisuan1(), args=())
#     pro = Process(target=jisuan1(), args=())
#     pro = Process(target=jisuan1(), args=())
#     pro = Process(target=jisuan1(), args=())
#     pro = Process(target=jisuan1(), args=())
#     pro = Process(target=jisuan1(), args=())
#     pro = Process(target=jisuan1(), args=())#构建10个子进程
#     pro.start()#运行
#     time4 = time.time()
#     print("time2为:",time4)
#     print("用时:",time4 - time3)
#     pro.join()


from threading import Thread

def jisuan2(num):
    j1=sum(range(1,999999))
    print(j1)

thread_list=[Thread(target=jisuan2,args=(num,)) for num in range(1,11)]
for thr in thread_list:
    thr.start()
    thr.join()
