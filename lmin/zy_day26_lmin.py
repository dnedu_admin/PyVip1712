# 1. PKCS#5(选择一种hash算法)加密算法加密你的中文名字,写出代码
#
# import hashlib
# m=hashlib.md5()
# m.update('lmin'.encode('utf-8'))
# print(m.hexdigest())
#
#
# # 2. 创建文件test.py,写入一些数据,并计算该文件的md5值
# import hashlib
# with open('test.py','w',encoding='utf-8') as f:
#     m=hashlib.md5('today I am very tired'.encode('utf-8'))
#     f.write(m.hexdigest())
#     print("已经建立了文件！！！")


# 3. 遵循上下文管理协议,自己编写一个类,去创建一个上下文管理器,然后自己使用看看
   # 方式1:普通方式
   # 方式2:使用contextmanager
# # ---------------------------------------------------------------------------------
# class Myclass():
#     def __init__(self,name):
#         self.name=name
#         print(name)
#     def __enter__(self):
#         print('请问老师：此处的业务逻辑应该和上面的self.name关联吗？还是独立的业务逻辑代码？')
#         for i in range(1,4):
#             print(i)
#         return self
#     def __exit__(self, exc_type, exc_val, exc_tb):
#         print('算了，还是不写了。就把资源给回收吧！')
#         print('请问ben老师：怎么把资源回收或者说怎么查看是不是已经把资源回收了？')
#     def work(self):
#         for i in self.name:
#             print(i)
# with Myclass('lmin') as f:
#     f.work()
#*****************************************************************************
from contextlib import  contextmanager
class Your_class():
    def __init__(self,a_lsit):
        self.a_list=a_lsit
        print('请问ben老师：传入的必须是可迭代对象吗？其他的对象可以吗？是根据业务逻辑来判断要传入的对像吗？')
    def run(self):
        for i in self.a_list:
            print(i)
        print('此处也是根据业务逻辑来写代码吗？还是说一定要和上面的初始化函数要联系一起使用？？？')
@contextmanager
def work(a_list):
    print('还是不找到咋使用，就是敲代码？')
    t=Your_class(a_list)
    yield t
    print(t)
    print("麻烦ben老师：这些问题直接给我QQ留言吧？")
with work([1,2,3,4]) as e:
    e.run()


# 4.使用colorama模块使用多颜色输出
# from   colorama import Fore,Back,Style
# print(Fore.BLACK+'你好')
# print(Fore.YELLOW+Back.WHITE+Fore.BLUE+"你好")
# print(Style.RESET_ALL)
# print('普通颜色')