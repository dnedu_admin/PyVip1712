# #coding=utf-8
#--------------------------------
#1.请写出一段Python代码实现删除一个list里面的重复元素
# def del_list(a_list):
#     b_list=[]
#     for i in a_list:
#         if i not in b_list:
#             b_list.append(i)
# #     print (b_list)
# # del_list([1,4,5,3,5,3,8,9,0])
# # #---------------------------------------
# #a=1, b=2, 不用中间变量交换a和b的值
# # # def change(a,b):
# # #     a,b=b,a
# # #     print(a,b)
# # # #---------------------------------------
# #如何用Python来进行查询和替换一个文本字符串？
# # def sel_replace(a_str,b_str,c_str):
# #    b=a_str.replace(b_str,c_str)
# #    print(b)
# # sel_replace('my name is liumin my fam is in henan','my','his')
# #
##----------------------------------------------
#用自己的算法, 按升序合并如下两个list, 并去除重复的元素
# def list_func(list1,list2):
#     list1.sort()
#     list2.sort()
#     b_list = list1 + list2
#     b_list.sort()
#     a_list=[]
#     for i in b_list:
#         if i not in a_list:
#             a_list.append(i)
#     print(a_list)
# list_func([1, 4, 5,8,9, 3, 8, 9, 0],[11, 44, 5, 35, 8, 6, 0])
#---------------------------------------------------
'''写一个函数, 输入一个字符串, 返回倒序排列的结果:?如: string_reverse(‘abcdef’), 返回: ‘fedcba’
5种方法的比较:
1. 简单的步长为-1, 即字符串的翻转;
2. 交换前后字母的位置;
3. 递归的方式, 每次输出一个字符; 起始索引 结束索引  步长（步长为正，从左往右。步长为负，从右往左。）
4. 双端队列, 使用extendleft()函数;
5. 使用for循环, 从左至右输出;
'''
def str_obj(str1):
    b=list(str1)
    b.reverse()
    print(str1[1:2:1])
    print(str1[-2:3:-1])
    print(b)
    print(str1[::-1])
    for i in str1:
        print(i)

str_obj('123456789')

import collections
d=collections.deque()
d.append(1)
d.extendleft(['a','b','c','d','e','f'])
print(d)