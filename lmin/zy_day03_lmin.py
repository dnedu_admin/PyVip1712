#coding:utf-8
#1.格式化显示浮点数12.78，要求保留3位小数，输出占20字符，空位用0补齐
# float_num=12.78
# print("%020.3f"%float_num)


#2. 将字符串”动脑学院”转换为bytes类型
# str_1="动脑学院"
# print(str.encode(str_1))

'''3. 将以下3个字符串合并为一个字符串，字符串之间用3个_分割

	hello  动脑  pythonvip
	合并为“hello___动脑___pythonvip'''
# str_1="hello"
# str_2="动脑"
# str_3="pythonvip"
# str_4=str_1+str_2+str_3#合并字符串
# str_5=str_4.partition("动脑")#以动脑来分割新的字符串
# print(str_5)
# str_6="___"
# str_7=str_6.join(str_5)#在新的字符串中指定以___生成新的字符串。
# print(str_7)

#4. 删除字符串  “    你好，动脑eric     ”首尾的空格符号
# str_1="    你好，动脑eric     "
# str_2= str_1.split()
# print(str_2)
'''
5. 用户信息存在如下所示字符串中，包含信息依次为  姓名   学号  年龄，
不同信息之间的空格数不确定，请提取用户信息分别保存到 xxx_name,xxx_num, xxx_age。
eric=“    Eric   dnpy_001     28”     
提取后 eric_name=“Eric”eric_num=”dnpy_001”,eric_age=”28”
zhangsan = “ 张三         dnpy_100    22     ”'''
# eric="    Eric   dnpy_001     28"
# eric_3=eric.split()
# eric_2=str(eric_3[0]).lower()
# name =eric_2+"_name"
# num= eric_2+"_num"
# age=eric_2+"_age"
# print("%s=%s %s=%s,%s=%s"%(name,eric_3[0],num,eric_3[1],age,eric_3[2]))
def get(user_info,data=None):
    eric_1=user_info.split()
    eric_2=str(eric_1[0]).lower()
    name =eric_2+"_name"
    num= eric_2+"_num"
    age=eric_2+"_age"
    print("%s=%s %s=%s,%s=%s"%(name,eric_1[0],num,eric_1[1],age,eric_1[2]))
get(user_info="    Eric   dnpy_001     28",data=None)
def get_all(user_info,data=None):
    get(user_info,data=None)
get_all(user_info="张三         dnpy_100    22 ",data=None)