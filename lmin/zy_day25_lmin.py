#coding=utf-8
#1.------------------------------------------------------------------------------------------------
# 在当前项目下创建目录abc,并在abc目录下,使用uuid产生唯一码并写入到文件
 # uuid.py中,并拷贝整个目录abc,新目录名为app,并将app目录下文件拷贝,新
 # 文件名为:new.py文件
# import os
# import uuid
# import shutil
# os.mkdir('./abc')
# with open('./abc/uuid.py','w')as e:
#     e.write(str(uuid.uuid1()))
#     e.close()
# shutil.copytree('abc','app')
# shutil.move('./abc/uuid.py','./app/new.py')#没有达到题目的要求。老师是怎么操作
#2.----------------------------------------------------------------------------------------------------
# 创建本地文件’one.txt’,在里写入部分数据(‘今天天气不错,我们去动物园n玩吧’
  # n从1-9),使用b64encode对每一行数据编码,然后写入到文件oen.txt中.每写
  # 完一行之后写入换行符.
  # 打开刚才创建的文件one.txt,以读取多行的形式读取文件中的内容,并把每一行的
  # 内容通过b64decode对每一行数据解码,解码之后每一行数据依然是二进制形式
  # 所以,继续对每一行数据通过utf-8的形式解码为可以普通字符串,然后判断每一行
  # 字符串是否包含’动物园6’的子字符串,如果包含,先输出换行符,然后则打印输出
  # 当前行的字符串,最后创建一个文件,’two.txt’,并把帅选之后的数据写入到这个新
  # 文件中.
import os
import uuid
import shutil
import base64
with open('one.txt','wb')as e:
    for i in range(1,10):
        t_text='今天天气不错,我们去动物园%s玩吧' %i
        # print(t_text)
        p_encode=base64.b64encode(t_text.encode('utf-8'))
        e.write(p_encode+'\n'.encode('utf-8'))
    e.close()
with open('one.txt','rb') as m:
    read_decodes=m.readlines()
    for read_decode in read_decodes:
        p_decode=base64.b64decode(read_decode)
        p_decode_u = bytes(p_decode).decode('utf-8')#网上查的。需要理解。。。
        # print(p_decode_u)
        a='动物园6'
        if a in p_decode_u:
            print('\n')
            print(p_decode_u)
            a_test=open('two.txt','w')
            a_test.write(p_decode_u)
            a_test.close()
        else:
            pass
# #----------------------------------------------------------------------------------------------------
# 3.准备一张.jpg图片,比如:mm.jpg,读取图片数据并通过b85encode加密之后写入到新文件mm.txt文件中,
#   然后读取mm.txt数据并解密之后然后写入到mmm.jpg文件中
# import base64
# with open("mm.jpg",'rb+')as e:
#     read_encode=base64.b64encode(e.read())
#     with open('mm.txt','wb+')as m:
#         ee=open('mm.jpg','rb+')
#         m.write(read_encode)
#         m.close()
#     e.close()
# with open('mmm.jpg','wb+')as f:
#     t=open('mm.txt','rb+')
#     write_decode = base64.b64decode(t.read())
#     f.write(write_decode)
#     f.close()
#     t.close()
