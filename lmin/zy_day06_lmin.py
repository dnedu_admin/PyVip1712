# #coding=utf-8
# 1.用while和for两种循环求计算1+3+5.....+45+47+49的值

a=1
b=0
while a<=49:
    b=a +b
    a=a+2
print(b)


j=0
for i in range(1,50,2):
    j=j+i
print(j)

# 代码实现斐波那契数列(比如前30个项)
list_1 = [1, 1]
for i in range(1,29):
    list_1.append(list_1[-2] + list_1[-1])
print(list_1)
print(len(list_1))