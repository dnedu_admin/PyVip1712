#coding:utf-8
import requests
from bs4 import BeautifulSoup
import os
import lxml
import re
import time
#
headers = {
    'User-Agent': "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36",
    'Cookie':"UM_distinctid=162dd6eb461493-0c861322392acd-3a614f0b-100200-162dd6eb4621d9; CNZZDATA3866066=cnzz_eid%3D1463299877-1494676185-%26ntime%3D1494676185; bdshare_firstime=1524133443090; Hm_lvt_9a737a8572f89206db6e9c301695b55a=1524133444,1524152852; Hm_lpvt_9a737a8572f89206db6e9c301695b55a=1524152854",
    "Referer":"http://www.mm131.com/mingxing/"
    }
def add_folder(web_txt):
    try:
        if os.path.exists(web_txt):
            pass
        else:
            os.mkdir(web_txt)
            print(web_txt)
        return web_txt
    except Exception as e:
        print(e)
def get_link(url):
    web_data=requests.get(url,headers=headers)
    web_data.encoding = "gb2312"
    Soup=BeautifulSoup(web_data.text,'lxml')
    page_link=Soup.find('dl',class_='list-left public-box').find_all('a',attrs={"target": "_blank"})
    try:
        for i in page_link:
            first_html=i['href']#找到总链接
            file_name=i.get_text()#找到文件名称
            print(file_name)
            data2=requests.get(first_html,headers=headers)
            data2.encoding='gb2312'
            Soup2=BeautifulSoup(data2.text,"lxml")
            web_text=Soup2.find('div',class_='content-page').find('span').get_text()#找到最大的页数
            mount_page=int(web_text.strip('共页'))+1
            for j in range(1,mount_page):
                if j==1:
                    new_all_link=first_html
                else:
                    new_all_link=first_html.replace('.html','_'+str(j)+'.html')
                end_url=get_pict_link(new_all_link)
                file_names=add_folder(file_name.strip(' '))
                time.sleep(0.5)
                down_pic(end_url,file_names)
    except Exception as e:
        print(e)
def down_pic(url,dir_name):#保存图片
    name=url.split('/')[-1]
    names=os.path.join(dir_name,name)
    print(names)
    data_end = requests.get(url, headers=headers)
    with open(names,'ab')as e:
        e.write(data_end.content)
def get_pict_link(url):#得到最总的图片链接
    datas=requests.get(url,headers=headers)
    datas.encoding = 'gb2312'
    Soup3 = BeautifulSoup(datas.text, "lxml")
    end_link = Soup3.find('div', class_='content-pic').find('a').find('img')['src']
    print(end_link)
    return end_link
if __name__=="__main__":
    for i in range(1,9):
        if i==1:
            links='http://www.mm131.com/mingxing/'
        else:
            links='http://www.mm131.com/mingxing/list_5_%s.html'%str(i)
        get_link('links')
