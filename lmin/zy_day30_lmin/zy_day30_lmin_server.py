#coding=utf-8
#******************************************************************
#点对点的数据传输，先接收客户端的一个文本文件，然后再传输一张图片给客户端。
# import socket
# host='127.0.0.1'
# port=9009
# s=socket.socket()
# s.bind((host,port))
# s.listen(5)
# connect,addr=s.accept()
# connt=connect.recv(1024)
# with open('2.txt','w') as e:
#     e.write(connt.decode('utf-8'))
#     print('已经接收完毕数据')
# print('开始给客户端回传数据')
# with open('1.jpg','rb')as e:
#     result=e.read()
#     connect.send(result)
# connt=connect.recv(8888)
# print(connt.decode('utf-8'))
# print("bye")
# connect.close()
# s.close()
#*************************************************************
# #一次链接多次通信（半双工模式）
# import  socket
# host='127.0.0.1'
# port =9009
# s=socket.socket()
# s.bind((host,port))
# s.listen(5)
# while True:
#     conn,addr=s.accept()
#     while True :
#         content=conn.recv(1024)
#         if len(content)==None:
#             break
#         else:
#             with open('lmin.txt','w')as e:
#                 e.write(content.decode('utf-8'))
#                 print('begin go back !')
#             with open('1.jpg','rb')as e:
#                 result=e.read()
#                 conn.send(result)
#                 print('传送完毕')
#     conn.close()
# s.close()
#*************************************************************
import socketserver
from socketserver import StreamRequestHandler as srh
host ='127.0.0.1'
port =9099
addr=(host,port)
class Myserver(srh):
    def handle(self):
        print('链接到服务器的客户端的信息为%s' %str(self.client_address))
        while 1:
            content=self.request.recv(1024)
            if not content:
                break
            with open('xianc.txt','w')as e:
                e.write(content.decode('utf-8'))
            with open('gg.jpg','rb')as e:
                result=e.read()
                self.request.send(result)
                print('data have transported')
            content=self.request.recv(1024)
            print('接收到的数据为%s' %content.decode('utf-8'))
            result=input('请输入需要发送给给客户端的数据：')
            self.request.send(result.encode('utf-8'))
            print('数据已经发送给客户端')
server=socketserver.ThreadingTCPServer(addr,Myserver)
server.serve_forever()