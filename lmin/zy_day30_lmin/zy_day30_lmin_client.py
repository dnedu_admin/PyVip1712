#coding=utf-8
# #点对点的传输（先向服务器传送一个文本字符，然后再接收一张图片）
# import  socket
# host ='127.0.0.1'
# port=9009
# s=socket.socket()
# s.connect((host,port))
# with open('1.txt','r')as e:
#     content=e.read()
#     s.send(content.encode('utf-8'))
#     print('数据已经传送完毕')
# result=s.recv(204800)
# with open('g.jpg','wb')as e:
#     e.write(result)
#     print('已经接收完毕')
# result=input('请输入： ')
# s.send(result.encode('utf-8'))
# s.close()
#**************************************************************
# import socket
# host='127.0.0.1'
# port =9009
# s=socket.socket()
# s.connect((host,port))
# while True:
#     with open('1.txt','r')as e:
#         connect=e.read()
#         s.send(connect.encode('utf-8'))
#         print('数据已经发送完毕')
#     result=s.recv(1900000)
#     with open('mm.jpg','wb')as e:
#        e.write(result)
#        print('已经接收了一个图片数据')
#     connect=input('请输入：')
#     print("ggg%s" %s.send(connect.encode('utf-8')))
# s.close()
#***********************************************************
#多线程来实现全双工传输
import socket
host ='127.0.0.1'
port =9099
addr =(host,port)
s=socket.socket()
s.connect(addr)
while 1:
    with open('1.txt','r')as e:
        res=e.read()
        s.send(res.encode('utf-8'))
        print('已经发送数据完毕')
    result=s.recv(999999)
    with open('picture.jpg','wb')as e:
        e.write(result)
        print('已经接收了一个图片')
    print('我要断开链接了')
    connt=input('请输入发送的消息： ')
    s.send(connt.encode('utf-8'))
    result=s.recv(9999999)
    if not result:
        break
s.close()

