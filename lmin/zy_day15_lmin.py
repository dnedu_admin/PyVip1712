#coding=utf-8
# 1.range(),需要生成下面的列表,需要填入什么参数:
#     1.[3,4,5,6]
#     2.[3,6,9,12,15,18]
#     3.[-20,200,420,640,860]
# print(list(range(3,7,1)))
# print(list(range(3,19,3)))
# print(list(range(-20,861,220)))

# 2.列表推导式或者生成器表达式(2选择1)完成1-100之间所有偶数和
# a_list=[n for n in range(2,101,2)]
# print(a_list)
# print(sum(n for n in range(1,100,2)))
# # 3.定义一个函数,使用关键字yield创建一个生成器,求1,3,5,7,9各数
# #    字的三次方,并把所有的结果放入到list里面打印输入
# def get_count(n):
#     num = n[0]
#     yield num **3
#     num = n[1]
#     yield num ** 3
#     num = n[2]
#     yield num ** 3
#     num = n[3]
#     yield num ** 3
#     num = n[4]
#     yield num ** 3
# g = get_count([1,3,5,7,9])
# list1=list()
# for num in g:
#     list1.append(num)
# print(list1)

# 4.写一个自定义的迭代器类(iter)
class T_iter():
    def __init__(self, id):
        self.id = id
    def __next__(self):
        if self.id == 0:
            raise StopIteration('over')
        self.id -= 1
        return self.id

    def __iter__(self):
        return self

a_iter = T_iter(5)
# for i in a_iter:
#     print(i)
print(next(a_iter))
print(next(a_iter))
print(next(a_iter))
print(next(a_iter))
print(next(a_iter))
print(next(a_iter))
print(next(a_iter))