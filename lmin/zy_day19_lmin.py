#coding=utf-8

# 使用装饰器增强完成1-999999所有奇数和计算10次
#    1.使用普通方法
#    2.使用多线程
#    3.使用多进程
#    4.采用线程池完成计算
#    5.采用进程池完成计算

from multiprocessing import Pool,cpu_count#导入进程池
from multiprocessing.dummy import Pool as xcpool #导入线程池
from multiprocessing import Process #导入进程
from threading import Thread#导入线程
import time#导入时间模块

def get_time(func):#定义装饰函数
    def inner(num):
        time1=time.time()
        func(num)
        time2=time.time()
        print(str(time2-time1))
    return inner

def get_sum(num):#单独封装一个求和的函数
    print(sum(range(1,num,2)))

@get_time#普通方法
def ptong(num):
    for i in range(10):
        print(sum(range(1, num, 2)))
@get_time#多进程
def jc(num):
    for i in range(10):
        pro = Process(target=get_sum, args=(num,))
<<<<<<< HEAD
        pro.start()
        pro.join()
=======
    pro.start()
    pro.join()
>>>>>>> 45ccc8ad42edec195e8247f5d0e40ad276c3b2fb


@get_time#多线程
def xc(num):
    for i in range(10):
        thr=Thread(target=get_sum, args=(num,))
<<<<<<< HEAD
        thr.start()
        thr.join()
=======
    thr.start()
    thr.join()
>>>>>>> 45ccc8ad42edec195e8247f5d0e40ad276c3b2fb
@get_time#进程池
def jc_pool(num):
    pools=Pool(cpu_count)
    for i in range(10):
<<<<<<< HEAD
        pools.apply_asyna(get_sum,args=(num,))
=======
        pools.apply_asyna(get_sum,(num,))
>>>>>>> 45ccc8ad42edec195e8247f5d0e40ad276c3b2fb
    pools.close()
    pools.join()
@get_time#线程池
def xc_pool(num):
    pools = xcpool(cpu_count)
    for i in range(10):
<<<<<<< HEAD
        pools.apply_asyna(get_sum, args=(num,))
=======
        pools.apply_asyna(get_sum, (num,))
>>>>>>> 45ccc8ad42edec195e8247f5d0e40ad276c3b2fb
    pools.close()
    pools.join()
if __name__=='__main__':

    ptong(999999)
    jc(999999)
    xc(999999)
    jc_pool(999999)
<<<<<<< HEAD
    xc_pool(999999)
=======
    # xc_pool(999999)
>>>>>>> 45ccc8ad42edec195e8247f5d0e40ad276c3b2fb

