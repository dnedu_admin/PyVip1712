#coding=utf-8
# 1.filter判断[0,False,True,5,{},(2,3)]对应bool值
# result = list(filter(s1,[0,False,True,5,{},(2,3)]))

# 2.map实现1-10中所有奇数项的三次方,打印输出结果
# s1=map(lambda x:x*x*x,list(filter(lambda y:y%2==1,range(10))))
# print(list(s1))
# 3.reduce实现1-100所有偶数项的和
# from functools import reduce
# print(reduce(lambda a,b:a+b,list(filter(lambda x:x%2==0,range(100)))))
# print(list(filter(lambda x:x%2==0,range(100))))

# 4.用递归函数实现斐波拉契数列  [1, 1, 2, 3, 5, 8, 13, 21, 34]
def s(n):
    if isinstance(n,int):
        if n <=2:
            return 1
        else:
            return s(n - 1) + s(n - 2)
def s1(m):
    map1=map(s,range(1,m+1))
    list1=list(map1)
    print(list1)
s1(30)
