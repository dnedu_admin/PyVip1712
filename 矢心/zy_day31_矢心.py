# coding=utf-8
# author:hhu_zym(qq:1045797922)
# 动脑学院VIP学员

# 1.请写出一段Python代码实现删除一个list里面的重复元素
# def delete_same(a_list):
#     a_set=set(a_list)
#     return list(a_set)
#
# a_list=[1,2,3,2,1]
# print(delete_same(a_list))

# 2.如何用Python来进行查询和替换一个文本字符串？
# import re
# p=re.findall('\d+','ben88p67pyweb23mm99')
# print(p) # ['88', '67', '23', '99']

# print(re.sub('\d+','#','ben88p67pyweb23mm99'))

# 3.a=1, b=2, 不用中间变量交换a和b的值
# a=1
# b=2
# a,b=b,a

# 4.写一个函数, 输入一个字符串, 返回倒序排列的结果:?如: string_reverse(‘abcdef’), 返回: ‘fedcba’

# 方法一：简单的步长为-1, 即字符串的翻转;
# def string_reverse(s):
#     ss=''
#     for i in range(len(s),0,-1):
#         ss=ss+s[i-len(s)-1]
#     return ss

# 方法二：交换前后字母的位置;  （错误）
# def string_reverse(s):
#     s_len=len(s)
#     for i in range(s_len-1):
#         for j in range(i+1,s_len):
#             s[i],s[j]=s[j],s[i]
#     return s

# 方法三：递归的方式, 每次输出一个字符;

# 方法四：双端队列, 使用extendleft()函数;
# def string_reverse(s):
#     from collections import deque
#     a = deque([])
#     for item in s:
#         a.extendleft(item)
#     return a

# 方法五：使用for循环, 从左至右输出
# def string_reverse(s):
#     ss=''
#     for item in s:
#         ss=item+ss
#     return ss

# 我的方法
# def string_reverse(s):
#     a_list=list(s)
#     a_list.reverse()
#     return ''.join(a_list)
#
# print(string_reverse('abcdef'))

# 5.请用自己的算法, 按升序合并如下两个list, 并去除重复的元素 合并链表, 递归的快速排序, 去重;
list1=[87,55,25,89,56]
list2=[22,68,98,78]
a_list=list1+list2


def sub_sort(array, low, high):
    pivotkey = array[low]
    while low < high:
        while low < high and array[high] >= pivotkey:
            high -= 1
        array[low] = array[high]
        while low < high and array[low] <= pivotkey:
            low += 1
        array[high] = array[low]
    array[low] = pivotkey
    return low


def quick_sort(array, low, high):
    if low < high:
        pivoloc = sub_sort(array, low, high)
        quick_sort(array, low, pivoloc - 1)
        quick_sort(array, pivoloc + 1, high)

quick_sort(a_list,0,len(a_list)-1)
print(a_list)