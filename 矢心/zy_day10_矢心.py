# coding=utf-8
# author:hhu_zym(qq:1045797922)
# 动脑学院VIP学员

# 1.自定义一个异常类,当list内元素长度超过10的时候抛出异常
class MyError(Exception):
    def __init__(self,message):
        self.message=message
    def __str__(self):
        return self.message

def fun(a_list):
    try:
        if len(a_list)>10:
            raise MyError('list内元素超过10个')
    except Exception as e:
        print(e)

a_list=[1,2,3,4,5,6,7,8,9,10,11]
fun(a_list)

# 2.思考如果对于多种不同的代码异常情况都要处理,又该如何去处理,自己写一个小例子
try:
    x=9/0
    y=int('这不是数字')
    f=open('hello.txt')
except (ZeroDivisionError,ValueError,IOError,Exception) as e:
    print(e)

# 3.try-except和try-finally有什么不同,写例子理解区别
try:
    num=int('这不是数字')
    print(111)
except Exception as e:
    print(e)
    print(222)

try:
    num=int('这不是数字')
except Exception as e:
    print(e)
finally:
    print(222)

'''
try...except...是对可能出现异常的语句进行异常捕获
try...except...finally...是在捕获异常之后，不管怎样都会执行的操作，保证了代码的健壮性
'''

'''
4.写函数，检查传入字典的每一个value的长度，如果大于2，
     那么仅仅保留前两个长度的内容，并将新内容返回给调用者
     dic = {“k1”: "v1v1","k2":[11,22,33}}
'''
def len_inspect(a_dict):
    for key,value in a_dict.items():
        if len(value)>2:
            a_dict[key]=value[:2]
    return a_dict

dic = {"k1": "v1v1","k2":[11,22,33]}
print(len_inspect(dic))

