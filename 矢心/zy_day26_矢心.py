# coding=utf-8
# author:hhu_zym(qq:1045797922)
# 动脑学院VIP学员

# 1. PKCS#5(选择一种hash算法)加密算法加密你的中文名字,写出代码
# import hashlib
# m=hashlib.sha1()
# m.update('zhouyanming'.encode('utf-8'))
# print(m.hexdigest())  # 2774ea742db5d65858102ea6591cfb1a7711c4b0

# 2. 创建文件test.py,写入一些数据,并计算该文件的md5值
# import hashlib
# with open('test.py','w',encoding='utf-8') as f:
#     f.write('将进酒，杯莫停')
# m=hashlib.md5()
# m.update('test.py'.encode('utf-8'))
# print(m.hexdigest())

'''
3. 遵循上下文管理协议,自己编写一个类,去创建一个上下文管理器,然后自己使用看看
   方式1:普通方式
   方式2:使用contextmanager
'''
# 方式1：普通方式
# class MyOffice():
#     def __init__(self,name):
#         self.name=name
#     def __enter__(self):
#         print('执行代码')
#         return self
#     def __exit__(self, exc_type, exc_val, exc_tb):
#         self.name=None
#         if exc_type:
#             print(exc_val)
#         else:
#             print('正常执行')
#     def work(self):
#         print('zym正在%s工作'%self.name)
#
# with MyOffice('办公室612') as f:
#     f.work()

# 方式2:使用contextmanager
# from contextlib import contextmanager
# class MyOffice():
#     def __init__(self,name):
#         self.name=name
#     def work(self):
#         print('zym正在%s工作'%self.name)
#
# @contextmanager
# def mk_context(name):
#     print('执行代码')
#     tls=MyOffice(name)
#     yield tls
#     print('正常执行')
#
# with mk_context('办公室612') as mk:
#     mk.work()

# 4.使用colorama模块使用多颜色输出
from colorama import Fore,Back,Style
print(Fore.LIGHTBLUE_EX+'zym正在办公室612工作')
print(Back.CYAN+'zym正在办公室612工作')
print(Style.RESET_ALL)
print('zym正在办公室612工作')