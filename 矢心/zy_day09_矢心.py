# coding=utf-8
# author:hhu_zym(qq:1045797922)
# 动脑学院VIP学员

'''
1.a_list = [1,2,3,2,2]
   删除a_list中所有的2
'''
a_list = [1,2,3,2,2]
a_list=list(set(a_list))
for num in range(len(a_list)-1):
    if a_list[num] == 2:
        a_list.pop(num)
        break
print(a_list)

'''
2.用内置函数compile创建一个文件xx.py,在文件里写你的名字,然后用eval
   函数读取文件中内容,并打印输出到控制台
'''
code = compile("print('周闫明')",'xx.py','eval')
eval(code)

'''
3.写一个猜数字的游戏,给5次机会,每次随机生成一个整数,然后由控制台输入一个
   数字,比较大小.大了提示猜大了,小了提示猜小了.猜对了提示恭喜你,猜对了.
    退出程序,如果猜错了,一共给5次机会,5次机会用完程序退出.
'''
import random

# 递归输入100以内正整数
def num_input():
    num_string = input('请输入100以内的正整数:')
    if num_string.isdigit():
        if 1<=int(num_string)<=100:
            return int(num_string)
        else:
            print('输入格式不正确，请重新输入！')
            return num_input()
    else:
        print('输入格式不正确，请重新输入！')
        return num_input()

# 游戏
def game():
    answer = random.randint(1, 100)
    player_answer=num_input()
    if player_answer > answer:
        return 0,answer
    elif player_answer < answer:
        return 1,answer
    else:
        return 2,answer

time = 1  # 记录游戏次数
while time<=5:
    result,answer=game()
    if result==0:
        print('结果为%s，猜大了,还剩%d次机会'%(answer,5-time))
    elif result==1:
        print('结果为%s，猜小了,还剩%d次机会'%(answer,5-time))
    else:
        print('结果为%s，恭喜你猜对了'%(answer))
        break
    time += 1


