# coding=utf-8
# author:hhu_zym(qq:1045797922)
# 动脑学院VIP学员

# 1.识别下面字符串:’ben’,’hit’或者’ hut’
# import re
# print(re.findall('ben|hit|hut','ben aas hit859hut6'))

# 2.匹配用一个空格分隔的任意一对单词,比如你的姓 名
# import re
# print(re.findall('\w+\s\w+','zym'))  # []
# print(re.findall('\w+\s\w+','ym z'))  # ['ym z']

# 3.匹配用一个逗号和一个空格分开的一个单词和一个字母
# import re
# print(re.findall('\w+,\s\w','shkg_9, u'))  # ['shkg_9, u']

# 4.匹配简单的以’www’开头,以’com’结尾的web域名,比如:www.baidu.com
import re
print(re.findall('^w{3}.+com$','www.baidu.com'))