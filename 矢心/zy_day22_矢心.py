# coding=utf-8
# author:hhu_zym(qq:1045797922)
# 动脑学院VIP学员

# 1.使用正则表达式匹配电话号码：0713-xxxxxx(湖南省座机号码)
# import re
# print(re.findall('^0713-\d{6}$','0713-123456'))

'''
2.区号中可以包含()或者-,而且是可选的,就是说你写的正则表达式可以匹配
   800-555-1212,555-1212,(800)555-1212
'''
# import re
# pattern=re.compile('^\(\d{3}\)\d{3}-\d{4}$|^\d{3}-\d{3}-\d{4}$|^\d{3}-\d{4}$')
# m=pattern.match('800-555-1212')
# print(m.group())  # 800-555-1212
# m=pattern.match('555-1212')
# print(m.group())  # 555-1212
# m=pattern.match('(800)555-1212')
# print(m.group())  # (800)555-1212

#!/usr/bin/python
#coding=utf8
import re       # 正则表达式
import urllib.request   # 获取网页源代码

# 用正则表达式写一个小爬虫用于保存贴吧里的所有图片

# 获取网页源代码
def getHtml(url):
        page = urllib.request.urlopen(url)      # 打开url，返回页面对象
        html = page.read()              # 读取页面源代码
        html=html.decode('utf-8')
        return html

# 获得图片地址
def getImg(html):
        reg = r'src="(.*?\.jpg)" size="'        # 定义一个正则来匹配页面当中的图片
        imgre = re.compile(reg)         # 为了让正则更快，给它来个编译
        #这个时候做个测试，把匹配的数据都给打印出来
        imglist = re.findall(imgre, html)                       # 通过正则返回所有数据列表
        # 把这个地址一个一个的拿下来进行下载
        x = 0
        for imgurl in imglist:
                urllib.request.urlretrieve(imgurl,'%s.jpg' % x)
                x+=1

html = getHtml("https://tieba.baidu.com/p/5154221980")
getImg(html)