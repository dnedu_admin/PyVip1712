# coding=utf-8
# author:hhu_zym(qq:1045797922)
# 动脑学院VIP学员

'''
1.datetime,获取你当前系统时间三天前的时间戳;并把当前时间戳增加
 899999,然后转换为日期时间对象,把日期时间并按照%Y-%m-%d %H:%M:%S
 输入打印这个日期对象,并获取date属性和time属性,最后判断当前日期时间
 对象是星期几;
'''
from datetime import datetime,timedelta
datetime1=datetime.now()
datetime2=datetime1-timedelta(days=3)
ts1=datetime2.timestamp()
print(ts1)   # 1521253089.908129
ts2=ts1+899999
datetime3=datetime.fromtimestamp(ts2)
print(datetime.strftime(datetime3,'%Y-%m-%d %H:%M:%S'))  # 2018-03-27 20:18:08
print(datetime3.date())  # 2018-03-27
print(datetime3.time())  # 20:18:08.908129
print(datetime3.weekday())  # 1（星期二）

'''
2.使用os模块线获取当前工作目录的路径,并打印当前目录下所有目录和文件,
获取当前脚本的绝对路径, 分割当前脚本文件的目录和文件在tuple中并判断
当前文件所在的目录下存不存在文件game.py,如果不存在则创建该文件.
并往该文件里写入数据信息,然后判断当前目录下是否存在目录
/wow/temp/,如果不存在则创建,最后把目录名称改为:/wow/map/
'''
import os
from os import path
# 获取当前工作目录的路径
print(os.getcwd())
# 打印当前目录下所有目录和文件
print(os.listdir('.'))
# 获取当前脚本的绝对路径
print(path.abspath('.'))
# 分割当前脚本文件的目录和文件在tuple
print(path.split('./zy_day23_矢心.py'))
# 判断当前文件所在的目录下存不存在文件game.py
print(path.exists(r'./game.py'))
# 如果不存在则创建该文件,并在该文件里写入数据信息
if not path.exists(r'./game.py'):
    f=open('game.py','w',encoding='utf-8')
    f.write('123456')
    f.close()
# 判断当前目录下是否存在目录/wow/temp/,如果不存在则创建,最后把目录名称改为:/wow/map/
if not path.exists(r'./wow/temp/'):
    os.makedirs(r'./wow/temp/')
    os.renames(r'wow/temp/',r'wow/map/')

# 3.写函数，检查获取传入列表或元组对象的所有奇数位索引对应的元素，并将其作为新的列表返回给调用者
def get_item(a_list):
    if not isinstance(a_list,(list,tuple)):
        raise TypeError('输入格式不正确')
    if not a_list:
        raise ValueError('该参数为空')
    b_list=[a_list[n] for n in range(0,len(a_list),2)]
    return b_list

a_list=[1,2,3,4,5,6,7,8,9]
b_list=get_item(a_list)
print(b_list)