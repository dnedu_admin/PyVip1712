# coding=utf-8
# author:hhu_zym(qq:1045797922)
# 动脑学院VIP学员

'''
1.写一个函数计算1 + 3 + 5 +…+97 + 99的结果
      再写一个装饰器函数, 对其装饰, 在运算之前, 新建一个文件
      然后结算结果, 最后把计算的结果写入到文件里
'''
def outer(func):
    def inner():
        f=open('zym.txt','w',encoding='utf-8')
        num=func()
        f.write(str(num))
        f.close()
        return num
    return inner

@outer
def func():
    s=0
    for item in range(1,100,2):
        s+=item
    return s

result=func()
print(result)

'''
2.写一个函数计算传入进来参数的平方, 并将结果.写一个带参数的装饰器, 
       将装饰器参数传入到被包装的函数,计算并输入结果
'''
def outer(arg):
    def middle(func):
        def inner(num):
            result=func(num)
            print(arg)
            return  result
        return inner
    return middle

@outer(20)
def num2(num):
    return num**2

result=num2(16)
print(result)