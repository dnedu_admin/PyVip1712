# coding=utf-8
# author:hhu_zym(qq:1045797922)
# 动脑学院VIP学员

import pymysql
import pandas as pd
from sqlalchemy import create_engine
from pandas.io.sql import read_sql

score_dict = {"name": ["eric", u"中国", "ben"],
              "english": [80, 90, 100],
              "c": [88.8, 99.9, 75.0],
              "python": [100, 100, 100]}

df_dict = pd.DataFrame(
    score_dict,
    index=score_dict['name'],
    columns=[
        'name',
        'english',
        'c',
        'python'])
engine = create_engine(
    'mysql+pymysql://root:197272ab@localhost:3306/dn_eric?charset=utf8',
    echo=False)
df_dict.to_sql(name='score', con=engine, if_exists='append', index=False)
db = pymysql.connect(
    'localhost',
    'root',
    '197272ab',
    'dn_eric',
    charset='utf8')
query = 'SELECT name,english,c,python from score'
df = read_sql(query, db)
print(df)
