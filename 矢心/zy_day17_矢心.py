# coding=utf-8
# author:hhu_zym(qq:1045797922)
# 动脑学院VIP学员

'''
1.使用pickle,json(注意:两个模块)
把字典a_dict = [1,2,3,[4,5,6]]
序列化与反序列化,序列化,保存到文件名为serialize.txt文件里面,然后大开,并读取数据
反序列化为python对象加载到内存,并打印输出
尝试序列化/反序列非文件中,直接操作
'''
# import pickle
# a_dict = [1,2,3,[4,5,6]]
# f=open('serialize.txt','wb+')
# pickle.dump(a_dict,f,0)
# f.close()
# f=open('serialize.txt','rb+')
# b_dict=pickle.load(f)
# f.close()
# print(b_dict)
#
# import json
# a_dict = [1,2,3,[4,5,6]]
# f=open('serialize.txt','w+')
# json.dump(a_dict,f)
# f.close()
# f=open('serialize.txt','r+')
# b_dict=json.load(f)
# f.close()
# print(b_dict)

'''
2.自己写代码对比深浅拷贝的区别,写完代码然后看看运行结果,然后分析原因
   比如：a_list=[1,2,3,4,[8,9,0]]
'''
# from copy import copy
# a_list=[1,2,3,4,[8,9,0]]
# b_list=copy(a_list)
# a_list[4][1]=50
# print(b_list[4][1])
#
# from copy import deepcopy
# a_list=[1,2,3,4,[8,9,0]]
# b_list=deepcopy(a_list)
# a_list[4][1]=50
# print(b_list[4][1])

# 3.分别写代码完成内存缓冲区文本数据和二进制数据的读/写操作
# from io import StringIO
# f=StringIO()
# f.write('我是zym\n我正在学习python')
# print(f.getvalue())
#
# f=StringIO('我是zym\n我正在学习python')
# while 1:
#     conn=f.readline()
#     if len(conn)!=0:
#         print(conn.strip())
#     else:
#         break
#
from io import BytesIO
f=BytesIO()
f.write('我是zym\n我正在学习python'.encode('utf-8'))
print(f.getvalue().decode('utf-8'))




