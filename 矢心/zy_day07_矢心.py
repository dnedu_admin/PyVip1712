# coding=utf-8
# author:hhu_zym(qq:1045797922)
# 动脑学院VIP学员

# 1.简述普通参数、默认参数、关键字参数、动态收集参数的区别,理解为主
"""
普通参数：在调用函数时必须给普通参数传值，注意参数的类型是否匹配，普通参数只在函数块内作用
默认参数：函数的参数预先给好默认值，当有参数传递时覆盖默认值，当没有参数传递时使用默认值
关键字参数：在调用函数传递参数时，可以指定参数的顺序，更自由地进行参数传递
动态收集参数，主要针对元组和字典，可以直接输入几个参数，在传递时可以自动生成一个序列，直接传递元组或数组时，要先用*或**进行解包
"""

"""
2.def foo(x, y, z, *args, **kw):
    sum = x + y + z
    print(sum)
    for i in args:
        print(i)
    print(kw)
    for j in kw.items():
        print(j)
a_tuple = (1,2,3)     此处参数修改为2个看看会怎么样?
b_dict = {'name': 'jim', 'age': 28, 'add': '上海'}
foo(*a_tuple, **b_dict)    分析这里会怎么样?
"""


def foo(x, y, z, *args, **kw):
    sum = x + y + z
    print(sum)
    for i in args:
        print(i)
    print(kw)
    for j in kw.items():
        print(j)


a_tuple = (1, 2, 3)
b_dict = {'name': 'jim', 'age': 28, "add": '上海'}
foo(*a_tuple, **b_dict)

"""
输出结果为：
6
{'name': 'jim', 'age': 28, 'add': '上海'}
('name', 'jim')
('age', 28)
('add', '上海')
第一个参数为*a_tuple解包后为1,2,3，三个参数传入x,y,z，所以sum=1+2+3=6
第二个参数是一个字典，用*解包后并不能转化为元组的形式，所以保留字典作为一个单个的值打印出来
第二个参数用**解包后成为一个字典，打印输出成为字典中的一个个项
但是我不清楚为什么第二个参数传入元组解包后，为啥还会传入第五个参数字典解包
"""

"""
3.题目:执行分析下代码
    def func(a, b, c=9, *args, **kw):
         print('a =', a, 'b =', b, 'c =', c, 'args =', args, 'kw =', kw)
    func(1,2)
    func(1,2,3)
    func(1,2,3,4)
    func(1,2,3,4,5)
    func(1,2,3,4,5,6,name='jim')
    func(1,2,3,4,5,6,name='tom',age=22)
    扩展: 如果把你的函数也定义成  def  get_sum(*args,**kw):pass 
    你的函数可以接受多少参数?
"""


def func(a, b, c=9, *args, **kw):
    print('a =', a, 'b =', b, 'c =', c, 'args =', args, 'kw =', kw)


func(1, 2)
func(1, 2, 3)
func(1, 2, 3, 4)
func(1, 2, 3, 4, 5)
func(1, 2, 3, 4, 5, 6, name='jim')
func(1, 2, 3, 4, 5, 6, name='tom', age=22)
# 扩展：动态收集参数的话，理论上可以收集任意个数的参数，按照类型和顺序转化为元组和字典


# 4.写一个函数函数，计算传入字符串中的数字、字母、空格和其他的个数分别为多少?
def str_count(string):
    num_count,s_count,_count,else_count=0,0,0,0
    for item in string:
        if str(item).isdigit():
            num_count+=1
        elif str(item).isalpha():
            s_count+=1
        elif str(item)==' ':
            _count+=1
        else:
            else_count+=1
    print('数字个数为：'+str(num_count),'字母个数为;'+str(s_count),'空格个数为：'+str(_count),'其他的个数为：'+str(else_count
                                                                                               ))
string=input('请输入字符串:') or 'abc 123  xy@'
str_count(string)