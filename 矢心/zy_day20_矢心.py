# coding=utf-8
# author:hhu_zym(qq:1045797922)
# 动脑学院VIP学员

'''
 创建两个协程函数对象,分别完成1,3,5…99,   2,4,6…,100
 然后让两个协程函数对象分别交替执行.在每一个协程函数对象的内部先打印输出
 每一个数字,然后执行完一轮之后打印提示信息,已经交替执行完毕一轮
 等所有的协程函数对象执行完毕之后,分析一下数据信息
'''
from types import coroutine
async def one():  # 第一个协程函数
    for item in range(1,100,2):
        print(item)
        await do_next()

async def two():  # 第二个协程函数
    for item in range(2,101,2):
        print(item)
        await do_next()

@coroutine  # 通过装饰器，协程函数（yield）
def do_next():
    yield

# 定义一个函数完成调度
def run(a_list):
    b_list=list(a_list)
    while b_list:  # 只要列表中还有协程对象
        for item in b_list:
            try:
                item.send(None)
            except StopIteration as e:
                print(e)
                b_list.remove(item)  # 当一个协程函数运行结束时，就会报错，此时将运行完的程序踢出，当b_list为空时，循环停止，说明所有协程都已经运行完成了

if __name__=='__main__':
    one=one()
    two=two()
    c_list=[]
    c_list.append(one)
    c_list.append(two)
    run(c_list)
    print('已经交替执行完一轮')
# 打印输出的数据为1-100，空两行后输出结束信息，这两行也就是两个协程程序结束完最后的异常处理留下的空行
