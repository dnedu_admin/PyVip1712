# coding=utf-8
# author:hhu_zym(qq:1045797922)
# 动脑学院VIP学员

'''
爬取
url = 'http://tieba.baidu.com/p/2460150866'
前三页所有的图片
'''
import requests
from urllib.request import urlretrieve  # 保存资源
import re

# 获取网页源代码
def get_code(url):
    try:
        page=requests.get(url)
        code=page.text
        return code
    except Exception as e:
        print(e)

# <img pic_type="0" class="BDE_Image" src="https://imgsa.baidu.com/forum/w%3D580/sign=294db374d462853592e0d229a0ee76f2/e732c895d143ad4b630e8f4683025aafa40f0611.jpg" pic_ext="bmp" height="328" width="560">

# 获取源代码里面的图片
def get_image(code,num):
    try:
        reg=r'src="(.+?\.jpg)" pic_ext'
        image=re.compile(reg)
        image_list=re.findall(image,code)
        # num=1
        for url in image_list:
            urlretrieve(url,'./picture/'+'%s.jpg'%num)
            num+=1
        return num
    except Exception as e:
        print(e)

if __name__=='__main__':
    url_list=['http://tieba.baidu.com/p/2460150866?pn=1','http://tieba.baidu.com/p/2460150866?pn=2','http://tieba.baidu.com/p/2460150866?pn=3']
    num=1
    for url in url_list:
        html=get_code(url)
        num=get_image(html,num)
    print('图片处理完成')