# coding=utf-8
# author:hhu_zym(qq:1045797922)
# 动脑学院VIP学员

# 1.filter判断[0,False,True,5,{},(2,3)]对应bool值
a_list=[0,False,True,5,{},(2,3)]
print(list(map(bool,a_list)))  # [False, False, True, True, False, True]

# 2.map实现1-10中所有奇数项的三次方,打印输出结果
print(list(map(lambda x:x**3,range(1,10,2))))  # [1, 27, 125, 343, 729]

# 3.reduce实现1-100所有偶数项的和
from functools import  reduce
print(reduce(lambda x,y:x+y,range(0,101,2)))  # 2550

# 4.用递归函数实现斐波拉契数列
def Calculate(num):  # 递归
    if not isinstance(num,int):
        raise TypeError('请输入整数')
    if num<0:
        raise ValueError('输入数值不正确')
    if num==0 or num==1:
        return 1
    return Calculate(num-1)+Calculate(num-2)

def f(num):
    if not isinstance(num,int):
        raise TypeError('请输入整数')
    if num<0:
        raise ValueError('输入数值不正确')
    if num==0:
        return [1]
    if num==1:
        return [1,1]
    a_list=[1,1]
    for element in range(2,num+1):
        a_list.append(Calculate(element))
    return a_list

print(f(20))  # [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987, 1597, 2584, 4181, 6765, 10946]
