# coding=utf-8
# author:hhu_zym(qq:1045797922)
# 动脑学院VIP学员

# 1.用while和for两种循环求计算1+3+5.....+45+47+49的值
sum=0
for num in range(1,50,2):
    sum+=num
print(sum)
sum=0
num=1
while num<=49:
    sum+=num
    num+=2
print(sum)

# 2.代码实现斐波那契数列(比如前30个项)
num_list=[0,1]
for num in range(2,30):     # 计算出前30个元素
    num_list.append(num_list[-1]+num_list[-2])
print(num_list)