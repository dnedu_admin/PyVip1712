# coding=utf-8
# author:hhu_zym(qq:1045797922)
# 动脑学院VIP学员

"""
1.自己写一个字典:
    name_dict = {'name':'ben','age':22,'sex':'男'}
    添加,删除,更新,清空操作
"""
name_dict = {'name':'ben','age':22,'sex':'男'}

# 添加
# name_dict['addr']='南京'
name_dict.setdefault('addr','南京')
print(name_dict)

# 删除
name_dict.pop('addr')
print(name_dict)

# 更新
a_dict={'addr':'南京'}
a_dict.update(name_dict)
print(a_dict)

# 清空
name_dict.clear()
print(name_dict)

"""
2.写两个集合:
    num1_set = {3,5,1,2,7}
    num2_set = {1,2,3,11}
    并分别进行&,|,^,-运算
"""
num1_set = {3,5,1,2,7}
num2_set = {1,2,3,11}
print(num1_set & num2_set)
print(num1_set | num2_set)
print(num1_set-num2_set)

"""
3.整数字典:
    num_dict = {'a':13,'b':22,'c':18,'d':24}
    按照dict中value从小到大的顺序排序
"""
# https://www.cnblogs.com/dylan-wu/p/6041465.html
num_dict = {'a':13,'b':22,'c':18,'d':24}
list_dict=sorted(num_dict.items(),key=lambda item:item[1])
print(list_dict)