# coding=utf-8
# author:hhu_zym(qq:1045797922)
# 动脑学院VIP学员

# 1.基于tcp协议socket套接字编程半双工模式
# 服务端代码
# import socket
#
# # tcp协议
# host='127.0.0.1'
# port=8008
# s=socket.socket()
# s.bind(host,port)
# s.listen(5)
# while 1:
#     # 等待连接
#     conn, addr = s.accept()
#     # 处理客户端发来的数据
#     while 1:
#         content=conn.recv(1024)  # 接收内容的大小
#         if len(content)==0:
#             print('接收到客户端数据为空')
#         else:
#             print('接收客户端发送来的数据为%s' % content.decode('utf-8'))
#             result=input('请输入要返回给客户端的数据：')
#             conn.send(result.encode('utf-8'))  # 返回数据
#     conn.close()
# s.close()
#
# # 客户端代码
# import socket
#
# host='127.0.0.1'
# post=8008
# s=socket.socket()
# s.connect((host,port))
# while 1:
#     content=input('请输入发送给服务端的数据：')
#     s.send(content.encode('utf-8'))
#     # 接收客户端返回来的数据
#     result=s.recv(1024)
#     print('接收服务器返回的数据为%s' % result.decode('utf-8'))
#     print('&'*38)
# s.close()

# 2.socketserver异步非阻塞编程,编写代码
# 服务器端
import socketserver
from socketserver import StreamRequestHandler as srh
host='127.0.0.1'
port=8008
addr=(host,port)

# 自定义请求处理类
class Server(srh):  # 集成基类srh
    # 处理请求的方法
    def handle(self):
        print('连接到服务器的客户端地址为%s' % str(self.client_address))
        while 1:
            content=self.request.recv(1024)
            if not content:
                break
            print('服务器接收的数据为%s'% content.decode('utf-8'))
            # 处理数据
            result=input('请输入返回给客户端的数据：')
            self.request.send(result.encode('utf-8'))

print('开始处理请求')
sever=socketserver.ThreadingTCPServer(addr,Server)
sever.serve_forever()

# 客户端
import socket
host='127.0.0.1'
port=8008
addr=(host,port)
s=socket.socket()
s.connect(addr)
while 1:
    content=input('请输入发送给服务器数据为：')
    if not content or content=='ex':
        break
    s.send(content.encode('utf-8'))
    result=s.recv(1024)
    if not result:
        break
    print('服务器端返回的数据为%s：'%result.decode('utf-8'))
s.close()