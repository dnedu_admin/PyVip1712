# coding=utf-8
# author:hhu_zym(qq:1045797922)
# 动脑学院VIP学员

'''
1.文件名称:use.txt
   创建该文件,路径由你自己指定,打开该文件,往里面写入
   str_content = ['tom is boy','\n','you are cool','\n',
                       '今天你怎么还不回来']
   把数据写入到该文件中,注意操作的模式,关闭文件句柄后
   再次打开该文件句柄,然后读取文件中内容,用with代码块
   实现,然后打印输出信息到控制台,注意换行符
'''
# 写入文件
str_content = ['tom is boy','\n','you are cool','\n', '今天你怎么还不回来']
f=open('zym.py','w',encoding='utf-8')
f.writelines(str_content)
f.close()

# 读取文件
with open('zym.py','r',encoding='utf-8') as f:
    f.seek(0)
    print(f.read())

# 2.with创建一个文件homework.txt,尝试多种操作模式.进行写读操作,注意区别
a_list=['hello\n','python\n', 'zym_learn']
with open('homework.py','w',encoding='utf-8') as f:
    f.writelines(a_list)

# 读
f=open('homework.py','r+',encoding='utf-8')
f.seek(0)
print(f.read(3))
f.close()

# 写
f=open('homework.py','a+',encoding='utf-8')
f.write('\n666')
f.seek(0)
print(f.read())
f.close()

# 3.理解文件句柄,对比read(),readline(),readlines()写一个小例子
a_list=['hello\n','python\n', 'zym_learn']
with open('homework.py','w',encoding='utf-8') as f:
    f.writelines(a_list)
f=open('homework.py','r',encoding='utf-8')
f.seek(0)
print(f.read())
f.seek(0)
print(f.readline())
f.seek(0)
print(f.readlines())
f.close()

'''
 4.准备一张jpg图片,把图片中数据读书出来,并写入到文件my_img.jpg文件中,
    操作完毕后尝试打开my_img.jpg文件看图片显示正不正常
'''
# with open('test.jpg','rb') as f:
#     p=open('my_img.jpg','wb')
#     print(type(f.read()))
#     p.write(f.read())
#     p.close()

from PIL import Image
im=Image.open('test.jpg')
#im.show()
im.save('my_img.jpg')