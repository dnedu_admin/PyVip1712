# coding=utf-8
# author:hhu_zym(qq:1045797922)
# 动脑学院VIP学员

'''
1.自己动手写一个学生类,创建属性名字,年龄,性别,完成
     __new__,__init__,__str__,__del__，__call__
'''
class Student():
    def __new__(cls, *args, **kwargs):
        return  super().__new__(cls)
    def __init__(self,name,age,sex):
        self.name=name
        self.age=age
        self.sex=sex
    def __str__(self):
        return '%s：%d,%s'%(self.name,self.age,self.sex)
    def __call__(self, *args, **kwargs):
        return '被当前对象调用'
    def __del__(self):
        print('销毁当前对象')

a_student=Student('zym',25,'男')
print(a_student)
print(a_student())

'''
2.创建父类Person,属性name,函数eat,run,创建子类Student,学生类
  继承Person,属性name,age,id,函数eat,study,全局定义一个函数
  get_name,传入一个Person类的实例参数,分别传入Person和Student
  两个类的实例对象进行调用.
'''
class Person():
    def __init__(self,name):
        self.name=name
    def eat(self):
        print('person正在吃东西')
    def run(self):
        print('person正在跑步')

class Student(Person):
    def __init__(self,name,age,id):
        self.name=name
        self.age=age
        self.id=id
    def eat(self):
        print('student正在吃饭')
    def study(self):
        print('student正在学习')

def get_name(Person):
    print(Person.name)

a_person=Person('zym_person')
a_student=Student('zym_student',25,'1201010229')
get_name(a_person)
get_name(a_student)

# 3.用代码完成python多态例子和鸭子模型

# 1.多态展示
class Person():
    def __init__(self,name):
        self.name=name
    def eat(self):
        print('person正在吃东西')
    def run(self):
        print('person正在跑步')

class Student(Person):
    def __init__(self,name,age,id):
        self.name=name
        self.age=age
        self.id=id
    def eat(self):
        print('student正在吃饭')
    def study(self):
        print('student正在学习')

p=Person('zym_person')
s=Student('zym_student',25,'1201010229')

def show(Person):
    Person.eat()

show(p)
show(s)

# 2.鸭子形态
class Person():
    def __init__(self,name):
        self.name=name
    def eat(self):
        print('person正在吃东西')
    def run(self):
        print('person正在跑步')

class Student():
    def __init__(self,name,age,id):
        self.name=name
        self.age=age
        self.id=id
    def eat(self):
        print('student正在吃饭')
    def study(self):
        print('student正在学习')

p=Person('zym_person')
s=Student('zym_student',25,'1201010229')

def show(Person):
    Person.eat()

show(p)
show(s)