# coding=utf-8
# author:hhu_zym(qq:1045797922)
# 动脑学院VIP学员

'''
1.range(),需要生成下面的列表,需要填入什么参数:
    1.[3,4,5,6]
    2.[3,6,9,12,15,18]
    3.[-20,200,420,640,860]
'''
# print(list(range(3,7)))
# print(list(range(3,19,3)))
# print(list(range(-20,900,220)))

# 2.列表推导式或者生成器表达式(2选择1)完成1-100之间所有偶数和
# 列表推导式
print(sum([x for x in range(1,101) if x % 2==0]))  # 2550
# 生成器表达式
def envn_sum(num):
    if not isinstance(num,int):
        raise TypeError('输入类型不正确')
    if num<1:
        raise ValueError('输入数值不正确')
    while num>=1:
        if num%2==0:
            yield num
        num-=1

gen=envn_sum(101)
s=0
for item in gen:
    s+=item
print(s)            #print(sum(gen)) 2550

'''
3.定义一个函数,使用关键字yield创建一个生成器,求1,3,5,7,9各数
   字的三次方,并把所有的结果放入到list里面打印输入
'''
def num3(num):
    if not isinstance(num,int):
        raise TypeError('输入类型不正确')
    if num<1:
        raise ValueError('输入数值不正确')
    while num>=1:
        yield num**3
        num-=2

numm=num3(9)
a_list=list(numm)
print(a_list)

# 4.写一个自定义的迭代器类(iter)
class MyIter():
    def __init__(self,num):  # 初始化数据
        self.num=num
    def __next__(self):      # 每一次取自定义迭代器里面元素的时候
        if self.num==0:
            raise StopIteration('已经没有元素了')
        self.num-=1
        return self.num**2
    def __iter__(self):      # （核心方法）返回自身
        return self

a_iter=MyIter(6)
for item in a_iter:
    print(item)