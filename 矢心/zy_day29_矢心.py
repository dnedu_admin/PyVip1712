# coding=utf-8
# author:hhu_zym(qq:1045797922)
# 动脑学院VIP学员

# 1.函数缓存的使用
# from functools import lru_cache
# from time import time
#
# @lru_cache(maxsize=10,typed=False)  # 使用装饰器函数进行函数缓存
# def fib(n):
#     if n<2:
#         return n
#     return fib(n-1)+fib(n-2)
#
# def count_time(num):
#     time1=time()
#     [fib(n) for n in range(num)]
#     time2=time()
#     print(time2-time1)
#
# count_time(35)

# 2.性能分析器的使用
# cProfile的使用
# import cProfile
#
# def fib(n):
#     if n<2:
#         return n
#     return fib(n-1)+fib(n-2)
#
# cProfile.run('fib(35)')

# line_Profile的使用
# from line_profiler import LineProfiler
# from time import time
#
# def fib(n):
#     if n<2:
#         return n
#     return fib(n-1)+fib(n-2)
#
# def count_time(num):
#     time1=time()
#     [fib(n) for n in range(num)]
#     time2=time()
#     print(time2-time1)
#
# prof=LineProfiler(count_time)  # 构建函数对象
# prof.enable()
# count_time(35)
# prof.disable()
# prof.print_stats()

# 3.内存分析器的使用
from memory_profiler import profile

@ profile
def run_time():
    x=[8]*(10**6*6)
    y=[9]*(10**6*6)
    del x
    del y

if __name__=='__main__':
    run_time()