# coding:utf-8


"""
1.自己写一个字典:
    name_dict = {'name':'ben','age':22,'sex':'男'}
    添加,删除,更新,清空操作
"""


def home_work_one():
    print("第1题")
    name_dict = {'name': 'ben', 'age': 22, 'sex': '男'}
    name_dict['size'] = '18cm'
    print(name_dict)
    print(name_dict.pop('size'))
    print(name_dict)
    name_dict['sex'] = '女'
    print(name_dict)
    name_dict.clear()
    print(name_dict)


"""
2.写两个集合:
    num1_set = {3,5,1,2,7}
    num2_set = {1,2,3,11}
    并分别进行&,|,^,-运算

"""


def home_work_two():
    print("第2题")
    num1_set = {3, 5, 1, 2, 7}
    num2_set = {1, 2, 3, 11}
    print(num1_set & num2_set)
    print(num1_set | num2_set)
    print(num1_set ^ num2_set)
    print(num2_set ^ num1_set)
    print(num1_set - num2_set)
    print(num2_set - num1_set)


"""
3.整数字典:
num_dict = {'a':13,'b':22,'c':18,'d':24}
按照dict中value从小到大的顺序排序
"""


def home_work_three():
    print("第3题")
    num_dict = {'a': 13, 'b': 22, 'c': 18, 'd': 24}
    num_list = num_dict.items()
    print(type(num_list))
    print(num_list)
    sorted_list = sorted(num_list, key=lambda x: x[1])
    print(dict(sorted_list))


if __name__ == "__main__":
    home_work_one()
    home_work_two()
    home_work_three()
