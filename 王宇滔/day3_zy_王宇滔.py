# coding:utf-8


"""
1. 格式化显示浮点数12.78，要求保留3位小数，输出占20字符，空位用0补齐
"""
print("")
float_num = 12.78
print('%20.3f' % float_num)

"""
2. 将字符串”动脑学院”转换为bytes类型。
"""
print("")
dn_str = "动脑学院"
print(bytes(dn_str, encoding="UTF-8"))

'''
3. 将以下3个字符串合并为一个字符串，字符串之间用3个_分割
   hello  动脑  pythonvip       合并为“hello___动脑___pythonvip”
'''
print("")
str_list = ["hello", "动脑", "pythonvip"]
str_split = "___"
print(str_split.join(str_list))

'''
4. 删除字符串  “    你好，动脑eric     ”首尾的空格符号
'''
print("")
str_with_space = "    你好，动脑eric     "
print(str_with_space.strip())

'''
5. 用户信息如下所示字符串中，包含信息依次为  姓名   学号  年龄，不同信息之间的空格数不确定，请提取用户信息分别保存到 xxx_name,xxx_num, xxx_age。
'''

students_info = {
    "eric": "    Eric   dnpy_001     28",
    "zhangsan": " 张三         dnpy_100    22     ",
}

for index, info in students_info.items():
    print(index)
    info_list = info.split()
    if info_list.__len__() != 3:
        break
    print(info_list)
    print((index + "_name:{0[0]}" + "\n" + index + "_num:{0[1]}" + "\n" + index + "_age:{0[2]}").format(info_list))
