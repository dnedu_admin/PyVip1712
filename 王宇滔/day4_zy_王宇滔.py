# coding:UTF-8

"""
第一题:
num_list = [[1,2],[‘tom’,’jim’],(3,4),[‘ben’]]
1. 在’ben’后面添加’kity’
2. 获取包含’ben’的list的元素
3. 把’jim’修改为’lucy’
4. 尝试修改3为5,看看
5. 把[6,7]添加到[‘tom’,’jim’]中作为第三个元素
6.把num_list切片操作:
num_list[-1::-1]
"""


def home_work_one(_list):
    # 1. 在’ben’后面添加’kity’
    _list.append('kity')
    print(_list)

    # 2. 获取包含’ben’的list的元素
    print(_list[-2])

    # 3. 把’jim’修改为’lucy’
    _list[1][1] = 'lucy'
    print(_list)

    # 4. 尝试修改3为5,看看
    # _list[2][0] = 5

    # 5. 把[6,7]添加到[‘tom’,’jim’]中作为第三个元素
    _list[1].append([6, 7])
    print(_list)

    # 6.把num_list切片操作:
    temp_list = _list[-1::-1]
    print(temp_list)
    temp_list2 = _list[::-1]
    print(temp_list2)


"""
第二题:
numbers = [1,3,5,7,8,25,4,20,29];
1.对list所有的元素按从小到大的顺序排序
2.求list所有元素之和
3.将所有元素倒序排列
"""


def home_work_two(_list):
    # 1.对list所有的元素按从小到大的顺序排序
    _list.sort()
    print(_list)

    # 2.求list所有元素之和
    print(sum(_list))

    # 3.将所有元素倒序排列
    _list.sort(reverse=True)
    print(_list)


if __name__ == "__main__":
    home_work_one([[1, 2], ['tom', 'jim'], (3, 4), ['ben']])
    home_work_two([1, 3, 5, 7, 8, 25, 4, 20, 29])
