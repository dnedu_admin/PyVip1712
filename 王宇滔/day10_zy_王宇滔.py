"""
  1.自定义一个异常类,当list内元素长度超过10的时候抛出异常
"""


class WrongTypeError(Exception):
    def __init__(self, msg="传入的类型错误"):
        Exception.__init__(self, msg)


class ListOutOfSizeError(Exception):
    def __init__(self, msg="list长度超过10"):
        Exception.__init__(self, msg)


def home_work_one(ob):
    if isinstance(ob, list):
        if len(ob) > 10:
            raise ListOutOfSizeError
    else:
        raise WrongTypeError


"""
  2.思考如果对于多种不同的代码异常情况都要处理,又该如何去
     处理,自己写一个小例子
"""
"""
  3.try-except和try-finally有什么不同,写例子理解区别
"""


def home_work_two_and_three():
    temp = input("请输入数字：")
    try:
        num = int(temp)
        print(100 / num)
    except ValueError as e:
        print(e)
    except ZeroDivisionError as e:
        print(e)
    finally:
        print("输入是：" + temp)


"""
  4.写函数，检查传入字典的每一个value的长度，如果大于2，
     那么仅仅保留前两个长度的内容，并将新内容返回给调用者
     dic = {“k1”: "v1v1","k2":[11,22,33}}
"""


def home_work_four(map):
    if isinstance(map, dict):
        for key in map:
            value = map.get(key)
            print(value)
            if len(value) > 2:
                map[key] = value[:2]
        return map
    else:
        raise WrongTypeError


if __name__ == "__main__":
    try:
        home_work_one([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11])
    except Exception as e:
        print(e)

    home_work_two_and_three()
    print(home_work_four({"k1": "v1v1", "k2": [11, 22, 33]}))
