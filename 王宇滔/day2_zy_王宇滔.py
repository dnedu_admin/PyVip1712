# coding:UTF-8
import random

import math

""" 
十进制数字2034 的二进制表示，八进制表示，16进制表示 
"""


def get_num_bin(num):
    """ 返回二进制数 """
    return bin(int(str(num), 10))


def get_num_oct(num):
    """ 返回八进制数 """
    return oct(int(str(num), 10))


def get_num_dec(num):
    """ 返回十进制数 """
    return int(str(num), 10)


def print_num_hex(num):
    """ 返回十六进制数 """
    return hex(int(str(num), 10))


print("")
print("第一题")
magic_num = 2034
print("二进制为:", get_num_bin(magic_num))
print("八进制为:", get_num_oct(magic_num))
print("十进制为:", get_num_dec(magic_num))
print("十六进制为:", print_num_hex(magic_num))

# -----------------------------------------------------------------------------------------

""" 
生成一个100到300之间的随机整数n，求其平方根，产生一个1到20之间的随机浮点数p，求大于p 的最小整数。 
"""
print("")
print("第二题")
random_int_num = random.randint(100, 300)
print("随机整数为:", random_int_num)
print("随机整数平方根为:", math.sqrt(random_int_num))

random_float_num = random.uniform(1, 20)
print("随机浮点数为:", random_float_num)
print("随机浮点数的最小整数为:", math.floor(random_float_num))

# -----------------------------------------------------------------------------------------

"""
运维系统中监控到磁盘信息，磁盘使用量为 12106450611211 bytes, 页面展示中需要展示单位为GB，保留2为小数。
"""
print("")
print("第三题")
b = 12106450611211
kb = b / 1024
mb = kb / 1024
gb = mb / 1024
print("磁盘使用量为:", round(gb, 2), "GB")

# -----------------------------------------------------------------------------------------

"""
使用input函数,记录键盘输入的内容,打印输出该值的类型, 并转换为数值型。
判断数值num大小,如果num>=90,输出打印:你的成绩为优秀
如果num>=80 and num<90,输出打印:你的成绩为良好
如果num>=60 and num <80,输出打印:你的成绩为一般
如果num<60,输出打印:你的成绩为不合格
"""

print("")
print("第四题")


def get_input_str():
    temp = input("随便输入点什么吧:")
    print("该值的类型:", type(temp))
    return temp


def print_num_level(num):
    if num >= 90:
        print("你的成绩为优秀")
    elif num >= 80:
        print("你的成绩为良好")
    elif num >= 60:
        print("你的成绩为一般")
    elif num >= 0:
        print("你的成绩为不合格")
    else:
        print("错误的成绩")


while True:
    try:
        int_num = int(get_input_str(), 10)
        print_num_level(int_num)
        break
    except ValueError:
        print("输入错误，重新输入")
