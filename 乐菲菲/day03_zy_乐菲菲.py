#coding:utf-8

'''
1. 格式化显示浮点数12.78，要求保留3位小数，输出占20字符，空位用0补齐

2. 将字符串”动脑学院”转换为bytes类型。

3. 将以下3个字符串合并为一个字符串，字符串之间用3个_分割
   hello  动脑  pythonvip       合并为“hello___动脑___pythonvip”

4. 删除字符串  “    你好，动脑eric     ”首尾的空格符号

5. 用户信息如下所示字符串中，包含信息依次为  姓名   学号  年龄，不同信息之间的空格数不确定，请提取用户信息分别保存到 xxx_name,xxx_num, xxx_age。
eric=“    Eric   dnpy_001     28”
提取后 eric_name=“Eric”eric_num=”dnpy_001”,eric_age=”28”
zhangsan = “ 张三         dnpy_100    22     ”
'''

#1. 格式化显示浮点数12.78，要求保留3位小数，输出占20字符，空位用0补齐
float_num = 12.78
print('%20.3f'%float_num)

#2. 将字符串”动脑学院”转换为bytes类型
str_string = "动脑学院"
bytes_string = bytes(str_string, encoding="UTF-8")
print(bytes_string)

#3. 将以下3个字符串合并为一个字符串，字符串之间用3个_分割
#   hello  动脑  pythonvip       合并为“hello___动脑___pythonvip”
str_list = ["hello", "动脑", "pythonvip"]
#str_merge = '___'.join(str(i) for i in str_list)
str_merge = '___'.join(str_list)
print(str_merge)

#4. 删除字符串  “    你好，动脑eric     ”首尾的空格符号
str_space = "    你好，动脑eric     "
str_del_space = str_space.strip()
print(str_del_space)

#5. 用户信息如下所示字符串中，包含信息依次为  姓名   学号  年龄，不同信息之间的空格数不确定，请提取用户信息分别保存到 xxx_name,xxx_num, xxx_age。
#eric=“    Eric   dnpy_001     28”
#提取后 eric_name=“Eric”eric_num=”dnpy_001”,eric_age=”28”
#zhangsan = “ 张三         dnpy_100    22     ”
zhangsan = " 张三         dnpy_100    22     "
str_temp = ','.join(filter(lambda x: x, zhangsan.split(' ')))
zhangsan_name = str_temp.split(',')[0]
zhangsan_num = str_temp.split(',')[1]
zhangsan_age = str_temp.split(',')[2]
print(' zhang_name:{}; zhangsan_num:{}; zhangsan_age:{} '.format(zhangsan_name,zhangsan_num,zhangsan_age))
