#coding:utf-8

'''
1.自己写一个字典:
    name_dict = {'name':'ben','age':22,'sex':'男'}
    添加,删除,更新,清空操作

2.写两个集合:
    num1_set = {3,5,1,2,7}
    num2_set = {1,2,3,11}
    并分别进行&,|,^,-运算

3.整数字典:
    num_dict = {'a':13,'b':22,'c':18,'d':24}
    按照dict中value从小到大的顺序排序
'''

#1.自己写一个字典:
#    name_dict = {'name':'ben','age':22,'sex':'男'}
#    添加,删除,更新,清空操作
name_dict = {'name':'ben','age':22,'sex':'男'}
#print(name_dict)
name_dict['address'] = 'hangzhou'   # 添加
#print(name_dict)
name_dict.pop('address')   # 删除
#print(name_dict)'
name_dict['age'] = 25   # 更新
#print(name_dict)
name_dict.clear()   # 清空
#print(name_dict)


# 2.写两个集合:
#     num1_set = {3,5,1,2,7}
#     num2_set = {1,2,3,11}
#     并分别进行&,|,^,-运算
num1_set = {3,5,1,2,7}
num2_set = {1,2,3,11}

num_yu_set = num1_set & num2_set
#print(num_yu_set)
num_huo_set = num1_set | num2_set
#print(num_huo_set)
num_yihuo_set = num1_set ^ num2_set
#print(num_yihuo_set)
num_cha_set = num1_set - num2_set
# print(num_cha_set)

# 3.整数字典:
# num_dict = {'a':13,'b':22,'c':18,'d':24}
# 按照dict中value从小到大的顺序排序
num_dict = {'a':13,'b':22,'c':18,'d':24}
num_sort_dict = sorted(num_dict.items(), key = lambda d:d[1])
# print(num_sort_dict)
