#coding:utf-8

'''
1.用while和for两种循环求计算1+3+5.....+45+47+49的值
2.代码实现斐波那契数列(比如前30个项)
'''

#1.用while和for两种循环求计算1+3+5.....+45+47+49的值
i = 1; t_num = 0
while i <= 25:
    t_num += i * 2 -1
    i +=  1
#print(t_num)

t_num1 = 0
for n in range(1,26):
    t_num1 += n * 2 -1
# print(t_num1)

# 2.代码实现斐波那契数列(比如前30个项)
fb = 0
list_fb = [1,1]
for i in range(0, 30):
    fb = list_fb[i] + list_fb[i + 1]
    list_fb.append(fb)
#print(list_fb)
