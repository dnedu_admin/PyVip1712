#coding:utf-8
'''
1.简述普通参数、默认参数、关键字参数、动态收集参数的区别,理解为主
例如函数： def func_ex(a, b = 1, *args, **kw)
2.def foo(x, y, z, *args, **kw):
    sum = x + y + z
    print(sum)
    for i in args:
        print(i)
    print(kw)
    for j in kw.items():
        print(j)
a_tuple = (1,2,3)     此处参数修改为2个看看会怎么样?
b_dict = {'name': 'jim', 'age': 28, 'add': '上海'}
foo(*a_tuple, **b_dict)    分析这里会怎么样?
3.题目:执行分析下代码
    def func(a, b, c=9, *args, **kw):
         print('a =', a, 'b =', b, 'c =', c, 'args =', args, 'kw =', kw)
    func(1,2)
    func(1,2,3)
    func(1,2,3,4)
    func(1,2,3,4,5)
    func(1,2,3,4,5,6,name='jim')
    func(1,2,3,4,5,6,name='tom',age=22)
    扩展: 如果把你的函数也定义成  def  get_sum(*args,**kw):pass
    你的函数可以接受多少参数?
4.写一个函数函数，计算传入字符串中的数字、字母、空格和其他的个数分别为多少?
'''

# 1.简述普通参数、默认参数、关键字参数、动态收集参数的区别,理解为主
# 例如函数：     def person(name, sex, age=28, *args, **kw):
#                    print('name:', name, 'age:', age, 'sex:', sex, 'args:', args, 'kw:', kw)
# 调用该函数：   person('leiff', 'man', 1, 2, 3, mobile = '13588881234')
# 其中name,sex是普通参数；
#     age=28是默认参数,默认值为28；
#     *args是动态收集参数（在该例子中收集的参数值为(2,3)）；
#     mobile = '13588881234'在调用函数时传进的参数，为关键字参数
# def person(name, sex, age=28, *args, **kw):
#     print('name:', name, 'age:', age, 'sex:', sex, 'args:', args, 'kw:', kw)
# person('leiff', 'man', 1, 2, 3,  mobile = '13588881234')

# 2.def foo(x, y, z, *args, **kw):
#     sum = x + y + z
#     print(sum)
#     for i in args:
#         print(i)
#     print(kw)
#     for j in kw.items():
#         print(j)
# a_tuple = (1,2,3)     此处参数修改为2个看看会怎么样?
# b_dict = {'name': 'jim', 'age': 28, 'add': '上海'}
# foo(*a_tuple, **b_dict)    分析这里会怎么样?

# def foo( *args, **kw):
#     # sum = x + y + z
#     # print(sum)
#     for i in args:
#         print(i)
#     print(kw)
#     for j in kw.items():
#         print(j)
# a_tuple = (1,2,3)
# b_dict = {'name': 'jim', 'age': 28, 'add': '上海'}
# foo(*a_tuple, **b_dict)

# 3.题目:执行分析下代码
#     def func(a, b, c=9, *args, **kw):
#          print('a =', a, 'b =', b, 'c =', c, 'args =', args, 'kw =', kw)
#     func(1,2)
#     func(1,2,3)
#     func(1,2,3,4)
#     func(1,2,3,4,5)
#     func(1,2,3,4,5,6,name='jim')
#     func(1,2,3,4,5,6,name='tom',age=22)
#     扩展: 如果把你的函数也定义成  def  get_sum(*args,**kw):pass
#     你的函数可以接受多少参数?
#     答: 参数的数量没有限制
# def func(a, b, c=9, *args, **kw):
#      print('a =', a, 'b =', b, 'c =', c, 'args =', args, 'kw =', kw)
# func(1,2)
# func(1,2,3)
# func(1,2,3,4)
# func(1,2,3,4,5)
# func(1,2,3,4,5,6,name='jim')
# func(1,2,3,4,5,6,name='tom',age=22)

# 4.写一个函数函数，计算传入字符串中的数字、字母、空格和其他的个数分别为多少?
# def cal_num(instr):
#     digit_num = 0; alpha_num = 0; space_num = 0; other_num = 0
#     for i in instr:
#         if i.isdigit():
#             digit_num += 1
#         elif i.isalpha():
#             alpha_num += 1
#         elif i.isspace():
#             space_num += 1
#         else:
#             other_num += 1
#     print('digit = %d, char = %d, space = %d, other = %d' %(digit_num, alpha_num, space_num, other_num))
# IN_STR = 'adka d i3823 kadifiew ew832n2  aoi8w23 **)*ll&aldf    39aldkfda ada39238239823 akdjfa !!!!!'
# cal_num(IN_STR)

