# -*- coding: UTF-8 -*-
# @ Author : 游于艺
# @ QQ : 58012007
# @ File: zy_day25_游于艺.py
# @ Time: 2018/3/26 19:03
import os
from os import path
import uuid
import shutil
import base64
"""
1.在当前项目下创建目录abc,并在abc目录下,使用uuid产生唯一码并写入到文件
  uuid.py中,并拷贝整个目录abc,新目录名为app,并将app目录下文件拷贝,新
  文件名为:new.py文件
"""
# if path.exists(r'./abc') == False:
#     os.makedirs('./abc')
# u = uuid.uuid5(uuid.NAMESPACE_DNS,"xion")
# with open('./abc/uuid.py','w+',encoding='utf-8') as f:
#     f.write(str(u))
## shutil.move('abc','app') #重命名
# shutil.copytree('abc','app')
# shutil.copyfile('./app/uuid.py','./app/new.py')

"""
2.创建本地文件’one.txt’,在里写入部分数据(‘今天天气不错,我们去动物园n玩吧’
  n从1-9),使用b64encode对每一行数据编码,然后写入到文件oen.txt中.每写
  完一行之后写入换行符.
  打开刚才创建的文件one.txt,以读取多行的形式读取文件中的内容,并把每一行的
  内容通过b64decode对每一行数据解码,解码之后每一行数据依然是二进制形式
  所以,继续对每一行数据通过utf-8的形式解码为可以普通字符串,然后判断每一行
  字符串是否包含’动物园6’的子字符串,如果包含,先输出换行符,然后则打印输出
  当前行的字符串,最后创建一个文件,’two.txt’,并把帅选之后的数据写入到这个新
  文件中.
"""

# with open('one.txt','wb') as f1:
#     for n in range(1,10):
#         txt = '今天天气不错,我们去动物园%s玩吧' %n
#         b64txt = base64.b64encode(txt.encode('utf-8'))
#         f1.write(b64txt)
#         f1.write('\n'.encode('utf-8'))
#
# with open('one.txt','rb') as f2:
#     for line in f2:
#         cont = line.decode('utf-8').strip()
#         # print(cont)
#         txt = base64.b64decode(cont).decode('utf-8')
#         # print(txt)
#         if '动物园6' in txt:
#             print("\n", line)
#             with open('two.txt','wb') as f3:
#                 f3.write(line)

with open('one.txt','rb') as f2:
    datas = f2.readlines()
for line in datas:
    data = base64.b64decode(line).decode('utf-8').strip()
    if '动物园6' in data:
        print('\n',data)
        with open('two.txt', 'w',encoding='utf-8') as f3:
            f3.write(data)

"""
3.准备一张.jpg图片,比如:mm.jpg,读取图片数据并通过b85encode加密之后写入到新文件mm.txt文件中,
  然后读取mm.txt数据并解密之后然后写入到mmm.jpg文件中
"""
# with open('mm.jpg','rb') as f4:
#     cont = f4.read()
#     b64cont = base64.b85encode(cont)
#     with open('mm.txt','wb') as f5:
#         f5.write(b64cont)
#     with open('mm.txt', 'rb') as f6:
#         with open('mmm.jpg','wb') as f7:
#             f7.write(base64.b85decode(f6.read()))

