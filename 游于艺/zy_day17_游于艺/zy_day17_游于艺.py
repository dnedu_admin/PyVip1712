# -*- coding: UTF-8 -*-
# @ Author : 游于艺
# @ QQ : 58012007
# @ File: zy_day17_游于艺.py
# @ Time: 2018/3/6 20:11

"""
1.使用pickle,json(注意:两个模块)
把字典a_dict = [1,2,3,[4,5,6]]
序列化与反序列化,序列化,保存到文件名为serialize.txt文件里面,然后大开,并读取数据
反序列化为python对象加载到内存,并打印输出
尝试序列化/反序列非文件中,直接操作
"""
import pickle,json
a_dict = [1,2,3,[4,5,6]]

# pickle
p = open("serialize.txt","wb+")
pickle.dump(a_dict,p,0) #序列化
print("--------分割线----------")
p = open("serialize.txt","rb+")
b_dict = pickle.load(p) #反序列化
print(b_dict)
p.close()

#json
j = open("serialize.txt","w+")
json.dump(a_dict,j) #序列化
print("--------分割线----------")
j = open("serialize.txt","r+")
b_dict = json.load(j) #反序列化
print(b_dict)
j.close()
"""
2.自己写代码对比深浅拷贝的区别,写完代码然后看看运行结果,然后分析原因
   比如：a_list=[1,2,3,4,[8,9,0]]
"""

print("==========================================")
# 浅拷贝
a_list=[1,2,3,4,[8,9,0]]
from copy import copy
b_list = copy(a_list)
a_list[2] = 100
print(b_list[2])    #3,第一层不会改变
a_list[4][0] = 100
print(a_list)
print(b_list[4][0])   #100，副本内层改变了

# 深拷贝
a_list=[1,2,3,4,[8,9,0]]
from copy import deepcopy
c_list = deepcopy(a_list)
a_list[2] = 100
print(c_list[2])    #3,第一层不会改变
a_list[4][0] = 100  #把内层8改成100
print(a_list)
print(c_list[4][0]) #8，副本内层也不会改变
print(c_list)

# 浅拷贝只是拷贝了对象的第一层，内层对象是没有被拷贝
"""
3.分别写代码完成内存缓冲区文本数据和二进制数据的读/写操作
"""
print("================================================")
from io import StringIO
s = StringIO()
# 写数据
s.write("我是荣哥！\n")
s.write("我是中国人！\n")
print(s.getvalue())

#读数据
s = StringIO("你这个大坏蛋。\n你是小日本！")
while 1:
    conn = s.readline()
    if len(conn)!=0:
        print(conn.strip())
    else:
        break
print("-----------------------------------------------")
from io import BytesIO
b = BytesIO()
b.write("我是荣哥，很高兴认识你！\n".encode("utf-8"))
b.write("我是中国人！\n".encode("utf-8"))
print(b.getvalue())
print(b.getvalue().decode('utf-8'))
#读数据
s = StringIO("你这个大坏蛋\n你是小日本！")
while 1:
    conn = s.readline()
    if len(conn)!=0:
        print(conn.strip())
    else:
        break