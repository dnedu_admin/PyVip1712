# -*- coding: UTF-8 -*-
# @ Author : 游于艺
# @ QQ : 58012007
# @ File: zy_day05_游于艺.py.py
# @ Time: 2018/1/24 23:11

print("-------------------------------1---------------------------------")
"""
1.自己写一个字典:
    name_dict = {'name':'ben','age':22,'sex':'男'}
    添加,删除,更新,清空操作
"""
name_dict = {'name':'ben','age':22,'sex':'男'}
name_dict["city"] = "长沙"    #{'name': 'ben','age':22, 'sex': '男','city': '长沙'}
name_dict.pop('age')    #{'name': 'ben', 'sex': '男','city': '长沙'} del name_dict['age']
name_dict["name"] = "游于艺"   #{'name': '游于艺', 'sex': '男','city': '长沙'}
name_dict.update({'tel':13808885888})   #{'name': '游于艺', 'sex': '男','city': '长沙','tel':13808885888}
print(name_dict)
name_dict.clear()   #{}
print(name_dict)


print("-------------------------------2---------------------------------")
"""
2.写两个集合:
    num1_set = {3,5,1,2,7}
    num2_set = {1,2,3,11}
    并分别进行&,|,^,-运算
"""
num1_set = {3,5,1,2,7}
num2_set = {1,2,3,11}

print(num1_set&num2_set)    #{1, 2, 3}
print(num1_set|num2_set)    #{1, 2, 3, 5, 7, 11}
print(num1_set^num2_set)    #{5, 7, 11}
print(num1_set-num2_set)    #{5, 7}

print("-------------------------------3---------------------------------")
"""
3.整数字典:
    num_dict = {'a':13,'b':22,'c':18,'d':24}
    按照dict中value从小到大的顺序排序
"""
num_dict = {'a':13,'b':22,'c':18,'d':24}

print("------方法1------")
def dict2list(dic:dict):
    ''' 将字典转化为列表 '''
    keys = dic.keys()
    vals = dic.values()
    lst = [(key, val) for key, val in zip(keys, vals)]
    return lst

#num_dict2 = sorted(dict2list(num_dict), key=lambda x:x[0], reverse=True)
num_dict2 = sorted(dict2list(num_dict), key=lambda x:x[1], reverse=False)
print(num_dict2)
print(dict(num_dict2))

print("------方法2------")
import operator
#num_dict3=sorted(num_dict.items(),key=operator.itemgetter(0), reverse=True)
num_dict3=sorted(num_dict.items(),key=operator.itemgetter(1))
print(num_dict3)
print(dict(num_dict3))