# -*- coding: UTF-8 -*-
# @ Author : 游于艺
# @ QQ : 58012007
# @ File: zy_day32_游于艺.py
# @ Time: 2018/4/13 10:32

# 将datafram 写入数据库并截图 2张图， 一张python运行图，一张数据库表

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import pymysql
from sqlalchemy import create_engine
from pandas.io.sql import read_sql

df = pd.DataFrame(data=np.random.randint(60, 100, (4, 3)), index=['第%s行' % i for i in range(1, 5)],columns=['第%s列' % j for j in range(1,4)])
# print(df)
# engine = create_engine("mysql+pymysql://root:root@localhost:3306/my_python?charset=utf8", echo=False)
# df.to_sql(name="score", con=engine, if_exists="append", index=False)

db = pymysql.connect("localhost", "root", "root", "my_python", charset='utf8')
query = "SELECT 第1列,第2列,第3列 from score"
df_score = read_sql(query, db)
print(df_score)