# -*- coding: UTF-8 -*-
# @ Author : 游于艺
# @ QQ : 58012007
# @ File: two.py
# @ Time: 2018/2/2 14:57
# import sys
# from mysite.myapp.one import name
# from importlib import reload
#
# def func():
#     print(name)
#     del sys.modules["name"]
#     reload(name)
#     print(sys.modules)

# 3.写函数，判断用户传入的对象（字符串、列表、元组）长度是否大于6,如果大于6,截取前4位,否则直接打印输出
def input_fun(obj):
    if isinstance(obj,(str,list,tuple)):
        if len(obj) > 6:
            print(obj[:4])
        else:
            print(obj)
    else:
        print('传入的数据类型不正确')

if __name__ == "__main__":  #解析器执行模块
    # func()
    input_fun("abcdefg")
    input_fun([1,2,3,4,5,6,7])
    input_fun((1,2,3,4,5,6,7,8))
else:
    print("被导入模块")