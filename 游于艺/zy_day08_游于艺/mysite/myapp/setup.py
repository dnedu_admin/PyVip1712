# -*- coding: UTF-8 -*-
# @ Author : 游于艺
# @ QQ : 58012007
# @ File: setup.py
# @ Time: 2018/2/2 14:38

from distutils.core import setup
setup (
    name = 'myAPP', #模块的名称
    version='1.0.0',    #版本信息
    author = '游于艺',    #作者信息
    author_email='58012007@qq.com',
    url = 'http://www.baidu.com',   #url
    description='这是一个测试打包的过程',  #描述
    py_modules = ['two'], #打包的模块
)


#进入要打包的文件目录下面，打开命令行窗口
# python setup.py sdist

