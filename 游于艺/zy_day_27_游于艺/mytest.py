# -*- coding: UTF-8 -*-
# @ Author : 游于艺
# @ QQ : 58012007
# @ File: mytest.py
# @ Time: 2018/3/30 13:11
def eat(what):
    if isinstance(what,str):
        return "吃"+what
    else:
        raise TypeError('参数类型不正确')

class Dog():
    def __init__(self, name):
        self.name = name

    def eat(self):
        print('小狗%s正在吃屎' % self.name)
