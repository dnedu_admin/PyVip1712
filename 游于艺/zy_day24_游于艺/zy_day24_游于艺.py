# -*- coding: UTF-8 -*-
# @ Author : 游于艺
# @ QQ : 58012007
# @ File: zy_day24_游于艺.py
# @ Time: 2018/3/21 22:56

import os
from os import path
import requests
from urllib.request import urlretrieve
import re
import time

def get_code(url):
    try:
        page = requests.get(url)
        return page.text
    except Exception as e:
        print(e)

def get_img(code):
    try:
        reg = r'src="(.+?\.jpg)" pic_ext'
        img = re.compile(reg)
        img_list = img.findall(code)
        print(img_list)

        return  #去掉建目录保存图片
        # 下载图片
        if path.exists(r'./images')==False:
            os.makedirs('./images')
        # 图片名字，保存图片
        num = 1
        for url in img_list:
            urlretrieve(url,'./images/%s.jpg' % num)    #下载保存
            num += 1
            time.sleep(1)
    except Exception as e:
        print(e)

def get_urls(code):
    try:
        reg = r'pb_list_pager">(.+?)</li>'
        url = re.compile(reg,re.S)
        url_code = url.findall(code)
        # print(url_code)
        ug = r'href="(.+?)">'
        url2 = re.compile(ug)
        url_list = url2.findall(url_code[0])
        # print(url_list)
        return url_list
    except Exception as e:
        print(e)

def set_url(urls):
    new_urls = []
    for url in urls:
        new_urls.append("http://tieba.baidu.com"+url)
    return new_urls

if __name__ == '__main__':
    url = 'http://tieba.baidu.com/p/2460150866'
    code = get_code(url)
    # code = '<li class="l_pager pager_theme_5 pb_list_pager"><span class="tP">1</span><a href="/p/2460150866?pn=2">2</a><a href="/p/2460150866?pn=3">3</a><a href="/p/2460150866?pn=4">4</a></li>'
    urls = get_urls(code)
    urls = set_url(urls)
    urls.insert(0,url)
    # print(urls)
    for i in range(1, 4):
        html = get_code(urls[i])
        get_img(html)


    # 方法二
    # for i in range(1,4):
    #     html = get_code(url+'?pn='+str(i))
    #     get_img(html)
    print("图片下载完毕")