# -*- coding: UTF-8 -*-
# @ Author : 游于艺
# @ QQ : 58012007
# @ File: zy_day04_游于艺.py.py
# @ Time: 2018/1/22 22:59

"""
第一题:
num_list = [[1,2],[‘tom’,’jim’],(3,4),[‘ben’]]
 1. 在’ben’后面添加’kity’
 2. 获取包含’ben’的list的元素
 3. 把’jim’修改为’lucy’
 4. 尝试修改3为5,看看
 5. 把[6,7]添加到[‘tom’,’jim’]中作为第三个元素
 6.把num_list切片操作:num_list[-1::-1]
"""
num_list = [[1,2],['tom','jim'],(3,4),['ben']]

num_list[3].append('kity')  # 1. 在’ben’后面添加’kity’

print(num_list[3])  #2. 获取包含’ben’的list的元素
for item in num_list[3]:
    print(item)

num_list[1][1] = 'lucy' #3. 把’jim’修改为’lucy’
print(num_list)

#num_list[2][0] = 5 #4. 尝试修改3为5,看看,,元组项目不能修改

num_list[1].append([6,7])    #5. 把[6,7]添加到[‘tom’,’jim’]中作为第三个元素
print(num_list)

print(num_list[-1::-1])    #6.把num_list切片操作:倒序

print(list("abcdefg"))

"""
第二题:
numbers = [1,3,5,7,8,25,4,20,29];
1.对list所有的元素按从小到大的顺序排序
2.求list所有元素之和
3.将所有元素倒序排列
"""
numbers = [1,3,5,7,8,25,4,20,29]
num1 = sorted(numbers)
print(num1)
numbers.sort()  #1.对list所有的元素按从小到大的顺序排序
print(numbers)

# 方法1
sum_numbers = sum(numbers)  #2.求list所有元素之和
print(sum_numbers)

# 方法2
sum_numbers_2 = 0
for item in numbers:
    sum_numbers_2 += item
print(sum_numbers_2)

#3.将所有元素倒序排列
#方法1，未改变原来列表的排序
numbers_reverse = numbers[-1::-1]
# print(numbers)
print(numbers_reverse)

#方法2,改变了原列表的排序
num2 = sorted(numbers,reverse=True)
print(num2)
numbers.sort(reverse=True)
print(numbers)
