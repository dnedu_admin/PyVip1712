# -*- coding: UTF-8 -*-
# @ Author : 游于艺
# @ QQ : 58012007
# @ File: zy_day20_游于艺.py
# @ Time: 2018/3/12 22:59

"""
 创建两个协程函数对象,分别完成1,3,5…99,   2,4,6…,100
 然后让两个协程函数对象分别交替执行.在每一个协程函数对象的内部先打印输出
 每一个数字,然后执行完一轮之后打印提示信息,已经交替执行完毕一轮
 等所有的协程函数对象执行完毕之后,分析一下数据信息
"""

from types import coroutine
import time

async def num1_fun():
    nlist = list(range(1,100,2))
    for num in nlist:
        print(num,"   -----From.列表一")
        await do_next()
async def num2_fun():
    nlist = list(range(2,101,2))
    for num in nlist:
        print(num,"   -----From.列表二")
        await do_next()

@coroutine
def do_next():
    yield

def run(xlist):
    cplist = list(xlist)
    while cplist:
        for item in cplist:
            try:
                item.send(None)
                time.sleep(1)   #休眠1秒看效果
            except StopIteration as e:
                cplist.remove(item)

if __name__ == "__main__":
    xlist = [num1_fun(),num2_fun()]
    run([num1_fun(),num2_fun()])

# 结论：等所有的协程函数对象执行完毕之后,分析一下数据信息
# 原来分开的奇数偶数列表，会按1-100步长为1的顺序打印