# -*- coding: UTF-8 -*-
# @ Author : 游于艺
# @ QQ : 58012007
# @ File: zy_day21_游于艺.py
# @ Time: 2018/3/16 18:21
import re
"""
1.识别下面字符串:’ben’,’hit’或者’ hut’
"""
p = re.compile(ben|hit|hut')
print(p.findall('ben123hit456hut'))

"""
2.匹配用一个空格分隔的任意一对单词,比如你的姓 名
""
print(re.findall('\w+\s\w+','游 于艺'))
"""
3.匹配用一个逗号和一个空格分开的一个单词和一个字母
"""
print(re.findall('\w+,\s\w','my, g'))
"""
4.匹配简单的以’www’开头,以’com’结尾的web域名,比如:www.baidu.com
"""
print(re.findall('^w{3}\.\w+\.com$','www.baidu.com'))
print(re.findall('^https://w{3}\.\w+\.com$','https://www.hao123.com'))