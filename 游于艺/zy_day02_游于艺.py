# -*- coding: UTF-8 -*-
# @ Author : 游于艺
# @ QQ : 58012007

print("1. 十进制数字2034 的二进制表示，八进制表示，16进制表示 ")
num = 2034
print("转2进制：",bin(num))
print("转8进制：",oct(num))
print("转16进制：",hex(num))

print("\n2. 生成一个100到300之间的随机整数n，求其平方根，产生一个1到20之间的随机浮点数p，求大于p 的最小整数。")
import random
import math
n = random.randint(100,300)
print("100-300间随机整数：",n)
print("n的平方根：",math.sqrt(n))
p = random.uniform(1,20)
print("1-20间随机浮点数：",p)
print("大于%s的最小整数：" %(p),int(p)+1)
print("大于%s的最小整数：" %(p),math.ceil(p))

print("\n3. 运维系统中监控到磁盘信息，磁盘使用量为1 2106450611211bytes, 页面展示中需要展示单位为GB，保留2为小数。")
size = 12106450611211
print("磁盘大小：",round(size/(1024**3),2),"GB")
print("磁盘大小：",round(size/(1024**4),2),"TB")

print("\n5. 使用input函数,记录键盘输入的内容,打印输出该值的类型, 并转换为数值型。")
while True:
    num = int(input("请输入数字按回车键："))
    if num > 90:
        print("你的成绩为优秀!")
    elif num>=80 and num<90:
        print("你的成绩为良好！")
    elif num>=60 and num <80:
        print("你的成绩为一般！")
    elif num<60:
        print("你的成绩为不合格！")

