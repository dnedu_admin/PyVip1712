# -*- coding: UTF-8 -*-
# @ Author : 游于艺
# @ QQ : 58012007
# @ File: zy_day12_游于艺.py
# @ Time: 2018/2/24 11:15

"""
1.自己动手写一个学生类,创建属性名字,年龄,性别,完成
     __new__,__init__,__str__,__del__，__call__
"""
# class Student():
#     def __new__(cls, *args, **kwargs):
#         print("构造函数")
#         return super().__new__(cls)
#     def __init__(self,name,age,sex):
#         self.name = name
#         self.age = age
#         self.sex = sex
#     def __str__(self):
#         pass
#     def __del__(self):
#         print("销毁当前对象%s" % self.name)
#     def __call__(self, *args, **kwargs):
#         print("当前对象被调用")
#
# s1 = Student("游于艺",32,"男")
# s1()

"""
2.创建父类Person,属性name,函数eat,run,创建子类Student,学生类
  继承Person,属性name,age,id,函数eat,study,全局定义一个函数
  get_name,传入一个Person类的实例参数,分别传入Person和Student
  两个类的实例对象进行调用.
"""
# class Person():
#     def __init__(self,name):
#         self.name = name
#     def eat(self):
#         print("吃饭啦！")
#     def run(self):
#         print("跑步去了！")
#
# class Student(Person):
#     def __init__(self,name,age,id):
#         self.name = name
#         # super().__init__(name)
#         self.age = age
#         self.id = id
#     def study(self):
#         print("学习了！")
#
# def get_name(self):
#     print(self.name)
#
# p = Person("游老师")
# s = Student("小艺",18,"男")
# s.eat()
# get_name(p)
# get_name(s)
"""
3.用代码完成python多态例子和鸭子模型
"""
# # 多态
# class A():
#     def eat(self):
#         print("A在吃饭！")
# class B(A):
#     def eat(self):
#         print("B在吃饭！")
# def show(obj):
#     obj.eat()
#
# a = A()
# b = B()
# show(a)
# show(b)

# 鸭子形态
# class A():
#     def eat(self):
#         print("A在吃饭！")
# class B():
#     def eat(self):
#         print("B在吃饭！")
# def show(obj):
#     obj.eat()
#
# a = A()
# b = B()
# show(a)
# show(b)