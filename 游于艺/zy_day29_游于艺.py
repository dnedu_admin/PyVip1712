# -*- coding: UTF-8 -*-
# @ Author : 游于艺
# @ QQ : 58012007
# @ File: zy_day29_游于艺.py
# @ Time: 2018/4/4 11:32
"""
    1.函数缓存的使用
"""
# from time import time
# from functools import lru_cache
# @lru_cache(maxsize=10,typed=False)
# def mk_sum(n,num):
#     return sum(range(n,num))
# def count_time(num):
#     t1 = time()
#     [mk_sum(n,num) for n in range(num)]
#     t2 = time()
#     print("耗时：",str(t2-t1))
#
# count_time(5500)    #疑惑：为什么使用函数缓存和不使用函数缓存耗时差不多
"""
    2.性能分析器的使用
"""
# # cProfile
# import cProfile
# def mk_sum(n,num):
#     return sum(range(n,num))
# cProfile.run('mk_sum(0,555555)')

# # LineProfiler
# from line_profiler import LineProfiler
# from time import time
# def mk_sum(n,num):
#     return sum(range(n,num))
# def count_time(num):
#     t1 = time()
#     [mk_sum(n,num) for n in range(num)]
#     t2 = time()
#     print("耗时：",str(t2-t1))
#
# prof = LineProfiler(count_time)
# prof.enable()
# count_time(99999)
# prof.disable()
# prof.print_stats()
"""
    3.内存分析器的使用
"""
# from memory_profiler import profile
# from time import time
# @profile
# def mk_sum(n,num):
#     return sum(range(n,num))
#
# mk_sum(1,99999)