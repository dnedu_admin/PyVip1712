# -*- coding: UTF-8 -*-
# @ Author : 游于艺
# @ QQ : 58012007
# @ File: zy_day11_游于艺.py
# @ Time: 2018/2/22 16:20

"""
1.文件名称:use.txt
   创建该文件,路径由你自己指定,打开该文件,往里面写入
   str_content = ['tom is boy','\n','you are cool','\n','今天你怎么还不回来']
   把数据写入到该文件中,注意操作的模式,关闭文件句柄后
   再次打开该文件句柄,然后读取文件中内容,用with代码块
   实现,然后打印输出信息到控制台,注意换行符
"""
file1 = open("use.txt","w",encoding='utf-8')
file1.writelines("str_content = ['tom is boy','\n','you are cool','\n','今天你怎么还不回来']")
file1.close()

with open("use.txt","r",encoding="utf-8") as f2:
    s = f2.read()
    str = s.replace('\n',r'\n')
    print(str)
    # for line in f2:
    #     print(line)

"""
 2.with创建一个文件homework.txt,尝试多种操作模式.进行写读操作,注意区别
"""
with open("homework.txt","w",encoding="utf-8") as f3:
    f3.write("你好，游于艺！")
with open("homework.txt","r",encoding="utf-8") as f4:
    str = f4.read()
    print(str)
"""
 3.理解文件句柄,对比read(),readline(),readlines()写一个小例子
"""
f = open("use.txt","r",encoding="utf-8")
s1 = f.read()
f.seek(0)
s2 = f.readline()
f.seek(0)
s3 = f.readlines()
print("s1:",s1,"\n\ns2:",s2,"\n\ns3:",s3)
f.close()
"""
 4.准备一张jpg图片,把图片中数据读书出来,并写入到文件my_img.jpg文件中,
    操作完毕后尝试打开my_img.jpg文件看图片显示正不正常
"""
with open("p.jpg","rb") as f5:
    s = f5.readlines()
    print(s)
    with open('my_img.jpg','wb') as f6:
        f6.writelines(s)