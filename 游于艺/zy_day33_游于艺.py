# -*- coding: UTF-8 -*-
# @ Author : 游于艺
# @ QQ : 58012007
# @ File: zy_day33_游于艺.py
# @ Time: 2018/4/16 19:29

"""
1.xlsxwriter包中，往工作表中插入写数据方法  work_sheet.write()
查看write函数定义，解释其函数定义中 @convert_cell_args这个装饰器的具体用途？


答：
扩展所有对单元格操作的方法，是一个公用方法。
旨在将用行列方式Excel表格：如A1，B1，L5 等转换为下标表示的行列方式
A——Z代表列，1——N代表行
work_sheet.write(0, 0, '你好')    #0,0是默认表示行列
work_sheet.write("B4", '琪琪')  # 3，1 #B4就是通过装饰器转换成3,1来表示行列
"""
