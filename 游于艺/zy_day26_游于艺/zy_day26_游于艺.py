# -*- coding: UTF-8 -*-
# @ Author : 游于艺
# @ QQ : 58012007
# @ File: zy_day26_游于艺.py
# @ Time: 2018/3/26 23:16
"""
1. PKCS#5(选择一种hash算法)加密算法加密你的中文名字,写出代码
"""
def oneSubject():
    import hashlib, binascii
    name = '游于艺'
    # sha1_name = hashlib.sha1(name.encode('utf-8'))
    # print(sha1_name.hexdigest())
    cont = hashlib.pbkdf2_hmac('sha512',b'xion',name.encode('utf-8'),iterations=99,dklen=32)
    print(binascii.hexlify(cont))

"""
2. 创建文件test.py,写入一些数据,并计算该文件的md5值
"""
def twoSubject():
    import hashlib
    with open('test.py','w',encoding='utf-8') as f:
        f.write("我是神！")
    with open('test.py','rb') as file:
        m5 = hashlib.md5(file.read())
        print(m5.hexdigest())
    # m5 = hashlib.md5()
    # m5.update('test.py'.encode('utf-8'))
    # print(m5.hexdigest())


"""
3. 遵循上下文管理协议,自己编写一个类,去创建一个上下文管理器,然后自己使用看看
   方式1:普通方式
   方式2:使用contextmanager
"""
class MyClass():
    def __init__(self,name):
        self.name = name
        print("初始化姓名：",name)
    def __enter__(self):
        self.age = 30
        print("进入上下文管理器，设置年龄属性！")
        return self
    def __exit__(self, exc_type, exc_val, exc_tb):
        self.name = None
        self.age = None
        self.doSomeThing = None
        if exc_type:
            print("异常",exc_val)
        else:
            print('正常状态！')
        print('退出上下文管理器')
    def work(self,doSomeThing):
        self.doSomeThing = doSomeThing
        print("{0}岁的{1}在{2}".format(self.age,self.name,self.doSomeThing))



from contextlib import contextmanager
class MyClass2():
    def __init__(self,name):
        self.name = name
        print("初始化姓名：",name)
    def work(self,doSomeThing):
        self.doSomeThing = doSomeThing
        print("{0}岁的{1}在{2}".format(self.age, self.name, self.doSomeThing))
@contextmanager
def mk_work(name):
    print('开始！')
    tls = MyClass2(name)
    tls.age = 30
    yield tls
    print('退出上下文管理器')

def threeSubject():
    print("-----------------方式一-----------------")
    with MyClass('游于艺') as f:
        f.work('在打飞机！')
    print("-----------------方式二-----------------")
    with mk_work('游于艺') as f:
        f.work('在打飞机！')

"""
4.使用colorama模块使用多颜色输出
"""
def fourSubject():
    from colorama import Fore, Back, Style
    print(Fore.RED + "红色文字")
    print(Back.YELLOW + Fore.MAGENTA + "黄色背景,紫色文字")
    print(Style.RESET_ALL)
    print("默认颜色")

if __name__ == "__main__":
    # oneSubject()
    # twoSubject()
    # threeSubject()
    # fourSubject()
    pass