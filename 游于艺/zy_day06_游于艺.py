# -*- coding: UTF-8 -*-
# @ Author : 游于艺
# @ QQ : 58012007
# @ File: zy_day06_游于艺.py.py
# @ Time: 2018/1/26 23:10

"""
# 1.用while和for两种循环求计算1+3+5.....+45+47+49的值
"""
print("********while循环方式实现********")
result = 0
num = 1
while num < 50:
    result += num
    num += 2
print(result)

print("********for循环方式实现********")
result = 0
for num in range(1,50,2):
    result += num
print(result)

"""
# 2.代码实现斐波那契数列(比如前30个项)
"""
fblist = []
num = 0
i, j = 0, 1
while num < 30:
    fblist.append(i)
    i, j = j, i+j
    num += 1
print(fblist)
print(len(fblist))

#方法2
fibon_list = []
for i in range(0,30):
    if i < 2:
        fibon_list.append(1)
    else:
        fibon_list.append(fibon_list[i-1] + fibon_list[i-2])
print(fibon_list)


# fblist = [0,1]
# for n in range(1,29):
#     item = fblist[len(fblist)-1] + fblist[len(fblist)+1]
#     fblist.append(item)
#     pass
# print(fblist)
# print(len(fblist))