# -*- coding: UTF-8 -*-
# @ Author : 游于艺
# @ QQ : 58012007
# @ File: zy_day31_游于艺.py
# @ Time: 2018/4/11 10:30

"""
1.请写出一段Python代码实现删除一个list里面的重复元素
"""
a_list = [1,2,3,1,2,3,4]
# 方法一
b_list = set(a_list)
print(list(b_list))
# 方法二
def qc(alist):
    b_list =[]
    for item in alist:
        if item not in b_list:
            b_list.append(item)
    return b_list
print(qc(a_list))

"""
2.如何用Python来进行查询和替换一个文本字符串？
"""
a_str = "my name is xion,30 years old!"
import re
p = re.compile('\d+')
r = p.sub('fuck',a_str)
print(r)

"""
3.a=1, b=2, 不用中间变量交换a和b的值
"""
a=1
b=2
b,a = a,b
print(a,b)
"""
4.写一个函数, 输入一个字符串, 返回倒序排列的结果:?如: string_reverse(‘abcdef’), 返回: ‘fedcba’5种方法的比较:
1. 简单的步长为-1, 即字符串的翻转;
2. 交换前后字母的位置;
3. 递归的方式, 每次输出一个字符;
4. 双端队列, 使用extendleft()函数;
5. 使用for循环, 从左至右输出;
"""
def string_reverse(str,stype):
    if stype==1:#翻转排序
        return str[::-1]
        # str_list.reverse()
        # return "".join(str_list)
    if stype == 2:#交换前后字母位置
        temp = str[1:-1:1]
        return str[-1]+temp+str[0]
    if stype == 3:#递归输出
        sc(str, i)
    if stype == 4:#双端队列
        from collections import deque
        str_list = list(str)
        A = deque(str_list)
        A.extendleft(["1","2","3","4","5","6"])
        return "".join(A)
    if stype == 5:#for循环输出
        for_sc(str)
i=0
def sc(str,i):
    if i<len(str):
        ts = str[i]
        print(ts)
        i+=1
        sc(str, i)
def for_sc(str):
    for item in str:
        print(item)
str = 'abcdef'
print(string_reverse(str,1))    #倒序
print(string_reverse(str,2))    #交换首尾
string_reverse(str,3)   #递归输出
print(string_reverse(str,4))    #双端队列
string_reverse(str,5)    #for循环输出
"""
5.请用自己的算法, 按升序合并如下两个list, 并去除重复的元素
合并链表, 递归的快速排序, 去重;
"""
list_1 = [1,2,3,4,5]
list_2 = [2,3,4,7,8,6,9,0]
# list_1.extend(list_2)
# print(list_1)
list_add = list_1+list_2
print(list_add)
# list_3 = set(list_1)
# print(list_3)
new_list = list(set(list_add))
print(new_list)