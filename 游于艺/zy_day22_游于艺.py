# -*- coding: UTF-8 -*-
# @ Author : 游于艺
# @ QQ : 58012007
# @ File: zy_day22_游于艺.py
# @ Time: 2018/3/17 11:29

import re
"""
1.使用正则表达式匹配电话号码：0713-xxxxxx(湖南省座机号码)
"""
p = re.compile("0731-\d{7,8}")
print(p.search("0731-6790580").group())
print(p.findall("0731-89905800"))
"""
2.区号中可以包含()或者-,而且是可选的,就是说你写的正则表达式可以匹配
   800-555-1212,555-1212,(800)555-1212
"""
# p = re.compile('\d+-\d+-\d+|\d+-\d+|\(\d+\)\d+-\d+')
p = re.compile('(?:\d{3}-|\(\d{3}\))?\d{3}-\d{4}')  #方法二
print(p.findall("800-555-1212"))
print(p.findall("555-1212"))
print(p.findall("(800)555-1212"))
"""
3.选作题:
  实现一个爬虫代码
"""
import requests

url = 'http://www.nipic.com/photo/renwu/mingxing/index.html'
response = requests.get(url)
html = response.text
img_urls = re.findall(r'http://img\d+\.nipic.com/file/\d+/\w+\.jpg',html)
i=0
for img_url in img_urls:
    img_response = requests.get(img_url)
    print(img_url)
    i+=1

print("已抓取{0}个图片路径".format(i))