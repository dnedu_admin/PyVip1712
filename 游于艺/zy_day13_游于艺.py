# -*- coding: UTF-8 -*-
# @ Author : 游于艺
# @ QQ : 58012007
# @ File: zy_13_游于艺.py
# @ Time: 2018/2/24 12:11

"""
动手完成:
1.自己写代码对比2.x深度优先和3.x广度优先的继承顺序
"""
# class A():
#     def run(self):
#         print("A")
# class B():
#     def run(self):
#         print("B")
# class C(A,B):
#    pass
# class D(A,B):
#     # pass
#     def run(self):
#         print("D")
# class E(C,D):
#     pass
# e = E()
# e.run()
# # py2.x 输出A，py3.x输出D
"""
2.写一个类MyClass,使用__slots__属性,定义属性(id,name),创建该类实例后,动态附加属性age,然后执行代码看看.
"""
# class MyClass():
#     __slots__ = ('id','name')
# m = MyClass()
# m.name = "游于艺"  # 可以
# # m.age = 23  # 报错
"""
3.使用type创建一个类,属性name,age,sex,方法sleep,run并创建此类的实例对象
"""
# import time
# def sleep(self):
#     print("休眠5秒")
#     time.sleep(5)
#     self.run()
# def run(self):
#     print("开始执行！")
# Girl = type("Girl",(),{"name":"游于艺","age":20,"sex":"女","sleep":sleep,"run":run})
# g = Girl()
# g.sleep()
"""
4.写例子完成属性包装,包括获取属性,设置属性,删除属性
"""
class Person():
    @property
    def sex(self):
        return self._sex
    @sex.setter
    def sex(self,sex):
        if not isinstance(sex, str):
            raise ValueError('必须为字符串')
        if sex!="男" and sex!="女":
            raise ValueError("性别类型不正确")
        self._sex = sex
    @sex.deleter
    def sex(self):
        del self._sex
    @sex.getter
    def sex(self):
        return "性别是{0}".format(self._sex)

p = Person()
# p.sex = 20
# p.sex = "Cao"
p.sex = "男"
print(p.sex)
# del p.sex
# print(p.sex)