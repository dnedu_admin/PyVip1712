# -*- coding: UTF-8 -*-
# @ Author : 游于艺
# @ QQ : 58012007
# @ File: zy_day03_游于艺.py.py
# @ Time: 2018/1/21 12:51

print("\n",r"【疑问】，为什么在print开始就出现\n 输出前面会有空格,如此行。",end="\n\n")


"""
1. 格式化显示浮点数12.78，要求保留3位小数，输出占20字符，空位用0补齐
"""
num1 = 12.78
print("%020.3f" %(num1))

"""
2. 将字符串”动脑学院”转换为bytes类型。
"""
str1 = '动脑学院'
print("\n",str1,"转bytes类型为1：",str1.encode('utf-8'),sep='')
print("转bytes类型为2：",bytes(str1,encoding='utf-8'))

"""
3. 将以下3个字符串合并为一个字符串，字符串之间用3个_分割
	hello  动脑  pythonvip       
	合并为“hello___动脑___pythonvip”
"""
str2 = 'hello  动脑  pythonvip'
print("\n","___".join(str2.split(" "*2)),sep='')

"""
4. 删除字符串  “    你好，动脑eric     ”首尾的空格符号
"""
str3 = '    你好，动脑eric     '
print("\n",str3.strip(" "),sep='',end="\n\n")

"""
5. 用户信息存在如下所示字符串中，包含信息依次为  姓名   学号  年龄，
不同信息之间的空格数不确定，请提取用户信息分别保存到 xxx_name,xxx_num, xxx_age。
eric=“    Eric   dnpy_001     28”        
提取后 eric_name=“Eric”eric_num=”dnpy_001”,eric_age=”28”
zhangsan = “ 张三         dnpy_100    22     ”
"""
eric='    Eric   dnpy_001     28'
zhangsan = ' 张三         dnpy_100    22     '

def getList(strs):
    str_list = strs.split(" ")
    str_new_list = []
    for i in range(len(str_list)):
        if str_list[i]!="":
            str_new_list.append(str_list[i])
    return str_new_list
    #print('eric_name=“{0}”，eric_num=“{1}”,eric_age=“{2}”'.format(str_new_list[0],str_new_list[1],str_new_list[2]))

eric_list = getList(eric)
zhangsan_list = getList(zhangsan)
print('eric_name=“{0}”，eric_num=“{1}”，eric_age=“{2}”'.format(eric_list[0],eric_list[1],eric_list[2]))
print('zhangsan_name=“{0}”，zhangsan_num=“{1}”，zhangsan_age=“{2}”'.format(zhangsan_list[0],zhangsan_list[1],zhangsan_list[2]))