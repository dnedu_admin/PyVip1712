# -*- coding: UTF-8 -*-
# @ Author : 游于艺
# @ QQ : 58012007
# @ File: zy_day09_游于艺.py
# @ Time: 2018/2/22 10:00
"""
1.
a_list = [1, 2, 3, 2, 2]
删除a_list中所有的2
"""
a_list = [1, 2, 3, 2, 2]
# a_list_set = set(a_list)
# a_list_set.remove(2)
# a_list = list(a_list_set)
# print(a_list)

# b_list = []
# for item in a_list:
#     if item != 2:
#         b_list.append(item)
# print(b_list)

# while 2 in a_list:
#     a_list.remove(2)
# print(a_list)


"""
2.
用内置函数compile创建一个文件xx.py, 在文件里写你的名字, 然后用eval函数读取文件中内容, 并打印输出到控制台
"""
# code = compile("open('xx.py','a',encoding='utf-8').write('我系游于艺')","xx.py","exec")
# exec(code)
print(eval("open('xx.py','r',encoding='utf-8').read()"))
"""
3.
写一个猜数字的游戏, 给5次机会, 每次随机生成一个整数, 然后由控制台输入一个
数字, 比较大小.大了提示猜大了, 小了提示猜小了.猜对了提示恭喜你, 猜对了.
退出程序, 如果猜错了, 一共给5次机会, 5
次机会用完程序退出.
"""
import random
num = random.randint(0,10)
print(num)
count = 0
while True:
    input_num = input("请输入一个0-10之间整数！")
    # print(input_num.isdigit())
    if input_num.isdigit():
        input_num = int(input_num)
        if input_num > num:
            print("输入数字大了！")
        elif input_num < num:
            print("输入数字小了！")
        elif input_num == num:
            print("完全正确，你真棒！")
            break
        count += 1
        if count < 5:
            print("你还有%d次机会" %(5-count))
        else:
            print("你已用完5次机会咯，再会！")
            break
    else:
        print("请输入0-10整数")