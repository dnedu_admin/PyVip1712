# -*- coding: UTF-8 -*-
# @ Author : 游于艺
# @ QQ : 58012007
# @ File: zy_day07_游于艺.py.py
# @ Time: 2018/1/31 11:59

"""1.简述普通参数、默认参数、关键字参数、动态收集参数的区别,理解为主
函数中的普通参数，没有指定默认值，参数可以是字符串、数字类型、布尔类型等，调用函数时必须传入参数值，否则报错 如：def func(x,y)
默认参数，定义函数时指定了默认值，调用函数时可以不传入参数则以默认值代替，传入参数则以传入值为准，如：def func(x,y=10)
关键字参数：定义函数时用普通参数，调用时指定参数名和值，可以改变参数顺序，如：func(y=20,x=10)
动态收集参数：如元组类型或字典类型参数，可以在外部定义元组或字典参数，调用函数时元组用 *元组，或**字典 来解包参数


2.def foo(x, y, z, *args, **kw):
    sum = x + y + z
    print(sum)
    for i in args:
        print(i)
    print(kw)
    for j in kw.items():
        print(j)
a_tuple = (1,2,3)     此处参数修改为2个看看会怎么样?
b_dict = {'name': 'jim', 'age': 28, 'add': '上海'}
foo(*a_tuple, **b_dict)    分析这里会怎么样?
"""
def foo(x, y, z, *args, **kw):
    sum = x + y + z
    print(sum)
    print(args)
    for i in args:
        print(i)
    print(kw)
    for j in kw.items():
        print(j)
a_tuple = (1,2,3)
b_dict = {'name': 'jim', 'age': 28, 'add': '上海'}
foo(*a_tuple, **b_dict) #前面参数x，y，z将元组解包后直接传入，第四个参数元组就只传入了个空元组了。如果a_tuple修改成只有两个值，那么原来普通参数z就没有值，程序就会报错
#第一次输出：6，a_tuple元组解包后变为xyz参数 相加，
#不会输出print(i)的值，然后再输出kw的值，最后输出kw里面每个项的元组对象
"""


3.题目:执行分析下代码
    def func(a, b, c=9, *args, **kw):
         print('a =', a, 'b =', b, 'c =', c, 'args =', args, 'kw =', kw)
    func(1,2)
    func(1,2,3)
    func(1,2,3,4)
    func(1,2,3,4,5)
    func(1,2,3,4,5,6,name='jim')
    func(1,2,3,4,5,6,name='tom',age=22)
    扩展: 如果把你的函数也定义成  def  get_sum(*args,**kw):pass
    你的函数可以接受多少参数?
"""
def get_sum(*args,**kw):
    print("args=", args, "kw=", kw)
    pass
get_sum(1,2,3,4,"b=",3,'a=',5,'b=',6)#可以接收N个参数，传入的参数都会放到元组中
"""
4.写一个函数函数，计算传入字符串中的数字、字母、空格和其他的个数分别为多少?
"""
def func(sobj):
    number = letter = space = other = 0
    for i in sobj:
        if i.isdigit():#数字
            number += 1
        elif i.isalpha():#字母
            letter += 1
        elif i.isspace():#空格
            space += 1
        else:
            other += 1
    print("number=",number , "letter=" , letter , "space=",space , "other=",other)

func("1,2,  a5 ?,but")