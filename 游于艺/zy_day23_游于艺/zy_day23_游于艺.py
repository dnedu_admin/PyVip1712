# -*- coding: UTF-8 -*-
# @ Author : 游于艺
# @ QQ : 58012007
# @ File: zy_day23_游于艺.py
# @ Time: 2018/3/21 17:12
from datetime import datetime,timedelta
"""
作业:
1.datetime,获取你当前系统时间三天前的时间戳;并把当前时间戳增加
 899999,然后转换为日期时间对象,把日期时间并按照%Y-%m-%d %H:%M:%S
 输入打印这个日期对象,并获取date属性和time属性,最后判断当前日期时间
 对象是星期几;
"""
datetime1 = datetime.now()-timedelta(days=3,hours=24)
datetimeStamp1 = datetime1.timestamp()
datetimeStamp2 = datetimeStamp1+899999
datetime2 = datetime.fromtimestamp(datetimeStamp2)
print(datetime2)
datetime3 = datetime.strftime(datetime2,'%Y-%m-%d %H:%M:%S')
print(datetime3)
datetime4 = datetime.strptime(datetime3.replace("-","/"),'%Y/%m/%d %H:%M:%S')   #转日期对象
print("date",datetime4.date())
print("time",datetime4.time())
def get_weekday(num):
    if num==0:
        return "星期一"
    elif num==1:
        return "星期二"
    elif num==2:
        return "星期三"
    elif num==3:
        return "星期四"
    elif num==4:
        return "星期五"
    elif num==5:
        return "星期六"
    elif num==6:
        return "星期日"
print(get_weekday(datetime4.weekday()))
"""
2.使用os模块线获取当前工作目录的路径,并打印当前目录下所有目录和文件,
获取当前脚本的绝对路径, 分割当前脚本文件的目录和文件在tuple中并判断
当前文件所在的目录下存不存在文件game.py,如果不存在则创建该文件.
并往该文件里写入数据信息,然后判断当前目录下是否存在目录
/wow/temp/,如果不存在则创建,最后把目录名称改为:/wow/map/
"""
import os
from os import path
print(os.getcwd())  #目录的路径
print(path.abspath(r'./'))  #方法二
print(os.listdir('.'))  #所有目录和文件
print(path.abspath(r'./zy_day23_游于艺.py'))   #获取当前脚本的绝对路径
print(path.split(r'./zy_day23_游于艺.py'))     #('.', 'zy_day23_游于艺.py')
print(path.isfile(r'./game .py'))   #False
if path.isfile(r'./game .py')==False:
    with open("game .py","w+",encoding="utf-8") as f:
        f.write("成功创建文件game.py")
print(path.exists(r'./wow/temp/'))
if path.exists(r'./wow/temp/')==False:
    os.makedirs('./wow/temp/')
if path.exists(r'./wow/map/')==False:
    os.rename('./wow/temp/','./wow/map/')

"""
3.写函数，检查获取传入列表或元组对象的所有奇数位索引对应的元素，并将其作为新的列表返回给调用者 

"""
# def getG(list):
#     num = 1
#     newlist = []
#     for item in list:
#         if num%2 != 0:
#             newlist.append(item)
#         num += 1
#     return newlist

def getG(content):
    if isinstance(content,(list,tuple)):
        newlist = []
        for index,item in enumerate(content,1):
            if index%2 != 0:
                newlist.append(item)

        return newlist
    else:
        print("数据类型不正确！")


# list_1 = [x for x in range(20)]
list_1 = (0,1,2,3,4,5,6,7,8,9,10)

list_2 = getG(list_1)
print("【原列表】",list_1)
print("【奇数列表】",list_2)