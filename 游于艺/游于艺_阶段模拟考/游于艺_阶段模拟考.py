# -*- coding: UTF-8 -*-
# @ Author : 游于艺
# @ QQ : 58012007
# @ File: 游于艺_阶段模拟考.py.py
# @ Time: 2018/4/26 15:35

"""
一、简答题
1. 列表list_1 = [[1,2,3,4,5],[{“name”:”张三”,”age”:28},{“name”:”James”:”age:30}]] ,获取James的年龄
"""
list_1 = [[1,2,3,4,5],[{"name":"张三","age":28},{"name":"James","age":30}]]
print("James的年龄：",list_1[1][1]["age"])
"""
2. 名词解释: 可迭代对象、迭代器、生成器、装饰器
【可迭代对象】
可迭代对象一般都可用for循环遍历元素，也就是能用for循环的对象都可称为可迭代对象。
字符串，列表，元组，集合，字典等都是可迭代对象
isinstance方法可以判断对象类型
"""
from collections import Iterable
a_dict = {"a":1}
print(isinstance(a_dict, Iterable))
"""
【迭代器】
迭代器是实现了__next__()方法的对象，可以通过next函数每次取出迭代器中的每个元素，迭代器只能前进不能后退。使用迭代器不要求事先准备好整个迭代过程中的所有元素。
迭代器仅仅在迭代到某个元素时才计算该元素，而在这之前或之后元素可以不存在或者被销毁。因此迭代器适合遍历一些数量巨大甚至无限的序列。
【生成器】
生成器可以理解为用于生成列表、元组等可迭代对象的机器。生成器比列表更加节省内存空间，必要时，生成器可以按照你的需要去生成列表。
通过yield方法可以定义无限能力的生成器，可以生成任意长度的列表。和函数的定义类似，把return语句改为yield语句。
gen = (x for x in range(5))  # 注意这里是()，而不是[]
for i in gen:
    print i
def make_generator(n):
    while n>2:
        yield n
        n -= 1
for num in make_generator(5):
     print(num)
3. 名词解释：面向对象编程中“类”中有几种方法？分别解释各种方法的用途。
class Person():
    def __init__(self, name, age):
        print('数据开始初始化')
        self.name = name
        self.age = age

    def __new__(cls, *args,**kwargs):
        print('开始创建实例')
        return super(Person, cls).__new__(cls)

    def __str__(self):
        return self.name

    def __del__(self):
        print('开始销毁实例')
        del self
"""

"""
4. 输出以下代码运行结果，解释变量作用域LEGB.
首先，是local,先查找函数内部 
然后，是enclosing，再查找函数内部与嵌入函数之间（是指在函数内部再次定义一个函数） 
其次，是global，查找全局 
最后，是build-in，内置作用域
遵循LEGB原则：以 L –> E –> G –>B 的规则查找，即：在局部找不到，便会去局部外的局部找（例如闭包），再找不到就会去全局找，再者去内建中找。
"""
x = 2   #global
z = 3   #global
def func_outer():
    y = 9
    x = 0   #enclosing
    global z
    print("z",z)
    def func_inner():
        nonlocal x  #使用上层x，x = 2
        print("x", x)
        x += 5  #local
        z = 6
        print("func_inner",max(x,z),max(x,y))
    z += 3
    x += 2
    print(x,y,z)
    return func_inner
func_outer()()
"""
5. 解释什么是GIL(全局解释器锁)
这玩意其实是Python解析器CPython用来做多线程控制和调度的全局锁，Python还有一些别的解析器，比如JPython, Pypy等，其中JPython就没有GIL。但是CPython现在已经成为Python的实际标准，各种Linux和其它OS默认集成的Python, 以及从官方网站上下载的Python，还有各种相关的工具如pip等都在使用CPython，所以GIL问题自然也就成为了Python语言的 问题了。
GIL的问题其实是由于近十几年来应用程序和操作系统逐步从多任务单核心演进到多任务多核心导致的 , 在一个古老的单核CPU上调度多个线程任务，大家相互共享一个全局锁，谁在CPU执行，谁就占有这把锁，直到因为IO或者Timer Tick到期让出CPU，没有在执行的线程就安静的等待着这把锁（除了等待之外，他们应该也无事可做）
"""

"""
6. 什么是线程、协程、进程
线程：有时被称为轻量级进程(Lightweight Process，LWP），是程序执行流的最小单元。在python中，threading模块提供线程的功能。通过它，我们可以轻易的在进程中创建多个线程。
进程：在python中multiprocess模块提供了Process类，实现进程相关的功能。
协程：线程和进程的操作是由程序触发系统接口，最后的执行者是系统，它本质上是操作系统提供的功能。而协程的操作则是程序员指定的，在python中通过yield，人为的实现并发处理。对于多线程应用，CPU通过切片的方式来切换线程间的执行，线程切换时需要耗时。协程，则只使用一个线程，分解一个线程成为多个“微线程”，在一个线程中规定某个代码块的执行顺序。
"""

"""
7. 什么时候该用多线程、什么情况该用多进程
"""

"""
8. 多个进程间如何通信？（注意多进程知识点回顾！！！）
"""