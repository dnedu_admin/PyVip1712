__author__ = 'Dragon'
# 爬取url = 'http://tieba.baidu.com/p/2460150866'前三页所有的图片
import requests
from urllib.request import urlretrieve # 保存资源
import re
# <img pic_type="0" class="BDE_Image" src="https://imgsa.baidu.com/forum/w%3D580/sign=aaf243e4d009b3deebbfe460fcbe6cd3/0713b31bb051f8197aa4e7c6dbb44aed2f73e7bf.jpg" pic_ext="jpeg" height="350" width="560">
def get_code(url):  # 参数:网址
    try:
        page = requests.get(url)
        return page.text
    except Exception as e:
        print(e)

# 第三步: 获取源代码里面图片
def get_image(code):   # 参数就是源代码
    try:
        # 使用正则匹配图片链接地址
        reg = r'src="(.+?\.jpg)" pic_ext'  # 规则
        image = re.compile(reg)
        image_list = re.findall(image, code)  # 所有图片的链接地址
        # 图片名字
        num = 1
        # 遍历列表获取每一个连接地址
        for url in image_list:
            # 拿到每一个图片的链接地址,下载
            urlretrieve(url, './image/' + '%s.jpg' % num)  # 下载,保存
            num += 1
    except Exception as e:    # 代码走了异常
        print(e)

if __name__=='__main__':
    # 获取源代码
    for i in range(1, 4):
        url = 'http://tieba.baidu.com/p/2460150866?pn={}'.format(i)
    html = get_code(url)
    # 获取图片
    get_image(html)
    print('图片处理完毕')
