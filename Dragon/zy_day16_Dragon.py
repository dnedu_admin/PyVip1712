__author__ = 'Dragon'
# 1.写一个函数计算1 + 3 + 5 +…+97 + 99的结果再写一个装饰器函数, 对其装饰, 在运算之前, 新建一个文件然后结算结果, 最后把计算的结果写入到文件里
# def file_object(func):
#     def file_inner(*args,**kwargs):
#         with open('nex.txt','w+',encoding='utf-8') as file:
#             z = func(*args,**kwargs)
#             file.write(str(z))
#         return z
#     return file_inner
#
# @file_object
# def get_add(nums):
#     result = 0
#     for n in nums:
#         result += n
#     return result
# result = get_add(range(1,100))
# print(result)

# 2.写一个函数计算传入进来参数的平方, 并将结果.写一个带参数的装饰器, 将装饰器参数传入到被包装的函数,计算并输入结果
def get_result(num):
    def middle(func):
        def inner(*args,**kwargs):
            return func(num)
        return inner
    return middle


@get_result(10)
def get_square(num):
    if not isinstance(num,int):
        raise TypeError('你传入的不是数字!')
    else:
        return pow(num,2)

result = get_square(10)
print(result)