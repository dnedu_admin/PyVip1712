__author__ = 'Dragon'
# ====================================  第一题   ====================================
# 1.在当前项目下创建目录abc,并在abc目录下,使用uuid产生唯一码并写入到文件
#   uuid.py中,并拷贝整个目录abc,新目录名为app,并将app目录下文件拷贝,新
#   文件名为:new.py文件
# import uuid
# import shutil
# with open('./abc/uuid.py','w',encoding='utf-8') as file_uuid:
#     u1 = uuid.uuid1()
#     file_uuid.write(str(u1))

# shutil.copytree('abc','app')
# shutil.move('./app/uuid.py','./app/new.py')


# ====================================  第二题   ====================================

import base64
# 创建本地文件’one.txt’, 在里写入部分数据(‘今天天气不错, 我们去动物园n玩吧’
# n从1 - 9), 使用b64encode对每一行数据编码, 然后写入到文件oen.txt中.每写
# 完一行之后写入换行符.
# with open('one.txt','wb') as file_object:
#     for n in range(1,10):
#         content = ('今天天气不错, 我们去动物园%s玩吧\n'%n).encode('utf-8')
#         file_object.write(content)
#
# with open('one.txt','rb+') as file_read:
#     oen_file = open('oen.txt','wb')
#     con_bytes = file_read.read()
#     result = base64.b85encode(con_bytes)
#     oen_file.write(result)
#     oen_file.close()

# 打开刚才创建的文件one.txt, 以读取多行的形式读取文件中的内容, 并把每一行的
# 内容通过b64decode对每一行数据解码, 解码之后每一行数据依然是二进制形式
# 所以, 继续对每一行数据通过utf - 8的形式解码为可以普通字符串, 然后判断每一行
# 字符串是否包含’动物园6’的子字符串, 如果包含, 先输出换行符, 然后则打印输出
# 当前行的字符串, 最后创建一个文件,’two.txt’, 并把帅选之后的数据写入到这个新
# 文件中.
# with open('oen.txt','rb+') as file_object:
#     two_file = open('two.txt','w',encoding='utf-8')
#     base_con = file_object.read()
#     oen_bytes = base64.b85decode(base_con)
#     oen_str = oen_bytes.decode('utf-8')
#     two_file.write(oen_str)
#     two_file.close()



# ====================================  第三题   ====================================

# 3.准备一张.jpg图片,比如:mm.jpg,读取图片数据并通过b85encode加密之后写入到新文件mm.txt文件中,
# 然后读取mm.txt数据并解密之后然后写入到mmm.jpg文件中
# with open('mm.png','rb') as file_object:
#     mm_file = open('mm.txt','wb')
#     mmmjpg_file = open('mmm.jpg','wb')
#     img_content = file_object.read()
#     result = base64.b85encode(img_content)
#     mm_file.write(result)
#     mmmjpg_file.write(img_content)
#     mm_file.close()
#     mmmjpg_file.close()