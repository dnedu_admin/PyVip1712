# coding utf-8
# Author: Dragon
# 1.简述普通参数、默认参数、关键字参数、动态收集参数的区别,理解为主
# 普通参数：由实参和形参组成
# 默认参数： 形参上默认的值，如果实参有其对应的值，会把默认值覆盖掉
# 关键字参数：在实参上指点值，参数顺序更加灵活
# 动态收集参数：形参用*arg表示为元组，**arg表示为字典

# 第二题
# def foo(x, y, z, *args, **kw):
#     sum = x + y + z
#     print(sum)
#     for i in args:
#         print(i)
#     print(kw)
#     for j in kw.items():
#         print(j)
# a_tuple = (1,2)     #此处参数修改为2个看看会怎么样?  答：正常运行
# b_dict = {'name': 'jim', 'age': 28, 'add': '上海'}
# foo(*a_tuple, **b_dict)    #分析这里会怎么样? 答：报错,x,y,z没有对应的实参

# 3.题目:执行分析下代码
# def func(a, b, c=9, *args, **kw):
#     print('a =', a, 'b =', b, 'c =', c, 'args =', args, 'kw =', kw)
#
# func(1, 2)
# func(1, 2, 3)
# func(1, 2, 3, 4)
# func(1, 2, 3, 4, 5)
# func(1, 2, 3, 4, 5, 6, name='jim')
# func(1, 2, 3, 4, 5, 6, name='tom', age=22)

# 扩展: 如果把你的函数也定义成
# def get_sum(*args, **kw):
#     print(args)
#     print(kw)
# get_sum(1,2,3,4,5,name='Dragon')
# 你的函数可以接受多少参数?
# 答：只要是元组或者字典则可以接受无数个参数

# 4.写一个函数函数，计算传入字符串中的数字、字母、空格和其他的个数分别为多少?
def test(*params, exp):
    print('参数长度是：', len(params))
    print('第二个参数是：', params[1])
test(1,'wdqw',' ',1.5,exp=1)