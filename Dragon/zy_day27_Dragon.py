__author__ = 'Dragon'
import unittest
# 1.写一个函数,然后写单元测试
# def add_result(num):
#     if isinstance(num,(int)):
#         return num + 10
#     else:
#         raise TypeError('传入的参数有误，请重新输入')
#
# class Fun_test(unittest.TestCase):
#     def setUP(self):
#         pass
#
#     def tearDown(self):
#         pass
#
#     # 测试单元函数
#     def testadd_result(self):
#         self.assertEqual(add_result(10),20,'测试失败')
#
# if __name__=='__main__':
#     unittest.main()

# 2.自定义一个类,完成单元测试
class Student():
    def __init__(self,name):
        self.name = name

    def playgame(self):
        print('%s在打游戏你来不来'%self.name)

# 单元测试
class MyclassTest(unittest.TestCase):
    # 实例化对象
    def setUp(self):
        self.student = Student('tom')

    def testplaygame(self):
        self.student.playgame()

    def tearDown(self):
        self.student = None

if __name__=='__main__':
    unittest.main()