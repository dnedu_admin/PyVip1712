__author__ = 'Dragon'

# from functools import reduce

# 1.filter判断[0,False,True,5,{},(2,3)]对应bool值
# print(tuple(filter(bool,[0,False,True,5,{},(2,3)])))

# 2.map实现1-10中所有奇数项的三次方,打印输出结果
# print(list(map(lambda x:x**3,range(1,11,2))))


# 3.reduce实现1-100所有偶数项的和
# print(reduce(lambda x,y:x+y,range(1,101,2)))

# 2.用递归函数实现斐波拉契数列
number = int(input('请输入一个数字: '))
def func(num):
    if num == 1 or num == 0:
        return num
    else:
        return func(num-1) + func(num-2)


for i in range(number):
    print(func(i),end=' ')

