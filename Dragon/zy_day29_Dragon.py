__author__ = 'Dragon'
# 1.函数缓存的使用
# from functools import lru_cache
# from time import time
# @lru_cache(maxsize=10,typed=False)
# def fib(n):
#     if n < 2:
#         return 1
#     else:
#         return fib(n-1) + fib(n-2)
#
# def get_time(num):
#     start_time = time()
#     [fib(n) for n in range(num)]
#     end_time = time()
#     print(end_time - start_time)
# get_time(10000)


# 2.性能分析器的使用
# 第一种
# import cProfile
# def fib(num):
#     if num < 2:
#         return num
#     else:
#         return fib(num - 1) + fib(num - 2)
# cProfile.run('fib(10)')

# 第二种
from line_profiler import LineProfiler
# from time import time
# def fib(n):
#     if n < 2:
#         return n
#     return fib(n -1) + fib(n - 2)
#
# def count_time(num):
#     t1 = time()
#     [fib(n) for n in range(num)]
#     t2 = time()
#     print(t2 - t1)
# 首先构造一个LineProfiler对象
# prof = LineProfiler(count_time)
# 打开开关
# prof.enable()
# 执行函数
# count_time(10)
# 关闭开关
# prof.disable()
# 查看信息：
# prof.print_stats()


# 3.内存分析器的使用
# from memory_profiler import profile
# @profile
# def run_time():
#     x = pow(9999,99999)
#     y = pow(99999,888880)
#     del x
#     del y
# run_time()