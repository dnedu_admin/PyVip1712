__author__ = 'Dragon'
# 使用装饰器增强完成1-999999所有奇数和计算10次

# 1.使用普通方法
import time
from threading import Thread
from multiprocessing import Pool,cpu_count,Process
from multiprocessing.dummy import Pool as threadpool

def timer(func):
    def time_inner(*args,**kwargs):
        start_time = time.time()
        func(*args,**kwargs)
        end_time = time.time()
        print('程序消耗的时间为：%s 秒'%(end_time-start_time))
    return time_inner

@timer
def add_num(num):
    for i in range(10):
        sum(range(1,num,2))

def my_num(num):
    for i in range(10):
        sum(range(1,num,2))



# 2.使用多线程
# @timer
# def my_thread(num):
#     for i in range(10):
#         thread = Thread(target=my_num,args=(num,))
#     thread.start()
#     thread.join()



# 3.使用多进程
# @timer
# def my_process(num):
#     for i in range(10):
#         p = Process(target=my_num,args=(num,))
#     p.start()
#     p.join()





# 4.使用线程池完成计算
# @timer
# def thread_pool(num):
#     thpool = threadpool(cpu_count())
#     for i in range(10):
#         thpool.apply_async(my_num,args=(num,))
#     thpool.close()
#     thpool.join()


# 5.采用进程池完成计算
# @timer
# def process_pool(num):
#     pools = Pool(cpu_count())
#     for i in range(10):
#         pools.apply_async(my_num,(num,))
#     pools.close()
#     pools.join()


if __name__=='__main__':
    add_num(999999)
    # my_thread(999999)
    # my_process(999999)
    # thread_pool(999999)
    # process_pool(999999)
