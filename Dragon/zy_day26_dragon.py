__author__ = 'Dragon'
import hashlib
# 1. PKCS5(选择一种hash算法)加密算法加密你的中文名字,写出代码
# name = hashlib.md5()
# name.update('Dragon'.encode('utf-8'))
# print(name.hexdigest())

# 2. 创建文件test.py,写入一些数据,并计算该文件的md5值
# with open('test.py','w',encoding='utf-8') as f1:
#     f1.write('print("I am Dragon")')
# with open('test.py','r',encoding='utf-8') as f2:
#     content = f2.read()
#     m = hashlib.md5()
#     m.update(content.encode('utf-8'))
#     print(m.hexdigest())

# 3. 遵循上下文管理协议,自己编写一个类,去创建一个上下文管理器,然后自己使用看看
#    方式1:普通方式
# class Myclass():
#     def __init__(self,my_list):
#         self.my_list = my_list
#
#     # 实现两个核心方法
#     def __enter__(self):
#         print('开始执行代码')
#         self.my_list.append('Dragon')
#         return self
#
#     def __exit__(self,exc_type,exc_val,exc_tb):
#         if exc_type:
#             print(exc_val,exc_tb)
#         else:
#             print('程序执行正常')
#
#     def run(self):
#         for num in self.my_list:
#             print(num)
#
# with Myclass([1,2,3,4,5]) as r:
#     r.run()

#    方式2:使用contextmanager
# from contextlib import contextmanager
# class Myclass():
#     def __init__(self,my_list):
#         self.my_list = my_list
#
#     def work(self):
#         for n in self.my_list:
#             print(n)
#
# @contextmanager
# def mk_manager(list):
#     print('开始处理')
#     my_list = Myclass(list)
#     yield my_list
#     print('程序执行完毕')
#
# with mk_manager(['a','b','c','d','e']) as mk:
#     mk.work()



# 4.使用colorama模块使用多颜色输出
# from colorama import Fore,Back,Style
# print(Fore.LIGHTBLUE_EX + Back.GREEN + 'Hi I am Dragon')
# print(Back.CYAN + '我是新加坡人')
# print(Fore.LIGHTYELLOW_EX + '我是美国人')
# print(Style.RESET_ALL)
# print('我是中国人')