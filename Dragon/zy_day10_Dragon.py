# coding utf-8
# author: Dragon
# 1.自定义一个异常类, 当list内元素长度超过10的时候抛出异常
# my_list = [1,2,3,4,5,6,7,8,9,10,11]
# class Lengtherror(Exception):
#     def __init__(self,message):
#         self.message = message
#     def __str__(self):
#         return self.message
#
# def if_info():
#     try:
#         if len(my_list)>10:
#             raise Lengtherror('list长度已经超过10了，大哥！')
#     except Exception as e:
#         print(e)
# if_info()

# 2.思考如果对于多种不同的代码异常情况都要处理, 又该如何去处理, 自己写一个小例子
# try:
#     print(name)
#     print('我被执行了')
# except (ZeroDivisionError,IOError,NameError,SyntaxError,ValueError,Exception) as e:
#     print('发生了异常！')
#     print(e)
# 答：一般处理多种不同代码的异常可以交给常规错误的基类Exception

# 3.try-except和try-finally有什么不同,写例子理解区别
# try:
#     x = 1/0
#     print('我被执行了')
# except Exception as e:
#     print(e)
#     print('程序异常我才执行')
# finally:
#     print('不管程序是否异常我都执行')

# 4.写函数，检查传入字典的每一个value的长度，如果大于2，那么仅仅保留前两个长度的内容，并将新内容返回给调用者
dic = {"k1": "v1v1", "k2": [11, 22, 33]}

def fun(**args):
    values_list = []  # 用于存放values
    keys_list = []  # 用于存放keys
    for attr in args.values():
        if len(attr) > 2:
            new_attr = attr[:2]
            values_list.append(new_attr)
    # print(values_list)
    for key in args.keys():
        keys_list.append(key)
    # print(keys_list)
    print(dict(zip(keys_list, values_list)))  # 把两个列表拼成一个新的字典

dic = {"k1": "v1v1", "k2": [11, 22, 33]}
fun(**dic)
