__author__ = 'Dragon'
import re
# 1.识别下面字符串:’ben’,’hit’或者’ hut’
p = re.compile('^ben{1}\Shit{1}')
m = p.match('ben,hit')
print(m.group())

# 2.匹配用一个空格分隔的任意一对单词,比如你的姓 名
print(re.findall('D\sr\sa\sg\so\sn','D r a g o n'))

# 3.匹配用一个逗号和一个空格分开的一个单词和一个字母
print(re.findall('new|\sA','new, A'))

# 4.匹配简单的以’www’开头,以’com’结尾的web域名,比如:www.baidu.com
print(re.findall('^www.baidu.com$','www.baidu.com'))