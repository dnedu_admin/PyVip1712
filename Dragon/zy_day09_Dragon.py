# coding utf-8
# Author: Dragon
# 1.a_list = [1,2,3,2,2] 删除a_list中所有的2
# a_list = [1, 2, 3, 2, 2]
#
# def fun(list):
#     new_list = []
#     for i in a_list:
#         if i == 2:
#             continue
#         new_list.append(i)
#     print(new_list)
#
# fun(a_list)

# 2.用内置函数compile创建一个文件xx.py, 在文件里写你的名字, 然后用eval函数读取文件中内容, 并打印输出到控制台
# code = compile('print("Dragon")','compile.py','eval')
# eval(code)

# 3.写一个猜数字的游戏,给5次机会,每次随机生成一个整数,然后由控制台输入一个
# 数字,比较大小.大了提示猜大了,小了提示猜小了.猜对了提示恭喜你,猜对了.
# 退出程序,如果猜错了,一共给5次机会,5次机会用完程序退出.
import random
i = 1
while i <= 5:
    # 随机生成一个数字
    number = random.randint(1, 100)
    print(number)
    # 获取用户输入的数字
    message = int(input('请输入一个数字：'))
    if message == number:
        print('恭喜你答对了！~~~')
        break
    else:
        if message < number:
            print('很遗憾！你的数字小了！~~~')
        else:
            print('很遗憾！你的数字大了')
    i += 1
