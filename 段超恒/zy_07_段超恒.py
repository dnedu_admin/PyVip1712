# coding:utf-8
'''
 @time: 18/1/25
'''
#1.简述普通参数、默认参数、关键字参数、动态收集参数的区别,理解为主
'''
普通参数：有顺序，传递的参数个数需对应
默认参数: 当有参数传递时替换默认参数，无参数传递则使用默认参数
关键字参数：参数顺序可以颠倒
动态收集参数: 传入多个参数时可以将参数，存储到元组或字典中
'''

#2.
def foo(x, y, z, *args, **kw):
    sum = x + y + z
    print(sum)
    for i in args:
        print(i)
    print(kw)
    for j in kw.items():
        print(j)
a_tuple = (1,2,3)     #此处参数修改为2个看看会怎么样?   报错
b_dict = {'name': 'jim', 'age': 28, 'add': '上海'}
foo(*a_tuple, **b_dict)    #分析这里会怎么样?   将第一个参数解包后为三个参数传给x,y,z，将第二个参数解包传给kw
print(*a_tuple)

#3.题目:执行分析下代码
def func(a, b, c=9, *args, **kw):
    print('a =', a, 'b =', b, 'c =', c, 'args =', args, 'kw =', kw)
func(1,2)# 将两个参数传给a,b。c使用默认参数，args，kw没有参数传递，默认为空
func(1,2,3)# 替换默认参数 9 为 3
func(1,2,3,4)  # 第4个参数被args接收
func(1,2,3,4,5)# 第4、5个参数被args接收
func(1,2,3,4,5,6,name='jim') # 第7个参数被kw接收、
func(1,2,3,4,5,6,name='tom',age=22) # 第8个参数也被kw接收
#扩展: 如果把你的函数也定义成  def  get_sum(*args,**kw):pass
#你的函数可以接受多少参数?    # 可以接收无限多个

#4
def string(*a_list):
    num = 0
    num1 = 0
    num2 = 0
    num3 = 0
    for i in a_list:
        #print i
        if ("z">=i>="a"):
            num =num+1
        elif ("9">=i>="0"):
            num1=num1+1
        elif(i==" "):
            num2=num2+1
        else:
            num3=num3+1
    print ("字母：%d "%num,"数字: %d "%num1,"空格：%d"%num2,"其他: %d "%num3)
a_list ="wer13434 34f234 234t24 rg23 ;'"
string(*a_list)

