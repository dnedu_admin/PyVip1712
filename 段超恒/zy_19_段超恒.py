# coding:utf-8
'''
   Author:段超恒
   Time: 18/3/11
   Email:296008606@qq.cm
'''
'''
使用装饰器增强完成1-999999所有奇数和计算10次
   1.使用普通方法
   2.使用多线程
   3.使用多进程
   4.采用线程池完成计算
   5.采用进程池完成计算
'''

def a_sum(func):
    def inner():
        print("$"*20)
        i=func()
        print("$" * 20)
        return i
    return inner
@a_sum
def js_sum(num):
    for i in range(10):
        a=sum(range(1,10000,2))
        return a

def j_sum():
    a=sum(range(1,10000,2))
    print(a)
# 多线程
from threading import Thread
@a_sum
def a_thread():
    thread_list=[Thread(target=j_sum,args=())for num in range(10)]
    for thr in thread_list:
        thr.start()
        thr.join()
# 多进程
from multiprocessing import Process
@a_sum
def a_process():
    thread_list=[Process(target=j_sum,args=())for i in range(10)]
    print(thread_list)
    for thr in thread_list:
        thr.start()
        thr.join()



from multiprocessing.dummy import Pool as threadpool #线程对象
# 开启10个子线程池
@a_sum
def c10_threadpool():
    pool = threadpool(cpu_count())
    for num in range(10):
        pool.apply_async(j_sum, args=())  # 异步
    pool.close()  # 关闭进程池
    pool.join()  # 主进程等待进程池进程完成
# 进程池
from multiprocessing import Pool ,cpu_count#进程对象
@a_sum
def c10_pool():
    pool=Pool(cpu_count())
    for i in range(10):
        pool.apply_async(j_sum,args=())
    pool.close()#关闭进程池，不接收新的进程
    pool.join()

if __name__=="__main__":
    #a=a_process()
    #a_thread()
    # c10_pool()
    c10_threadpool()

