# coding:utf-8
'''
   Author:段超恒
   Time: 18/3/15
   Email:296008606@qq.cm
'''
'''
1.识别下面字符串:’ben’,’hit’或者’ hut’
2.匹配用一个空格分隔的任意一对单词,比如你的姓 名
3.匹配用一个逗号和一个空格分开的一个单词和一个字母
4.匹配简单的以’www’开头,以’com’结尾的web域名,比如:www.baidu.com
'''
import  re
print(re.findall("[a-z]{3}","benhithut"))
print(re.findall("[a-z]*\s+[a-z]*","benh ithut"))
print(re.findall("[a-z]*,\s+[a-z]*","print, a"))
print(re.findall("^www.*com$","www.baidu123.com"))

