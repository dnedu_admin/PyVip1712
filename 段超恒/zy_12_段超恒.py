# coding:utf-8
'''
   Author:段超恒
   Time: 18/2/25
   Email:296008606@qq.cm
'''
print("#1 -------------")
#1.自己动手写一个学生类,创建属性名字,年龄,性别,完成
#     __new__,__init__,__str__,__del__，__call__
class Student():
    def __new__(cls, *args, **kwargs):#构造方法
        print("111")
        return  super().__new__(cls)
    def __init__(self,name,age,sex):# 初始化函数
        self.name=name
        self.age=age
        self.sex=sex
        print("222")
    def __call__(self, *args, **kwargs): #实例对象被调用
        print("333")
        return self.age
    def __str__(self):
        return "%s"%self.name
    def __del__(self):
        print("销毁当前对象%s" % self)
p0=Student("ben",33,"男")
print(p0.sex)
p0()  # 实例对象
print(p0) # str



print("#2 --------------")
#2.创建父类Person,属性name,函数eat,run,创建子类Student,学生类
 # 继承Person,属性name,age,id,函数eat,study,全局定义一个函数
 # get_name,传入一个Person类的实例参数,分别传入Person和Student
 # 两个类的实例对象进行调用.

class Person():
    name="father"
    def eat(self):
        print("吃")
    def run(self):
        print("奔跑")
class Student(Person):
    def __init__(self,name,age,id):
        self.name=name
        self.age=age
        self.id=id
    def eat(self):
        print("chi")
    def study(self):
        print("xuexi")
def get_name(person):  # 全局函数
    print(person.name)
def get_eat(person):
    person.eat()

p=Person()
p1=Student("ben",20,111)
get_name(p)
get_eat(p)

print("#3 -----------------")

#3.用代码完成python多态例子和鸭子模型

#多态 -- 发生继承关系
class Father():
    def sleep(self):
        print("father在睡觉")
class Son(Father):
    def sleep(self):
        print("son在睡觉")
    def play(self):
        print("son玩")
def get_sleep(self):
    self.sleep()

p4=Father()
p5=Son()
get_sleep(p4)
get_sleep(p5)

#鸭子模型
class Father():
    def sleep(self):
        print("father在睡觉")
class Son():#没有继承关系
    def sleep(self):
        print("son在睡觉")
    def play(self):
        print("son玩")
def get_sleep(self):
    self.sleep()

p4=Father()
p5=Son()
get_sleep(p4)
get_sleep(p5)