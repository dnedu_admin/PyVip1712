# coding:utf-8
'''
   Author:段超恒
   Time: 18/3/6
   Email:296008606@qq.cm
'''
'''
1.使用pickle,json(注意:两个模块)
把字典a_dict = [1,2,3,[4,5,6]]
序列化与反序列化,序列化,保存到文件名为serialize.txt文件里面,然后大开,并读取数据
反序列化为python对象加载到内存,并打印输出
尝试序列化/反序列非文件中,直接操作
'''

def  a_pickle():
    # 序列化
    import pickle
    a_dict = [1, 2, 3, [4, 5, 6]]
    file = open("serialize.txt", "wb+")
    pickle.dump(a_dict, file, 0)
    file.close()
    # 反序列化
    file1= open("serialize.txt","rb+")
    print(pickle.load(file1))
    file1.close()

def a_json():
    # 序列化
    import json
    a_dict = [1, 2, 3, [4, 5, 6]]
    file=open("serialize1.txt", "w+") #字串格式
    json.dump(a_dict,file)
    file.close()
    # 反序列化
    file1=open("serialize1.txt", "r+")
    print(json.load(file1))
    file1.close()

def a_test():
    pass

'''
2.自己写代码对比深浅拷贝的区别,写完代码然后看看运行结果,然后分析原因
   比如：a_list=[1,2,3,4,[8,9,0]]
'''
from copy import copy,deepcopy
# 浅拷贝
a_list=[1,2,3,4,[8,9,0]]
b_list=a_list
c_list=copy(a_list)
a_list[4][1]=80
print(a_list)
print(c_list)

# 深拷贝
e_list=[5,6,7,8,[9,10,11]]
f_list=e_list
g_list=deepcopy(e_list)
e_list[4][1]=90
print(e_list)
print(g_list)

# 浅拷贝只拷贝了最外层，内层修改了也会跟着修改
# 深拷贝内外层全部拷贝


'''
3.分别写代码完成内存缓冲区文本数据和二进制数据的读/写操作
'''
#stringio,byteio
from io import StringIO,BytesIO
def string():
    f=StringIO()
    f.write("去玩儿群若")
    print(f.getvalue())
def bytes():
    f=BytesIO()
    f.write("去是的".encode("utf-8")) #二进制编码
    print(f.getvalue().decode("utf-8")) #解码

if __name__ == "__main__":

    a_pickle()
    a_json()
    string()
    bytes()
    a_test()