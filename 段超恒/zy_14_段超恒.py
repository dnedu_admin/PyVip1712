# coding:utf-8
'''
   Author:段超恒
   Time: 18/2/27
   Email:296008606@qq.cm
'''
from functools import reduce
#1.filter判断[0,False,True,5,{},(2,3)]对应bool值

def a_bool(n):  #返回bool值
     return bool(n)
a_list=[0,False,True,5,{},(2,3)]
f_list=filter(a_bool,a_list)# a_bool 判断函数，a_list 可迭代对象
print(f_list)

#2.map实现1-10中所有奇数项的三次方,打印输出结果

def req(n):
    return pow(n,3)
num=map(req,[1,3,5,7,9])
print(num)
#print(req(2))

#3.reduce实现1-100所有偶数项的和
def add(x,y):
    return x+y
numc=reduce(add,range(2,101,2)) #第一个参数函数，第二个可迭代对象
print(numc)

#for i in range(2,101,2):
#    print(i)

#4.用递归函数实现斐波拉契数列


def item(num):
    if num == 0 :
        res = 0
    elif num == 1:
        res = 1
    else:
        res = item ( num - 1) + item (num -2)
    return res
print(item(9))
