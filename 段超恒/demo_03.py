# coding:utf-8
'''
 @time: 18/1/19
'''
#
# print(abs(-3))
# a=3
# b=11
# print(b/a)
# print(b%a)
# print("wo si 'sk '")
# b='a'
# # print(int(b))
# a="中文"
# bytes_a = a.encode("utf-8")
# print(bytes_a,type(bytes_a))
#
# utf8_str = bytes_a.decode("utf-8")
# print(utf8_str)
#
# print(len(a))
# # gbk_str = bytes_a.decode("gbk")

# a = r"\\"
#
# print(a)
# b= "{0}{1}{0}".format("hello","word")
# print(b)


# 1. 格式化显示浮点数12.78，要求保留3位小数，输出占20字符，空位用0补齐
num1 = 12.74
print("第1题:   ","%020.4f"%num1)

# 2. 将字符串”动脑学院”转换为bytes类型。
st1 = "动脑学院"
bytes_a = st1.encode("utf-8")
print("第2题:   ",type(bytes_a))
# 3. 将以下3个字符串合并为一个字符串，字符串之间用3个_分割
lis=["hellp","word","python"]
str ="___"
print("第3题:   ",str.join(lis))
# 4. 删除字符串  “    你好，动脑eric     ”首尾的空格符号
str = "     你好，动脑eric    "
print("第4题:   ",str.strip(' '))
"""
5. 用户信息存在如下所n示字符串中，包含信息依次为  姓名   学号  年龄，
不同信息之间的空格数不确定，请提取用户信息分别保存到 xxx_name,xxx_num, xxx_age。
eric=“    Eric   dnpy_001     28”        
提取后 eric_name=“Eric”eric_num=”dnpy_001”,eric_age=”28”
zhangsan = “ 张三         dnpy_100    22     ”
"""
def xinxi(eric1):
    # print(eric.partition("dnpy_001"))
    #eric1=eric.partition("dnpy_001") # 分割字符串
    lists="xxx_name,xxx_num,xxx_age"
    eric2=[]
    for i in eric1: # 删除字串左边和右边空格
        eric2.append(i.strip(' '))
        #print(eric2)
    lists_new=lists.replace("xxx",eric2[0]) # 替换名字
    lists_new=lists_new.split(",") # 将字符串切片存入lists_new
    #print(lists_new)
    for j in range(0,3):
        str="{0[%d]}"%j
        #print(str)
        print("第5题:   ",str.format(lists_new),"=",eric2[j])

#提取eric的信息
eric = "   Eric   dnpy_001     28"
eric1=eric.partition("dnpy_001")
xinxi(eric1)
# 提取张三的信息
zhangsan = " 张三         dnpy_100    22     "
zhangsan1=zhangsan.partition("dnpy_100")
xinxi(zhangsan1)
