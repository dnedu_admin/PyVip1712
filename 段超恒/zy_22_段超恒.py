# coding:utf-8
'''
   Author:段超恒
   Time: 18/3/18
   Email:296008606@qq.cm
'''
import re
'''
1.
使用正则表达式匹配电话号码：0713 - xxxxxx(湖南省座机号码)
'''
print(re.findall("0713\s+-\s+\d{6}","0713 - 567656"))

'''
2.
区号中可以包含()或者 -, 而且是可选的, 就是说你写的正则表达式可以匹配
800 - 555 - 1212, 555 - 1212, (800)555 - 1212
'''
#print(re.findall("\(*\d{0,3}\)*\s*-*\s*555 - 1212","800 - 555 - 1212"))
a_list=('800 - 555 - 1212', '555 - 1212', '(800)555 - 1212')
for i in a_list:
    n=re.findall("\(*\d{0,3}\)*\s*-*\s*555 - 1212",i)
    print(n)
'''
3.选作题:
  实现一个爬虫代码
'''

import urllib
import urllib.request

import re
import os

page = 1
x=0
for i in range(2,5):
    url = 'http://www.qiubaichengren.com/%d.html'%i

    user_agent = 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6'
    headers = { 'User-Agent' : user_agent }
    request = urllib.request.Request(url,headers = headers)
    response = urllib.request.urlopen(request)

    html = response.read().decode('gbk')
    #print (html)
    pattern = re.compile('http://.*.sinaimg.cn.*?jpg')
    content = html
    items = re.findall(pattern,content)

    del items[len(items)-1]
    for img in items :
        print (img)
        if not os.path.exists("/Users/chduan/Pictures/photos/p3"):
           os.mkdir("/Users/chduan/Pictures/photos/p3")
        urllib.request.urlretrieve(img, '/Users/chduan/Pictures/photos/p3/%s.jpg' %x)
        x+=1
