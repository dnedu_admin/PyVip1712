# coding:utf-8
'''
 @time: 18/1/27
'''

# 1.用while和for两种循环求计算1+3+5.....+45+47+49的值
i=1
sun=0
while i<=49:
    if i%2!=0:
        sun=sun+i
    i+=1
print(sun)

sun1=0
for i in range(0,50):
    if i%2 !=0:
        sun1=sun1+i
print(sun1)

f1 = 1
f2 = 1
for i in range(0,15):
    print (f1)
    print (f2)

    f1 = f1 + f2
    f2 = f1 + f2
    