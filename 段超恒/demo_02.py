
# coding:utf-8
# 我的第一个python程序
import random
a=2034
print(hex(a)) # 16进制
print(oct(a)) # 8进制
print(bin(a)) # 2进制

# 求一个100-300之间随机数的平方根
n = random.randint(100,300)
print(pow(n,2))
# 求大于一个1到20之间的随机浮点数的最小整数
p=random.uniform(1,20)
print(int(p))
# 磁盘大小
num = 12106450611211
size = num / pow(1024,3)
print(round(size,2))
# 使用input函数,记录键盘输入的内容,打印输出该值的类型, 并转换为数值型。
dc=input()
print(type(dc))
dc=float(dc)  #输入字母报错  input()默认输入就是字符串类型，怎么判断?

#最后一题

num1=input()
num1=int(num1)
if num1>=90:
    print('你的成绩优秀')
elif num1 in range(80,90):
    print('你的成绩良好')
elif num1 in range(60,80):
    print('你的成绩一般')
else:
    print('你的成绩不合格')



