# coding:utf-8
'''
   Author:段超恒
   Time: 18/3/3
   Email:296008606@qq.cm
'''
'''
1.
写一个函数计算1 + 3 + 5 +…+97 + 99
的结果
再写一个装饰器函数, 对其装饰, 在运算之前, 新建一个文件
然后结算结果, 最后把计算的结果写入到文件里
'''


def zhushi(snc):
    def writ():
        try:
            with open("a_sum.py","w+",encoding="utf-8") as f:
                print("创建文件")
                f.write(str(snc()))
                print("写数据")
                #f.seek(0)
                #f1=f.read()
                #print("读取")
            #return print(f1)
        except IOError as e:
            print("读写失败")
    return writ

@zhushi
def a_sum():
    return  sum(list(range(1,100,2))) #迭代对象需要强转列表



'''
2.
写一个函数计算传入进来参数的平方, 并将结果.写一个带参数的装饰器,
将装饰器参数传入到被包装的函数, 计算并输入结果
'''

def c_out(y):
    def millder(bpow):
        def inner(x):
            pri=bpow(x)
            return print(pri)
        return inner
        #print(y)
    return millder


@c_out(20)
def b_pow(x):
    return pow(x,2)


if __name__=="__main__":
    a_sum()
    b_pow(4)
