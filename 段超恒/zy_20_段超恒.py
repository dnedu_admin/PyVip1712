# coding:utf-8
'''
   Author:段超恒
   Time: 18/3/13
   Email:296008606@qq.cm
'''
'''
创建两个协程函数对象,分别完成1,3,5…99,   2,4,6…,100
 然后让两个协程函数对象分别交替执行.在每一个协程函数对象的内部先打印输出
 每一个数字,然后执行完一轮之后打印提示信息,已经交替执行完毕一轮
 等所有的协程函数对象执行完毕之后,分析一下数据信息
'''
from types import coroutine
async def jishu():
    for i in range(1,100,2):
        print(i)
        await do_next()


async def oushu():
    for i in range(2,101,2):
        print(i)
        await do_next()
@coroutine
def do_next():
    yield

def run(a_list):
    a_list=list(a_list)
    while a_list:
        for item in a_list:
            try:
                item.send(None)
            except StopIteration as  e:
                print(e)
                a_list.remove(item)

if  __name__=="__main__":
    one=jishu()
    two=oushu()
    c_list=[]
    c_list.append(one)
    c_list.append(two)
    run(c_list)