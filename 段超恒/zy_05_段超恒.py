# coding:utf-8
'''
 @time: 18/1/24
'''
# 1
a_dict={"dog":3,"pig":5}
#添加
a_dict["cat"]=4
print(a_dict)
#删除
a_dict.pop("cat")
print(a_dict)
#更新
a_dict["pig"]=4
print(a_dict)
#清空
a_dict.clear()
print(a_dict)

# 2
num1_set ={1,2,4,6,3,8}
num2_set ={2,3,6,4,7}
num1=num1_set&num2_set 
num2=num1_set|num2_set
num3=num1_set^num2_set
num4=num1_set-num2_set
print(num1,num2,num3,num4)


#3
num_dict = {'a':13,'b':22,'c':18,'d':24}
i=sorted(num_dict.items(),key = lambda x:x[1],reverse = False)
print(dict(i))

