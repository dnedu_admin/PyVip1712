# coding:utf-8
'''
   Author:段超恒
   Time: 18/3/1
   Email:296008606@qq.cm
'''
#1.range(),需要生成下面的列表,需要填入什么参数:
#    1.[3,4,5,6]
#    2.[3,6,9,12,15,18]
#    3.[-20,200,420,640,860]

def a_range():
    print([x for x in range(3,7,1)])
    print([x for x in range(3,19,3)])
    print([x for x in range(-20,870,220)])

#2.列表推导式或者生成器表达式(2选择1)完成1-100之间所有偶数和
def b_range():
    print(sum([x for x in range(1,101)if x%2==0]))

#3.定义一个函数,使用关键字yield创建一个生成器,求1,3,5,7,9各数
#   字的三次方,并把所有的结果放入到list里面打印输入a

def c_yield():
    i = 0
    while i<10:
        if i%2!=0:
            yield i
        i+=1
def d_yield():
    list=[]
    for i in c_yield():
        #print(i)
        list.append(pow(i,3))
    print(list)
# print(type(c_yield()))
if __name__ == "__main__":
    a_range() #t1
    b_range() #t2
    d_yield() #t3