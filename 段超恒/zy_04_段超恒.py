# coding:utf-8
'''
 @time: 18/1/23
'''
#第一题：
num_list = [[1,2],['tom','jim'],(3,4),['ben']]
#1
def a_list():
    num_list.append("kity")
    print("1.1  ",num_list)
#2
def b_list(ben):   # 遍历获取下标 k
    for i in range(0,len(num_list)):
        #print(num_list[i])
        for j in range(0,len(num_list[i])):
            #print (num_list[i][j])
            if num_list[i][j]==ben:
                k=i
    print ("1.2  ",num_list[k])
#3
def c_list(j_name,l_name):
    for i in range(0,len(num_list)):
        for j in range(0,len(num_list[i])):
            #print(num_list[i][j])
            if num_list[i][j]==j_name:
                num_list[i][j]=l_name
                print("1.3  ",num_list)
                break
#5
def d_list(lists):
    num_list[1].append(lists)
    print("1.5  ",num_list)

#6

def e_list():
    print("1.6  ",num_list[-1::-1])

#第二题
numbers = [1,3,5,7,8,25,4,20,29]

print(numbers)
#1.对list所有的元素按从小到大的顺序排序
def f_list(numbers,n_bool):  #n_bool=Ture 降序 fals升序
    numbers.sort(reverse=n_bool)
    print(numbers)

def sum_list(numbers):
    sum=0
    for i in numbers:
        sum=i+sum
    print(sum)
def g_list(numbers):
    numbers.sort()
#第一题：
#1.在’ben’后面添加’kity’
a_list()
#2. 获取包含’ben’的list的元素
b_list("ben")
#3. 把’jim’修改为’lucy’
c_list("jim","lucy")
#4. 尝试修改3为5,看看
#c_list(3,5)
#5.把[6,7]添加到[‘tom’,’jim’]中作为第三个元素
d_list([6,7])
#把num_list切片操作:
e_list()

#第二题：
#1.对list所有的元素按从小到大的顺序排序
f_list(numbers,False)
#2.求list所有元素之和
sum_list(numbers)
#3.将所有元素倒序排列
f_list(numbers,True)