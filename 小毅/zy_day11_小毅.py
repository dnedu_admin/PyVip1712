# coding=utf-8
# author:dn_XiaoYi(QQ:162054033)

#作业1.
# with open('use.txt','w+',encoding='utf-8') as f:
#     str_content = ['tom is boy', '\n', 'you are cool', '\n','今天你怎么还不回来']
#     f.writelines(str_content)
#
# with open('use.txt','r',encoding='utf-8') as f1:
#     for line in f1:
#         print((line.strip()))


#作业2.with创建一个文件homework.txt,尝试多种操作模式.进行写读操作,注意区别
# with open('homework.txt','w+',encoding="utf-8") as f:
#     homework_list = ['第11次作业','\n','人生苦短，我用python','\n','学好python，过好生活']
#     f.write('2018年加油\n')
#     f.writelines(homework_list)
#
# with open('homework.txt','a+',encoding="utf-8") as f1:
#     f1.write('这是我要追加的内容：新年大家快乐！')
#     f1.seek(0)
#     for line in f1:
#         print(line.strip() )


# 作业3.理解文件句柄, 对比read(), readline(), readlines()写一个小例子
# with open('homework.txt','r+',encoding="utf-8") as f:
#     print(f.read())
#read()是读取全部数据

# with open('homework.txt','r+',encoding="utf-8") as f:
#     print(f.readline())
#readline()读取一行数据，可填写参数，写几，读取几

# with open('homework.txt','r+',encoding="utf-8") as f:
#     print(f.readlines())
#readlines()是读取全部数据，返回列表形式

# 作业4.准备一张jpg图片,把图片中数据读书出来,并写入到文件my_img.jpg文件中,操作完毕后尝试打开my_img.jpg文件看图片显示正不正常
with open('baymax.jpg','rb') as p:
    old = p.read()

with open('dabai.jpg','wb') as p1:
    p1.write(old)
