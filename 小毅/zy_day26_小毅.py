# coding=utf-8
# author:dn_XiaoYi(QQ:162054033)
# 1. PKCS#5(选择一种hash算法)加密算法加密你的中文名字,写出代码
# import hashlib,binascii
# def Name(name):
#     if isinstance(name,str):
#         pk = hashlib.pbkdf2_hmac('sha256',b'abc',name.encode('utf-8'),iterations=10,dklen=8)
#         b = binascii.hexlify(pk)
#         print(b)
# Name('xiaoyi')


# 2. 创建文件test.py,写入一些数据,并计算该文件的md5值
# import hashlib
# with open('text.py','w') as f:
#     f.write('hello')
# with open('text.py','rb') as f1:
#     m = hashlib.md5(f1.read())
#     print(m.hexdigest())


# 3. 遵循上下文管理协议,自己编写一个类,去创建一个上下文管理器,然后自己使用看看
#    方式1:普通方式
#    方式2:使用contextmanager
# class Myclass():
#     def __init__(self,a_list):
#         print('初始化数据')
#         self.a_list = a_list
#
#     def __enter__(self):
#         print('执行')
#         return self
#     def __exit__(self, exc_type, exc_val, exc_tb):
#         self.a_list = None
#         if exc_type:
#             print(exc_val)
#         else:
#             print('无异常')
#         print('执行完毕')
#     def work(self):
#         for item in self.a_list:
#             print(item)
#
# with Myclass([1,2,3,4,5]) as f:
#     f.work()
'''方法二'''
# from contextlib import contextmanager
# class Myclass():
#     def __init__(self,a_list):
#         print('初始化数据')
#         self.a_list = a_list
#     def work(self):
#         for item in self.a_list:
#             print(item)
# @contextmanager
# def conter(a_list):
#     print('开始处理')
#     a = Myclass(a_list)
#     yield a
#     print('处理完毕')
#
# with conter([1,2,3,4,5,6]) as e:
#     e.work()



# 4.使用colorama模块使用多颜色输出
# from colorama import Fore,Back,Style
# print(Fore.BLUE + Back.BLACK +'HELLOWORD')
# print(Style.RESET_ALL)
# print(Fore.CYAN + Back.YELLOW + '你好')