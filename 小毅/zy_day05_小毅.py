# coding=utf-8
# author:dn_XiaoYi(QQ:162054033)
# 作业1
#1.1添加
# name_dict = {'name':'ben','age':22,'sex':'男'}
# name_dict['addr'] = '长沙'
# print(name_dict)
#1.2删除
# name_dict = {'name':'ben','age':22,'sex':'男'}
# name_dict.pop('sex')
# print(name_dict)
# 1.3更新
# name_dict = {'name':'ben','age':22,'sex':'男'}
# name1_dict = {'Ht':180,'Wt':180,'addr':'长沙'}
# name_dict.update(name1_dict)
# print(name_dict)
# 1.4清空
# name_dict = {'name':'ben','age':22,'sex':'男'}
# name_dict.clear()
# print(name_dict)

# 作业2
# num1_set = {3,5,1,2,7}
# num2_set = {1, 2, 3, 11}
# print(num1_set&num2_set)
# print(num1_set|num2_set)
# print(num1_set^num2_set)
# print(num1_set-num2_set)

# 作业3
# num_dict = {'a':13,'b':22,'c':18,'d':24}
#     按照dict中value从小到大的顺序排序
# num_dict = {'a':13,'b':22,'c':18,'d':24}
# a_dict = sorted(num_dict.items(),key=lambda item:item[1])
# print(set(a_dict))