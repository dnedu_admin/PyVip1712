# coding=utf-8
# author=dn_小毅(qq.162054033)
# 动脑学院pythonVIP课程
# 1.函数缓存的使用
# from functools import lru_cache
# @lru_cache(maxsize=10,typed=False)
# def my(n):
#     if n < 2:
#         return n
#     return my(n-2) + my(n-1)
# from time import time
# def fun(num):
#     t1 = time()
#     [my(n) for n in range(num)]
#     t2 = time()
#     print(t2-t1)
# fun(40)


# 2.性能分析器的使用
# from line_profiler import LineProfiler
# from time import time
# def fun():
#     a = 0
#     b = 0
#     for i in range(10000):
#         a = a + i *i
#     for i in range(10):
#         b += 1
#     return a +b
#
# lpf = LineProfiler(fun)
# lpf.enable()
# fun()
# lpf.disable()
# lpf.print_stats()

# 3.内存分析器的使用
from memory_profiler import profile
@profile
def my_func():
    a = 'a'* 1024 *1024 *1024
    del a
    a = 'a' * 1024 * 1024
    del a
    a = 'a' *1024
    del a
    print('结束')
if __name__ == '__main__':
    my_func()
