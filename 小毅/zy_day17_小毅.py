# coding=utf-8
# author:dn_XiaoYi(QQ:162054033)

# 1.使用pickle,json(注意:两个模块)
# 把字典a_dict = [1,2,3,[4,5,6]]
# 序列化与反序列化,序列化,保存到文件名为serialize.txt文件里面,然后大开,并读取数据
# 反序列化为python对象加载到内存,并打印输出
# # 尝试序列化/反序列非文件中,直接操作
# import pickle
# a_dict = [1,2,3,[4,5,6]]
# file = open('serialize.txt','wb')
# pickle.dump(a_dict,file,0)
# file.close()
#
# file = open('serialize.txt','rb')
# read = pickle.load(file)
# print(read)
# file.close()


# import json
# a_dict = [1,2,3,[4,5,6]]
# f = open('serialize.txt','w')
# json.dump(a_dict, f,0)
# f.close()
#
# f = open('serialize.txt','r')
# read = json.load(f)
# print(read)
# f.close()


# 2.自己写代码对比深浅拷贝的区别,写完代码然后看看运行结果,然后分析原因比如：a_list=[1,2,3,4,[8,9,0]]
# from copy import copy  # 浅拷贝
#
# a_list = [1, 2, 3, 4, [8, 9, 0]]
# b_list = copy(a_list)
# a_list[4][0] = 22
# print(a_list)
# print(b_list)
# # b列表浅拷贝了a列表，当将a列表内部数据重新赋值，b列表也放生了改变


# from copy import deepcopy #深拷贝
#
# a_list = [1, 2, 3, 4, [8, 9, 0]]
# b_list = deepcopy(a_list)
# a_list[4][0] = 22
# print(a_list)
# print(b_list)
# #b列表深拷贝了a列表，当将a列表内部数据重新赋值，b列表不会改变


# 3.分别写代码完成内存缓冲区文本数据和二进制数据的读/写操作
# from io import StringIO
# f = StringIO()
# f.write('你好python')
# print(f.getvalue())

# from io import BytesIO
# f1 = BytesIO()
# f1.write('你好python'.encode('utf-8'))
# print(f1.getvalue().decode('utf-8'))