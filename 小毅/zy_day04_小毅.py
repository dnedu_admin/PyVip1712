# coding=utf-8
# author:dn_XiaoYi(QQ:162054033)
# 第一题:
# num_list = [[1,2],[‘tom’,’jim’],(3,4),[‘ben’]]
#  1. 在’ben’后面添加’kity’
#  2. 获取包含’ben’的list的元素
#  3. 把’jim’修改为’lucy’
#  4. 尝试修改3为5,看看
#  5. 把[6,7]添加到[‘tom’,’jim’]中作为第三个元素
#  6.把num_list切片操作:
#       num_list[-1::-1]

# num_list = [[1,2],['tom','jim'],(3,4),['ben']]
# num_list[3].append('kity')
# print(num_list)

# 第二小题
# num_list = [[1,2],['tom','jim'],(3,4),['ben']]
# print(num_list[-1])

# 第三小题
# num_list = [[1,2],['tom','jim'],(3,4),['ben']]
# num_list[1][1] = 'lucy'
# print(num_list)

# 第四小题
# num_list = [[1,2],['tom','jim'],(3,4),['ben']]
# num_list[2][0] = 5
# print(num_list)  #元组对象不支持修改

# 第五小题
# num_list = [[1,2],['tom','jim'],(3,4),['ben']]
# num_list[1].append([6.7])
# print(num_list)

# 第六小题
# num_list = [[1,2],['tom','jim'],(3,4),['ben']]
# print(num_list[-1::-1])


# 第二题:
# numbers = [1,3,5,7,8,25,4,20,29];
# 1.对list所有的元素按从小到大的顺序排序
# 2.求list所有元素之和
# 3.将所有元素倒序排列

# 1.
# numbers = [1,3,5,7,8,25,4,20,29]
# numbers.sort()
# print(numbers)

# 2.
# numbers = [1,3,5,7,8,25,4,20,29]
# sum_numbers = sum(numbers)
# print(sum_numbers)

# 3.
# numbers = [1,3,5,7,8,25,4,20,29]
# print(numbers[-1::-1])