# coding=utf-8
# author:dn_XiaoYi(QQ:162054033)
# 1.range(),需要生成下面的列表,需要填入什么参数:
#     1.[3,4,5,6]
#     2.[3,6,9,12,15,18]
#     3.[-20,200,420,640,860]

# 1.1
# print(list(range(3,7)))
# # 1.2
# print(list(range(3,19,3)))
# # 1.3
# print(list(range(-20,900,220)))


# 2.列表推导式或者生成器表达式(2选择1)完成1-100之间所有偶数和
# 方法1.
# print(sum([x for x in range(1,101) if x% 2 == 0]))
#
# # 方法2.
# a_list = (x for x in range(1,101) if x % 2==0)
# b_list = sum(a_list)
# print(b_list)


# 3.定义一个函数,使用关键字yield创建一个生成器,求1,3,5,7,9各数字的三次方,并把所有的结果放入到list里面打印输入
def squ(num):
    while num >0 :
        yield num**3
        num -=2
a = squ(9)
for item in a:
    print(item)