# coding=utf-8
# author:dn_XiaoYi(QQ:162054033)
# 1.datetime,获取你当前系统时间三天前的时间戳;并把当前时间戳增加
#  899999,然后转换为日期时间对象,把日期时间并按照%Y-%m-%d %H:%M:%S
#  输入打印这个日期对象,并获取date属性和time属性,最后判断当前日期时间
#  对象是星期几;
# from datetime import datetime,timedelta
#
# time1 = datetime.now()
# time3 = time1 - timedelta(days=3)
#
# time2 = time3.timestamp() + 899999
# time4 = datetime.fromtimestamp(time2)
# print(time4.strftime('%Y-%m-%d %H:%M:%S'))
# print(time4.date())
# print(time4.time())
# print(datetime.today().weekday()+1)


# 2.使用os模块线获取当前工作目录的路径,并打印当前目录下所有目录和文件,
# 获取当前脚本的绝对路径, 分割当前脚本文件的目录和文件在tuple中并判断
# 当前文件所在的目录下存不存在文件game.py,如果不存在则创建该文件.
# 并往该文件里写入数据信息,然后判断当前目录下是否存在目录
# /wow/temp/,如果不存在则创建,最后把目录名称改为:/wow/map/
import os
# p = os.getcwd()
# print(p)
# print(os.listdir(os.getcwd()))
#
#第二小题
# cata = __file__
# print(cata)
# cata_tuple = os.path.split(cata)
# print(cata_tuple)

#第三小题
# if os.path.exists('./game.py'):
#     print('存在该文件')
# else:
#     with open('game.py','w',encoding= 'utf-8') as f:
#         f.write('写入数据')
# if os.path.exists('./wow/temp/'):
#     print('存在wow和temp文件')
# else:
#     os.makedirs('./wow/temp/')
#     print('创建wow和temp文件')
# if os.path.exists('./wow/map/'):
#     os.rmdir('./wow/map/')
# os.renames('./wow/temp/','./wow/map/')


# 3.写函数，检查获取传入列表或元组对象的所有奇数位索引对应的元素，并将其作为新的列表返回给调用者 
def a_list(seq):
    if (isinstance(seq,(list,tuple))):
        new_list = []
        for index,item in enumerate(seq,1):
            if index % 2 !=0:
                new_list.append(item)
        return new_list
    else:
        print('传入元组或列表')
print(a_list([1,2,3,4,5,6,7,8,9,10]))

