# coding=utf-8
# author=dn_小毅(qq.162054033)
# 动脑学院pythonVIP课程
# 1.基于tcp协议socket套接字编程半双工模式
# import socket
# host = socket.gethostname()
# port = 999
# s = socket.socket()
# s.bind((host,port))
# s.listen(5)
#
# while 1:
#     c,addr = s.accept()
#     while 1:
#         content = c.recv(1024)
#         if len(content) == 0:
#             print('接收到的客户端数据为空')
#         else:
#             print('接收客服端发送来的数据为%s' % content.decode('utf-8'))
#             result = input('请输入要返回给客户端的数据：')
#             c.send(result.encode('utf-8'))
#     c.close()
# s.close()


# 2.socketserver异步非阻塞编程,编写代码
import socketserver
from socketserver import StreamRequestHandler as srh
host = '127.0.1.1'
port = 9999

# 封装绑定的元祖
addr = (host,port)
# 自定义请求处理类
class Server(srh):
    # 处理请求的方法
    def handle(self):
        print('连接到服务器的客服端地址为%s' % str(self.client_address))
        while 1:
            content = self.request.recv(1024)
            if not content:
                break
            print('服务器接收的数据为%s' % content.decode('utf-8'))
            result = input('请输入返回给客户端的数据:')
            self.request.send(result.encode('utf-8'))
print('开始处理请求:')
# 构造一个多线程tcp
server = socketserver.ThreadingTCPServer(addr,Server)
server.serve_forever()