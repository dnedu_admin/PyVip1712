# coding=utf-8
# author:dn_XiaoYi(QQ:162054033)
# 1.在当前项目下创建目录abc,并在abc目录下,使用uuid产生唯一码并写入到文件
#   uuid.py中,并拷贝整个目录abc,新目录名为app,并将app目录下文件拷贝,新
#   文件名为:new.py文件
# import os
# import shutil
# import uuid
# if not os.path.exists('./abc/'):
#     os.mkdir('abc')
# with open('abc/uuid.py','w',encoding= 'utf-8') as f:
#     f.write(str(uuid.uuid1()))
# shutil.copytree('abc','app')
# shutil.copyfile('app/uuid.py','app/new.py')


# 2.创建本地文件’one.txt’,在里写入部分数据(‘今天天气不错,我们去动物园n玩吧’
#   n从1-9),使用b64encode对每一行数据编码,然后写入到文件oen.txt中.每写
#   完一行之后写入换行符.
#   打开刚才创建的文件one.txt,以读取多行的形式读取文件中的内容,并把每一行的
#   内容通过b64decode对每一行数据解码,解码之后每一行数据依然是二进制形式
#   所以,继续对每一行数据通过utf-8的形式解码为可以普通字符串,然后判断每一行
#   字符串是否包含’动物园6’的子字符串,如果包含,先输出换行符,然后则打印输出
#   当前行的字符串,最后创建一个文件,’two.txt’,并把帅选之后的数据写入到这个新
#   文件中.

# import base64
# with open('one.txt','wb') as f:
#     for n in range(1,10):
#         f.write(base64.b64encode(('今天天气不错,我们去动物园%d玩吧' %n).encode('utf-8')))
#         f.write(b'\n')
# print('数据已写入')
#
# with open('one.txt', 'rb') as ff:
#     d = ff.readlines()
# for line in d:
#     c = base64.b64decode(line).decode('utf-8')
#     if '动物园6' in c:
#         with open('two.txt' , 'w' , encoding='utf-8') as f1:
#             f1.write(c)
#             print('已完成')


# 3.准备一张.jpg图片,比如:mm.jpg,读取图片数据并通过b85encode加密之后写入到新文件mm.txt文件中,
#   然后读取mm.txt数据并解密之后然后写入到mmm.jpg文件中
# import base64
# with open('mm.jpg', 'rb') as f1:
#     with open('mm.txt' , 'wb') as f2:
#         f2.write(base64.b85encode(f1.read()))
#
# with open('mm.txt','rb') as m1:
#     with open('mmm.jpg' , 'wb') as m2:
#         m2.write(base64.b85decode(m1.read()))