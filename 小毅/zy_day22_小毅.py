# coding=utf-8
# author:dn_XiaoYi(QQ:162054033)
# 1.使用正则表达式匹配电话号码：0713-xxxxxx(湖南省座机号码)
import re
Phone_number = '0731-12345678'
p = re.compile('0731-\d{8}')
m = p.search(Phone_number)
# if m:
print(m.group())


# 2.区号中可以包含()或者-,而且是可选的,就是说你写的正则表达式可以匹配800-555-1212,555-1212,(800)555-1212
a_str = '800-555-1212,555-1212,(800)555-1212'
p = re.compile('\d{3}-\d{3}-\d{4}|\d{3}-\d{4}|\(\d{3}\)\d{3}-\d{4}')
m2 = p.findall(a_str)
print(m2)