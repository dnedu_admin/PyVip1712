# coding=utf-8
# author:dn_XiaoYi(QQ:162054033)

# 作业1.自己写代码对比2.x深度优先和3.x广度优先的继承顺序
# class A():
#     def run(self):
#         print('A')
# class B():
#     def run(self):
#         print('B')
# class C(A,B):
#     pass
# class D(A,B):
#     def run(self):
#         print('D')
# class E(C,D):
#     pass
# e = E()
# e.run()
# python2.X版本采用深度优先    打印结果‘A’
# python3.X版本采用广度优先，C3算法  打印结果‘D’

# 作业2.写一个类MyClass,使用__slots__属性,定义属性(id,name),创建该类实例后,动态附加属性age,然后执行代码看看.
# class MyClass():
#     __slots__ = ('id','name')
#
# m= MyClass()
# m.age = 30
# print(m.age)

# 作业3.使用type创建一个类,属性name,age,sex,方法sleep,run并创建此类的实例对象
# def run(self):
#     print('奔跑中')
# def sleep(self):
#     print('睡觉中')
# Person = type('Person',(),{'name':'Yi','age':29,'sex':'男','sleep': sleep,'run': run})
# p = Person()
# p.run()
# p.sleep()
# print(p.name,p.age,p.sex)


# 作业4.写例子完成属性包装,包括获取属性,设置属性,删除属性
# class Person():
#     @property
#     def age(self):
#         return  self._age
#     @age.setter
#     def age(self,age):
#         if not isinstance(age, int):
#             raise TypeError('必须为整数')
#         if 0 < age <= 120:
#             self._age = age
#         else:
#             raise ValueError('年龄超出范围')
#     @age.deleter
#     def age(self):
#         del self._age
# p = Person()
# p.age = 30
# print(p.age)