# coding=utf-8
# author=dn_小毅(qq.162054033)
# 动脑学院pythonVIP课程

# 使用装饰器增强完成1-999999所有奇数和计算10次
#    1.使用普通方法
#    2.使用多线程
#    3.使用多进程
#    4.采用线程池完成计算
#    5.采用进程池完成计算

from multiprocessing import Pool,Process,cpu_count
from threading import Thread
from multiprocessing.dummy import Pool as ThreadPool
import time


def count_time(func):
    def inner(num):
        time1 = time.time()
        func(num)
        time2 = time.time()
<<<<<<< HEAD
        print('计算时间为',(func.__name__,str(time2 - time1)))
=======
        print('计算时间为',time2 - time1)
>>>>>>> 45f97f2c0e9e222b546ce52585234845a673f88e
    return inner

# 1.普通方法
@count_time
def fun(num):
    for i in range(10):
        sum(range(1,num,2))

def func(num):
    sum(range(1,num,2))
#2.多线程方法
@count_time
def fun_thread(num):
    thread_list = [Thread(target=func, args=(num,)) for i in range(10)]
    for thread in thread_list:
        thread.start()
        thread.join()


#3.多进程
@count_time
def fun_process(num):
    process_list = [Process(target=func,args=(num,))for i in range(10)]
    for process in process_list:
        process.start()
        process.join()

#4.进程池
@count_time
def process_pool(num):
    pool = Pool(cpu_count())
    for i in range(10):
        pool.apply_async(func,args=(num,))
    pool.close()
    pool.join()

#5.线程池
@count_time
def thread_pool(num):
    pool = ThreadPool(cpu_count())
    for i in range(10):
        pool.apply_async(func,args=(num,))
    pool.close()
    pool.join()


if __name__ == '__main__':
    fun(999999)
    fun_process(999999)
    fun_thread(999999)
    process_pool(999999)
    thread_pool(999999)