# coding=utf-8
# author:dn_XiaoYi(QQ:162054033)
# 爬取url = 'http://tieba.baidu.com/p/2460150866'前三页所有的图片
import requests
from urllib.request import urlretrieve
import re
import os

# url = 'http://tieba.baidu.com/p/2460150866'

def get_html(url):
    try:
        r = requests.get(url)
        return r.text
    except Exception as e:
        print(e)
    return None

def download(html,page):
    try:
        reg = r'src="(.+?\.jpg)" pic_ext'
        img = re.compile(reg)
        imglist = re.findall(img,html)
        num = 1
        for url in imglist:
            urlretrieve(url,'./image/第%d页-%d.jpg' %(page,num))
            num += 1
    except Exception as e:
        print(e)

def flip():
    try:
        for page in range(1,4):
            url_all = url + '?pn=%d' %page
            html = get_html(url_all)
            download(html,page)
    except Exception as e:
        print(e)
if __name__ == '__main__':
    url = 'http://tieba.baidu.com/p/2460150866'
    flip()
    print('下载完毕')






