# coding=utf-8
# author:dn_XiaoYi(QQ:162054033)


# 作业1.自定义一个异常类,当list内元素长度超过10的时候抛出异常
# class MyListError(Exception):
#     def __init__(self,message):
#         self.message = message
#     def __str__(self):
#         return  self.message
#
# try:
#     a_list = []
#     for item in range(1,15):
#         a_list.append(item)
#
#     if len(a_list) > 10:
#         raise MyListError('列表元素过长')
# except MyListError as e:
#     print(e)
# except Exception as e:
#     print(e)

# 作业2.思考如果对于多种不同的代码异常情况都要处理, 又该如何去处理, 自己写一个小例子
# try:
#     f = open('作业.py') #此文件不存在时
#     print('没有异常')
# except Exception as e:
#     print(e)
#     print('出现异常')
#
# try:
#     print(name)
# except NameError as e:
#     print('XiaoYi')

# 3.try-except和try-finally有什么不同,写例子理解区别
# try:
#     y = int('转整数')
# except ValueError as e:      #发生异常走except
#     print(e)
#     print('字符串不能转整数')
# finally:                     #不管发生不发生异常都会执行finally
#     print(666)

# 4.写函数，检查传入字典的每一个value的长度，如果大于2，那么仅仅保留前两个长度的内容，并将新内容返回给调用者
# dict = {“k1”: "v1v1", "k2": [11, 22, 33]}
def fun(**a_dict):
    if isinstance(a_dict, dict):
        for key,value in a_dict.items():
            if isinstance(value,(list,str,tuple)):
                if len(value) > 2:
                    a_dict[key] = value[0:2]
        return a_dict


old_dict = {"k1": "v1v1", "k2": [11, 22, 33]}
new_dict = fun(**old_dict)
print(new_dict)