# coding=utf-8
# author:dn_XiaoYi(QQ:162054033)
# 1.请写出一段Python代码实现删除一个list里面的重复元素
# import random
# a_list = random.randint(0,10)
# b_list = [a_list for i in range(10)]
# print(list(set(b_list)))

# 2.如何用Python来进行查询和替换一个文本字符串？
# a_str = 'My name is xiaoyi'
# print(a_str.find('nihao'))
# print(a_str.replace('xiaoyi','jam'))


# 3.a=1, b=2, 不用中间变量交换a和b的值
# a=1
# b=2
# a,b = b,a
# print(a,b)



# 4.写一个函数, 输入一个字符串, 返回倒序排列的结果:?如: string_reverse(‘abcdef’), 返回: ‘fedcba’
# 5种方法的比较:
# 1. 简单的步长为-1, 即字符串的翻转;
# 2. 交换前后字母的位置;
# 3. 递归的方式, 每次输出一个字符;
# 4. 双端队列, 使用extendleft()函数;
# 5. 使用for循环, 从左至右输出;

# def string_reverse(string):
#     string = string[::-1]
#     print(string)
#     for i in string:
#         print(i)
#
# string_reverse('abcdef')


# 5.请用自己的算法, 按升序合并如下两个list, 并去除重复的元素
# 合并链表, 递归的快速排序, 去重;
a_list = [3,5,7,6,2,4,6,7,6,0,1]
b_list = [1,5,7,3,6,4,1,9]
c_list = list(set(a_list+b_list))
print(c_list)
