# coding=utf-8
# author:dn_XiaoYi(QQ:162054033)

# 1.计算range(1,99999)的总和,采取三种方式,第一:重复10次计算总时间;第二:开启十个子线程去计算;第三:开启十个进程去计算,然后对比耗时
import time
from multiprocessing import Process,Pool
from  threading import Thread,current_thread
#普通计算
# def compute():
#     print("开始计算")
#     time1 = time.time()
#     def num():
#         num = sum(range(1,99999))
#         print(num)
#     for a in range(10):
#         num()
#     print('计算结束')
#     time2 = time.time()
#     end = str(time2 - time1)
#     print('计算时间为',end)
# compute()


def decorator_time(func):
    def inner(num):
        time1 = time.time()
        func(num)
        time2 = time.time()
        print('计算时间为%s' % str(time2 - time1))
    return inner

def total(num):
    print(sum(range(1,num)))

@decorator_time
def pro(num):
    if __name__=='__main__':
        pro_list =[Process(target=total,args=(num,)) for i in range(10)]
        for process in pro_list:
            process.start()
            process.join()
pro(9999)


# @decorator_time
# def thr(num):
#     thr_list = [Thread(target=total,args=(num,))for i in range(10)]
#     for thread in thr_list:
#         thread.start()
#         thread.join()
# thr(999999)

