# coding=utf-8
# author=dn_小毅(qq.162054033)
# 动脑学院pythonVIP课程
# 1.自己动手写一个学生类,创建属性名字,年龄,性别,完成__new__,__init__,__str__,__del__，__call__
# class Person():
#     def __new__(cls, *args, **kwargs):
#         print('__new__方法')
#         return super().__new__(cls)
#     def __init__(self,name, age, sex):
#         self.name = name
#         self.age = age
#         self.sex = sex
#         print('实例对象')
#     def __str__(self):
#         return '__str__方法'
#     def __del__(self):
#         print("销毁对象")
#     def __call__(self, *args, **kwargs):
#         print('__call__方法')
# p1 = Person('xiaoyi',28,'man')
# print(p1)
# p1()

# 作业2.创建父类Person,属性name,函数eat,run,创建子类Student,学生类继承Person,属性name,age,id,函数eat,study,全局定义一个函数
# get_name,传入一个Person类的实例参数,分别传入Person和Student两个类的实例对象进行调用.
# class Person():
#     name = ''
#     def eat(self):
#         print('输出eat1')
#     def run(self):
#         print('输出run')
#
# class Student(Person):
#     age = 0
#     id = 0
#     def eat(self):
#         print('输出eat2')
#     def study(self):
#         print('输出study')
# def get_name(person):
#     person.eat()
#
# p1 = Person()
# s1 = Student()
#
# get_name(p1)
# get_name(s1)

# 作业3.用代码完成python多态例子和鸭子模型
# 多态
# p2 = Person()
# s2 = Student()
# get_name(p2)
# get_name(s2)

# 鸭子形态
class Man():
    def work(self):
        print('男人需要工作')

class Woman():
    def work(self):
        print('女人也需要工作')

def job(person):
    person.work()

XiaoMing = Man()
XiaoHong = Woman()

job(XiaoMing)
job(XiaoHong)