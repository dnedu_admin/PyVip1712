# coding=utf-8
# author=dn_小毅(qq.162054033)
# 动脑学院pythonVIP课程
# import socket
# host = socket.gethostname()
# port = 999
#
# s = socket.socket()
# s.connect((host,port))
#
# while 1:
#     content = input('请输入发送给服务器的数据：')
#     s.send(content.encode('utf-8'))
#
#     result = s.recv(1024)
#     print('接收服务器返回的数据为%s' % result.decode('utf-8'))
#     print('#'*50)
# s.close()


import socket
host = '127.0.1.1'
port = 9999

addr = (host,port)

s = socket.socket()
s.connect(addr)

while 1:
    content = input('请输入发送给服务器的数据为：')
    if not content or content == '退出':
        break
    s.send(content.encode('utf-8'))
    result = s.recv(1024)
    if not result:
        break
    print('服务器返回给客服端的数据为%s' % result.decode('utf_8'))
s.close()