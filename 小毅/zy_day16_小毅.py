# coding=utf-8
# author:dn_XiaoYi(QQ:162054033)
# 1.写一个函数计算1 + 3 + 5 +…+97 + 99的结果,再写一个装饰器函数, 对其装饰, 在运算之前, 新建一个文件然后结算结果,
# 最后把计算的结果写入到文件里
# def outer(func):
#     def inner():
#         file = open('计算结果.py','w',encoding = 'utf-8')
#         num = func()
#         file.write(str(num))
#         file.close()
#     return inner
#
# @outer
# def num():
#     a = sum(range(1,100,2))
#     # print(a)
#     return a
# num()


# 2.写一个函数计算传入进来参数的平方, 并将结果.写一个带参数的装饰器, 将装饰器参数传入到被包装的函数,计算并输入结果
def fun(arg):
    def outer(func):
        def inner(num):
            print('计算之前',arg+1)
            num = func(num)
            print('计算之后',arg+2)
            return num
        return inner
    return outer

@fun(20)
def func(num):
    if isinstance(num,int):
        return pow(num,2)
    else:
        print('必须传入整数')

a = 3
print(func(a))