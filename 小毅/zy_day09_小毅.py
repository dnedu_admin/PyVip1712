# coding=utf-8
# author:dn_XiaoYi(QQ:162054033)

# 作业1.a_list = [1,2,3,2,2]删除a_list中所有的2
# a_list = [1,2,3,2,2]
# new_list = set(a_list)
# new_list.remove(2)
# print(list(new_list))

# 2.用内置函数compile创建一个文件xx.py,在文件里写你的名字,然后用eval函数读取文件中内容,并打印输出到控制台
# code = compile("print('XiaoYi')",'作业','eval')
# print(eval(code))

# 3.写一个猜数字的游戏, 给5次机会, 每次随机生成一个整数, 然后由控制台输入一个
# 数字, 比较大小.大了提示猜大了, 小了提示猜小了.猜对了提示恭喜你, 猜对了.
# 退出程序, 如果猜错了, 一共给5次机会, 5次机会用完程序退出.
import random
num = random.randint(0,20)
print(num)
for n in range(1,6):
    a_num = int(input('请输入一个整数：'))
    if a_num > num:
        print('您猜高了')
    elif a_num < num:
        print('您猜低了')
    elif a_num == num:
        print('恭喜您猜对了')
        continue   #猜对了停止