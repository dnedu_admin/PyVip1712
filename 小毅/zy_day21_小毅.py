# coding=utf-8
# author:dn_XiaoYi(QQ:162054033)
# 1.识别下面字符串:’ben’,’hit’或者’ hut’
import re

p = re.compile('ben|hit|hut')

match = p.findall('benhithut')
if match:
    print(match)

# 2.匹配用一个空格分隔的任意一对单词,比如你的姓 名
print(re.findall('Xiao\sYi','Xiao Yi'))


# 3.匹配用一个逗号和一个空格分开的一个单词和一个字母
print(re.findall('windows\D+x','windows, x'))

# 4.匹配简单的以’www’开头,以’com’结尾的web域名,比如:www.baidu.com
print(re.findall('^www.*com$','www.baidu.com'))