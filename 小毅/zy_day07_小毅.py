# coding=utf-8
# author=dn_小毅(qq.162054033)
# 动脑学院pythonVIP课程
# 作业1.简述普通参数、默认参数、关键字参数、动态收集参数的区别,理解为主
# 普通参数：须以正确的顺序传入函数，调用时的数量必须和声明时的一样。
# 默认参数：调用函数时，如果没有传递参数，则会使用默认参数
# 关键字参数：函数调用使用关键字参数来确定传入的参数值。使用关键字参数允许函数调用时参数的顺序与声明时不一致，因为Python解释器能够用参数名匹配参数值。
# 动态收集参数：* 对应元祖tuple，**对应字典dict

# 作业2.def foo(x, y, z, *args, **kw):
#     sum = x + y + z
#     print(sum)
#     for i in args:
#         print(i)
#     print(kw)
#     for j in kw.items():
#         print(j)
# a_tuple = (1,2,3)     此处参数修改为2个看看会怎么样? 如果此处修改为2个参数会报错，因为位置参数有x,y,z三个参数
# b_dict = {'name': 'jim', 'age': 28, 'add': '上海'}
# foo(*a_tuple, **b_dict)    分析这里会怎么样? a_tuple =(1,2,3)会传递x,y,z，b_dict会传递**kw，这里没有输出。

# 3.题目:执行分析下代码
# def func(a, b, c=9, *args, **kw):
#     print('a =', a, 'b =', b, 'c =', c, 'args =', args, 'kw =', kw)
# func(1, 2)
# func(1, 2, 3)
# func(1, 2, 3, 4)
# func(1, 2, 3, 4, 5)
# func(1, 2, 3, 4, 5, 6, name='jim')
# func(1, 2, 3, 4, 5, 6, name='tom', age=22)
# 扩展: 如果把你的函数也定义成
# def get_sum(*args, **kw): pass
# 你的函数可以接受多少参数?
# def get_sum(*args, **kw):
#     for i in args:
#         print('args:',i)
#     for j in kw.items():
#         print('kw:',j)
# get_sum(1,2,3,4,name='Tom',age=18)
# 可以接受无限参数

# 作业4.写一个函数，计算传入字符串中的数字、字母、空格和其他的个数分别为多少?
# def function(s):
#     digit_nums = 0
#     alpha_nums = 0
#     space_nums = 0
#     else_nums = 0
#     for i in s:
#         if i.isdigit():
#             digit_nums +=1
#         elif i.isalpha():
#             alpha_nums +=1
#         elif i.isspace():
#             space_nums +=1
#         else:
#             else_nums +=1
#     return (digit_nums,alpha_nums,space_nums ,else_nums)
# nums = function(input('请输入一个字符串:'))
# print(nums)