# coding=utf-8
# author:dn_XiaoYi(QQ:162054033)

# 作业1.用while和for两种循环求计算1+3+5.....+45+47+49的值
# num = 1
# sum = 0
# while(num<50):
#     sum += num
#     num += 2
# print(sum)
#
# sum = 0
# for num in range(1,50,2):
#     sum +=num
# print(sum)

# 作业2.代码实现斐波那契数列(比如前30个项)
list = [1,2]
for x in range(1,29):
    item = list[len(list)-2] + list[len(list)-1]
    list.append(item)
print(list)
print(len(list))