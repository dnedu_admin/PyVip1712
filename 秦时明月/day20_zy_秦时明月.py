# _*_ coding:utf-8 _*_


# 创建两个协程函数对象, 分别完成1, 3, 5…99, 2, 4, 6…, 100
# 然后让两个协程函数对象分别交替执行.在每一个协程函数对象的内部先打印输出
# 每一个数字, 然后执行完一轮之后打印提示信息, 已经交替执行完毕一轮
# 等所有的协程函数对象执行完毕之后, 分析一下数据信息

from types import coroutine

async def one():#定义第一个协程函数range(1,100,2)
    for i in range(1,100,2):
        print(i)
        print("&&"*10)
        await next_step()#代码暂停的标记
async def two():#定义第二个协程函数 range(2,101,2)
    for j in range(2, 101, 2):
        print(j)
        await next_step()#代码暂停的标记
@coroutine
def next_step():#开关
    yield


def work(a_list):
    b_list=list(a_list)
    while b_list:
        for item in b_list:
            try:
                item.send(None)
            except StopIteration as e:
                print(e)
                print('已经交替执行完毕一轮')

                b_list.remove(item)


if __name__=="__main__":
    one=one()
    two=two()
    c_list=[]
    c_list.append(one)
    c_list.append(two)

    work(c_list)
