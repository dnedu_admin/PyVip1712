# _*_ coding:utf-8 _*_
import re
# 1.使用正则表达式匹配电话号码：0713-678456
p = re.compile('\d+\-\\d+')
m = p.match('0713-678456')
print(m.group())


# 2.区号中可以包含()或者-,而且是可选的,就是说你写的正则表达式可以匹配
# 800-555-1212,555-1212,(800)555-1212
p = re.compile('\d+\\W\\d+\\W\\d+\,\\d+\\W\\d+\,\(\d+\)\d+\W\\d+')
m = p.match('800-555-1212,555-1212,(800)555-1212')
print(m.group())

# 3.选作题:
# 实现一个爬虫代码
import requests
from lxml import html

url = 'https://movie.douban.com/'
page = requests.Session().get(url)
tree = html.fromstring(page.text)
result = tree.xpath('//td[@class="title"]//a/text()')
print(result)
