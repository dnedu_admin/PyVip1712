# _*_ coding:utf-8 _*_

# 1.用while和for两种循环求计算1+3+5.....+45+47+49的值

def sumb():
    num = 1
    sum = 0
    while num <= 49:
        sum += num
        num += 2
    print(sum)


def _sumb():
    sum = 0
    for i in range(1, 50):
        if i % 2 != 0:
            sum += i
    print(sum)


sumb()
_sumb()


# 2.代码实现斐波那契数列(比如前30个项)
# 1 1 2 3 5 8 13
def fblac():
    time = 0
    a = 1
    b = 1
    _list = []
    _list.append(a)
    _list.append(b)
    while time < 28:
        a, b = b, a + b
        _list.append(b)
        time += 1
    print(_list)
    print(len(_list))


fblac()