# _*_ coding:utf-8 _*_

# 1.写一个函数计算1 + 3 + 5 +…+97 + 99
# 的结果再写一个装饰器函数, 对其装饰, 在运算之前, 新建一个文件
# 然后结算结果, 最后把计算的结果写入到文件里
def outer(fun):
    def inner():
        file = open('111.txt','w',encoding='utf-8')
        result = fun()
        file.write(str(result))
        file.close()
    return inner
@outer
def count():
    return sum(range(1,100,2))
count()

# 2.写一个函数计算传入进来参数的平方, 并将结果.写一个带参数的装饰器,
# 将装饰器参数传入到被包装的函数, 计算并输入结果
def outer(args):
    def outer(fun):
        def inner(*kw):
            receive = fun(args)
            return receive
        return inner
    return outer
@outer(6)
def square(num):
    if isinstance(num,int):
        a = pow(num,2)
        return a
    else:
        print('传入的参数不是整数！')
num = square()

print(num)

# 装饰器 闭包的使用

def outter(func):
    def inner(x,y):
        print('对func 函数引用')
        func(x,y)
    print('call outer')
    return inner
# outter(bar) ->innner
#bar = innner
#bar() inner() bar()
@outter
def bar(x,y):
    print('两参数的求值%d '%(x+y))
bar(1,3)

print(num)

