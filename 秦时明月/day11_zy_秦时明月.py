# _*_ coding:utf-8 _*_

# 第一题:
num_list = [[1, 2], ['tom', 'jim'], (3, 4), ['ben']]

# 1. 在’ben’后面添加’kity’
num_list[-1].append('kity')

#  2. 获取包含’ben’的list的元素z
for item in num_list:
    if 'ben' in item:
        print(item)
        break

# 3. 把’jim’修改为’lucy’
i, j = 0, 0
for item in num_list:
    j = 0
    for k in item:
        if k == 'jim':
            num_list[i][j] = 'lucy'
        j += 1
    i += 1

# 4. 尝试修改3为5,看看
# i, i = 0, 0
# for item in num_list:
#     j = 0
#     for k in item:
#         if k == 3:
#             num_list[i][j] = 5  # TypeError: 'tuple' object does not support item assignment
#         j += 1
#     i += 1

# 5. 把[6,7]添加到[‘tom’,’jim’]中作为第三个元素
t_list = [6, 7]
num_list[1].append(t_list)

# 6.把num_list切片操作:
print(num_list[-1::-1])

# 第二题:
# 1.对list所有的元素按从小到大的顺序排序
numbers = [1, 3, 5, 7, 8, 25, 4, 20, 29];
numbers.sort()
print(numbers)

# 2.求list所有元素之和
sum = 0
for item in numbers:
    sum += item
print(sum)

# 3.将所有元素倒序排列
numbers.sort(reverse=True)
print(numbers)
