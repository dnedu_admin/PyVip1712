# _*_ coding:utf-8 _*_

# 1.
str_content = ['tom is boy', '\n', 'you are cool', '\n', '今天你怎么还不回来']
file = open('use.txt', 'w', encoding='utf-8')
file.writelines(str_content)
file.close()

with open('use.txt', 'r', encoding='utf-8') as f:
    content = f.read()
    print(content)

# 2.
# 写入
# file=open('homework.txt','w',encoding='utf-8')
# file.write('hello python')
# file.close()

# 读取
# file=open('homework.txt','r',encoding='utf-8')
# content=file.read()
# print(content)
# file.close()

# 追加
file = open('homework.txt', 'a', encoding='utf-8')
file.write('hello world')
file.close()

# 3.
f = open('test.txt', 'r', encoding='utf-8')
# print(f.read()) #返回所有数据
# print(f.readline()) #返回一行数据
print(f.readlines())  # 返回一个列表
f.close()

# 4.
img_file = open('img.png', 'rb',)
my_img_file = open('my_img.png', 'wb')
content_file = img_file.read()
my_img_file.write(content_file)
img_file.close()
my_img_file.close()