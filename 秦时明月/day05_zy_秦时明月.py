# _*_ coding:utf-8 _*_
# 1.自己写一个字典:
my_dict = {'name': 'Gemery','age': '18','sex': 'man'}
# 添加
my_dict['addr'] = 'ShenZhen'
# 删除
my_dict.pop('sex')
# 更新，两个字典合并在一起
current_dict = {'job': 'QAQ','height': '163cm'}
my_dict.update(current_dict)
print(my_dict)
# 清空操作
my_dict.clear()

# 2.写两个集合:
num1_set = {0,1,2,3,4,5}
num2_set = {4,5,6,7,8}
# & 交集
print(num1_set & num2_set)  #两个集合的公共部分
# | 并集运算
print(num1_set | num2_set)  #{0, 1, 2, 3, 4, 5, 6, 7, 8}
# ^ 异或运算
print(num1_set ^ num2_set)  #两个集合公共部分之外的元素

# - 差集
print(num1_set - num2_set)  #num1_set公共部分减去了输出剩下的

# 3.整数字典:
num_dict = {'a':13,'b':22,'c':18,'d':24}
# 按照dict中value从小到大的顺序排序
new_dict = sorted(num_dict.items(), key=lambda x:x[1])
print(dict(new_dict))