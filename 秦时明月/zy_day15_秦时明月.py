# 1.range(),需要生成下面的列表,需要填入什么参数:
# 1.[3,4,5,6]
print(list(range(3,7,1)))

# 2.[3,6,9,12,15,18]
print(list(range(3,20,3)))

# 3.[-20,200,420,640,860]
print(list(range(-20,861,220)))


# 2.列表推导式或者生成器表达式(2选择1)完成1-100之间所有偶数和
print(sum([x for x in range(2,101,2)]))

# 3.定义一个函数,使用关键字yield创建一个生成器,求1,3,5,7,9各数字的三次方,并把所有的结果放入到list里面打印输入
def get_num():
    one = 1
    yield pow(one, 3)
    three = 3
    yield pow(three,3)
    five = 3
    yield pow(five,3)
    seven = 7
    yield pow(seven, 3)
    nine = 7
    yield pow(nine, 3)

result = get_num()

num_list = []
for i in result:
    num_list.append(i)

print(num_list)


print(list(map(lambda x: pow(x,3),[1,2,3,4,5,6,7,8,9])));