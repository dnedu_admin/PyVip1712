# _*_ coding:utf-8 _*_
#
import re
# 1.简述普通参数、默认参数、关键字参数、动态收集参数的区别,理解为主
#
def question_01():
    print('''普通参数：一定要根据相应函数参数个数传值，否则报错；
             默认参数：这个可传可不传，不传则有个默认值，传则为传的值；默认参数位置最好在普通参数后面；
             关键字参数：实参的位置可以随时改变，不像普通参数那些一一对应
             动态收集参数：不需要规定要传递参数的个数。* 接受N个参数，转成元组；** 接受N个参数，转成字典
           ''')


def foo(x, y, z, *args, **kw):
    sum = x + y + z
    print(sum)
    for i in args:
        print(i)
    print(kw)
    for j in kw.items():
        print(j)
a_tuple = (1,2,3)    # 此处参数修改为2个看看会怎么样?
b_dict = {'name': 'jim', 'age': 28, 'add': '上海'}
foo(*a_tuple, **b_dict)    #分析这里会怎么样?

print(*a_tuple)
print('''a_tuple = (1,2,3)     此处参数修改为2个看看会怎么样?
          当a_tuple = (1,2)时，foo(*a_tuple,**b_dict)会报错

          foo(*a_tuple, **b_dict) 会先输出 6 因为 1+2+3
          接着输出{'name': 'jim', 'age': 28, 'add': '上海'}，kw的值
          再接着输出('name', 'jim')('age', 28)('add', '上海')
       ''')


# 3.题目:执行分析下代码
def func(a, b, c=9, *args, **kw):
         print('a =', a, 'b =', b, 'c =', c, 'args =', args, 'kw =', kw)
func(1,2)
func(1,2,3)
func(1,2,3,4)
func(1,2,3,4,5)
func(1,2,3,4,5,6,name='jim')
func(1,2,3,4,5,6,name='tom',age=22)
#     扩展: 如果把你的函数也定义成  def  get_sum(*args,**kw):pass
#     你的函数可以接受多少参数?
print('question3 ： 可以输如N个参数')
#
# 4.写一个函数函数，计算传入字符串中的数字、字母、空格和其他的个数分别为多少?

def count_str(str):
    space = number = letter = other = 0
    for i in str:
        if i == ' ':
            space += 1
        elif re.match(r'\d',i):
            number += 1
        elif re.match(r'[a-zA-Z]',i):
            letter += 1
        else:
            other += 1
    return space,number,letter,other