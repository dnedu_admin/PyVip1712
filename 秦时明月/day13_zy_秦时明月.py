# _*_ coding:utf-8 _*_

# 1.自己写代码对比2.x深度优先和3.x广度优先的继承顺序
# class A():
#     def test(self):
#         print('I am A')
#
# class B():
#     def test(self):
#         print('I am B')
#
# class C(A,B):
#     pass
#
# class D(A,B):
#     def test(self):
#         print('I am D')
#
# class E(C,D):
#     pass
#
# e = E()
# e.test()

# 2.写一个类MyClass,使用__slots__属性,定义属性(id,name),创建该类实例后,动态附加属性age,然后执行代码看看
# class MyClass():
#     __slots__ = ('id','name')
#
# myclass = MyClass()
# myclass.age = 100
# print(myclass.age)  # 报错

# 3.使用type创建一个类,属性name,age,sex,方法sleep,run并创建此类的实例对象
# def play(self):
#     print('I am play')
#
# def run(self):
#     print('I am running now')
#
# Person = type('boy',(),{'play': play,'run':run,'name':'Gemery','age':18,'sex':'man'})
# p = Person()
# p.play()
# p.run()
# print(p.name)

# 4.写例子完成属性包装,包括获取属性,设置属性,删除属性
class Person():
    @property
    def age(self):
        return self._age

    @age.setter
    def age(self,age):
        if not isinstance(age,int):
            raise TypeError('年龄必须为数字！')
        if age < 0 or age > 100:
            raise ValueError('你的年龄已经超过正常范围啦！~')
        self._age = age

    @age.deleter
    def age(self):
        del self._age

p = Person()
p.age = 100
print(p.age)
del p.age

