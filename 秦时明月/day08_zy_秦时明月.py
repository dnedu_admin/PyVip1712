# _*_ coding:utf-8 _*_

from multiprocessing.dummy import Pool


# 多进程和多线程（池），装饰器
# 分布式进程
# 全局解释器锁GIL

# 多进程 和多线程的各自优缺点

from multiprocessing import Pool,cpu_count
from multiprocessing.dummy import Pool as threadpool

#属性包装
class Person():
    @property
    def age(self):
        return self._age;
    @age.setter
    def age(self,age):
        if not isinstance(age ,int):
            raise TypeError('必须为整数')
        if age<0 or age>120:
            raise ValueError('年龄超出范围')
        self._age = age
    @age.getter
    def age(self):
        return self._age
p= Person()
p.age = 10.1;
print(p.age)