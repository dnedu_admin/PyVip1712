# _*_ coding:utf-8 _*_

# 1.datetime,获取你当前系统时间三天前的时间戳;并把当前时间戳增加
#  899999,然后转换为日期时间对象,把日期时间并按照%Y-%m-%d %H:%M:%S
#  输入打印这个日期对象,并获取date属性和time属性,最后判断当前日期时间
#  对象是星期几;
from datetime import datetime,timedelta
datetime1 = datetime.now()
datetime2 = datetime1 - timedelta(days=3,)
timestamp1 = datetime2.timestamp() + 899999
timestamp2 = datetime.fromtimestamp(timestamp1)
print(timestamp2)
print(datetime2)
print(timestamp2.weekday())



# 第二题

import os
from os import path
# 使用os模块线获取当前工作目录的路径,并打印当前目录下所有目录和文件
print(os.getcwd())
print(os.listdir('.'))

# 获取当前脚本的绝对路径, 分割当前脚本文件的目录和文件在tuple中并判断
print(path.abspath('./zy_day23_gemery.py'))
print(path.splitext(r'./zy_day23_gemery.py'))

# # 当前文件所在的目录下存不存在文件game.py,如果不存在则创建该文件.
if path.exists('game.py') == True:
    pass
else:
    os.mkdir('game.py')

# 并往该文件里写入数据信息,然后判断当前目录下是否存在目录
print(path.exists(r'./game.py/test.py'))

# /wow/temp/,如果不存在则创建,最后把目录名称改为:/wow/map/
if path.exists('./wow/temp/') == True:
    pass
else:
    os.makedirs('./wow/temp/')
os.renames('./wow/temp','./wow/map')



# 3.写函数，检查获取传入列表或元组对象的所有奇数位索引对应的元素，并将其作为新的列表返回给调用者
def get_obj(a_list):
    new_list = []
    if isinstance(a_list,(list,tuple)):
        for i in range(1,len(a_list),2):
            new_list.append(a_list[i])
        return new_list
    else:
        raise TypeError('对不起，您传入的参数有误')

result = get_obj(['a','b','c','d','e','f','g'])
print(result)
