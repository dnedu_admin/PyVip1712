# _*_ coding:utf-8 _*_

# 1.计算range(1,99999)的总和,采取三种方式,第一:重复10次计算总时间;
# 第二:开启十个子线程去计算;第三:开启十个进程去计算,然后对比耗时
# 重复10次计算总时间
from multiprocessing import Process,Pool
import time
def child_process(name):
    sum(range(1,99999))

if __name__=='__main__':
    process = Process(target=child_process,args=('Gemery',))
    time_start = time.time()
    process.start()
    process.join()
    time_end = time.time()
    print('程序运行10次的总时间为%s' % (time_end - time_start))

# 开启十个子线程去计算
from threading import Thread
def mk_thr():
    print(sum(range(1,99999)))

thread_list = [Thread(target=mk_thr,args=()) for num in range(10)]
start_time = time.time()
for thr in thread_list:
    thr.start()
    thr.join()
end_time = time.time()
print('线程运行消耗的时间为%s'%(end_time-start_time))

# 开启十个进程去计算
from multiprocessing import Pool
import time
def child_process():
    print(sum(range(1,99999)))

if __name__=='__main__':
    pool = Pool(10)
    for i in range(10):
        pool.apply_async(child_process,args=())
    time_start = time.time()
    pool.close()
    pool.join()
    time_end = time.time()
    print('程序运行10次的总时间为%s'%(time_end-time_start))


# 2.将第一题计算十次改为使用线程池去计算,线程池线程个数由你自己电脑cpu逻辑核数决定
def add_num(name):
    print(sum(range(1,99999)))
from multiprocessing.dummy import Pool as Threadpool
pool = Threadpool(20)
pool.map(add_num,'Gemery')
pool.close()
pool.join()