# _*_ coding:utf-8 _*_

import pickle,json
a_dict = [1,2,3,[4,5,6]]
with open('serialize.txt','wb') as my_file:
    pickle.dump(a_dict,my_file)

with open('serialize.txt','rb') as my_file:
    content = pickle.load(my_file)
    print(content)


with open('serialize.txt','w') as my_file:
    json.dump(a_dict,my_file)

with open('serialize.txt','r') as my_file:
    con = json.load(my_file)
    print(con)


# pickle 序列化/反序列非文件中
byte_data = pickle.dumps(a_dict)
print(byte_data)

sequence = pickle.loads(byte_data)
print(sequence)

# json 序列化/反序列非文件中
str_data = json.dumps(a_dict)
print(str_data,type(str_data))

sequence = json.loads(str_data)
print(sequence,type(sequence))



# 2.自己写代码对比深浅拷贝的区别,写完代码然后看看运行结果,然后分析原因比如：a_list=[1,2,3,4,[8,9,0]]
from copy import copy, deepcopy
a_list=[1,2,3,4,[8,9,0]]
b_list = list(a_list)
c_list = a_list.copy()
b_list[4][2] = 10
print(a_list)
print(b_list)
print(c_list)
# 浅拷贝: 只拷贝第一层,第一层以外是共享的，随着其他层的改变而改变

a_list=[1,2,3,4,[8,9,0]]
b_list = list(a_list)
c_list = deepcopy(a_list)
b_list[4][2] = 10
print(a_list)
print(b_list)
print(c_list)
# 深拷贝: 拷贝所有的层，不会因为其他层的改变而改变


# 3.分别写代码完成内存缓冲区文本数据和二进制数据的读/写操作
# 文本数据数据的写
from io import StringIO, BytesIO

my_str = StringIO()
my_str.write('Hello Python\n')
my_str.write('I am from GuangZhou')

# 文本数据数据的读
my_str = StringIO('Hello Python\nI am from GuangZhou')
while 1:
    content = my_str.readline()
    if len(content) != 0:
        print(content.strip())
    else:
        break

# 二进制数据的写
my_bytes = BytesIO()
my_bytes.write('Hello Python\n'.encode('utf-8'))
my_bytes.write('I am studying now!'.encode('utf-8'))
print(my_bytes.getvalue().decode('utf-8'))
print('我是Gemery\n'.encode('utf-8'))

# 二进制数据的读
my_bytesio = BytesIO('Hello Python\nI am studying now!'.encode('utf-8'))
print(my_bytesio.getvalue().decode('utf-8'))