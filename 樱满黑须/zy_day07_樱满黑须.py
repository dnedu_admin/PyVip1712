# coding : utf-8

# 1.简述普通参数、默认参数、关键字参数、动态收集参数的区别,理解为主
# 默认参数：位于函数定义部分。调用时，形参未接收参到数值时，会直接使用默认参数的值
# 关键字参数：位于函数调用部分。调用时，对要赋值的形参，可以无序的，以关键字为参数赋值。
# 动态收集参数：位于函数定义部分。调用时，动态收集散列值，形成(*)元组，(**)字典

# 2.
def foo(x, y, z, *args, **kw):
    sum = x + y + z
    print(sum)
    for i in args:
        print(i)
    print(kw)
    for j in kw.items():
        print(j)


a_tuple = (1, 2, 3)

b_dict = {'name': 'jim', 'age': 28, 'add': '上海'}
foo(*a_tuple, **b_dict)


# a_tuple = (1,2,3)     此处参数修改为2个看看会怎么样?
# 报错，调用时赋值的参数个数应与形参个数一致

# foo(*a_tuple, **b_dict)    分析这里会怎么样
# 元组解包成三个数字，赋值进xyz，*args没有接受数值，**kw接受b_dict数值


# 3.题目:执行分析下代码
def func(a, b, c=9, *args, **kw):
    print('a =', a, 'b =', b, 'c =', c, 'args =', args, 'kw =', kw)


func(1, 2)
func(1, 2, 3)
func(1, 2, 3, 4)
func(1, 2, 3, 4, 5)
func(1, 2, 3, 4, 5, 6, name='jim')
func(1, 2, 3, 4, 5, 6, name='tom', age=22)


# 扩展: 如果把你的函数也定义成  def  get_sum(*args,**kw):pass
# 你的函数可以接受多少参数?
def get_sum(*args, **kw): print(args, kw)


tuple_a = (1, 2, 3)
dict_b = {'name': 'abc', 'age': 20}
get_sum(*tuple_a)
get_sum(**dict_b)
get_sum(*tuple_a, **dict_b)


# 一个解包的元组参数和一个解包的字典参数

# 4.写一个函数函数，计算传入字符串中的数字、字母、空格和其他的个数分别为多少?
def fun_sum(str1):
    if isinstance(str1, str):
        sum_num = 0
        sum_abc = 0
        sum_space = 0
        str1 = list(str1)
        for i in range(0, len(str1)):
            if str1[i].isdigit():
                sum_num += 1
            elif str1[i] == ' ':
                sum_space += 1
            elif str1[i].isalpha():
                sum_abc += 1
        print("字母有：" + str(sum_abc) + "个，数字有：" + str(sum_num) + "个，空格有:" + str(sum_space) + "个")
    else:
        print("输入非字符串")


fun_sum('a  b12c  ')
