# coding : utf-8

# 1.a_list = [1,2,3,2,2]
#    删除a_list中所有的2

a_list = [1, 2, 3, 2, 2]
for i in range(len(a_list) - 1, 0, -1):
    if a_list[i] == 2:
        a_list.pop(i)
print(a_list)

# 2.
# 用内置函数compile创建一个文件xx.py, 在文件里写你的名字, 然后用eval
# 函数读取文件中内容, 并打印输出到控制台

code = compile("print('樱满黑须')", 'xx.py', 'eval')
eval(code)

# 3.写一个猜数字的游戏,给5次机会,每次随机生成一个整数,然后由控制台输入一个
#    数字,比较大小.大了提示猜大了,小了提示猜小了.猜对了提示恭喜你,猜对了.
#     退出程序,如果猜错了,一共给5次机会,5次机会用完程序退出.

import random

print("猜数字游戏开始！\n 系统生成要猜测的数字！")
a = random.randint(1, 10)
print("整数数字已经生成，现在请开始猜数字，共有5次机会")
print(a)
i = 0
while i < 6:
    if i == 5:
        print("5次机会已经用完，您没有猜到数字")
        break

    print("请第%d次输入你猜测的整数数字，并回车确认:" % (i + 1))
    b = input()

    if b.isdigit():
        b = int(b)
        if b == a:
            print("恭喜你,猜对了")
            break
        elif b > a:
            print("您所猜的数字大了")
        else:
            print("您所猜的数字小了")
        i += 1
    else:
        print("您输入的数字不规范，请输入整数数字:")
