# coding ; utf-8

# 1.自定义一个异常类,当list内元素长度超过10的时候抛出异常

class LenError(Exception):
    def __init__(self, length):
        # super(MyError, self).__init__(self)
        self.message = "异常：序列长度上线为10,您输入了" + length + "组字符"

    def __str__(self):
        return self.message


def main():
    try:
        list_a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 11, 12]
        if len(list_a) > 10:
            raise LenError(str(len(list_a)))
    except Exception as e:
        print(e)


if __name__ == '__main__':
    main()

# 2.思考如果对于多种不同的代码异常情况都要处理,又该如何去处理,自己写一个小例子

try:
    f = open('hello.txt')
    x = 9 / 0
    y = 6 / int('不是数字, 大哥')
except (ZeroDivisionError, IOError, Exception) as e:
    print(e)

# 3.try-except和try-finally有什么不同,写例子理解区别

try:
    x = 9 / 0
except ZeroDivisionError as e:
    print("只有异常才执行")
finally:
    print("不论是否异常都执行")

    # 4.
    # 写函数，检查传入字典的每一个value的长度，如果大于2，
    # 那么仅仅保留前两个长度的内容，并将新内容返回给调用者
dic = {"k1": "v1v1", "k2": [11, 22, 33]}
for k, v in dic.items():
    if len(v) > 2:
        dic[k] = v[:2]
        print(v)
print(dic)
