# coding : utf-8

# Q1. 格式化显示浮点数12.78，要求保留3位小数，输出占20字符，空位用0补齐
print("%020.3f" % (12.78))

# Q2. 将字符串”动脑学院”转换为bytes类型。
bytes_str = "动脑学院".encode('utf-8')
print(bytes_str)

# Q3. 将以下3个字符串合并为一个字符串，字符串之间用3个_分割
#
# 	hello  动脑  pythonvip
# 	合并为“hello___动脑___pythonvip”

print('hello', '动脑', 'pythonvip', sep='___')

# Q4. 删除字符串  “    你好，动脑eric     ”首尾的空格符号
print("    你好，动脑eric     ".lstrip(' ').rstrip(' '))

# Q5. 用户信息存在如下所示字符串中，包含信息依次为  姓名   学号  年龄，
# 不同信息之间的空格数不确定，请提取用户信息分别保存到 xxx_name,xxx_num, xxx_age。
# eric=“    Eric   dnpy_001     28”
# 提取后 eric_name=“Eric”eric_num=”dnpy_001”,eric_age=”28”
# zhangsan = “ 张三         dnpy_100    22     ”

list_str = " 张三         dnpy_100    22     ".lstrip(' ').rstrip(' ').split(' ')
list_str.remove("")
name = list_str[0]
num = list_str[1]
age = list_str[2]

print("{0}_name = {0},{0}_num = {1},{0}_age = {2}".format(name, num, age))
