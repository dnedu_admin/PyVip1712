# while循环求和1,3,5...49
i = 1
sum = 0
while i <= 49:
    sum += i
    i += 2
print(sum)

# 写出斐波那契级数前30项
i = 0
f0 = 0
f1 = 1
print(1)
while i < 30:
    f2 = f0 + f1
    f0 = f1
    f1 = f2
    print(f2)
    i += 1
