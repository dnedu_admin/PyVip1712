# conding : utf-8

# 第一题:
num_list = [[1, 2], ['tom', 'jim'], (3, 4), ['ben']]

#  1. 在’ben’后面添加’kity’
num_list[-1].append('kitty')
print(num_list)

#  2. 获取包含’ben’的list的元素
print(num_list[-1])

#  3. 把’jim’修改为’lucy’
num_list[1][1] = 'lucy'
print(num_list)

#  4. 尝试修改3为5,看看
# num_list[2][0] = 5 (元组无法修改值)

#  5. 把[6,7]添加到[‘tom’,’jim’]中作为第三个元素
num_list[3].append([6, 7])
print(num_list)

#  6.把num_list切片操作:
#       num_list[-1::-1]
print(num_list[-1:-4:-1])

# 第二题:
numbers = [1, 3, 5, 7, 8, 25, 4, 20, 29];
# 1.对list所有的元素按从小到大的顺序排序
print(sorted(numbers))
# 2.求list所有元素之和
sum = 0
for number in numbers:
    sum += number
print(sum)
# 3.将所有元素倒序排列
print(sorted(numbers, reverse=True))
