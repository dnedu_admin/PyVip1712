#coding=utf-8
#author:yan_严聪秦(qq:1727053412)
#动脑学院vip学员
# 1.datetime,获取你当前系统时间三天前的时间戳;并把当前时间戳增加
#  899999,然后转换为日期时间对象,把日期时间并按照%Y-%m-%d %H:%M:%S
#  输入打印这个日期对象,并获取date属性和time属性,最后判断当前日期时间
# from datetime import datetime,timedelta,date,time
# d1=datetime.now()
# d2=d1-timedelta(days=3)
# t2=d2.timestamp()#将datetime对象转换成时间戳对象（三天前的时间戳）
# print(t2)
#
# timestamp1=899999
# d3=d1.timestamp()#当前时间戳
# d4=datetime.fromtimestamp(timestamp1)
# d5=datetime.fromtimestamp(d3+timestamp1)#当前时间戳加上899999的总和的datetime对象
#
# # print(d4)
# print(datetime.strftime(d5,'%Y-%m-%d  %H:%M:%S'))#将总时间戳以%Y-%m-%d %H:%M:%S的形式转换成str打印输出
# print(datetime.date(d5))#获取d5的date属性
# print(datetime.time(d5))#获取d5的time属性




# 2.使用os模块线获取当前工作目录的路径,并打印当前目录下所有目录和文件,
# 获取当前脚本的绝对路径,
# from os import path
# import os
# print(os.getcwd())#获取当前工作目录的路径
# print(os.listdir('.'))#打印当前目录下所有的目录和文件
# a = os.path.basename(__file__)#获取当前文件的文件名
# print(a)
# # print(path.abspath(a))#获取当前文件的绝对路径
# # 分割当前脚本文件的目录和文件在tuple中并判断
# # 当前文件所在的目录下存不存在文件game.py,如果不存在则创建该文件.
# # 并往该文件里写入数据信息,然后判断当前目录下是否存在目录
# # /wow/temp/,如果不存在则创建,最后把目录名称改为:/wow/map/
# a_tuple = os.path.split(a)
# print(a_tuple)
# if os.path.exists('./game.py'):
#     print('当前文件所在目录下存在game。py文件!!!')
# else:
#     with open('game.py','w')as f:
#         f.write('已经被创建！！！')
# if os.path.exists('./wow/temp/'):
#     print('当前文件所在目录下存在.wow/temp/')
# else:
#     os.makedirs('./wow/temp/')
#     print('已经创建了文件')
# if os.path.exists('./wow/map/'):
#     os.rmdir('.wow/map')
# os.renames('./wow/temp/','./wow/map/')









# 3.写函数，检查获取传入列表或元组对象的所有奇数位索引对应的元素，
# 并将其作为新的列表返回给调用者
# def fun(s):
#     a = []
#     if isinstance(s,(list,tuple)):
#         for i in range(1,len(s),2):
#             if i :
#                 a.append(s[i])
#         return a
# s=['a',1,'b',2,'c',3]
# print(fun(s))


