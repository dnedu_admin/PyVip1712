#    -*- utf-8 -*-
#    Python Learn
#    yyg
#    @time:2018/3/6 20:10
# 1 使用pickle,json模块序列化和反序列化。
#pickle模块
#1）序列化
import pickle    #数据操作的二进制数据（）
a_dict = {'name':'yangguang','age':'27'}   #内存中
f = open('serialize.txt','wb+')
pickle.dump(a_dict,f,0)
f.close()
#2）反序列化
import pickle
f = open('serialize.txt','rb+')   #改变模式
b_dict = pickle.load(f)
print(type(b_dict))
f.close()
#json模块
#1）序列化
import json     #字符串（）
a_dict = {'name':'yangguang','age':'27'}   #内存中
f = open('serialize.txt','w+')
json.dump(a_dict,f)
f.close()



#2）反序列化
import json     #字符串（）
f = open('serialize.txt','r+')
b_dict = json.load(f)
print(type(b_dict))
f.close()
#2 深浅拷贝
a_list=[1,2,3,4,[8,9,0]]
#1)浅拷贝
from copy import copy
a_list=[1,2,3,4,[8,9,0]]
b = list(a_list)
c = copy(a_list)
print(a_list)
print(id(a_list))
print(b)
print(id(b))
print(c)
print(id(c))#地址不同

from copy import copy
a_list=[1,2,3,4,[8,9,0]]
b = list(a_list)
c = copy(a_list)
b[4][1] = 100
print(a_list[4][1])
print(c[4][1])#浅拷贝对象是第一层内层对象没有拷贝，内层[8,9,0]是三个对象所共享的
#2)深拷贝
from copy import deepcopy
a_list=[1,2,3,4,[8,9,0]]
b = deepcopy(a_list)
print(a_list is b)   #也是不同的对象
a_list[4][1] = 100    #修改了a_list列表内层元素，对比浅拷贝
print(a_list[4][1])
print(b[4][1])       #深拷贝，每一层数据都拷贝



#3.分别写代码完成内存缓冲区文本数据和二进制数据的读/写操作
from io import StringIO
f = open()
f = StringIO()   #构造对象
f.wtite('我爱学习Python\n')
f.write('学习还是有一定难度的\n')
#查看数据
print(f.getvalue())
#没有写入到文件，代码没有提到文件名称，所以在缓冲区里


#读取数据
from io import StringIO
f = StringIO('我爱学习Python'\n'学习还是有一定难度的\n')
while 1:
    conn = f.readline()
    if len(conn)!=0:
        print(conn.strip())
    else:
        break
#该方法与open较为相似，也就是引入一个对象 from io import StringIO

#二进制数据，BytesIO
from io import BytesIO
f = BytesIO()#构造BytesIO 空对象
f.wtite('我爱学习Python\n'.encode('utf-8'))#文本字符串----》二进制数据
f.write('学习还是有一定难度的\n'.encode('utf-8'))
print(f.getvalue().decode('utf-8'))