# 作业:
# 1.datetime,获取你当前系统时间三天前的时间戳;并把当前时间戳增加
#  899999,然后转换为日期时间对象,把日期时间并按照%Y-%m-%d %H:%M:%S
#  输入打印这个日期对象,并获取date属性和time属性,最后判断当前日期时间
#  对象是星期几;
from datetime import datetime, timedelta

timenow = datetime.now()
threedayago = timenow - timedelta(days=3)
print(threedayago)
timestamp = threedayago.timestamp() + 899999
timedate = datetime.fromtimestamp(timestamp)
print(timedate.strptime('%Y-%m-%d %H:%M:%S'))
print(timedate.date())
print(timedate.time())
print(datetime.today().weekday())


# 2.使用os模块线获取当前工作目录的路径,并打印当前目录下所有目录和文件,
# 获取当前脚本的绝对路径, 分割当前脚本文件的目录和文件在tuple中并判断
# 当前文件所在的目录下存不存在文件game.py,如果不存在则创建该文件.
# 并往该文件里写入数据信息,然后判断当前目录下是否存在目录
# /wow/temp/,如果不存在则创建,最后把目录名称改为:/wow/map/
#
import os
from os import path
print(os.getcwd())
print(os.listdir('.'))
print(os.path.abspath(r'D:\python软件\wenjiancunfangpy'))
print(path.splitext(r'new_file.py'))
print(path.exists(r'game.py'))
os.makedirs(r'D:\python软件\wenjiancunfangpy\wow\map')


# 3.写函数，检查获取传入列表或元组对象的所有奇数位索引对应的元素，并将其作为新的列表返回给调用者 

def hanshu(content):
    if (isinstance(content, (list, tuple))):
        a_list = []
        for index, item in enumerate(content, 1):
            if index % 2 != 0:
                a_list.append(item)
            print(a_list)
    else:
        print("传输对象")


new_list((1, 2, 3, 4))
