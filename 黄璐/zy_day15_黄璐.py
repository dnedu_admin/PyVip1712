
# 1.
# range(), 需要生成下面的列表, 需要填入什么参数:
# 1.[3, 4, 5, 6]
# 2.[3, 6, 9, 12, 15, 18]
# 3.[-20, 200, 420, 640, 860]

print(list(range(3,7)))
print(list(range(3,19,3)))
print(list(range(-20,861,220)))


# 2.
# 列表推导式或者生成器表达式(2
# 选择1)完成1 - 100
# 之间所有偶数和

def number(num):
    while num>=2 and num<=100 and num%2==0:
        yield num
        num-=2
p=number(100)
a_list=list(p)
# for a in p:
#     print(list(a))
from functools import reduce
print(reduce(lambda x,y:x+y,a_list))


# 3.
# 定义一个函数, 使用关键字yield创建一个生成器, 求1, 3, 5, 7, 9
# 各数
# 字的三次方, 并把所有的结果放入到list里面打印输入

def number(num):
    while num>0 and num<10 and num%2==1:
        yield num
        num+=2
p=number(1)
a_list=list(p)
print([x**3 for x in a_list])

#
# 4.
# 写一个自定义的迭代器类(iter)
class MyDDQ():
    def __init__(self, age):
        self.age = age

    def __next__(self):
        if self.age == 0:
            raise StopIteration("必须是数字")
        self.age -= 1
        return self.age

    def __iter__(self):
        return self


a_myddq = MyDDQ(8)
for num in a_myddq:
    print(num)
