# coding=utf-8
# 联系方式 qq:638476


result = 0
for num in range(1,50, 2):
    result += num
print(result)

result = 0
num=1
while num <= 49:
    result += num
    num += 2
print(result)


def recur_fibo(n):               #这个题我完全不知道怎么做，百度上抄来的。
    if n <= 1:
        return n
    else:
        return (recur_fibo(n - 1) + recur_fibo(n - 2))
nterms = int(input("您要输出几项? "))
if nterms <= 0:
    print("输入正数")
else:
    print("斐波那契数列:")
    for i in range(nterms):
        print(recur_fibo(i))      #这个题我完全不知道怎么做，百度上抄来的。
