# 1.
# 自己动手写一个学生类, 创建属性名字, 年龄, 性别, 完成
# __new__, __init__, __str__, __del__，__call__
#
# 2.
# 创建父类Person, 属性name, 函数eat, run, 创建子类Student, 学生类
# 继承Person, 属性name, age, id, 函数eat, study, 全局定义一个函数
# get_name, 传入一个Person类的实例参数, 分别传入Person和Student
# 两个类的实例对象进行调用.
#
# 3.
# 用代码完成python多态例子和鸭子模型


# #########################################################
class Student():
    def __new__(cls, *args, **kwargs):
        return ()

    def __init__(self, name, age, sex):
        self.name = name
        self.age = age
        self.sex = sex
        print(self.name)
        print(self.age)
        print(self.sex)


p1 = Student("anson", 17, "men")
print(p1)


# new可以在init之前 让类改变属性


# ############################################################


class Student():
    def __init__(self, name, age, sex):
        self.name = name
        self.age = age
        self.sex = sex

    def __del__(self):
        print(self.age)
        print("删除")


p1 = Student("anson", 17, "men")


# ############################################################################


class Student():
    def __init__(self, name, age, sex):
        self.name = name
        self.age = age
        self.sex = sex

    def __call__(self, *args, **kwargs):  # 死记住了，这个__call__就是为了让变量支持括号调用的
        # print("当前对象被调用")
        return self.age


new_student = Student("anson", 17, "men")
print(new_student())


# new_student()指的是  Student()


# ###########################################################################

class Student():
    def __init__(self, name, age, sex):
        self.name = name
        self.age = age
        self.sex = sex

    def __str__(self):
        return str(self.age)


new_student = Student("anson", 17, "men")
print(new_student)  # 通过print打印自动变成    str 字符串


# ##########################################################################



class Person():                             #创建父类Person
    name = "anson"                          #属性name
    def eat(self):                          #函数eat
        print("不知道输入什么")
    def run(self):                          #函数run
        print("输入什么什么")
class Student(Person):                      #创建子类Student  继承Person
    name= "ben"
    age = 18                                 #属性name, age, id
    id = 22
    def eat(self):                          #函数eat
        print("输入什么")
    def study(self):                        #study
        print("输入什么什么")
def get_name(self):                           # 全局定义一个函数get_name
    print(self.name)                              #传入一个Person类的实例参数
new_Per = Person()                                  #给父类定义一个变量
new_Stu= Student()                                 #给子类定义一个变量
get_name(new_Per)                                  #自定义的get_name方法调用这个父类Person
get_name(new_Stu)                                  #自定义的get_name方法调用这个子类Student


##############################################################################################

# #3.
# #用代码完成鸭子模型

class Person():                    #创建 Person类
    def work(self):                #定义函数work
        print("打印什么玩意")      #打印
class Afice():                    #创建 Afice类
    def work(self):               #定义函数work
        print("打印什么呢")       #打印
def fun(obj):                     #全局定义函数fun，给出形参
    obj.work()                    #用形参 obj 去访问 work方法
paly=Person()                     #Person类赋值变量给PLAY
chui=Afice()                      #Afice类赋值变量给chui
fun(paly)                         #全局函数fun调用变量play
fun(chui)                         #全局函数fun调用变量chui

#################################################################################
##python多态例子



class Person():                    #创建 Person类
    def work(self):                #定义函数work
        print("打印什么玩意")      #打印
class Afice(Person):                    #创建 Afice类
    def work(self):               #定义函数work
        print("打印什么呢")       #打印
def fun(obj):                     #全局定义函数fun，给出形参
    obj.work()                    #用形参 obj 去访问 work方法
paly=Person()                     #Person类赋值变量给PLAY
chui=Afice()                      #Afice类赋值变量给chui
fun(paly)                         #全局函数fun调用变量play
fun(chui)                         #全局函数fun调用变量chui


################################################################################






