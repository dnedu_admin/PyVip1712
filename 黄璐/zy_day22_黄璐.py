# 1.
# 使用正则表达式匹配电话号码：0713 - xxxxxx(湖南省座机号码)
#
import re
p=re.compile('\d+\D+\d+')
r=p.findall('number=0713 - 2938495')
for item in r:
    print(item)


# 2.
# 区号中可以包含()
# 或者 -, 而且是可选的, 就是说你写的正则表达式可以匹配
# 800 - 555 - 1212, 555 - 1212, (800)
# 555 - 1212
#
p=re.compile('\d+\D+\d+\D+\d+\d+\D+\d+\d+\d+\D+\d+\D\s\d+\D+\d+')
r=p.findall('number=800 - 555 - 1212, 555 - 1212, (800)\n555 - 1212')
for item in r:
    print(item)



# 3.
# 选作题:
# 实现一个爬虫代码



