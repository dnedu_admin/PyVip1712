

# 动手完成:
# 1.自己写代码对比2.x深度优先和3.x广度优先的继承顺序

#######################################################################################

class A():
    def bcc(self):
        print(A)
class B():
    def bcc(self):
        print(B)
class D(A):
    pass
class C(B):
    pass
class E(C,D):
    pass

f=E()
f.bbc()





#py2.x版本 是深度  py3.x版本是广度
#如果当前是py2.x版本  继承顺序是E 先找第一个父类C，C是pass,马上去找C的父类B ，B返回结果
#如果当前是py3.x版本，继承顺序是E 先找第一个父类C，C是pass占位符，马上找D，D也是pass占位符，D父类A有返回值  选择A


######################################################################################





class A():
    def run(self):
        print(A)
class B():
    def run(self):
        print(B)
class C(A,B):
    def run(self):
        pass
class D(B,C):
    def run(self):
        print(B)
class E(C,D):
    def run(self):
        pass
e=E()
e.run()
#3.x版本是广度优先  E先找（C,D）中的C 的结果是PASS 那么会过头 找D第一个父类是B 最终是找到B了



# 2.写一个类MyClass,使用__slots__属性,定义属性(id,name),创建该类实例
#     后,动态附加属性age,然后执行代码看看.

class Person():
    __slots__ = ('name','age','id')
p=Person()
p.name="anson"
p.age=18
p.id=888
print(p.name)

p.sex="女" #当slots 固定好元组name age id 后 不可以在随便动态绑定参数  sex绑定P实例 报错


# 3.使用type创建一个类,属性name,age,sex,方法sleep,run并创建此类的实例对象

def sleep():
    return sleep
def run():
    return run
Myhanshu=type('Ttt',(),{'num':1,'sleep':sleep,'run':run})
print(type(Myhanshu))
t1=Myhanshu()
print(t1.sleep)
print(t1.run)
print(t1.num)


# 4.写例子完成属性包装,包括获取属性,设置属性,删除属性
class Person():
    @property
    def age(self):
        return self._age
    @age.setter
    def age(self,age):
        if not isinstance(age,int):
            raise Exception("必须为整数")
        if age<0 or age>120:
            raise Exception("超过值的范围")
        self._age=age
    @age.deleter
    def age(self):
        del self._age
p=Person()
p.age=80
print(p.age)































