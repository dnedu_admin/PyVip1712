# 1.使用pickle,json(注意:两个模块)
# 把字典a_dict = [1,2,3,[4,5,6]]
# 序列化与反序列化,序列化,保存到文件名为serialize.txt文件里面,然后大开,并读取数据
# 反序列化为python对象加载到内存,并打印输出
# 尝试序列化/反序列非文件中,直接操作


import pickle
a_dict=[1,2,3,[4,5,6]]
f=open('serialize.txt','wb+')
pickle.dump(a_dict,f,0)
print(a_dict)
f.close()

f=open('serialize.txt','rb+')
b_dict=pickle.load(f)
print(type(a_dict))
print(a_dict)
f.close()

import json
a_dict1=[1,2,3,[4,5,6]]
f=open('serialize1.txt','w+')
json.dump(a_dict1,f)
print(a_dict1)
f.close()


f=open('serialize1.txt','r+')
b_dict1=json.load(f)
print(type(a_dict1))
print(a_dict1)
f.close()






#
# 2.自己写代码对比深浅拷贝的区别,写完代码然后看看运行结果,然后分析原因
#    比如：a_list=[1,2,3,4,[8,9,0]]


from copy import deepcopy
a=[1,2,3,4,[8,9,0]]
b=list(a)
c=deepcopy(a)
print(a)
print(b)
print(c)
a[4][2]=99   #a改了外层的值 其他的没有变   如果改了内层的值 则一起变 内层值 是共享的
print(a)
print(id(a))
print(b)
print(id(b))
print(c)
print(id(c))

# 反正就明白了一点  以后拷贝数据  就是deepcopy 如果遇到嵌套的 用copy命令  就玩完了

from copy import deepcopy
a=[1,2,3,4,[8,9,0]]
b=a
c=deepcopy(a)
print(a)
print(b)
print(c)
b[4][0]=22
print(a)
print(b)
print(c)


#
# 3.分别写代码完成内存缓冲区文本数据和二进制数据的读/写操作

from io import StringIO
f=StringIO()
f.write("我是中国人\n")
f.write("我喜欢上海")
print(f.getvalue())

from io import StringIO
f=StringIO("我是中国人\n我喜欢上海")
while 1:
    conn=f.readline()
    if len(conn)!=0:
        print(conn.strip())
    else:
        break


from io import BytesIO
f=BytesIO()
f.write("我是中国人".encode("utf-8"))
f.write("我喜欢上海".encode("utf-8"))
print(f.getvalue().decode("utf-8"))

from io import BytesIO
f=BytesIO("我是中国人\n,我喜欢上海".encode("utf-8"))
while 1:
    conn=f.readline()
    if len(conn)!=0:
        print(conn.strip())
    else:
        break