


# 1.filter判断[0,False,True,5,{},(2,3)]对应bool值

result=filter(lambda x:x!=0,[0,False,True,5,{},(2,3)])
print(list(result))

# 2.map实现1-10中所有奇数项的三次方,打印输出结果
result=map(lambda x:x**3,range(1,11,2))
print(list(result))


# 3.reduce实现1-100所有偶数项的和

from functools import reduce
print(reduce(lambda x,y:x+y,range(1,101)))


# 4.用递归函数实现斐波拉契数列

def digui(num):
    if isinstance(num,int):
        if num<=2:
            return 1
        else:
            return digui(num-1)+digui(num-2)
    else:
        raise Exception("错")

def numbers(ppp):
    maps=map(digui,range(1,ppp+1))
    num_list=list(maps)
    print(num_list)

if __name__=='__main__':
    numbers(30)





