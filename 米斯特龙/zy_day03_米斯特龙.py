#coding=utf-8
#author:zhaoxiaolong(QQ:937455638)
#time:2018/1/21 22:35

#1. 格式化显示浮点数12.78，要求保留3位小数，输出占20字符，空位用0补齐
a=12.78
print("%020.3f" %(a))


#2. 将字符串”动脑学院”转换为bytes类型。
b="动脑学院"
print(b.encode("utf-8"))

"""
3. 将以下3个字符串合并为一个字符串，字符串之间用3个_分割
    hello  动脑  pythonvip       合并为“hello___动脑___pythonvip”
"""
c=["hello","动脑","pythonvip"]
print("___".join(c))

# 4. 删除字符串  “    你好，动脑eric     ”首尾的空格符号
d="    你好，动脑eric     "
print(d.strip(" "))

"""
5. 用户信息如下所示字符串中，包含信息依次为  姓名   学号  年龄，不同信息之间的空格数不确定，
请提取用户信息分别保存到 xxx_name,xxx_num, xxx_age。
eric=“    Eric   dnpy_001     28”        
提取后 eric_name=“Eric”eric_num=”dnpy_001”,eric_age=”28”
zhangsan = “ 张三         dnpy_100    22     ”
"""
import re
zhangsan = " 张三         dnpy_100    22     "
zs_name,zs_num,zs_age=re.split("\s+",zhangsan.strip(" "))
print(zs_name,zs_num,zs_age)