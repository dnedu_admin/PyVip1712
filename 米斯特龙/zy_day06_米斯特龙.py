#coding=utf-8
#author:zhaoxiaolong(QQ:937455638)
#time:2018/1/27 20:57


# 1.用while和for两种循环求计算1+3+5.....+45+47+49的值
# 2.代码实现斐波那契数列(比如前30个项)

#1
sum_i=0
i=1
while i<50:
    sum_i+=i
    i+=1
print(sum_i)

sum_a=0
for a in range(50):
    sum_a+=a
print(sum_a)

#2
fib_start=[0,1]
n=2
while n<30:
    fib_add=fib_start[n-2]+fib_start[n-1]
    fib_start.append(fib_add)
    n += 1
print(fib_start)
print(len(fib_start))


