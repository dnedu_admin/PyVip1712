#coding=utf-8
#author:zhaoxiaolong(QQ:937455638)
#time:2018/2/8 18:49


# 1.
# 文件名称: use.txt
# 创建该文件, 路径由你自己指定, 打开该文件, 往里面写入
# str_content = ['tom is boy', '\n', 'you are cool', '\n',
#                '今天你怎么还不回来']
# 把数据写入到该文件中, 注意操作的模式, 关闭文件句柄后
# 再次打开该文件句柄, 然后读取文件中内容, 用with代码块
# 实现, 然后打印输出信息到控制台, 注意换行符

# with open('use.txt','w+',encoding='utf-8') as f:
#     str_content = ['tom is boy', '\n', 'you are cool', '\n','今天你怎么还不回来']
#     f.writelines(str_content)
# with open('use.txt','r',encoding='utf-8') as f1:
#     for line in f1:
#         print((line.strip()))


# 2.
# with创建一个文件homework.txt, 尝试多种操作模式.进行写读操作, 注意区别

with open('homework.txt','w+',encoding="utf-8") as f:
    homework_list = ['妞：在干么？','\n','龙哥：把在去掉，因为我一直都在。','\n','妞：干么？','\n']
    f.writelines(homework_list)
    f.write('龙哥：干！\n')

with open('homework.txt','a+',encoding="utf-8") as f1:
    f1.write('龙哥：你妹！\n')
    f1.seek(0)
    for line in f1:
        print(line.strip() )


# 3.
# 理解文件句柄, 对比read(), readline(), readlines()
# 写一个小例子
with open('homework.txt','r+',encoding="utf-8") as f:
    print(f.read())
# read()是读取全部数据

with open('homework.txt','r+',encoding="utf-8") as f:
    print(f.readline())
# readline()读取一行数据，可填写参数，写几，读取几

with open('homework.txt','r+',encoding="utf-8") as f:
    print(f.readlines())
# readlines()是读取全部数据，返回列表形式

# 4.
# 准备一张jpg图片, 把图片中数据读书出来, 并写入到文件my_img.jpg文件中,
# 操作完毕后尝试打开my_img.jpg文件看图片显示正不正常
with open('tupian.jpg','rb') as p:
    old = p.read()
with open('my_img.jpg','wb') as p1:
    p1.write(old)