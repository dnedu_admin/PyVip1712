#coding=utf-8
#author:zhaoxiaolong(QQ:937455638)
#time:2018/1/27 20:19
'''

1.自己写一个字典:
    name_dict = {'name':'ben','age':22,'sex':'男'}
    添加,删除,更新,清空操作

2.写两个集合:
    num1_set = {3,5,1,2,7}
    num2_set = {1,2,3,11}
    并分别进行&,|,^,-运算

3.整数字典:
    num_dict = {'a':13,'b':22,'c':18,'d':24}
    按照dict中value从小到大的顺序排序

'''
#1
name_dict = {'name':'ben','age':22,'sex':'男'}
name_dict['address']="上海"#添加
print(name_dict)

name_dict.pop('age')#删除
print(name_dict)

new_dict={'A':'a'}
name_dict.update(new_dict)#更新
print(name_dict)

name_dict.clear()#清空
print(name_dict)

#2
num1_set = {3,5,1,2,7}
num2_set = {1,2,3,11}
print(num1_set&num2_set)
print(num1_set|num2_set)
print(num1_set^num2_set)
print(num1_set-num2_set)

#3
num_dict = {'a':13,'b':22,'c':18,'d':24}
import operator
sorted_num=sorted(num_dict.items(),key=operator.itemgetter(1))#按照item的第二个字符排序，即value排序
print(dict(sorted_num))

