#coding=utf-8
#author:zhaoxiaolong

#第2题  十进制数字2034 的二进制表示，八进制表示，16进制表示
a=2034
print(bin(a),oct(a),hex(a))

#第3题  生成一个100到300之间的随机整数n，求其平方根，产生一个1到20之间的随机浮点数p，求大于p 的最小整数。
import random,math

b=random.randint(100,300)
c=random.uniform(1,20)
print(b,math.sqrt(b),c,int(math.floor(c)+1),math.ceil(c))

#第4题   运维系统中监控到磁盘信息，磁盘使用量为1 2106450611211bytes, 页面展示中需要展示单位为GB，保留2位小数。
d=12106450611211
print(round(d/(1024**3),2))


"""第5题  使用input函数,记录键盘输入的内容,打印输出该值的类型, 并转换为数值型。
     判断数值num大小,如果num>=90,输出打印:你的成绩为优秀
     如果num>=80 and num<90,输出打印:你的成绩为良好
     如果num>=60 and num <80,输出打印:你的成绩为一般
     如果num<60,输出打印:你的成绩为不合格
"""
num=input("请输入一个数:")
print(type(num))
e=float(num)
if e>=90:
    print("你的成绩为优秀")
elif 80<=e<90:
    print("你的成绩为良好")
elif 60<=e<80:
    print("你的成绩为良好")
elif e<60:
    print("你的成绩不合格")

