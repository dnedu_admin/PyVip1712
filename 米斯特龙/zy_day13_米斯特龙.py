#coding=utf-8
#author:zhaoxiaolong(QQ:937455638)
#time:2018/2/26 20:04



"""
1.自己写代码对比2.x深度优先和3.x广度优先的继承顺序
2.写一个类MyClass,使用__slots__属性,定义属性(id,name),创建该类实例
    后,动态附加属性age,然后执行代码看看.
3.使用type创建一个类,属性name,age,sex,方法sleep,run并创建此类的
    实例对象
4.写例子完成属性包装,包括获取属性,设置属性,删除属性
"""
#1.自己写代码对比2.x深度优先和3.x广度优先的继承顺序

class A():
    mai = 90
    nai = 10
class B():
    mai = 80
    nai = 20
class C(A,B):
    pass
class D(A,B):
    mai = 60
    nai = 40
class E(C,D):
    pass

hh = D()
#3.x是并行查找,就是所谓的广度广度优先,先找E没有就找C没有就找D没有就找C的父类A没有就找B,没有就找D的父类A没有就找B
print(hh.mai)
# 2.x是深度深度优先,串行查找E没有就找C没有就找A没有就返回E的父类D没有就找A没有返回D从B找
print(hh.mai)

"""
2.写一个类MyClass,使用__slots__属性,定义属性(id,name),创建该类实例
    后,动态附加属性age,然后执行代码看看.
"""

class MyClass():
    __slots__ = ('id','name')


KK = MyClass()
#使用了__slots__属性限制了你不想要的属性
#KK.age = 40
KK.id = 30
print(KK.id)


"""
3.使用type创建一个类,属性name,age,sex,方法sleep,run并创建此类的
    实例对象
"""

jjjo = type('Ben',(),{'name':'xiaoxiao','age':69,'sex':'op'})
kk = jjjo()
print(kk.sex)


#4.写例子完成属性包装,包括获取属性,设置属性,删除属性

class Hao():
    @property
    def fao(self):
        return self._fao
    @fao.setter
    def fao(self,fao):
        self._fao = fao
    @fao.deleter
    def fao(self):
        del self._fao
hh = Hao()
hh.fao = 40
print(hh.fao)