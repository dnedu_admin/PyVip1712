#coding=utf-8
#author:zhaoxiaolong(QQ:937455638)
#time:2018/1/29 20:48
'''
1.简述普通参数、默认参数、关键字参数、动态收集参数的区别,理解为主
答：普通参数必须传值，默认参数不用必须传值有默认值，关键字参数通过"键值"形式
加以指定,动态收集参数无论给多少参数，都能传入多少参数
'''


#2.
def foo(x, y, z, *args, **kw):
    sum = x + y + z
    print(sum)
    for i in args:
        print(i)
    print(kw)
    for j in kw.items():
        print(j)
a_tuple = (1,2,3)     #此处参数修改为2个看看会怎么样? 会报错因为不同参数传值不够
b_dict = {'name': 'jim', 'age': 28, 'add': '上海'}
foo(*a_tuple, **b_dict)    #分析这里会怎么样?

#3.题目:执行分析下代码
def func(a, b, c=9, *args, **kw):
    print('a =', a, 'b =', b, 'c =', c, 'args =', args, 'kw =', kw)
func(1,2)
func(1,2,3)
func(1,2,3,4)
func(1,2,3,4,5)
func(1,2,3,4,5,6,name='jim')
func(1,2,3,4,5,6,name='tom',age=22)
#扩展: 如果把你的函数也定义成
def  get_sum(*args,**kw):pass
#你的函数可以接受多少参数? -----无限个参数

#4.写一个函数，计算传入字符串中的数字、字母、空格和其他的个数分别为多少?

def calculate(zifuchuan):
    shuzi_num=0
    zimu_num=0
    kongge_num=0
    others_num=0
    for i in zifuchuan:
        if i.isdigit():
            shuzi_num+=1
        elif i.isalpha():
            zimu_num+=1
        elif i.isspace():
            kongge_num+=1
        else:
            others_num+=1
    print ("数字个数=",shuzi_num,"字母个数=",zimu_num,"空格数=",kongge_num,"其他数=",others_num)

calculate("sa%^*awd2 55$%^")


