#coding=utf-8
#author:dn_李四海(qq:653378495)
#动脑学院VIP
import re
'''
1.使用正则表达式匹配电话号码：0713-xxxxxx(湖南省座机号码)
'''
# phone = "0713-88866666(湖南省座机号码)"
# num = re.sub(r'\D',"",phone)
# print("电话号码是：",num)
'''
2.区号中可以包含()或者-,而且是可选的,就是说你写的正则表达式可以匹配
   800-555-1212,555-1212,(800)555-1212
'''
# text = "800-555-1212 555-1212 (800)555-1212 (021)88776543"
# m = re.findall(r'\(*\d{0,3}\)*\s*-*\s*555-1212',text)
# if m:
#     print(m)
# else:
#     print('not match')
'''
3.选作题:
  实现一个爬虫代码
'''
import urllib.request
url = "http://www.baidu.com"
page_info = urllib.request.urlopen(url).read()
page_info = page_info.decode('utf-8')
print(page_info)