#coding=utf-8
#author:dn_李四海(qq:653378495)
#动脑学院VIP
'''
1.range(),需要生成下面的列表,需要填入什么参数:
    1.[3,4,5,6]
    2.[3,6,9,12,15,18]
    3.[-20,200,420,640,860]
'''
#1.[3, 4, 5, 6]
#print([x for x in range(7) if x >= 3])
#print([x for x in range(3,7)])
# 2.[3,6,9,12,15,18]
# print([x for x in range(3,19) if x % 3 == 0])
#3.[-20,200,420,640,860]
#print([x-20 for x in range(-20,881) if x % 220 == 0])
'''
2.列表推导式或者生成器表达式(2选择1)完成1-100之间所有偶数和
'''
#print(sum([x for x in range(1,100) if x % 2 == 0]))
'''
3.定义一个函数,使用关键字yield创建一个生成器,求1,3,5,7,9各数
   字的三次方,并把所有的结果放入到list里面打印输入
'''
def get_count():
    for num in ([1,3,5,7,9]):
        yield num ** 3
g = get_count()
print(list(g))