#coding=utf-8
#author:dn_李四海(qq:653378495)
#动脑学院VIP
'''
1.请写出一段Python代码实现删除一个list里面的重复元素
'''
# a_list = ['a','bb','ss','33','bb','33']
# print(set(a_list))
'''
2.如何用Python来进行查询和替换一个文本字符串？
'''
# import re
# p=re.compile('blue|white|red')
# print(p.sub('colour','blue socks and red shoes'))
'''
3.a=1, b=2, 不用中间变量交换a和b的值
'''
# a=1
# b=2
# a=a+b
# b=a-b
# a=a-b
# print(a,b)
'''
4.写一个函数, 输入一个字符串, 返回倒序排列的结果:?如: string_reverse(‘abcdef’), 返回: ‘fedcba’
5种方法的比较:
1. 简单的步长为-1, 即字符串的翻转;
'''
# def string_reverse1(text='abcdef'):
#     return text[::-1]
# print(string_reverse1('fsdfsdafsdfsdf'))
'''
2. 交换前后字母的位置;
'''
# def string_reverse2(text='abcdef'):
#     new_text=list(text)
#     new_text.reverse()
#     return ''.join(new_text)
# print(string_reverse2('abcikghkhhgkjdef'))
'''
3. 递归的方式, 每次输出一个字符;
'''
# def string_reverse5(text='abcdef'):
#     if len(text) <= 1:
#         return text
#     else:
#         return string_reverse5(text[1:]+text[0])
# print(string_reverse5('abcdef'))

'''
4. 双端队列, 使用extendleft()函数;
'''
# from collections import deque
# def string_reverse4(text='abcdef'):
#     d = deque()
#     d.extendleft(text)
#     return ''.join(d)
# print(string_reverse4('abcdeffdsafsa'))


'''
5. 使用for循环, 从左至右输出;
'''
# def string_reverse3(text='abcdef'):
#     new_text=[]
#     for i in range(1,len(text)+1):
#         new_text.append(text[-i])
#     return ''.join(new_text)
# print(string_reverse3('abcdefdfsgds'))
'''
5.请用自己的算法, 按升序合并如下两个list, 并去除重复的元素
合并链表, 递归的快速排序, 去重;
list1 = [2, 3, 8, 4, 9, 5, 6]
list2 = [5, 6, 10, 17, 11, 2]
'''
import random
list1 = [2, 3, 8, 4, 9, 5, 6]
list2 = [5, 6, 10, 17, 11, 2]
def qsort(L):
    if len(L) < 2: return L
    pivot_element = random.choice(L)
    small = [i for i in L if i < pivot_element]
    large = [i for i in L if i > pivot_element]
    return qsort(small) + [pivot_element] + qsort(large)
def merge(list1, list2):
    return qsort(list1 + list2)
print(merge(list1, list2))

###222222222222222222222222222222
all_hele_slot = []
all_hele_slot.extend(list1)
all_hele_slot.extend(list2)
all_hele_slot.sort()
print(list(set(all_hele_slot)))