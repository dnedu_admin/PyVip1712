#coding=utf-8
#author:dn_李四海(qq:653378495)
#动脑学院VIP
'''
爬取
url = 'http://tieba.baidu.com/p/2460150866'
前三页所有的图片
'''
# 第一步:
import requests
from urllib.request import urlretrieve  # 保存资源
import re
import time
num = 1
# 第二步:  # 获取网页源代码
def get_code(url):  # 参数:网址
    try:
        page = requests.get(url)
        return page.text
    except Exception as e:
        print(e)
# < img pic_type = "0" class ="BDE_Image" src="https://imgsa.baidu.com/forum/w%3D580/sign=294db374d462853592e0d229a0ee76f2/e732c895d143ad4b630e8f4683025aafa40f0611.jpg" pic_ext="bmp" height="328" width="560" >
# < img pic_type = "0" class ="BDE_Image" src="https://imgsa.baidu.com/forum/w%3D580/sign=750661a0fcfaaf5184e381b7bc5594ed/75fafbedab64034fc3ed0b80aec379310a551d11.jpg" pic_ext="jpeg" height="315" width="560" >
# 第三步: 获取源代码里面图片
def get_image(code):   # 参数就是源代码
    try:
        # 使用正则匹配图片链接地址
        reg = r'src="(.+?\.jpg)" pic_ext'  # 规则
        image = re.compile(reg)
        image_list = re.findall(image, code)  # 所有图片的链接地址
        # 图片名字
        global num
        # 遍历列表获取每一个连接地址
        for url in image_list:
            # 拿到每一个图片的链接地址,下载
            urlretrieve(url, 'd:/img/' + '%s.jpg' % num)  # 下载,保存
            num += 1
            time.sleep(0.1)    #
    except Exception as e:    # 代码走了异常
        print(e)

if __name__=='__main__':
    # 获取源代码
    for i in range(1,4):
        url = 'http://tieba.baidu.com/p/2460150866?pn={}'.format(i)
        print(i)
        html = get_code(url)
        # 获取图片
        get_image(html)
        print('图片处理完毕')