#coding=utf-8
#author:dn_李四海(qq:653378495)
#动脑学院VIP
'''
1.自己写代码对比2.x深度优先和3.x广度优先的继承顺序
'''
# class A():
#     def eat(self):
#         print('A正在吃饭')
# class B():
#     def eat(self):
#         print('B正在吃饭')
# class C(A,B):
#     pass
# class D(A,B):
#     def eat(self):
#         print('D正在吃饭')
# class E(C,D):
#     pass
#
# e=E()
# e.eat()
# py2.x环境下输出：A正在吃饭
# py3.x环境下输出：D正在吃饭
'''
2.写一个类MyClass,使用__slots__属性,定义属性(id,name),创建该类实例
    后,动态附加属性age,然后执行代码看看
'''
# class MyClass():
#     __slots__ = ('id','name')
# m=MyClass()
# m.age='lsh'
# 输出错误AttributeError: 'MyClass' object has no attribute 'age'
'''
3.使用type创建一个类,属性name,age,sex,方法sleep,run并创建此类的实例对象
'''
# def sleep(self):
#     print('lsh正在睡觉')
# def run(self):
#     print('lsh正在跑步')
# Hello=type('Hello',(),{'name':'lsh','age':'29','sex':'男','sleep':sleep,'run':run})
# h=Hello()
# h.run()
'''
# 4.写例子完成属性包装,包括获取属性,设置属性,删除属性
'''
# class Student():
#     @property
#     def id(self):
#         return self._age
#     @id.setter
#     def id(self,id):
#         if not isinstance(int):
#             raise TypeError('必须为整数')
#         if id<0:
#             raise ValueError('超出正常范围')
#         self._id=id
#     @id.deleter
#     def id(self):
#         del self._id
#
# s=Student()
# s.id=27
# print(s.id)
# del s.id
class Person():
    @property
    def age(self):
        return  self._age
    @age.setter
    def age(self,age):
        if not isinstance(age, int):
            raise TypeError('必须为整数')
        if 0 < age <= 120:
            self._age = age
        else:
            raise ValueError('年龄超出范围')
    @age.deleter
    def age(self):
        del self._age
p = Person()
p.age = 30
print(p.age)