# coding=utf-8
# author:dn_李四海(qq:653378495)
# 动脑学院VIP
# 第一题:num_list = [[1,2],[‘tom’,’jim’],(3,4),[‘ben’]]
# 1.在’ben’后面添加’kity’
# 2.获取包含’ben’的list的元素
# 3.把’jim’修改为’lucy’
# 4.尝试修改3为5, 看看
# 5.把[6, 7]添加到[‘tom’, ’jim’]中作为第三个元素
# 6.把num_list切片操作:num_list[-1::-1]
num_list = [[1, 2], ['tom', 'jim'], (3, 4), ['ben']]
# 1
num_list.append('lucy')
print(num_list)
# 2
print(num_list[len(num_list) - 1])
# 3
num_list[1][1] = 'lucy'
print(num_list)
# 4  #tuple不可改变！！！
#num_list1 = tuple(num_list) #tuple 不可改变！！！
#num_list1[2][0] = '5'
#print(num_list1)
# 5
num_list[1].append('6,7')
print(num_list)
# 6
age_list = num_list[-1::-1]
print(age_list)
'''
第二题:
numbers = [1,3,5,7,8,25,4,20,29];
1.对list所有的元素按从小到大的顺序排序
2.求list所有元素之和
3.将所有元素倒序排列
'''
numbers = [1,3,5,7,8,25,4,20,29]
#1
print (sorted(numbers))
#2
n = 0
for i in numbers:
    n += i
    print(n)
#3
numbers.sort(reverse=True)
print(numbers)