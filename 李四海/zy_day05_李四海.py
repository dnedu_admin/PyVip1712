#coding=utf-8
#author:dn_李四海(qq:653378495)
#动脑学院VIP
'''
1.自己写一个字典:
    name_dict = {'name':'ben','age':22,'sex':'男'}
    添加,删除,更新,清空操作
'''
#添加
#name_dict = {'name':'ben','age':22,'sex':'男'}
# name_dict['dn'] = "vip"
# print(name_dict)
#删除
# pop_odj=name_dict.pop('age')
# print(name_dict)
#更新
# name_dict['name'] = 'lsh'
# print(name_dict)
#清空操作
# name_dict.clear()
# print(name_dict)
'''
2.写两个集合:
    num1_set = {3,5,1,2,7}
    num2_set = {1,2,3,11}
    并分别进行&,|,^,-运算
'''
# num1_set = {3, 5, 1, 2, 7}
# num2_set = {1, 2, 3, 11}
# print(num1_set&num2_set)
# print(num1_set|num2_set)
# print(num1_set^num2_set)
# print(num1_set-num2_set)
'''
3.整数字典:
    num_dict = {'a':13,'b':22,'c':18,'d':24}
    按照dict中value从小到大的顺序排序
'''
num_dict = {'a':13,'b':22,'c':18,'d':24}
#(1)
print(sorted( num_dict.items(), key=lambda d: d[1] ))
#(2)
print([v for v in sorted(num_dict.values())])

