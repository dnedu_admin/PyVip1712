#coding=utf-8
#author:dn_李四海(qq:653378495)
#动脑学院VIP
'''
1.在当前项目下创建目录abc,并在abc目录下,使用uuid产生唯一码并写入到文件
  uuid.py中,并拷贝整个目录abc,新目录名为app,并将app目录下文件拷贝,新
  文件名为:new.py文件
'''
# import os
# import uuid
# import shutil
# if not os.path.exists('d:/lsh/abc'):#判断文件夹不存在
#     os.mkdir('d:/lsh/abc')#创建文件夹
# os.chdir('d:/lsh/abc') #指定文件路径
# u1 = uuid.uuid1()#产生唯一码
# open('abc.py','w',encoding='utf-8').write(str(u1))#写入到abc
# os.chdir('d:/lsh') #指定文件路径
# if os.path.exists('d:/lsh/app'): #判断文件存在
#     shutil.rmtree('app')#删除已有文件
# shutil.copytree('abc', 'app') #拷贝文件夹
# os.chdir('d:/lsh/app/')#指定文件路径
# shutil.copy('abc.py', 'new.py')#拷贝文件
'''
2.创建本地文件’one.txt’,在里写入部分数据(‘今天天气不错,我们去动物园n玩吧’
  n从1-9),使用b64encode对每一行数据编码,然后写入到文件oen.txt中.每写
  完一行之后写入换行符.
  打开刚才创建的文件one.txt,以读取多行的形式读取文件中的内容,并把每一行的
  内容通过b64decode对每一行数据解码,解码之后每一行数据依然是二进制形式
  所以,继续对每一行数据通过utf-8的形式解码为可以普通字符串,然后判断每一行
  字符串是否包含’动物园6’的子字符串,如果包含,先输出换行符,然后则打印输出
  当前行的字符串,最后创建一个文件,’two.txt’,并把帅选之后的数据写入到这个新
  文件中.
'''
# import base64
# import re
# import os
# fl = open('one.txt', 'w',encoding='utf-8')
# for i in range(1,10):
#     a = base64.b64encode(("今天天气不错,我们去动物园%s玩吧" % i).encode(encoding='utf-8')).decode()
#     fl.write(a)
#     fl.write("\n")
# fl.close()
#
# myfile = open(r'one.txt','r')
# lines = myfile.readlines()
# for line in lines:
#     conn = base64.b64decode((line).encode(encoding='utf-8')).decode()
#     llll = re.findall(r'.+6.+',conn)
#     if llll:
#         fl2 = open('two.txt', 'w', encoding='utf-8')
#         fl2.write("\n")
#         b = (("%s" % llll).encode(encoding='utf-8')).decode()
#         fl2.write(b)
#         fl2.close()
# myfile.close()

'''
3.准备一张.jpg图片,比如:mm.jpg,读取图片数据并通过b85encode加密之后写入到新文件mm.txt文件中,
  然后读取mm.txt数据并解密之后然后写入到mmm.jpg文件中
'''
import base64
with open('picl1.jpg', 'rb+') as f:
    content = base64.b85encode(f.read())
    with open('mm.txt', 'wb+') as f2:
        f2.write(content)
with open('mm.txt', 'rb+') as f3:
    result = base64.b85decode(f3.read())
    print(result)
    with open('mmm.jpg', 'wb+') as f4:
        f4.write(result)