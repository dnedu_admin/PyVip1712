# coding=utf-8
# author:dn_李四海(qq:653378495)
# 动脑学院VIP
'''1.xlsxwriter包中，往工作表中插入写数据方法  work_sheet.write()
查看write函数定义，解释其函数定义中 @convert_cell_args这个装饰器的具体用途？
'''
import xlsxwriter
import pandas as pd
import numpy as np

work_book_name = 'day33_test1.xlsx'
work_sheet_name = 'newSheet'

work_book = xlsxwriter.Workbook(work_book_name)
work_sheet = work_book.add_worksheet(work_sheet_name)

# df = pd.DataFrame(np.random.randint(1, 100, (4, 5)),
#                   index=['object%d' % i for i in range(1, 5)],
#                   columns=['星期%s' % j for j in ['一', '二', '三', '四', '五']]
#                   )

work_sheet.write('A1', 'hello the world')

work_book.close()


# @convert_cell_args


'''
    单元格转化,是将"B1"这种形式的表达形式变为(0,2)第二行,第一列
'''
