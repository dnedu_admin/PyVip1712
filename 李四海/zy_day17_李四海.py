#coding=utf-8
#author:dn_李四海(qq:653378495)
#动脑学院VIP
'''
1.使用pickle,json(注意:两个模块)
把字典a_dict = [1,2,3,[4,5,6]]
序列化与反序列化,序列化,保存到文件名为serialize.txt文件里面,然后大开,并读取数据
反序列化为python对象加载到内存,并打印输出
尝试序列化/反序列非文件中,直接操作
'''
####pickle
#import pickle
# a_dict = [1,2,3,[4,5,6]]
# f = open('serialize.txt','wb+')
# pickle.dump(a_dict,f)
# f.close()
#
# f = open('serialize.txt','rb+')
# r = pickle.load(f)
# print(r)
# f.close()
####json
# import json
# a_dict = [1,2,3,[4,5,6]]
# f = open('serialize.txt','w+')
# json.dump(a_dict,f)
# f.close()
#
# f = open('serialize.txt','r+')
# r = json.load(f)
# print(r)
# f.close()

'''
2.自己写代码对比深浅拷贝的区别,写完代码然后看看运行结果,然后分析原因
   比如：a_list=[1,2,3,4,[8,9,0]]
'''
####浅拷贝
# a_list=[1,2,3,4,[8,9,0]]
# b_list=a_list[:]
# a_list[0]='1000'
# print(a_list)
# print(b_list)
####结果a_list=['1000', 2, 3, 4, [8, 9, 0]]
####结果b_list=[1, 2, 3, 4, [8, 9, 0]]#   不同步
# a_list=[1,2,3,4,[8,9,0]]
# b_list=a_list[:]
# a_list[-1][0]='1000'
# print(a_list)
# print(b_list)
####结果a_list=['1', 2, 3, 4, [1000, 9, 0]]
####结果b_list=[1, 2, 3, 4, [1000, 9, 0]]#   同步
####深拷贝
# import copy
# a_list=[1,2,3,4,[8,9,0]]
# b_list=copy.deepcopy(a_list)
# a_list[-1][0]='1000'
# print(a_list)
# print(b_list)
####结果a_list=['1', 2, 3, 4, [1000, 9, 0]]
####结果b_list=[1, 2, 3, 4, [8, 9, 0]]#
####浅拷贝只能拷贝第一层的数，深拷贝可以深层次的拷贝。
'''
3.分别写代码完成内存缓冲区文本数据和二进制数据的读/写操作
'''
# from io import StringIO
# f = StringIO()
# f.write('我是lsh，你是谁？\n')
# f.write('今天天气不错，我们出去玩吧\n')
# f.write('去哪里玩？\n')
# print(f.getvalue())

# from io import StringIO
# f = StringIO('今天天气不错\n我想和你一起出去走走\n但是我不知道你想去哪？')
# while True:
#     conn = f.readline()
#     if len(conn) != 0:
#         print(conn.strip())
#     else:
#         break

#from io import BytesIO

# f = BytesIO()
# f.write('我是lsh\n'.encode('utf-8'))
# f.write('who are you\n'.encode('utf-8'))
# f.write('you are good boy\n'.encode('utf-8'))
#print(f.getvalue().decode('utf-8'))

# print('我是lsh\n'.encode('utf-8'))
# print(type('我是lsh\n'.encode('utf-8')))
# print(b'\xe6\x88\x91\xe6\x98\xaflsh\n'.decode('utf-8'))

from io import BytesIO
f = BytesIO(b'\xe6\x88\x91\xe6\x98\xaflsh\n'b'\xe6\x88\x91\xe6\x98\xaflsh2\n')
while True:
    conn = f.readline()
    if len(conn) != 0:
        print(conn.strip())
    else:
        break