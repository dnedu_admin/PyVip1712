#coding=utf-8
#author:dn_李四海(qq:653378495)
#动脑学院VIP
#1.高阶函数:filter判断[0,False,True,5,{},(2,3)]对应bool值
# def fun():
#     b = [0, False, True, 5, {}, (2, 3)]
#     a_filter = filter(lambda x: x, b)
#     print(list(a_filter))
# fun()
#2.map实现1 - 10中所有奇数项的三次方, 打印输出结果
# def fun2():
#     a = map(lambda x:x**3,range(1,10,2))
#     print(list(a))
# fun2()
#3.reduce实现1 - 100所有偶数项的和
# def fun3():
#     from functools import reduce
#     c = reduce(lambda x,y:x+y,range(0,101,2))
#     print(c)
# fun3()
#4.用递归函数实现斐波拉契数列
def func(num):
    if isinstance(num, int):
        if num < 2:
            return 1
        else:
            return func(num-1) + func(num -2)
    else:
        raise TypeError('必须输入整数')


def fun5(num2):
    a = map(func,range(1, num2 + 1))
    num_list = list(a)
    print(num_list)

fun5(5)