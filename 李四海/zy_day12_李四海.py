# coding=utf-8
# author:dn_李四海(qq:653378495)
# 动脑学院VIP
'''
1.自己动手写一个学生类,创建属性名字,年龄,性别,完成
     __new__,__init__,__str__,__del__，__call__
'''
# class stuinfo:
#     def __init__(self,name,age,sex):
#         self.name=name
#         self.age=age
#         self.sex=sex
#     def __del__(self):
#         print('正在销毁对象',self.name)
#     def __call__(self, ):
#         print(self.name,'当对象被调用')
#         return self.name,self.age,self.sex
#     def __str__(self):
#         return self.name+"123"
# stuinf=stuinfo("lsh","22","男")
# print(stuinf())
# class stuinfo2:
#     def __init__(self,name,age,sex):
#         self.name=name
#         self.age=age
#         self.sex=sex
#     def __str__(self):
#         return self.name+"123"
# test = stuinfo2('lsh',22,"男")
# print (test)
'''
2.创建父类Person,属性name,函数eat,run,创建子类Student,学生类
  继承Person,属性name,age,id,函数eat,study,全局定义一个函数
  get_name,传入一个Person类的实例参数,分别传入Person和Student
  两个类的实例对象进行调用
'''
# class Person():
#     def __init__(self, name):
#         self.name = name
#
#         def eat(self):
#             print('person正在吃东西')
#
#         def run(self):
#             print('person正在看电视')
#
# class Student(Person):
#     def __init__(self, name, age, id):
#         self.name = name
#         self.age = age
#         self.id = id
#
#     def eat(self):
#         print('student正在吃饭')
#
#     def study(self):
#         print('student正在看电视')
#
# def get_name(Person):
#     print(Person.name)
#
# a_person = Person('lsh_person')
# a_student = Student('lsh_student', 29, '20188888')
# get_name(a_person)
# get_name(a_student)

'''
3.用代码完成python多态例子和鸭子模型
'''
# class Animal(object):
#     def eat(self):
#         print('动物在吃东西')
#     def drink(self):
#         print('动物在喝牛奶')
#
# class Dog(Animal):
#     def run(self):
#         print('狗在跑')
# a = Animal()
# d = Dog()
#
# def fun(animal):
#     animal.eat()
#     animal.drink()
#     animal.run()
# fun(d)

# 鸭子形态:
class Animal(object):
    def run(self):
        print('动物在跑')

class Person(object):
    def run(self):
        print('人在跑')

def run(object):
    object.run()

a = Animal()
p = Person()

run(a)
run(p)