#coding=utf-8
#author:dn_李四海(qq:653378495)
#动脑学院VIP
'''
1.识别下面字符串:’ben’,’hit’或者’ hut’
2.匹配用一个空格分隔的任意一对单词,比如你的姓 名
3.匹配用一个逗号和一个空格分开的一个单词和一个字母
4.匹配简单的以’www’开头,以’com’结尾的web域名,比如:www.baidu.com
'''
import re
##1#############################
# match = 'ben hit hut'
# nnn = '[bh][eui][nt]'
# print(re.findall(nnn,match))
##2#############################
#print(re.findall('si\shai', 'si hai'))
#print(re.findall('[A-Za-z][a-z]+ [A-Za-z][a-z]+', 'si hai'))
##3#############################
#print(re.match('([A-Z]+\.)+ ?[A-Z][a-z]+','SI.HAI. Li').group())
#4#############################
print(re.search('www','www.baidulcom').span())
print(re.match('w{3}[.\w]+.com','www.baidu.com').group())