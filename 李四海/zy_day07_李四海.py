#coding=utf-8
#author:dn_李四海(qq:653378495)
#动脑学院VIP
#1.简述普通参数、默认参数、关键字参数、动态收集参数的区别,理解为主
'''
1.普通参数须以正确的顺序传入函数。调用时的数量必须和声明时的一样。
2.调用函数时，如果没有传递参数，则会使用默认参数。
3.关键字参数和函数调用关系紧密，函数调用使用关键字参数来确定传入的参数值。

使用关键字参数允许函数调用时参数的顺序与声明时不一致，因为 Python 解释器能够用参数名匹配参数值。
4.你可能需要一个函数能处理比当初声明时更多的参数。这些参数叫做动态收集参数，和上述2种参数不同，声明时不会命名。
'''
'''
2.def foo(x, y, z, *args, **kw):
    sum = x + y + z
    print(sum)
    for i in args:
        print(i)
    print(kw)
    for j in kw.items():
        print(j)
a_tuple = (1,2,3)     此处参数修改为2个看看会怎么样?
b_dict = {'name': 'jim', 'age': 28, 'add': '上海'}
foo(*a_tuple, **b_dict)    分析这里会怎么样?
'''
#.答缺少一个参数，报错。
'''
3.题目:执行分析下代码
    def func(a, b, c=9, *args, **kw):
         print('a =', a, 'b =', b, 'c =', c, 'args =', args, 'kw =', kw)
    func(1,2)
    func(1,2,3)
    func(1,2,3,4)
    func(1,2,3,4,5)
    func(1,2,3,4,5,6,name='jim')
    func(1,2,3,4,5,6,name='tom',age=22)
    扩展: 如果把你的函数也定义成  def  get_sum(*args,**kw):pass 
    你的函数可以接受多少参数?
'''
#答：可以接受所有参数
#4.写一个函数函数，计算传入字符串中的数字、字母、空格和其他的个数分别为多少?
def func1(s):
    al_num = 0
    spance_num = 0
    digit_num = 0
    others_num = 0
    for i in s:
        if i.isdigit():    # isdigit 判断有没有数字
            digit_num += 1
        elif i.isspace():   # isspace 判断有没有空格
            spance_num += 1
        elif i.isalpha():    #isalpha 判断有没有字符
            al_num += 1
        else:
            others_num += 1
    return (al_num,spance_num,digit_num,others_num)
r = func1("h gj11 111 1kjlk jl")
print(r)