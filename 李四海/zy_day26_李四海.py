#coding=utf-8
#author:dn_李四海(qq:653378495)
#动脑学院VIP
# 1. PKCS#5(选择一种hash算法)加密算法加密你的中文名字,写出代码
#
# import hashlib
# m=hashlib.md5()
# m.update('李四海'.encode('utf-8'))
# print(m.hexdigest())
#
#
# # 2. 创建文件test.py,写入一些数据,并计算该文件的md5值
# import hashlib
# with open('test.py','w',encoding='utf-8') as f:
#     m=hashlib.md5('today I am very tired'.encode('utf-8'))
#     f.write(m.hexdigest())
#     print("有整个py文件加密的方法吗！")

# 3. 遵循上下文管理协议,自己编写一个类,去创建一个上下文管理器,然后自己使用看看
   # 方式1:普通方式
   # 方式2:使用contextmanager
#
# class Myclass():
#     def __init__(self,name):
#         self.name=name
#         print(name)
#     def __enter__(self):
#         for i in range(1,4):
#             print(i)
#         return self
#     def __exit__(self, exc_type, exc_val, exc_tb):
#         pass
#     def work(self):
#         for i in self.name:
#             print(i)
# with Myclass('blake') as f:
#     f.work()
#
# from contextlib import  contextmanager
# class Your_class():
#     def __init__(self,a_lsit):
#         self.a_list=a_lsit
#     def run(self):
#         for i in self.a_list:
#             print(i)
# @contextmanager
# def work(a_list):
#     t=Your_class(a_list)
#     yield t
#     print(t)
# with work([1,2,3,4]) as e:
#     e.run()


# 4.使用colorama模块使用多颜色输出
# from colorama import Fore,Back,Style
# print(Fore.BLACK+'你好')
# print(Fore.YELLOW+Back.WHITE+Fore.BLUE+"你好")
# print(Style.RESET_ALL)
# print('普通颜色')