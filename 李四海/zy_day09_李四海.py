#coding=utf-8
#author:dn_李四海(qq:653378495)
#动脑学院VIP
'''
1.a_list = [1,2,3,2,2]
   删除a_list中所有的2
'''
#a_list = [1,2,3,2,2]

# a = []
# for b in a_list:
#     if b != 2:
#         a.append(b)
# print(a)
'''
 2.用内置函数compile创建一个文件xx.py,在文件里写你的名字,然后用eval
   函数读取文件中内容,并打印输出到控制台
'''
# code=compile("open('mycode.py','w',encoding='utf-8').write('我是齊天大聖\\n我是齊天大聖2')",'mycode.py','exec')
# print(exec(code))
# code3 = eval("open('mycode.py','r',encoding='utf-8').read()")
# print(code3)
'''
 3.写一个猜数字的游戏,给5次机会,每次随机生成一个整数,然后由控制台输入一个
   数字,比较大小.大了提示猜大了,小了提示猜小了.猜对了提示恭喜你,猜对了.
    退出程序,如果猜错了,一共给5次机会,5次机会用完程序退出.
'''
#import random
# a = 1
# while a <= 5:
#     a += 1
#     n = random.choice(range(6))
#     s = int(input("请输入一个0到6之间的整数："))
#     print(n)
#     if s > 6:
#         print("超出范围了")
#     elif s == n:
#         print("恭喜你，猜对了。")
#         break
#     elif s > n:
#         print("猜大了")
#     elif s < n:
#         print("猜小了")
import random
for i in range(5):
    i += 1
    n = random.choice(range(6))
    s = int(input("请输入一个0到6之间的整数："))
    print(n)
    if s > 6:
        print("超出范围了")
    elif s == n:
        print("恭喜你，猜对了。")
        break
    elif s > n:
        print("猜大了")
    elif s < n:
        print("猜小了")

