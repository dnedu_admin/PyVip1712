#coding=utf-8
#author:dn_李四海(qq:653378495)
#动脑学院VIP
'''
1.写一个函数计算1+3+5+…+97+99的结果
再写一个装饰器函数,对其装饰,在运算之前,新建一个文件
然后结算结果,最后把计算的结果写入到文件里
'''
# def outer(func):
#     def inner(b_list):
#         fo = open('lsh123.py', 'w', encoding='utf-8')
#         num = func(b_list)
#         fo.write('结果%s' % num)
#         fo.close
#         return num
#     return inner
# @outer
# def work(a_list):
#     result = 0
#     for num in a_list:
#         result += num
#     return result
# result = work(range(1,100,2))
# print('结果%s' %result)

'''
2.写一个函数计算传入进来参数的平方, 并将结果.写一个带参数的装饰器, 
       将装饰器参数传入到被包装的函数,计算并输入结果
'''
def outer():
    def middle(func):
        def inner(*args,**kwargs):
            num = func(*args,**kwargs)
            return num
        return inner
    return middle
@outer()
def fun(a):
    return a**a
result = fun(3)
print(result)



