#coding=utf-8
#author:dn_李四海(qq:653378495)
#动脑学院VIP

'''
创建两个协程函数对象,分别完成1,3,5…99,   2,4,6…,100
 然后让两个协程函数对象分别交替执行.在每一个协程函数对象的内部先打印输出
 每一个数字,然后执行完一轮之后打印提示信息,已经交替执行完毕一轮
 等所有的协程函数对象执行完毕之后,分析一下数据信息
'''

import os
import time
from types import coroutine

@coroutine
def wait_signal():
    yield

# 奇数协程函数对象
async def odd_number():
    for i in range(1, 100, 2):
        print(i)
        await wait_signal()
    print('已经交替执行完毕一轮')
    time.sleep(3)

# 偶数协程函数对象
async def even():
    for i in range(2, 101, 2):
        print(i)
        await wait_signal()
    print('已经交替执行完毕二轮')


def run(cor_list):
    while cor_list:
        for func in cor_list:
            try:
                func.send(None)
            except StopIteration as e:
                cor_list.remove(func)


if __name__ == '__main__':
    run_list = [odd_number(), even()]
    run(run_list)