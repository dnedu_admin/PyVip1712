# 1.计算range(1,99999)的总和,采取三种方式,第一:重复10次计算总时间;
# 第二:开启十个子线程去计算;第三:开启十个进程去计算,然后对比耗时
# from multiprocessing import Pool,cpu_count,Process
# from multiprocessing.dummy import Pool as Tpool
# import time
#
# def count_time(func):
#     def inner(num):
#         time1 = time.time()
#         func(num)
#         time2 = time.time()
#         print('计算总时间为%s' % str(time2-time1))
#     return inner
# @ count_time
# def count_10times(n):
#     for i in range(10):
#         r = sum(list(range(n)))
# count_10times(99999)

# @ count_time
# def count_10timesP(n):
#     process_l = [Process(target = count_time,args=(n,))for i in range(10)]
#     for p in process_l:
#         p.start()
#         p.join()
# if __name__ == '__main__':
#     count_10timesP(99999)
#
# @count_time
# def count_10timesP1(n):
#     proc_pool = Pool(cpu_count())
#     for i in range(10):
#         proc_pool.apply_async(count_time,args = (n,))
#     proc_pool.close()
#     proc_pool.join()
# if __name__ == '__main__':
#     count_10timesP1(99999)
# 2.将第一题计算十次改为使用线程池去计算,线程池线程个数由你自己电脑cpu逻辑核数决定
# from multiprocessing.dummy import Pool as xPool
# from multiprocessing import cpu_count
# import time
# def count_time(func):
#     def inner(n):
#         time1 = time.time()
#         func(n)
#         time2 = time.time()
#         print('计算消耗时间为:%s' % str(time2-time1))
#     return inner
#
# @count_time
# def count_10timesXp(n):
#     pool = xPool(cpu_count())
#     for i in range(10):
#         pool.apply_async(count_time,args=(n,))
#     pool.close()
#     pool.join()
# count_10timesXp(99999)
