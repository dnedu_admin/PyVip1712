#1.自定义一个异常类, 当list内元素长度超过10的时候抛出异常
# class MyError(Exception):
#     def __init__(self,message):
#         self.message = message
#     def __str__(self):
#         return self.message
#
# def fun(x):
#     try:
#         if len(x) > 10:
#             raise MyError('列表元素太多了')
#     except Exception as e:
#         print(e)
# num_list = [1,2,3,4,5,6,7,8,9,10,11,55]
# fun(num_list)
# 2.
# 思考如果对于多种不同的代码异常情况都要处理, 又该如何去
# 处理, 自己写一个小例子
# try:
#     print(name)
#     print(1/0)
#     list = [1,2,3]
#     print(list[6])
#     open('jjjj')
# except (NameError,ZeroDivisionError,SyntaxError,IndexError) as e:
#     print(e)
# 3.
# try-except和try-finally有什么不同, 写例子理解区别
# try:
#     print(1/0)
# except Exception as e:
#     print(e)
# finally:
#     print('end')
# try:
#     print(1)
# except Exception as e:
#     print(e)
# finally:
#     print('end')
# 4.
# 写函数，检查传入字典的每一个value的长度，如果大于2，
# 那么仅仅保留前两个长度的内容，并将新内容返回给调用者
# dic = {“k1”: "v1v1", "k2": [11, 22, 33}}
# def func(a):
#     for k, v in a.items():
#         if isinstance(v,(str,list)):
#             if len(a[k])>2:
#                 a[k]=v[:2]
#     print(a)
#
# dic = {'k1': "v1v1","k2":[11,22,33]}
# func(dic)