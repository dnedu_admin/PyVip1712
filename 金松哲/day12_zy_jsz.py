# 1.
# 自己动手写一个学生类, 创建属性名字, 年龄, 性别, 完成
# __new__, __init__, __str__, __del__，__call__
# class Student():
#     def __init__(self,name,age,sex):
#         self.name = name
#         self.age = age
#         self.sex = sex
#         print('调用方法__init__')
#     def __call__(self, *args, **kwargs):
#         def work():
#             print('调用方法__call__')
#         return work
#     def __new__(cls, *args, **kwargs):
#         print('调用方法__new__')
#         return super().__new__(cls)
#     def __del__(self):
#         print('调用方法__del__')
#     def __str__(self):
#         return 'Student:%s,%s,%s'%(self.name,self.age,self.sex)
# s1 = Student('jsz',18,'man')
# s1()()
# print(s1)
# 2.创建父类Person,属性name,函数eat,run,创建子类Student,学生类
#   继承Person,属性name,age,id,函数eat,study,全局定义一个函数
#   get_name,传入一个Person类的实例参数,分别传入Person和Student
#   两个类的实例对象进行调用.
# class Person():
#     def __init__(self,name):
#         self.name = name
#     def eat(self):
#         print('人在吃饭')
#     def run(self):
#         print('人在跑步')
# class Student(Person):
#     def __init__(self,name,age,id):
#         self.name = name
#         self.age = age
#         self.id = id
#     def eat(self):
#         print('学生在吃饭')
#     def study(self):
#         print('学生在学习')
# p1 = Person('jsz')
# p2 = Student('jinsongzhe',18,1)
# def get_name(Person):
#     Person.eat()
#     Person.run()
# get_name(p1)
# get_name(p2)
#
# 3.
# 用代码完成python多态例子和鸭子模型
# class Animal():
#     def __init__(self,ability,speed):
#         self.ability = ability
#         self.speed = speed
#     def shoulie(self):
#         print('动物大部分可以进行狩猎')
#     def run(self):
#         print('动物大部分可以奔跑')
# class Dog(Animal):
#     def __init__(self,ablity,speed,name):
#         self.ability = ablity
#         self.speed = speed
#         self.name = name
#     def sajiao(self):
#         print('向主人撒娇')
# A1 = Animal('爬树','快')
# A2 = Dog('可爱','快','土豆')
# def show(Animal):
#     Animal.shoulie()
#     Animal.run()
# show(A1)
# show(A2)
# class Animal():
#     def __init__(self,ability,speed):
#         self.ability = ability
#         self.speed = speed
#     def shoulie(self):
#         print('动物大部分可以进行狩猎')
#     def run(self):
#         print('动物大部分可以奔跑')
# class Dog():
#     def __init__(self,ablity,speed,name):
#         self.ability = ablity
#         self.speed = speed
#         self.name = name
#     def shoulie(self):
#         print('狗大部分可以进行狩猎')
#     def run(self):
#         print('狗大部分可以奔跑')
#     def sajiao(self):
#         print('向主人撒娇')
# A1 = Animal('爬树','快')
# A2 = Dog('可爱','快','土豆')
# def show(Animal):
#     Animal.shoulie()
#     Animal.run()
# show(A1)
# show(A2)





