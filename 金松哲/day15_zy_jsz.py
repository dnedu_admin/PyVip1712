# 1.range(),需要生成下面的列表,需要填入什么参数:
#     1.[3,4,5,6]
#     2.[3,6,9,12,15,18]
#     3.[-20,200,420,640,860]
# list_1 = list(range(3,7))
# list_2 = list(range(3,19,3))
# list_3 = list(range(-20,861,220))
# 2.列表推导式或者生成器表达式(2选择1)完成1-100之间所有偶数和
# a_list = [x for x in range(1,101) if x%2 == 0]
# print(sum(a_list))
# 3.定义一个函数,使用关键字yield创建一个生成器,求1,3,5,7,9各数
# 字的三次方,并把所有的结果放入到list里面打印输入
# def func_gen(num):
#     while num <10:
#         yield num
#         num += 2
# gen = func_gen(1)
# z_list = []
# for i in gen:
#     z_list.append(i**3)
# print(z_list)

# 4.写一个自定义的迭代器类(iter)
# class MyIter():
#     def __init__(self,id):
#         self.id = id
#     def __next__(self):
#         if self.id == 0:
#             raise StopIteration('error')
#         self.id -= 2
#         return self.id
#     def __iter__(self):
#         return self
#
# iter_1 = MyIter(20)
# for i in iter_1:
#     print(i)
