# 1.用while和for两种循环求计算1+3+5.....+45+47+49的值
# s=0
# for n in range(1,50,2):
#     s += n
# print(s)
# n = 1
# sum = 0
# while n < 50:
#     sum += n
#     n += 2
# print(sum)

# 2.代码实现斐波那契数列(比如前30个项)
# n1 = 0
# n2 = 1
# s = 0
# print(n1)
# print(n2)
# n=1
# while n<29:
#     s = n1+n2
#     print(s)
#     n1, n2 = n2, s
#     n += 1
