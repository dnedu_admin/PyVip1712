# 1.a_list = [1,2,3,2,2]
#    删除a_list中所有的2
a_list = [1,2,3,2,2]
b_list = []
for x in a_list:
    if x !=2:
        b_list.append(x)
a_list = b_list
print(a_list)
# 2.用内置函数compile创建一个文件xx.py, 在文件里写你的名字, 然后用eval
# 函数读取文件中内容, 并打印输出到控制台'''
import os

current_path = os.getcwd()
file_name = "xx.py"
file_path = current_path + os.path.sep + file_name
with open(file_path,'w') as f:
    f.write('name = "jin_song_zhe"')
import xx
s_t_r = 'print(xx.name)'
a = compile(s_t_r, '', 'eval')
eval(a)
# 3.
# 写一个猜数字的游戏, 给5次机会, 每次随机生成一个整数, 然后由控制台输入一个
# 数字, 比较大小.大了提示猜大了, 小了提示猜小了.猜对了提示恭喜你, 猜对了.
# 退出程序, 如果猜错了, 一共给5次机会, 5次机会用完程序退出.
import random
num = random.randint(0,99)
i = 1
while i<6:
    user_num = int(input('请输入一个数字:'))
    if user_num < num:
        print('猜小了')
    elif user_num > num:
        print('猜大了')
    elif user_num == num:
        print('恭喜你猜对了！可以去买彩票了！！')
    i = i+1




