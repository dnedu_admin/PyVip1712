# 1.使用pickle,json(注意:两个模块)
# 把字典a_dict = [1,2,3,[4,5,6]]序列化与反序列化,序列化,
# 保存到文件名为serialize.txt文件里面,
# 然后大开,并读取数据反序列化为python对象加载到内存,
# 并打印输出尝试序列化/反序列非文件中,直接操作
# import pickle #encode
# a_dict = [1,2,3,[4,5,6]]
# f = open('serialize.txt','wb+')
# pickle.dump(a_dict,f,0)
# f.close()
#
# r = open('serialize.txt','rb+')
# b_dict = pickle.load(r)
# print(b_dict)
# f.close()

# import json #str
# a_dict = [1,2,3,[4,5,6]]
# f = open('serialize.txt','w+')
# json.dump(a_dict,f)
# f.close()
#
# r = open('serialize.txt','r+')
# b_dict = json.load(r)
# print(b_dict)
# f.close()


# 2.自己写代码对比深浅拷贝的区别,写完代码然后看看运行结果,
# 然后分析原因   比如：a_list=[1,2,3,4,[8,9,0]]
# from copy import copy,deepcopy
# a_list = [1,2,3,4,[8,9,0]]
# b_list = copy(a_list)
# c_list = deepcopy(a_list)
# d_list = a_list
# a_list[4][1] = 22
# print(b_list)
# print(c_list)
# print(d_list)
# 因为浅拷贝共享内层元素，深拷贝不共享内层元素


# 3.分别写代码完成内存缓冲区文本数据和二进制数据的读/写操作
# from io import StringIO
# f = StringIO()
# f.write('金松哲\n')
# f.write('jsz')
# print(f.getvalue())

# from io import StringIO
# f = StringIO('金松哲')
# conn = f.readline()
# print(conn.strip())

# from io import BytesIO
# f = BytesIO()
# f.write('金松哲'.encode('utf-8'))
# print(f.getvalue().decode('utf-8'))

# from io import BytesIO
# f = BytesIO('我爱python'.encode('utf-8'))
# r = f.readline()
# print(r.decode('utf-8'))


