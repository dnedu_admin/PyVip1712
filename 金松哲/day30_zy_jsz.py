# 1.基于tcp协议socket套接字编程半双工模式


# 服务器---------------------------------------------------------------------------
# import socket
#
# host = '192.168.31.166'
# port = 1000
# addr = (host,port)
# s1 = socket.socket()
# s1.bind(addr)
# s1.listen(10)
#
# conn,address = s1.accept()
# while 1:
#     content = conn.recv(1000)
#     if len(content) == 0:
#         print('接收到空数据')
#     else:
#         print('客户端发出来的数据%s'% content.decode('utf-8'))
#         result = input('请输入要返回给客户端的数据:')
#         conn.send(result.encode('utf-8'))
# conn.close()
# s1.close()


# 客户端-----------------------------------------------------------------------------
# import socket
#
# host = '192.168.31.166'
# port = 1000
# addr = (host,port)
# s1 = socket.socket()
# s1.connect(addr)
#
# while 1:
#     content = input('请输入发送给服务器的数据:')
#     s1.send(content.encode('utf-8'))
#     result = s1.recv(1000)
#     print('接受服务器返回的数据为:%s'%result.decode('utf-8'))
#     print('$'*100)
# s1.close()


# 2.socketserver异步非阻塞编程,编写代码


# 服务器-----------------------------------------------------------------------------
# import socketserver
# from socketserver import StreamRequestHandler as srh
#
# host = '192.168.31.166'
# port = 8888
#
# addr = (host,port)
#
# class Server(srh):
#
#     def handle(self):
#         print('连接到客户端的地址为%s' % str(self.client_address))
#         while 1:
#             content = self.request.recv(1024)
#             if not content:
#                 break
#             print('服务端接受的数据为%s'% content.decode('utf-8'))
#             result = input('请输入返回给客户端的数据:')
#             self.request.send(result.encode('utf-8'))
#
# print('开始处理请求')
# server = socketserver.ThreadingTCPServer(addr,Server)
# server.serve_forever()


# 客户端-----------------------------------------------------------------------------
# import socket
#
# host = '192.168.31.166'
# port = 8888
#
# addr = (host,port)
#
# s1 = socket.socket()
# s1.connect(addr)
#
# while 1:
#     content = input('请输入发送给服务器的数据为')
#     if not content or content == 'ex':
#         break
#     s1.send(content.encode('utf-8'))
#     result = s1.recv(1024)
#     if not result:
#         break
#     print('服务器返回的数据为%s'% result.decode('utf-8'))
#
# s1.close()