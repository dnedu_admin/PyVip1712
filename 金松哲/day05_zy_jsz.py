# 1.自己写一个字典,添加，删除，更新，清空操作
name_dict = {'name':'jsz','age':'23','sex':'男'}
name_dict['addr'] = '延吉'
print(name_dict)
name_dict.pop('age')
print(name_dict)
name_dict.popitem()
print(name_dict)
new_dict = {'ability':'korean','appearance':'hansome'}
name_dict.update(new_dict)
print(name_dict)
name_dict.clear()
print(name_dict)

# 2.写两个集合，并分别进行&,|,^,- 运算
num1_set = {3,5,1,2,7}
num2_set = {1,2,3,11}
print(num1_set&num2_set)
print(num1_set|num2_set)
print(num1_set^num2_set)
print(num1_set-num2_set)

# 3. 将整数字典num_dict按照dict中value从小到大排序
num_dict = {'a':13,'b':22,'c':18,'d':24}
new_list=[]
for value in num_dict.values():
    new_list.append(value)
new_list.sort()
print(new_list)
finished_dict = {}
i=0
while i<4:
    for key,value in num_dict.items():
        d = {key:value}
        if value == new_list[i]:
            finished_dict.update(d)
            i=i+1
        else:
            i=i+0
print(finished_dict)







