# 主要是练习装饰器,和多线程,多进程的使用
# 使用装饰器增强完成1-999999所有奇数和计算10次
# 1.使用普通方法
# 2.使用多线程
# 3.使用多进程
# 4.采用线程池完成计算
# 5.采用进程池完成计算
# from multiprocessing import Process,cpu_count,Pool
# from multiprocessing.dummy import Pool as TPool
# from threading import Thread
# import time
#
# def zsq(func):
#     def inner(num):
#         time1 = time.time()
#         func(num)
#         time2 = time.time()
#         print('函数%s所消耗时间为%s' % (func.__name__,str(time2-time1)))
#     return inner
#
# @ zsq
# def nomalfunc(num):
#     for i in range(10):
#         r = sum(list(range(1,num,2)))
#
#
# def zero_func(num):
#     sum(range(1,num,2))
#
# @ zsq
# def djc_func(num):
#     for i in range(10):
#         jc = Process(target=zero_func,args=(num,))
#         jc.start()
#         jc.join()
#
# @ zsq
# def dxc_func(num):
#     dxc_list = [Thread(target=zero_func,args=(num,))for i in range(10)]
#     for xc in dxc_list:
#         xc.start()
#         xc.join()
#
#
# @ zsq
# def jcpool_func(num):
#     jc_pool = Pool(cpu_count())
#     for i in range(10):
#         jc_pool.apply_async(zero_func,args=(num,))
#     jc_pool.close()
#     jc_pool.join()
#
#
# @ zsq
# def xcpool_func(num):
#     xc_pool = TPool(cpu_count())
#     for i in range(10):
#         xc_pool.apply_async(zero_func,args=(num,))
#     xc_pool.close()
#     xc_pool.join()
#
#
# if __name__ == '__main__':
#     nomalfunc(999999)
#     djc_func(999999)
#     dxc_func(999999)
#     jcpool_func(999999)
#     xcpool_func(999999)










