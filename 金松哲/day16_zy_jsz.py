# 1.写一个函数计算1 + 3 + 5 +...+97 + 99的结果
# 再写一个装饰器函数, 对其装饰, 在运算之前, 新建一个文件  然后结算结果, 最后把计算的结果写入到文件里
#
# def zsq_func(func):
#     f = open('jsz.py','w+',encoding='utf-8')
#     print('这是装饰器')
#     b = func()
#     f.write(str(b))
#
# @ zsq_func
# def func():
#     a = sum(range(1,100,2))
#     print(a)
#     return a
# func

# 2.写一个函数计算传入进来参数的平方, 并将结果.写一个带参数的装饰器,
#   将装饰器参数传入到被包装的函数,计算并输入结果
# def zsq_func(func):
#     def inner(x):
#         print('开始')
#         y = func(x)
#         print('结束')
#         return y
#     return inner
# @zsq_func
# def fun(c):
#     cc = c**2
#     return cc
# m = fun(5)
# print(m)