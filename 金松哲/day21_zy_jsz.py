# 1.识别下面字符串:’ben’,’hit’或者’ hut’
# import re
# print(re.findall('\sben',' ben'))
# print(re.findall('\shit',' hit'))
# print(re.findall('\s\shut','  hut'))
# 2.匹配用一个空格分隔的任意一对单词,比如你的姓名
# print(re.findall('jin\ssong\szhe','jin song zhe'))
# 3.匹配用一个逗号和一个空格分开的一个单词和一个字母
# print(re.findall('song\Wzhe','song,zhe'))
# print(re.findall('j\ss\sz','j s z'))
# 4.匹配简单的以’www’开头,以’com’结尾的web域名,比如:www.baidu.com
# print(re.findall('w+.baidu.com','www.baidu.com'))
# print(re.findall('.+','www.baidu.com'))