# 1. 爬取以下站点中明星图片，各明星图片单独建文件夹存放。
# 起始URL地址：http://www.mm131.com/mingxing
# 提交作业代码 上传gitee
import os
import time
import requests
from bs4 import BeautifulSoup
import lxml

headers = {'User-Agent':'Mozilla/5.0 (Windows NT 6.1; WOW64)\
AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36 QIHU 360SE',
           'Referer':'http://www.mm131.com/mingxing'}

s_url = 'http://www.mm131.com/mingxing'
s_html = requests.get(s_url,headers=headers)

image_soup = BeautifulSoup(s_html.text,'lxml').find('dl',class_='list-left public-box').find_all('a')

for i in image_soup[2:22]:
    a_text = i.get_text()
    jpg_url = i['href']
    jpg_html = requests.get(jpg_url,headers=headers)
    jpg_html_soup = BeautifulSoup(jpg_html.text,'lxml')
    name = jpg_url.split('/')[-1].split('.')[0] + '.jpg'
    os.mkdir(name+' dir')
    name_dir = os.path.join(name+' dir',name)
    with open(name+' dir'+'/'+name,'ab') as f:
        f.write(jpg_html.content)




