# 1.爬取url = 'http://tieba.baidu.com/p/2460150866'前三页所有的图片
url1 = 'http://tieba.baidu.com/p/2460150866?pn=1'
url2 = 'http://tieba.baidu.com/p/2460150866?pn=2'
url3 = 'http://tieba.baidu.com/p/2460150866?pn=3'
import requests
from urllib.request import urlretrieve
import re

def get_code(url):
    try:
        page = requests.get(url)
        return page.text
    except Exception as e:
        print(e)

# <img pic_type="0" class="BDE_Image" src="https://imgsa.baidu.com/forum/w%3D580/sign=294db374d462853592e0d229a0ee76f2/e732c895d143ad4b630e8f4683025aafa40f0611.jpg" pic_ext="bmp" height="328" width="560">
# <img pic_type="0" class="BDE_Image" src="https://imgsa.baidu.com/forum/w%3D580/sign=941c6a9596dda144da096cba82b6d009/e889d43f8794a4c2e5d529ad0ff41bd5ac6e3947.jpg" pic_ext="jpeg" height="350" width="560">
def get_image(code,n):
    try:
        reg = r'src="(.+?\.jpg)" pic_ext'
        image = re.compile(reg)
        image_list = re.findall(image,code)
        for u in image_list:
            urlretrieve(u,'./image/'+'%s.jpg'%n)
            n += 1
        return n
    except Exception as e:
        print(e)
if __name__ =='__main__':
    html1 = get_code(url1)
    x1 = get_image(html1,1)
    html2 = get_code(url2)
    x2 = get_image(html2,x1)
    html3 = get_code(url3)
    get_image(html3,x2)
    print('done')