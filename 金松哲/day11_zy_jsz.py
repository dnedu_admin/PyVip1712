# 1.文件名称:use.txt
#    创建该文件,路径由你自己指定,打开该文件,往里面写入
#    str_content = ['tom is boy','\n','you are cool','\n',
#                        '今天你怎么还不回来']
#    把数据写入到该文件中,注意操作的模式,关闭文件句柄后
#    再次打开该文件句柄,然后读取文件中内容,用with代码块
#    实现,然后打印输出信息到控制台,注意换行符
# f=open('use.txt','w',encoding='utf-8')
# str_content = ['tom is boy','\n','you are cool','\n',
#                        '今天你怎么还不回来']
# f.writelines(str_content)
# f.close()
# with open('use.txt','r',encoding='utf-8') as t:
#     print(t.read())

# 2.with创建一个文件homework.txt,尝试多种操作模式.进行写读操作,注意区别
# with open('homework.txt','w',encoding='utf-8') as f:
#     s=['tom is boy','\n','hello']
#     f.writelines(s)
# with open('homework.txt', 'r', encoding='utf-8') as w:
#     w.seek(0)
#     print(w.read())

# another way
# with open('homework.txt','a+',encoding='utf-8') as f:
#     s=['tom is boy','\n','hello','\n']
#     f.writelines(s)
#     f.seek(0)
#     print(f.read())

# 3.理解文件句柄,对比read(),readline(),readlines()写一个小例子
# with open('_1.py','r',encoding='utf-8') as f:
#     print(f.read(166))

# with open('_1.py','r',encoding='utf-8') as f:
#     print(f.readline(100))
#
# with open('_1.py','r',encoding='utf-8') as f:
#     print(f.readlines())

# 4.准备一张jpg图片, 把图片中数据读书出来, 并写入到文件my_img.jpg文件中,
# 操作完毕后尝试打开my_img.jpg文件看图片显示正不正常
# with open('a.jpg','r',encoding='utf-8') as f:
#     b=f.read()
# with open('my_img.jpg', 'w+', encoding='utf-8') as w:
#     print(w.write(b))
#