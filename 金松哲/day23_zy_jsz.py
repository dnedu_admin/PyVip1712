# 作业:
# 1.datetime, 获取你当前系统时间三天前的时间戳;
# 并把当前时间戳增加899999, 然后转换为日期时间对象,
# 把日期时间并按照 % Y - % m - % d % H: % M: % S
# 输入打印这个日期对象, 并获取date属性和time属性, 最后判断当前日期时间
# 对象是星期几;

# from datetime import datetime,timedelta,date,time
# datetime1 = datetime.now()
# datetime2 = datetime1 - timedelta(days=3)
# timestamp1 = datetime2.timestamp()
# timestamp1 += 899999
# datetime3 = datetime.fromtimestamp(timestamp1)
# print(datetime3)
# date_str = datetime.strftime(datetime3,'%Y-%m-%d %H:%M:%S')
# date1 = datetime3.date()
# print(date1)
# time1 = datetime3.time()
# print(time1)
# print(datetime3.weekday())


# 2.使用os模块线获取当前工作目录的路径, 并打印当前目录下所有目录和文件,
# 获取当前脚本的绝对路径, 分割当前脚本文件的目录和文件在tuple中
# 并判断当前文件所在的目录下存不存在文件game.py,
# 如果不存在则创建该文件.并往该文件里写入数据信息,
# 然后判断当前目录下是否存在目录/ wow / temp /,
# 如果不存在则创建, 最后把目录名称改为: / wow / map /
import os
# from os import path
# a = os.getcwd()
# print(a)
# b = os.listdir()
# print(b)
# c = path.abspath('./day23_zy_jsz.py')
# print(c)
# print(path.split('./day23_zy_jsz.py'))
# print(path.exists('./game.py'))
# f = open('game.py','w+',encoding='utf-8')
# f.write('hi python!')
# f.close()
# print(path.exists('/wow/temp/'))
# os.makedirs('./wow/temp')
# os.renames('./wow/temp','./wow/map/')




#3.写函数，检查获取传入列表或元组对象的所有奇数位索引对应的元素，
# 并将其作为新的列表返回给调用者
# def func(lt):
#     new_list = []
#     n = 0
#     if isinstance(lt,(list,tuple)):
#         for i in lt:
#             if n %2 != 0:
#                 new_list.append(i)
#             n += 1
#     else:
#         raise TypeError('请传入元组或者列表！！')
#     return new_list
# x1 = [1,2,3,4,5,6,7,8,9]
# x2 = (1,2,3,4,5,6,7,8,9)
# x3 = '123456789'
# a1 = func(x1)
# a2 = func(x2)
# print(a1)
# print(a2)
# func(x3)


