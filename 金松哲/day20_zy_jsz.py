# 创建两个协程函数对象, 分别完成1, 3, 5…99, 2, 4, 6…, 100
# 然后让两个协程函数对象分别交替执行.在每一个协程函数对象的内部先打印输出
# 每一个数字, 然后执行完一轮之后打印提示信息, 已经交替执行完毕一轮
# 等所有的协程函数对象执行完毕之后, 分析一下数据信息
#
# from types import coroutine
# @coroutine
# def x0_func():
#     yield
#
# async def x1_func():
#     for i in range(1,100,2):
#         print(i)
#         await x0_func()
#         print('已经交替执行')
#
# async def x2_func():
#     for i in range(2,101,2):
#         print(i)
#         await x0_func()
#         print('已经交替执行')
#
# def manager(a_list):
#     b_list = list(a_list)
#     while b_list:
#         for i in b_list:
#             try:
#                 i.send(None)
#             except StopIteration as e:
#                 print(e)
#                 b_list.remove(i)
#
# if __name__ == '__main__':
#     x1 = x1_func()
#     x2 = x2_func()
#     c_list = []
#     c_list.append(x1)
#     c_list.append(x2)
#     manager(c_list)