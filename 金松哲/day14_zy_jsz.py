# 1.filter判断[0,False,True,5,{},(2,3)]对应bool值
# print(list(filter(lambda x:bool(x)== True,[0,False,True,5,{},(2,3)])))
# 2.map实现1-10中所有奇数项的三次方,打印输出结果
# def func(x):
#     if x%2 == 1:
#         x = x**3
#         return x
# print(list(map(func,range(1,11))))
# 3.reduce实现1-100所有偶数项的和
# from functools import reduce
# z_list = []
# for i in range(1,101):
#     if i % 2 == 0:
#         z_list.append(i)
# print(reduce(lambda x,y:x+y,z_list))
# 4.用递归函数实现斐波拉契数列
# def digui_func(n):
#     if n <= 1:
#         return n
#     else:
#         return (digui_func(n-1) + digui_func(n-2))
# for i in range(10):
#     print(digui_func(i))
