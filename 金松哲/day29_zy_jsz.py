# 1.函数缓存的使用
from functools import lru_cache
# @lru_cache(maxsize=10,typed=False)
# def myfunc(n):
#     a = [0, 1]
#     for i in range(n):
#         b = a[-1]+a[-2]
#         a.append(b)
#         # print(a)
#     for i in a:
#         print(i)
# myfunc(80)

# 2.性能分析器的使用
# import cProfile
# def myfunc(n):
#     a = [0, 1]
#     for i in range(n):
#         b = a[-1]+a[-2]
#         a.append(b)
#     for i in a:
#         print(i)
# cProfile.run('myfunc(50)')
# from line_profiler import LineProfiler
# def myfunc(n):
#     a = [0, 1]
#     for i in range(n):
#         b = a[-1]+a[-2]
#         a.append(b)
#         # print(a)
#     for i in a:
#         print(i)
#
# prof = LineProfiler(myfunc)
# prof.enable()
# myfunc(5000)
# prof.disable()
# prof.print_stats()

# 3.内存分析器的使用
# from memory_profiler import profile
# @profile
# def myfunc():
#     a = '99'*pow(2,20)
#     b = 'qwe'*pow(2,15)
#     del a
#     del b
#
# if __name__ == '__main__':
#     myfunc()