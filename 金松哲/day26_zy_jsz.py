# 1. PKCS#5(选择一种hash算法)加密算法加密你的中文名字,写出代码
# import hashlib,binascii
# name = '金松哲'.encode('utf-8')
# p1 = hashlib.pbkdf2_hmac('md5',b'jsz',name,100,dklen=10)
# print(p1)
# r1 = binascii.hexlify(p1)
# print(r1.decode('utf-8'))

# 2. 创建文件test.py,写入一些数据,并计算该文件的md5值
# import hashlib
# with open('test.py','w',encoding='utf-8') as f:
#     f.write('i love python')
# with open('test.py','r',encoding='utf-8') as f1:
#     r2 = hashlib.md5(f1.read().encode('utf-8'))
#     print(r2)
#     print(r2.hexdigest())
# 3. 遵循上下文管理协议,自己编写一个类,去创建一个上下文管理器,然后自己使用看看
# 方式1:普通方式
# 方式2:使用contextmanager
# class Jszclass():
#     def __init__(self,v):
#         print('先执行INIT')
#         self.v = v
#     def __enter__(self):
#         print('执行代码')
#         return self
#     def __exit__(self, exc_type, exc_val, exc_tb):
#         self.v = None
#         if exc_type:
#             print('发生异常')
#         else:
#             print('运行正常')
#     def work(self):
#         print(self.v)
# with Jszclass('python') as f:
#     f.work()
#
# from contextlib import contextmanager
# class Jszclass1():
#     def __init__(self, v):
#         print('初始化数据')
#         self.v = v
#
#     def work(self):
#         print(self.v)
# @contextmanager
# def func(v):
#     print('开始处理')
#     p1 = Jszclass1(v)
#     yield p1
#     print('处理完毕')
#
# with func('Python') as w:
#     w.work()

# 4.使用colorama模块使用多颜色输出
# from colorama import Fore,Style,Back
#
# print(Fore.BLUE + 'python')
# print(Back.CYAN + 'i am jsz')
# print(Style.BRIGHT + 'i love python')
# print(Style.RESET_ALL)
# print('its normal')

