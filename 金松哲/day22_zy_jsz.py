# 1.使用正则表达式匹配电话号码：0713-xxxxxx(湖南省座机号码)
# import re
# p = re.compile('0713-\d{6}')
# print(p.search('4515-0713-154215444erq0713-48181555555').group())
# 2.区号中可以包含()或者-,而且是可选的,就是说你写的正则表达式可以匹配800-555-1212,555-1212,(800)555-1212
# import re
# p1 = re.compile('\d{3}-\d{3}-\d{4}')
# p2 = re.compile('\d{3}-\d{4}')
# p3 = re.compile('\(\d{3}\)\d{3}-\d{4}')
# str_str = '1541(800)555-1212-e344800-555-4545'
# p_list = [p1,p2,p3]
# for i in range(3):
#     if p_list[i].search(str_str):
#         print(p_list[i].search(str_str).group())
