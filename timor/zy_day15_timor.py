# 1.range(),需要生成下面的列表,需要填入什么参数:
#     1.[3,4,5,6]
#     2.[3,6,9,12,15,18]
#     3.[-20,200,420,640,860]
print(list(range(3,7,1)))
print(list(range(3,19,3)))
print(list(range(-20,870,220)))

#2.列表推导式或者生成器表达式(2选择1)完成1-100之间所有偶数和
from functools import reduce
print(reduce(lambda x,y:x+y,[x for x in range(2,101,2)]))


#3.定义一个函数,使用关键字yield创建一个生成器,求1,3,5,7,9各数字的三次方,并把所有的结果放入到list里面打印输入
cf_list=[]
def c_f(num):
    # cf_1 = (list(range(1,10,2)))
    # for x in cf_1:
    while num < 0:
       yield num
       num -=2

cf_1=c_f(9)
for num in cf_1:
    num=num**3
    cf_list.append(num)
print(cf_list)

