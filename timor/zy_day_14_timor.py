#1.1 高阶函数:filter判断[0,False,True,5,{},(2,3)]对应bool值

bool_1=list(filter(bool,[0,False,True,5,{},(2,3)]))
print(bool_1)

#1.2 map实现1-10中所有奇数项的三次方,打印输出结果

age_filter=filter(lambda x:x%2==0,[1,2,3,4,5,6,7,8,9,10])
print(list(map(lambda x:x**3,age_filter)))

#1.3  reduce实现1-100所有偶数项的和

from functools import reduce
def add(x,y):
    return x+y
oushu_sun =reduce(add,list(filter(lambda x:x%2==0,range(1,101))))
print(oushu_sun)



# 2.用递归函数实现斐波拉契数列

lis = []
for i in range(int(input("请输入一个正数"))):
    if i == 0 or i == 1:  # 第1,2项 都为1
        lis.append(1)
    else:
        lis.append(lis[i - 2] + lis[i - 1])  # 从第3项开始每项值为前两项值之和
print(lis)