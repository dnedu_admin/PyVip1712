#1.计算range(1,99999)的总和,采取三种方式,
# 第一:重复10次计算总时间;
# 第二:开启十个子线程去计算;
# 第三:开启十个进程去计算,然后对比耗时
#
import time
stat=time.time()
sun_1=sum(range(1,99999))*10
end=time.time()
print(end - stat)


# from threading import Thread
# import time
# start=time.time()
# sum_1=0
# def sum_zhu(x):
#     global sum_1
#     sum_1 = sum(range(x))
# sum1_1=Thread(target=sum_zhu,args=(99999,))
# sum1_1.start()
# sum1_1.join()
# sum1_2=Thread(target=sum_zhu,args=(99999,))
# sum1_2.start()
# sum1_2.join()
# sum1_3=Thread(target=sum_zhu,args=(99999,))
# sum1_3.start()
# sum1_3.join()
# sum1_4=Thread(target=sum_zhu,args=(99999,))
# sum1_4.start()
# sum1_4.join()
# sum1_5=Thread(target=sum_zhu,args=(99999,))
# sum1_5.start()
# sum1_5.join()
# sum1_6=Thread(target=sum_zhu,args=(99999,))
# sum1_6.start()
# sum1_6.join()
# sum1_7=Thread(target=sum_zhu,args=(99999,))
# sum1_7.start()
# sum1_7.join()
# sum1_8=Thread(target=sum_zhu,args=(99999,))
# sum1_8.start()
# sum1_8.join()
# sum1_9=Thread(target=sum_zhu,args=(99999,))
# sum1_9.start()
# sum1_9.join()
# sum1_9=Thread(target=sum_zhu,args=(99999,))
# sum1_9.start()
# sum1_9.join()
# sum1_10=Thread(target=sum_zhu,args=(99999,))
# sum1_10.start()
# sum1_10.join()
# end=time.time()
# print(end-start)


#
# from multiprocessing import Process
# import time
# start=time.time()
# sum_=0
# def sum_zhu(x):
#     global sum_
#     sum_ = sum(range(x))
# if __name__ == '__main__':
#     print(sum_)
#     sum_1 = Process(target=sum_zhu, args=(99999,))
#     sum_1.start()
#     sum_1.join()
#     sum_2 = Process(target=sum_zhu, args=(99999,))
#     sum_2.start()
#     sum_2.join()
#     sum_3 = Process(target=sum_zhu, args=(99999,))
#     sum_3.start()
#     sum_3.join()
#     sum_4 = Process(target=sum_zhu, args=(99999,))
#     sum_4.start()
#     sum_4.join()
#     sum_5 = Process(target=sum_zhu, args=(99999,))
#     sum_5.start()
#     sum_5.join()
#     sum_6 = Process(target=sum_zhu, args=(99999,))
#     sum_6.start()
#     sum_6.join()
#     sum_7 = Process(target=sum_zhu, args=(99999,))
#     sum_7.start()
#     sum_7.join()
#     sum_8 = Process(target=sum_zhu, args=(99999,))
#     sum_8.start()
#     sum_8.join()
#     sum_9 = Process(target=sum_zhu, args=(99999,))
#     sum_9.start()
#     sum_9.join()
#     sum_10 = Process(target=sum_zhu, args=(99999,))
#     sum_10.start()
#     sum_10.join()
#
# end=time.time()
# print(end-start)

#2.将第一题计算十次改为使用线程池去计算,线程池线程个数由你自己电脑cpu逻辑核数决定

# from multiprocessing import Process,Pool
# import time
# start=time.time()
# num=0
# def sum_zhu(args):
#     global num
#     num=sum(range(args))
# if __name__ == '__main__':
#     p0 = Pool(2)
#     for i in  range(10):
#         p0.apply_async(func=sum_zhu, args=(99999,))
#     p0.close()
#     p0.join()
# end=time.time()
# print(end-start)