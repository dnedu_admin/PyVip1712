# coding:utf-8
# Funny (qq:516110288)

#1.uuid 唯一码
#2.shutil 文件操作open函数
#3.colections 集合工具类
#4.base64 明文--加密
#5.itertools 迭代器

import requests
from bs4 import BeautifulSoup
import lxml
from urllib.request import urlretrieve

def get_url(url):
    try:
        r=requests.get(url)
        html=r.text
        return html
    except Exception as e:
        print(e)


#<img src="https://img3.doubanio.com/lpic/s28891775.jpg" alt="Python编程：从入门到实践 : 从入门到实践" class="cover">
#<img src="https://img3.doubanio.com/lpic/s27262723.jpg" alt="Head First Python（中文版） : Head First Python" class="cover">
#<img src="https://img3.doubanio.com/lpic/s4676930.jpg" alt="Python灰帽子 : 黑客与逆向工程师的Python编程之道" class="cover">
def get_img(html,page):
    soup=BeautifulSoup(html,"lxml")
    img_list=soup.find_all("img","src")
    n=1
    for img in img_list:
        url=img.get("src")
        name="./ok123/%d页-%d.jpg" %page
        urlretrieve(url,name)
        n+=1
# https://book.douban.com/subject_search?search_text=python&cat=1001
# https://book.douban.com/subject_search?search_text=python&cat=1001&start=15
# https://book.douban.com/subject_search?search_text=python&cat=1001&start=30
def get_all_img():
    try:
        for page in range(1,4):
            url=url_1+"cat=%d&start=%d" %page
            print(url)
            html=get_html(url)
            get_img(html,page)
    except Exception as e:
        print(e)

if __name__=="__main__":
    url_1 = "https://book.douban.com/subject_search?search_text=python&cat=1001"
    get_all_img()
    print("ok")

