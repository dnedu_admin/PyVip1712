# coding:utf-8
# Funny (qq:516110288)

"""
    1.函数缓存的使用
    2.性能分析器的使用
    3.内存分析器的使用
"""

from functools import lru_cache
@lru_cache(maxsize=10, typed=False)
def lib(n):
    if n < 2:
        return n
    return lib(n - 1) + lib(n - 2)
from time import time
def count_time(num):
    t1 = time()
    [lib(n) for n in range(num)]
    t2 = time()
    print(t2 - t1)
count_time(10)
print(lib.cache_info())

import cProfile

def fib(n): #
    if n <2:   #n=0  return 0   n=1 1
        return n
    return fib(n-1) + fib(n-2)
cProfile.run('fib(35)')  # run函数:注意传递的参数为字符串

from line_profiler import LineProfiler
from time import time

def fib(n): #
    if n <2:   #n=0  return 0   n=1 1
        return n
    return fib(n-1) + fib(n-2)

def count_time(num):
    t1 = time()
    [fib(n) for n in range(num)]  # 这行代码太耗时
    t2 = time()
    print(t2 - t1)

# 首先构造一个LineProfiler对象
prof = LineProfiler(count_time)  # 函数对象
# 打开开关
prof.enable()
# 执行函数
count_time(25)
# 关闭开关
prof.disable()
# 查看信息:
prof.print_stats()

