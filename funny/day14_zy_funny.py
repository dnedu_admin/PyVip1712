# coding:utf-8
# Funny (qq:516110288)

# 1.filter判断[0,False,True,5,{},(2,3)]对应bool值
def abc():
    value = [0, False, True, 5, {}, (2, 3)]
    a_filter = filter(lambda x: x, value)
    print(list(value))
    print(abc)


if __name__ == "__main__":
    abc()


# 2.map实现1-10中所有奇数项的三次方,打印输出结果
def abc1():
    a_abc1 = map(lambda x: x ** 3, range(1, 10, 2))
    print(list(a_abc1))


if __name__ == "__main__":
    abc1()

    # 3.reduce实现1-100所有偶数项的和
    from functools import reduce


# print(reduce(lambda x, y: x + y, range(0, 101, 2)))
def abc2():
    a_abc2 = reduce(lambda x, y: x + y, range(2, 101, 2))
    print(a_abc2)


if __name__ == "__main__":
    abc2()


# 4.用递归函数实现斐波拉契数列
def digui(n):
    if isinstance(n, int):
        if n <= 1:
            return 1
        else:
            return digui(n - 1) + digui(n - 2)
    else:
        raise TypeError('传入的参数错误')


def abc3(a):
    a_map = map(digui, range(1, a + 1))
    n_list = list(a_map)
    print(n_list)

if __name__ == "__main__":
    abc3(5)