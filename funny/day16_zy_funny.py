# coding:utf-8
# Funny (qq:516110288)

# 1.使用pickle,json(注意:两个模块)
# 把字典a_dict = [1,2,3,[4,5,6]]
# 序列化与反序列化,序列化,保存到文件名为serialize.txt文件里面,然后大开,并读取数据
# 反序列化为python对象加载到内存,并打印输出
# 尝试序列化/反序列非文件中,直接操作
import pickle

a_dict = [1, 2, 3, [4, 5, 6]]
f = open("serialize.txt", "wb")
pickle.dump(a_dict, f)
f.close()

f = open("serialize.txt", "rb")
content = pickle.load(f)
print(content)
f.close()
import json

a_dict = [1, 2, 3, [4, 5, 6]]
f = open("serialize.txt", "w")
json.dump(a_dict, f)
f.close()

f = open("serialize.txt", "r")
content = json.load(f)
print(content)
f.close()
# 2.自己写代码对比深浅拷贝的区别,写完代码然后看看运行结果,然后分析原因
#    比如：a_list=[1,2,3,4,[8,9,0]]
from copy import copy
a = [1, 2, 3, 4, [8, 9, 0]]
b = list(a)
c = copy(a)
print(a)
print(id(a))
print(b)
print(id(b))
print(c)
print(id(c))
# 3.分别写 代码完成内存缓冲区文本数据和二进制数据的读/写操作
f = open("day16.py", "r", encoding="utf-8")
ff = f.read()
print(ff)
f.close()

f = open("day16.py","w",encoding="utf-8")
f.write("123")
f.close()