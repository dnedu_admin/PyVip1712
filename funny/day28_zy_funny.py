# coding:utf-8
# Funny (qq:516110288)


#1.写一个函数,然后写单元测试
import unittest
from myfun import run

class Mytest(unittest.TestCase):
    def setUp(self):
        pass
    def tearDown(self):
        pass

    def testrun(self):
        self.assertEquals(run(5),25,"测run函数失败")
if __name__=="__main__":
    unittest.main()
#2.自定义一个类,完成单元测试

from myfun import Person
class MyClasTest(unittest.TestCase):
    def setUp(self):
        self.p=Person("tom")
    def testrun(self):
        self.p.run()
    def tearDown(self):
        self.p=None
if __name__=="__main__":
    unittest.main()
