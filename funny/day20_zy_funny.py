# coding:utf-8
# Funny (qq:516110288)

# 创建两个协程函数对象, 分别完成1, 3, 5…99, 2, 4, 6…, 100
# 然后让两个协程函数对象分别交替执行.在每一个协程函数对象的内部先打印输出
# 每一个数字, 然后执行完一轮之后打印提示信息, 已经交替执行完毕一轮
# 等所有的协程函数对象执行完毕之后, 分析一下数据信息

from types import coroutine


@coroutine
def do_nex():
    yield


async def run():
    for i in range(1, 100, 2):
        print(i)
        await do_nex()


async def run_1():
    for i in range(1, 101, 2):
        print(i)
        await do_nex()


def run_func(a_list):
    b_list = list(a_list)
    while b_list:
        for item in b_list:
            try:
                item.send(None)
            except StopIteration as e:
                print(e)
                b_list.remove(item)


if __name__ == "__main__":
    c_list = []
    one = run()
    two = run_1()
    c_list.append(one)
    c_list.append(two)
    run_func(c_list)
