# coding:utf-8
# Funny (qq:516110288)


# 1. PKCS#5(选择一种hash算法)加密算法加密你的中文名字,写出代码
import hashlib
#md5
m=hashlib.md5()
m.update("funny".encode("utf-8"))
print(m.hexdigest())
#sha1
n=hashlib.sha1()
n.update("funny".encode("utf-8"))
print(n.hexdigest())
# 2. 创建文件test.py,写入一些数据,并计算该文件的md5值
with open("test.py","w",encoding="utf-8")as f:
    f.write("my name is funny")
with open("test.py","rb")as f2:
    ff=hashlib.md5(f2.read())
    print(ff.hexdigest())
# 3. 遵循上下文管理协议,自己编写一个类,去创建一个上下文管理器,然后自己使用看看
#    方式1:普通方式
class abc():
    def __init__(self,a_list):
        print("初始化数据")
        self.a_list=a_list
#实现俩个核心方法
    def __enter__(self): #进入上下文，可以执行某些逻辑
        print("执行代码")
        self.a_list.append("funny")
        return self
    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_tb:
            print("触发异常")
        else:
            print("正常状态")
            print("结束")
    def work(self):
        for item in self.a_list:
            print(item)
with abc(["a","b","c","d"])as f:
    f.work()
#    方式2:使用contextmanager
from contextlib import contextmanager
class abd():
    def __init__(self,b_list):
        print("初始化数据")
        self.b_list=b_list
    def work(self):
        for item in self.b_list:
            print(item)
@contextmanager
def abf(b_list):
    print("开始处理")
    ab=abc(b_list)
    yield ab
    print("处理完毕")
    print("结束")
with abf(["a","b","c","d"])as f1:
    f1.work()



# 4.使用colorama模块使用多颜色输出
from colorama import Fore,Back,Style
print(Fore.BLUE+"funny")
print(Fore.GREEN+"funny")
print(Back.GREEN+"funny")
print(Back.BLUE+"funny")
print(Style.BRIGHT+"funny")
print(Style.DIM+"funny")