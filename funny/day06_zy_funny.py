# coding:utf-8
# Funny (qq:516110288)
# 1.用while和for两种循环求计算1+3+5.....+45+47+49的值
result = 0
num = 49
while num > 0:
    result += num
    num -= 2
print(result)

result = 0
for n in range(1,50,2):
    result += n
print(result)

# 2.代码实现斐波那契数列(比如前30个项)

m, k = 0, 1
while k < 30:
    print(k)
    m,k = k, m+k