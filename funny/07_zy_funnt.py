# coding:utf-8
# Funny (qq:516110288)

#1.简述普通参数、默认参数、关键字参数、动态收集参数的区别,理解为主

# def func(args): print(args)
# def print_hello(name, sex=1):
# print_hello('tanggu', sex=1)
# def func(*args):
#     print(args)
# func(123456,456,456,456)

#2
def foo(x, y, z, *args, **kw):
    sum = x + y + z
    print(sum)
    for i in args:
        print(i)
    print(kw)
    for j in kw.items():
        print(j)
a_tuple = (1,2,3,"abc")     #此处参数修改为2个看看会怎么样?
b_dict = {'name': 'jim', 'age': 28, 'add': '上海'}
foo(*a_tuple, **b_dict)   # 分析这里会怎么样?
# # print(**a_dict)  # 调用函数
# work(**a_dict)   #  先解包


# 3.题目:执行分析下代码
def func(a, b, c=9, *args, **kw):
    print('a =', a, 'b =', b, 'c =', c, 'args =', args, 'kw =', kw)
func(1,2)
func(1,2,3)
func(1,2,3,4)
func(1,2,3,4,5)
func(1,2,3,4,5,6,name='jim')
func(1,2,3,4,5,6,name='tom',age=22)
   # 扩展: 如果把你的函数也定义成  def  get_sum(*args,**kw):pass
    #你的函数可以接受多少参数?
# def func(a, b, c=9, *args, **kw):pass
# print('a =', a, 'b =', b, 'c =', c, 'args =', args, 'kw =', kw)
# func(1,2)
# func(1,2,3)
# func(1,2,3,4)
# func(1,2,3,4,5)
# func(1,2,3,4,5,6,name='jim')
# func(1,2,3,4,5,6,name='tom',age=22)

# 4.写一个函数函数，计算传入字符串中的数字、字母、空格和其他的个数分别为多少?

def func1(s):
    al_num = 0
    spance_num = 0
    digit_num = 0
    others_num = 0
    for i in s:
        if i.isdigit():    # isdigit 判断有没有数字
            digit_num += 1
        elif i.isspace():   # isspace 判断有没有空格
            spance_num += 1
        elif i.isalpha():    #isalpha 判断有没有字符
            al_num += 1
        else:
            others_num += 1
        return (al_num,spance_num,digit_num,others_num)

r = func1("kuh231jku ygkuhg")
print(r)