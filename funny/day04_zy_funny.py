# coding:utf-8
# Funny (qq:516110288)

num_list = [[1,2],["tom","jim"],(3,4),["ben"]]
num_list.append("kity")
print(num_list)
#2
print(num_list[3])
#3
num_list[1][0] = "lucy"
print(num_list)
#4
num_lists = list(num_list[2])
num_lists[0] = "5"
#5
num_list[1].append('6,7')
print(num_list)

#6
print(num_list[-1::-1])

numbers = [1,3,5,7,8,25,4,20,29]
#1
numbers.sort()
print(numbers)
#2
print(sum(numbers))
#3
numbers.sort(reverse=True)
print(numbers)