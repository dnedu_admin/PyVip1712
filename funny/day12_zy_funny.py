# coding:utf-8
# Funny (qq:516110288)

#自己动手写一个学生类,创建属性名字,年龄,性别,完成__new__,__init__,__str__,__del__，__call__
class Student():
    def __init__(self, name, age, sex):
        self.name = name
        self.age = age
        self.sex = sex
        print("实例对象初始化")
    def __str__(self):
        return "我是__str__的方法"

    def __del__(self):
        print("正在销毁对象")
    def __call__(self, *args, **kwargs):
        print("call行为正在被调用")
stu1 = Student("xiaohong","18","women")
stu1()
# 2.创建父类Person,属性name,函数eat,run,创建子类Student,学生类
#   继承Person,属性name,age,id,函数eat,study,全局定义一个函数
#   get_name,传入一个Person类的实例参数,分别传入Person和Student
#   两个类的实例对象进行调用.
class Person():
      name = ""
      def eat(self):
          print("111")
      def run(self):
          print("222")
class Student1(Person):
      age = 0
      id =0
      def eat(self):
          print("333")
      def study(self):
          print("444")
def get_name(Student1):
    Student1.eat()

p1 = Person()
s1 = Student1()

get_name(p1)
get_name(s1)

      # 3.用代码完成python多态例子和鸭子模型
class dog():
    def work(self):
        print("sss")
class cat():
    def work(self):
        print("aaa")
def fun(obj):
    obj.work()

abc = dog()
abd = cat()

fun(abc)
fun(abd)