# coding:utf-8
# Funny (qq:516110288)
#
# import socket
#
# # tcp协议
# host = '127.0.0.1'
# port = 9009
#
# # 构造socket对象
# s = socket.socket()
# s.bind((host, port))  # 绑定的参数是一个元组
# s.listen(5)
# #等待连接
# conn, addr = s.accept()  # 等待客户端的连接,代码执行没问题,就返回新的一个套接字对象(conn)
# # 接受客户端发来的数据
# content = conn.recv(1024)
# print("接受客户端发来的数据为%s" %content.decode("utf-8"))
# result = input('请输入要返回给客户端的数据:')  # unicode字符串
# conn.send(result.encode("utf-8"))  #返回数据
# conn.close()
# s.close()

#循环
import socket
# tcp协议
host = '127.0.0.1'
port = 9009

# 构造socket对象
# s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)  # ip制式/流式套接字
# s = socket.socket()
# s.bind((host, port))  # 绑定的参数是一个元组
# s.listen(5)
# # 添加循环
# while 1:    # 循环等待连接
#     # 等待连接
#     conn, addr = s.accept()  # 等待客户端的连接,代码执行没问题,就返回新的一个套接字对象(conn)
#     # 接受客户端发来的数据
#     while 1:    # 多次通信
#         content = conn.recv(1024)
#         if len(content)==0:
#             print('接受到客户端数据为空')
#         else:
#             print('接受客户端发送来的数据为%s' % content.decode('utf-8'))
#             result = input('请输入要返回给客户端的数据:')  # unicode字符串
#             conn.send(result.encode('utf-8'))  # 返回数据
#     conn.close()
# s.close()
# 使用socketserver框架

import socketserver
# 这个模块里面一个重要的对象
from socketserver import StreamRequestHandler as srh  # (别名)

# 处理请求的基类(自定义类继承StreamRequestHandler)
host = '127.0.0.1'
port = 9999

# 封住绑定的元组
addr = (host,port)

# 需要自定义请求处理类
class Server(srh):     # 你的类--->基类发生关系
    # 处理请求的方法
    def handle(self):    # 处理请求
        print('连接到服务器的客户端地址为%s' % str(self.client_address))  # 客户端地址信息
        while 1:
            content = self.request.recv(1024)   # 接受数据
            if not content:
                break
            print('服务器接收的数据为%s' % content.decode('utf-8'))
            # 处理数据
            result = input('请输入返回给客户端的数据:')
            self.request.send(result.encode('utf-8'))  # request接受和发送数据

# 怎么把服务器开启来?
print('开始处理请求:')
# 构造一个多线程tcp

server = socketserver.ThreadingTCPServer(addr,Server)   #  异步通信机制
server.serve_forever()  # 开启来
