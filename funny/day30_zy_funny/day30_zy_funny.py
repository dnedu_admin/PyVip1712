# coding:utf-8
# Funny (qq:516110288)


# 1.基于tcp协议socket套接字编程半双工模式
# 2.socketserver异步非阻塞编程,编写代码

# import socket
#
# host="127.0.0.1"
# port=9009
#
# s=socket.socket()
# s.connect((host,port))  # 连接服务器
# # 客户端发送数据:
# connect=input("请输入发给服务器数据：")
# s.send(connect.encode("utf-8"))
# # 接受客户端返回来的数据
# result=s.recv(1024)
# print("接收服务器返回数据%s" % result.decode("utf-8"))
# s.close()

#循环

# import socket
# # tcp协议
# host = "127.0.0.1"
# port = 9009
#
# s = socket.socket()  # 默认留空   ip4,流式套接字tcp
# s.connect((host, port))  # 连接服务器
# while 1:   # 多次来回来回通信
#     # 客户端发送数据:
#     content = input('请输入发送给服务器的数据:')
#     s.send(content.encode('utf-8'))
#
#     # 接受客户端返回来的数据
#     result = s.recv(1024)
#     print('接受服务器返回的数据为%s' % result.decode('utf-8'))
#     print('-'*44)   # 一次通信完毕
# s.close()

import socket

host = '127.0.0.1'
port = 9999

addr = (host, port)
s = socket.socket()

s.connect(addr)

while 1:
    content = input('请输入发送给服务器的数据为%s:')
    if not content or content == 'ex':
        break
    s.send(content.encode('utf-8'))
    result = s.recv(1024)
    if not result:
        break
    print('服务器返回给客户端的数据为%s' % result.decode('utf-8'))

s.close()