# coding:utf-8
# Funny (qq:516110288)


# 1.xlsxwriter包中，往工作表中插入写数据方法  work_sheet.write()
# 查看write函数定义，解释其函数定义中 @convert_cell_args这个装饰器的具体用途？

import xlsxwriter
file_name="第一个工作表"
work_book=xlsxwriter.Workbook(file_name)
work_sheet=work_book.add_worksheet("文本")
work_sheet.write()
#write(写行，列，参数)
@convert_cell_args
# 写数据到工作表单元格中调用合适的write_ *（）
# 基于传递数据类型的方法。
#
# args：
# 行：单元格行（零索引）。
# 单元格列（零索引）。
# *参数：参数传递给子函数。
#
# 返回：
# 0：成功。
# - 1：行或列超出工作表范围。
# 其他：调用方法的返回值
