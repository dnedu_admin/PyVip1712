# coding:utf-8
# Funny (qq:516110288)
#
# 作业:
# 1.datetime,获取你当前系统时间三天前的时间戳;并把当前时间戳增加
#  899999,然后转换为日期时间对象,把日期时间并按照%Y-%m-%d %H:%M:%S
#  输入打印这个日期对象,并获取date属性和time属性,最后判断当前日期时间
#  对象是星期几;
from datetime import datetime,timedelta
nowtime=datetime.now()
daytime3=nowtime-timedelta(days=3)
print(daytime3)
# print(datetime_1(date1,time1))
# datetime_1=(datetime.strftime(datetime1, "%Y-%m-%d %H:%M:%S")
timestamp1=daytime3.timestamp()+899999
tempdate=datetime.fromtimestamp((timestamp1))
print(tempdate.strftime("%Y-%m-%d %H:%M:%S"))
print(tempdate.date())
print(tempdate.time())
print(datetime.today().weekday())


# 2.使用os模块线获取当前工作目录的路径,并打印当前目录下所有目录和文件,
# 获取当前脚本的绝对路径, 分割当前脚本文件的目录和文件在tuple中并判断
# 当前文件所在的目录下存不存在文件game.py,如果不存在则创建该文件.
# 并往该文件里写入数据信息,然后判断当前目录下是否存在目录
# /wow/temp/,如果不存在则创建,最后把目录名称改为:/wow/map/
#
import os
print(os.listdir(os.getcwd())) #目录下所有目录和文件,
dqdir=os.getcwd()
print(dqdir)
dqline=__file__
print(dqdir)
dqtuple=os.path.split(dqline)
print(dqtuple)
if os.path.exists("./game.py"):
    print("存在")
else:
    print("不存在")
    with open("game.py","w")as f:
        f.write("123")
if os.path.exists("./wow/temp/"):
    print("已存在")
else:
    os.makedirs("./wow/temp/")
    print("已创建")
if os.path.exists("./wow/map/"):
    os.rmdir("./wow/map/")
os.renames("./wow/temp/","./wow/map/")

# 3.写函数，检查获取传入列表或元组对象的所有奇数位索引对应的元素，并将其作为新的列表返回给调用者 
def abc(content):
    if isinstance(content,(list,tuple)):
        a=[]
        for index,item in enumerate(content):
            if index % 2 != 0:
                a.append(item)
        print(a)
    else:
        print("传入元素")
abc((1,2,3,4,5,))