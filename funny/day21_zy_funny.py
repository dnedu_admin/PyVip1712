# coding:utf-8
# Funny (qq:516110288)

# 1.识别下面字符串:’ben’,’hit’或者’ hut’
# 2.匹配用一个空格分隔的任意一对单词,比如你的姓 名
# 3.匹配用一个逗号和一个空格分开的一个单词和一个字母
# 4.匹配简单的以’www’开头,以’com’结尾的web域名,比如:www.baidu.com

import re
print(re.findall("ben","ben"))
print(re.findall("hit","hit"))
print(re.findall("hit","hut"))

print(re.findall("f\sun\sny","f un ny"))
print(re.findall("a.b\sa","a,b a"))
print(re.findall("\Awww.abc.com\Z","www.abc.com"))