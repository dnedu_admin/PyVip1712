# coding:utf-8
# Funny (qq:516110288)

# #1.写一个函数计算1 + 3 + 5 +…+97 + 99的结果再写
# 一个装饰器函数, 对其装饰, 在运算之前, 新建一个文件
# 然后结算结果, 最后把计算的结果写入到文件里
def abc(fun):
    def abc1():
        file = open("day15.py", "w", encoding="utf-8")
        result = fun()
        file.write(str(result))
        file.close()    
    return abc1
@abc
def a():
    return sum(range(1, 101, 2))
a()
# 2.
# 写一个函数计算传入进来参数的平方, 并将结果.写一个带参数的装饰器,
# 将装饰器参数传入到被包装的函数, 计算并输入结果
def work(args):
    def outer(fun):
        def inter(*kw):
            print("开始计算")
            a = fun(args)
            print("结束")
            return a
        return inter
    return outer
@work(2)
def func(num):
    if isinstance(num,int):
        return pow(num,2)
    else:
        print("传入的不是整数")
num=func(4)
print(num)