# coding:utf-8
# Funny (qq:516110288)

# 1.计算range(1,99999)的总和,采取三种方式,第一:重复10次计算总时间;
#  第二:开启十个子线程去计算;第三:开启十个进程去计算,然后对比耗时
from multiprocessing import Process,Pool
import os,time
# def mk_time():
#     print(sum(range(1,99999)))
#     # print((num))
#
# if __name__=="__main__":
#     p=Process(target=mk_time,args=())
#     time_start=time.time()
#     p.start()
#     p.join()
#     time_end=time.time()
#     time.all=time_start+time_end
#     for i in range(11):
#         print("运算这次的总时间为%s" % (time.all))
#     print("结束")

#多线程
# from threading import Thread,Lock
# import time
# def many_process():
#     print(sum(range(1,99999)))
#
# some_process=[Thread(target=many_process,args=()) for i in range(10)]
# time_start_1=time.time()
# for a in some_process:
#     a.start()
#     a.join()
# time_end_1=time.time()
# time_all_1=time_start_1+time_end_1
# print("总时间为%s" %(time_all_1))
#多进程
# from multiprocessing import Pool,Process
# import os,time
# def ten_process():
#     num = sum(range(1,99999))
#     print(num)
#
# if __name__=="__main__":
#     pool=Pool(10)
#     for num in range(10):
#         pool.apply_async(ten_process,args=())
#     time_1 = time.time()
#     pool.close()
#     pool.join()
#     time_2 = time.time()
#     time_all= time_1+ time_2
#     print("计算总时间为%s" % (time_all))

# 2.将第一题计算十次改为使用线程池去计算,线程池线程个数由你自己电脑cpu逻辑核数决定

from threading import Thread
import time
def many_process():
    print(sum(range(1,99999)))

some_process=[Thread(target=many_process,args=()) for i in range(3)]
time_start_1=time.time()
for a in some_process:
    a.start()
    a.join()
time_end_1=time.time()
time_all_1=time_start_1+time_end_1
print("总时间为%s" %(time_all_1))