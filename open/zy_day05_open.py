# 第一题
name_dict = {'name': 'ben', 'age': 22, 'sex': '男'}

name_dict['height'] = 170  # 添加
name_dict.pop('name')  # 删除
del name_dict['age']  # 删除
a_dict = {'a': 'A', 'b': 'B'}
name_dict.update(a_dict)  # 更新
name_dict.clear()  # 清空
print(name_dict)

# 第二题
num1_set = {3, 5, 1, 2, 7}
num2_set = {1, 2, 3, 11}
a_set = num1_set & num2_set  # 交集
a_set = num1_set | num2_set  # 并集
a_set = num1_set ^ num2_set  # 异或运算
m_set = num1_set - num2_set  # 差集
n_set = num2_set - num1_set
print(m_set, n_set)

# 第三题
num_dict = {'a': 13, 'b': 22, 'c': 18, 'd': 24}
t_dict = {}
values = num_dict.values()
values = sorted(values)

for i in values:
    for k, v in num_dict.items():
        if v == i:
            t_dict[k] = v

print(t_dict)
