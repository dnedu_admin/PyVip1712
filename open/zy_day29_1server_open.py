# 1.基于tcp协议socket套接字编程半双工模式
from socket import *

host = '127.0.0.1'
port = 8899
addr = (host, port)
socket = socket(AF_INET, SOCK_STREAM)
socket.bind(addr)
socket.listen(5)

while 1:
    conn, clientAddr = socket.accept()
    while 1:
        content = conn.recv(1024)
        if len(content) > 0:
            print('服务端接收数据:%s' % content.decode('utf-8'))
        else:
            break
        result = input('向客户端发送数据:')
        if not result:
            break
        conn.send(result.encode('utf-8'))
    conn.close()

socket.close()
