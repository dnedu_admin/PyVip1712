def run(num):
    if isinstance(num, (int, float)):
        return (num ** 2)
    else:
        raise TypeError('参数类型不正确')


class Person():
    def __init__(self, name):
        self.name = name

    def get_name(self):
        return self.name
