import re
from collections import deque


# 1.请写出一段Python代码实现删除一个list里面的重复元素
def no_repeat(t_list):
    new_list = []
    for i in t_list:
        if not i in new_list:
            new_list.append(i)
    return new_list


def func1():
    a_list = [1, 1, 2, 3, 4, 5, 6]
    print(a_list)
    a_list = no_repeat(a_list)
    print(a_list)


# func1()


# 2.如何用Python来进行查询和替换一个文本字符串？
def func2(a_str):
    result = re.sub('\d+', '#', a_str)
    print(result)


# func2('abac12cas444c')


# 3.a=1, b=2, 不用中间变量交换a和b的值
def func3():
    num1, num2 = 11, 22
    print(num1, num2)
    num1, num2 = num2, num1
    print(num1, num2)


# func3()


# 4.写一个函数, 输入一个字符串, 返回倒序排列的结果:?如: string_reverse(‘abcdef’), 返回: ‘fedcba’
# 1. 简单的步长为-1, 即字符串的翻转;
def func4_1():
    a_str = 'abcdef'
    a_str = a_str[::-1]
    print(a_str)


# 2. 交换前后字母的位置;
def func4_2():
    a_str = 'abcdef'
    a_list = list(a_str)
    length = len(a_list)
    for i in range(length // 2):
        a_list[i], a_list[length - 1 - i] = a_list[length - 1 - i], a_list[i]
    print(''.join(a_list))


# 3. 递归的方式, 每次输出一个字符;
def func4_3():
    a_str = 'abcdef'
    end = len(a_str) - 1

    def fun(end):
        print(a_str[end])
        if end == 0:
            return
        else:
            fun(end - 1)

    fun(end)


# 4. 双端队列, 使用extendleft()函数;
def func4_4():
    a_str = 'abcdef'
    a_deque = deque()
    for i in a_str:
        a_deque.extendleft(i)
    a_list = list(a_deque)
    print(''.join(a_list))


# 5. 使用for循环, 从左至右输出;
def func4_5():
    a_str = 'abcdef'
    a_list = list(a_str)
    a_list.reverse()
    b_str = ''.join(a_list)
    print(b_str)


# func4_1()
# func4_2()
# func4_3()
# func4_4()
# func4_5()

# 5.
def func5():
    a_list = [88, 99, 11, 22, 33, 55]
    b_list = [44, 55, 66, 77, 88, 11, 22, 33, ]
    t_list = list(set(a_list + b_list))
    t_list.sort()
    print(t_list)


func5()
