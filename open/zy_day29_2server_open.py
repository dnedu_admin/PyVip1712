# 2.socketserver异步非阻塞编程,编写代码
import socketserver
from socketserver import StreamRequestHandler

host = '127.0.01'
port = 8899
addr = (host, port)


class Server(StreamRequestHandler):
    def handle(self):
        print('客户端地址:%s' % str(self.client_address))

        while 1:
            content = self.request.recv(1024)
            if not content:
                break
            print('服务端接收数据:%s' % content.decode('utf-8'))

            result = input('发送客户端:')
            if not result:
                break
            self.request.send(result.encode('utf-8'))


server = socketserver.ThreadingTCPServer(addr, Server)
server.serve_forever()
