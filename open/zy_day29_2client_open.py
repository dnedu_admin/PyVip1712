from socket import *

host = '127.0.01'
port = 8899
addr = (host, port)
socket = socket(AF_INET, SOCK_STREAM)
socket.connect(addr)

while 1:
    content = input('发送服务器数据:')
    if not content:
        break
    socket.send(content.encode('utf-8'))

    result = socket.recv(1024)
    if not result:
        break
    print('客户端接收数据:%s' % result.decode('utf-8'))

socket.close()
