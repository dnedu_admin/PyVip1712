# 1.基于tcp协议socket套接字编程半双工模式
from socket import *

host = '127.0.0.1'
port = 8899
addr = (host, port)
socket = socket(AF_INET, SOCK_STREAM)
socket.connect(addr)

while 1:
    content = input('向服务端发送数据:')
    if len(content) > 0:
        socket.send(content.encode('utf-8'))
    else:
        break
    recvData = socket.recv(1024)
    print('客户端接收数据:%s' % recvData.decode('utf-8'))

socket.close()
