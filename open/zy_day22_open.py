import re

# 1.使用正则表达式匹配电话号码：0713-xxxxxx(湖南省座机号码)
p = re.compile('\d{4}\-\d{8}')
result = p.match('0713-88945688')
if result:
    print(result.group())

# 2.区号中可以包含()或者-,而且是可选的,就是说你写的正则表达式可以匹配
p2 = re.compile('((\(\d{3}\))|(\d{3}\-))?\d{3}\-\d{4}')
r1 = p2.match('800-555-1212')
if r1:
    print(r1.group())
r2 = p2.match('555-1212')
if r2:
    print(r2.group())
r3 = p2.match('(800)555-1212')
if r3:
    print(r3.group())

#