import base64
import os
import uuid
import shutil


# 1.
def func1():
    global file
    os.mkdir('./abc')
    u1 = str(uuid.uuid1())
    with open('./abc/uuid.py', 'w') as file:
        file.write(u1)
    shutil.copytree('./abc', './app')
    shutil.copyfile('./app/uuid.py', './app/new.py')


# func1()


# 2.
def func2():
    with open('one.txt', 'wb') as f:
        for i in range(1, 10):
            f.write(base64.b64encode(('今天天气不错,我们去动物园%s玩吧' % i).encode('utf-8')))
            f.write(b'\n')

    with open('one.txt', 'rb') as f2:
        datas = f2.readlines()

    for item in datas:
        data = base64.b64decode(item).decode('utf-8').strip()
        if data.find('动物园6') != -1:
            with open('two.txt', 'w',encoding='utf-8') as f3:
                f3.write(data)


func2()


# 3.
def func3():
    img_file = open('img.png', 'rb')
    encode_img = base64.b85encode(img_file.read())
    write_img = open('img.txt', 'wb+')
    write_img.write(encode_img)
    write_img.seek(0, 0)
    decode_img = base64.b85decode(write_img.read())
    img2_file = open('img2.png', 'wb')
    img2_file.write(decode_img)
    img_file.close()
    write_img.close()
    img2_file.close()

# func3()
