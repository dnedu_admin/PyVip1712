# 1.
def func1():
    a_list = [[1, 2, 3, 4, 5], [{'name': '张三', 'age': 28}, {'name': 'James', 'age': 30}]]
    print('James的年龄:%s' % a_list[1][1]['age'])


func1()

# 2.
'''
可迭代对象:
可迭代对象是指可以被for in 循环的对象；可以用instance()判断是否是Iteralbe对象
比如：list，tuple，dict，set，str，生成器

迭代器：
能被next()调用的函数就是迭代器；迭代器对象都是可迭代对象，迭代器可以解决内存

生成器：
使用yield关键字的函数式生成器；每次调用生成器遇到yield关键字时都会暂停代码，并返回yield值
下次next()，就会从暂停的地方继续运行

装饰器：
对原函数进行装饰加工的函数；在不改变原函数的情况下进行功能拓展
'''

# 3.
'''
实例方法：与对象绑定，每个对象都有属于自己的实例方法，通过对象调用
静态方法：只是定义在类的内部，不属于类和对象，方便分类划分，可以通过类和对象调用
类方法：与类绑定，属于类，通过类调用
'''

# 4.
x = 2
z = 3


def func_outer():
    '''
    L：局部作用域
    E：闭包函数外作用域
    G：全局作用域
    B：内建作用域

    L –> E –> G –>B 查找顺序
    '''
    x = 0
    y = 9
    global z

    def func_inner():
        nonlocal x
        x += 5  # 7
        z = 6
        print('func_inner', max(x, z), max(x, y))  # 7,9

    z += 3  # 6
    x += 2  # 2
    print(x, y, z)
    return func_inner


func_outer()()

# 5.
'''
GIL：全局解释器锁。
在解释器处理数据的时候，同一时间只能有一个线程执行
'''

# 6.
'''
线程：每个进程都必须有一个线程执行，多线程就是开启多个线程同时并发执行多个任务，
进程：程序开启多个任务，每个任务互相独立，多进程可以执行多个任务处理
协程：在同一线程内，可以开启多个子程序，每个子程序可以执行任务，协程的调度通过程序控制
'''

# 7.
'''
多线程：执行多个任务，每个任务单独执行

多进程：避开GIL锁，
'''

# 8.
# 多个进程间如何通信:
import multiprocessing
import random
import time


def produce(q):
    fruits = ('香蕉', '苹果', '葡萄')
    while True:
        f = random.choices(fruits)
        q.put(f)
        print('生成：%s' % f)
        time.sleep(2)


def consume(q):
    while True:
        f = q.get()
        print('消费:%s' % f)
        time.sleep(1)


def func8():
    '''
    进程间通信使用Queue队列
    '''
    q = multiprocessing.Queue()
    p1 = multiprocessing.Process(target=produce, args=(q,))
    p2 = multiprocessing.Process(target=consume, args=(q,))
    p1.start()
    p2.start()
    p1.join()
    p2.join()


if __name__ == '__main__':
    func8()
