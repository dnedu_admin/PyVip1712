# 1.
class Student(object):
    def __new__(cls, *args, **kwargs):
        return super().__new__(cls)

    def __init__(self, name, age, sex):
        self.name = name
        self.age = age
        self.sex = sex

    def __str__(self):
        return '学生姓名:%s,学生年龄%d,学生性别%s' % (self.name, self.age, self.sex)

    def __del__(self):
        print('学生%s被删除' % self.name)

    def __call__(self, *args, **kwargs):
        print('学生%s被调用' % self.name)


stu = Student('Tom', 25, '男')
print(stu)
stu()

print('-' * 30)


# 2.
class Person(object):
    def __init__(self, name):
        self.name = name

    def eat(self):
        print('%s在吃东西' % self.name)

    def run(self):
        print('%s在奔跑' % self.name)


class Student(Person):
    def __init__(self, id, name, age):
        super().__init__(name)
        self.id = id
        self.age = age

    def study(self):
        print('%s在学习' % self.name)


def get_name(instance):
    if isinstance(instance, Student):
        instance.study()
        instance.eat()
        instance.run()
    elif isinstance(instance, Person):
        instance.eat()
        instance.run()


p = Person('人')
stu = Student(1, 'Tom', 20)
get_name(p)
get_name(stu)

print('-' * 30)


# 3.
class Animal(object):
    def __init__(self, name):
        self.name = name


class Dog(Animal):
    def info(self):
        print('%s是狗' % self.name)


class Cat(Animal):
    def info(self):
        print('%s是猫' % self.name)


def show_info(instan):
    instan.info()


d = Dog('dog')
c = Cat('cat')
show_info(d)
show_info(c)
