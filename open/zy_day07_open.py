# 1.简述普通参数、默认参数、关键字参数、动态收集参数的区别,理解为主
'''

普通参数：必须传入参数，并且一一对应
默认参数：调用函数时，如果没有传递参数，则会使用默认参数
关键字参数：调用函数时，可以指定参数位置
不定长参数：加了星号"*"的变量args会存放所有未命名的变量参数，args为元组；
          而加"**"的变量kwargs会存放命名参数，即形如key=value的参数， kwargs为字典。

'''


# 2.
def foo(x, y, z, *args, **kw):
    sum = x + y + z
    print(sum)
    for i in args:
        print(i)
    print(kw)
    for j in kw.items():
        print(j)


a_tuple = (1, 2, 3)
b_dict = {'name': 'jim', 'age': 28, 'add': '上海'}
foo(*a_tuple, **b_dict)


# 3.
def func(a, b, c=9, *args, **kw):
    print('a =', a, 'b =', b, 'c =', c, 'args =', args, 'kw =', kw)


func(1, 2)
func(1, 2, 3)
func(1, 2, 3, 4)
func(1, 2, 3, 4, 5)
func(1, 2, 3, 4, 5, 6, name='jim')
func(1, 2, 3, 4, 5, 6, name='tom', age=22)


# 4.写一个函数函数，计算传入字符串中的数字、字母、空格和其他的个数分别为多少?

def calculate(string):
    _dict = {'number': 0, 'chars': 0, 'space': 0, 'other': 0}
    for c in string:
        if c.isdigit():
            _dict['number'] += 1
        elif c.isalpha():
            _dict['chars'] += 1
        elif c.isspace():
            _dict['space'] += 1
        else:
            _dict['other'] += 1

    print('数字：%d，字母：%d，空格：%d，其他：%d' % (_dict['number'], _dict['chars'], _dict['space'], _dict['other']))


_str = input('请输入一个字符串:')
calculate(_str)
