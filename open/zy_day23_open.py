# 1.
from datetime import datetime, timedelta

temp_datetime = datetime.now()
temp_tiemstamp = (temp_datetime - timedelta(days=3)).timestamp() + 899999
datetime1 = datetime.fromtimestamp(temp_tiemstamp)
str_datetime = datetime.strftime(datetime1, '%Y-%m-%d %H:%M:%S')
print('日期时间%s' % str_datetime)
print('date属性%s，time属性%s' % (datetime1.date(), datetime1.timestamp()))
print('当前星期%s' % (datetime1.weekday() + 1))

# 2.
import os
from os import path

print(os.getcwd())
print(os.listdir('.'))
print(path.abspath('.zy_day23_open.py'))
print(path.split('./zy_day23_open.py'))

if not path.exists('./game.py'):
    with open('game.py', 'w') as file:
        file.write('hello python')
else:
    print('game.py存在')

if not path.exists('./wow/temp/'):
    os.makedirs('./wow/temp/')
    os.rename('./wow/temp/', './wow/map/')


# 3.
def func(data):
    if isinstance(data, list):
        return [item for index, item in enumerate(data) if index % 2 != 0]
    elif isinstance(data, tuple):
        return (item for index, item in enumerate(data) if index % 2 != 0)
    else:
        print('请输入列表或元组')


new_data = func(['a', 'b', 'c', 'd'])
print(new_data)
