from functools import lru_cache
from time import time
from line_profiler import LineProfiler
from memory_profiler import profile


# 1.函数缓存的使用
@lru_cache(maxsize=10, typed=True)
def fib(n):
    if n < 2:
        return n
    else:
        return fib(n - 1) + fib(n - 2)


def func1():
    t1 = time()
    [fib(n) for n in range(10000)]
    t2 = time()
    print('函数缓存耗时%s' % (t2 - t1))


# func1()  # 函数缓存耗时0.014000892639160156


# 2.性能分析器的使用
def fib2(n):
    if n < 2:
        return n
    else:
        return fib2(n - 1) + fib2(n - 2)


def count_time():
    t1 = time()
    [fib2(n) for n in range(30)]
    t2 = time()
    print('性能分析%s' % (t2 - t1))


def func2():
    profiler = LineProfiler(count_time)
    profiler.enable()
    count_time()
    profiler.disable()
    profiler.print_stats()


# func2()

# 3.内存分析器的使用

@profile()
def func3():
    list1 = [i for i in range(10000)]
    list2 = [j for j in range(99999)]
    del list1
    del list2


func3()
