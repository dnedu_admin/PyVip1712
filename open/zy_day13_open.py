# 1.

class A():
    def run(self):
        print('A')


class B():
    def run(self):
        print('B')


class C(A, B):
    pass


class D(A, B):
    pass


class E(C, D):
    pass


e = E()
e.run()


# 2.
class MyClass():
    __slots__ = ('id', 'name')


myClass = MyClass()
myClass.id = 100
myClass.name = 'Tom'
print(myClass.id, myClass.name)


# myClass.age = 12  # 'MyClass' object has no attribute 'age'

# 3.
def sleep(self):
    print('sleep')


def run(self):
    print('run')


Person = type('Person', (), {'name': 'Tom', 'age': 20, 'sex': '男', 'sleep': sleep, 'run': run})
# print(dir(Person))
p = Person()
print(p.name)
print(p.age)
print(p.sex)
p.sleep()
p.run()


# 4.
class Person():
    def __init__(self, name, age):
        self.__name = name
        self.__age = age

    @property
    def age(self):
        return self.__age

    @age.setter
    def age(self, age):
        self.__age = age

    @age.deleter
    def age(self):
        print('被删除了')


p = Person('Tom', 20)
p.age = 30
print(p.age)
del p.age
