# 1.
from functools import reduce


def outer(func):
    def inner():
        file = open("text.txt", "w")
        result = str(func())
        file.write(result)
        file.close()

    return inner


@outer
def func():
    result = reduce(lambda x, y: x + y, range(1, 100, 2))
    return result


# func()


# 2.

def outer(arg):
    def middle(fun):
        def inner(x):
            return fun(x)+arg
        return inner
    return middle

@outer(10)
def fun(x):
   return x ** 2

print(fun(3))