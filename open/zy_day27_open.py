import unittest
from zy.test_fun import run, Person


class MyTest(unittest.TestCase):
    def setUp(self):
        print('测试开始')

    def tearDown(self):
        print('测试结束')

    def test_int(self):
        self.assertEqual(run(3), 9, 'int类型测试函数失败')

    # def test_str(self):
    #     self.assertEqual(run('1.0'), 1, 'str类型测试函数失败')

    def test_class(self):
        p = Person('Tom')
        self.assertEqual(p.get_name(),'Tom','测试类不成功')

if __name__ == '__main__':
    unittest.main()
