import re

# 1.识别下面字符串:’ben’,’hit’或者’ hut’
p = re.compile(r'\s?\w{3}')
r1 = p.match('ben').group()
r2 = p.match('hit').group()
r3 = p.match(' hut').group()
print(r1, r2, r3)

# 2.匹配用一个空格分隔的任意一对单词,比如你的姓 名
r = re.findall(r'(\b\w{1,}){1,}', 'open fire')
print(r)

# 3.匹配用一个逗号和一个空格分开的一个单词和一个字母
r=re.findall(r'\b\w+, \w+','op, en')
print(r)


# 4.匹配简单的以’www’开头,以’com’结尾的web域名,比如:www.baidu.com
r=re.findall(r'^www.\w+.com$','www.baidu.com')
print(r)