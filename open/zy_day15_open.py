# 1.
print(list(range(3, 7)))
print(list(range(3, 19, 3)))
print(list(range(-20, 867, 220)))

# 2.
# 列表推导式
a_list = [x for x in range(0, 101, 2)]
print(sum(a_list))


# 生成器
def func(x):
    for i in range(x + 1):
        if i % 2 == 0:
            yield i


sum = 0
for item in func(100):
    sum += item
print(sum)


# 3.
def func2(x):
    for i in range(x):
        if i % 2 != 0:
            yield i ** 3


print(list(func2(10)))


# 4.
class MyIter():
    def __init__(self, id):
        self.id = id
        self.start = -1

    def __next__(self):
        # if self.id == 0:
        #     raise StopIteration("没有数据了")
        #
        # self.id -= 1
        # return self.id
        if self.start == self.id:
            raise StopIteration("没有数据了")

        self.start += 1
        return self.start

    def __iter__(self):
        return self


for i in MyIter(10):
    print(i, end=' ')
