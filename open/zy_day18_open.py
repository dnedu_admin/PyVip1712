# 1.
import time
from multiprocessing import Process, Pool
from threading import Thread


def func_normal(num):
    start = time.time()
    for i in range(num):
        sum = 0
        for j in range(1, 99999):
            sum += j
    end = time.time()

    print("func_normal总耗时：%f" % (end - start))


def mk_thread():
    sum = 0
    for j in range(1, 99999):
        sum += j


def func_thread(num):
    start = time.time()
    thread_list = [Thread(target=mk_thread) for i in range(num)]
    for thr in thread_list:
        thr.start()
        thr.join()
    end = time.time()
    print("func_thread总耗时：%f" % (end - start))


def mk_process():
    sum = 0
    for j in range(1, 99999):
        sum += j


def func_process(num):
    # start = time.time()
    # process_list = [Process(target=mk_thread) for i in range(num)]
    # for thr in process_list:
    #     thr.start()
    #     thr.join()
    # end = time.time()
    # print("func_process总耗时：%f" % (end - start))

    start = time.time()
    pool = Pool()
    for i in range(10):
        pool.apply_async(mk_process)
    pool.close()
    pool.join()
    end = time.time()
    print("func_process总耗时：%f" % (end - start))


# if __name__ == "__main__":
# func_normal(100)  # func_normal总耗时：0.669038
# func_thread(100)  # func_thread总耗时：0.634036
# func_process(100)  # func_process总耗时：0.475027

# 2.
import threadpool
from multiprocessing import cpu_count


def mk_thread2(i):
    sum = 0
    for j in range(1, 99999):
        sum += j


def func_threadpool():
    start = time.time()
    pool = threadpool.ThreadPool(cpu_count())
    requests = threadpool.makeRequests(mk_thread2, list(range(10)))
    [pool.putRequest(req) for req in requests]
    pool.wait()
    end = time.time()
    print("func_threadpool总耗时：%f" % (end - start))


func_threadpool()
