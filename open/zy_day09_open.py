# 1.a_list = [1,2,3,2,2]
#    删除a_list中所有的2

a_list = [1, 2, 3, 2, 2]
new_list = []
for i in a_list:
    if i != 2:
        new_list.append(i)

print(new_list)

# 2.用内置函数compile创建一个文件xx.py, 在文件里写你的名字, 然后用eval
# 函数读取文件中内容, 并打印输出到控制台

with open('xx.py','r') as file:
    file_str=file.read()
    c = compile(file_str, '', 'eval')
    eval(c)

# 3.写一个猜数字的游戏, 给5次机会, 每次随机生成一个整数, 然后由控制台输入一个
# 数字, 比较大小.大了提示猜大了, 小了提示猜小了.猜对了提示恭喜你, 猜对了.
# 退出程序, 如果猜错了, 一共给5次机会, 5
# 次机会用完程序退出.

import random


def guess_number():
    times = 0
    while True:
        num_str = input('第%d次猜数字:' % (times + 1))
        if num_str.isdigit():
            num = int(num_str)
            num_random = random.randint(0, 10)
            print('随机数为%d' % num_random)
            if num > num_random:
                print('猜大了')
            elif num < num_random:
                print('猜小了')
            else:
                print('恭喜你, 猜对了')
                break

            times += 1
            if times >= 5:
                print('5 次机会用完')
                break
        else:
            print('请输入一个数字!')


guess_number()
