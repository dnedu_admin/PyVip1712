# 1.
a_list = [0, False, True, 5, {}, (2, 3)]
print(list(map(bool, a_list)))

# 2.
print(list(map(lambda x: x ** 3 if x % 2 != 0 else x, range(1, 11))))

# 3.
from functools import reduce

result = reduce(lambda x, y: x + y, [i for i in range(1, 101) if i % 2 == 0])
print(result)

# 4.用递归函数实现斐波拉契数列
def func(x):
    if x<=1:
        return x
    else:
       return func(x-1)+func(x-2)
a_list=[]
for i in range(20):
    a_list.append(func(i))
print(a_list)