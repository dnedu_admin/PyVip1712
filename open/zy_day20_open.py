from types import coroutine


async def one():
    for i in range(1, 100, 2):
        print("one", i)
        await  next()


async def two():
    for i in range(2, 101, 2):
        print("two", i)
        await next()


@coroutine
def next():
    yield


def run(b_list):
    while b_list:
        for item in b_list:
            try:
                item.send(None)
            except Exception as e:
                b_list.remove(item)


if __name__ == "__main__":
    one = one()
    two = two()
    a_list = []
    a_list.append(one)
    a_list.append(two)
    run(a_list)
