# 1. 格式化显示浮点数12.78，要求保留3位小数，输出占20字符，空位用0补齐
a_float = 12.78
b_float = '%020.3f' % a_float
print(b_float)

# 2. 将字符串”动脑学院”转换为bytes类型。
a_str = '动脑学院'
b_bytes = a_str.encode('utf-8')
print(b_bytes)

# 3. 将以下3个字符串合并为一个字符串，字符串之间用3个_分割
a_str = 'hello 动脑 pythonvip'
a_list = a_str.split()
b_str = '___'.join(a_list)
print(b_str)

# 4. 删除字符串  “    你好，动脑eric     ”首尾的空格符号
a_str = '    你好，动脑eric     '
b_str = a_str.strip(' ')
print(b_str)

# 5. 用户信息存在如下所示字符串中，包含信息依次为  姓名   学号  年龄，
# 不同信息之间的空格数不确定，请提取用户信息分别保存到 xxx_name,xxx_num, xxx_age。
eric = "    Eric   dnpy_001     28"
eric = eric.strip(' ').split()
eric_name, eric_num, eric_age = eric[0], eric[1], eric[2]
print(eric_name, eric_num, eric_age)
