# !/usr/bin/env python
# coding:utf-8
"""
    Author = 上善若水(QQ:564667570)
    Affiliation = Institute of Subtropical Agriculture, Chinese Academy of Sciences
    Time = '2018-3-19, 20:44'
"""

from datetime import datetime, date, time

# time = time(21, 3, 30)
# # print(time)
# # datetime1 = datetime.now()
# datetime2 = datetime.utcnow()
# # # print(datetime1, datetime2)
# # print(datetime.resolution)  # 0:00:00.000001
# # print(datetime.utcnow())   # 格林威治时间 2018-03-19 13:10:44.934818
# # print(datetime.now())   # 北京时间 2018-03-19 21:10:44.934818
# #
# # print(datetime.combine())    # 组合对象
#
# print(type(datetime.strftime(datetime2, '%Y-%m-%d %H-%M-%S')))
#
# print(datetime2.timetuple()[0])  # time.struct_time(tm_year=2018, tm_mon=3, tm_mday=19, tm_hour=13, tm_min=21, tm_sec=5, tm_wday=0, tm_yday=78, tm_isdst=-1)

# 2.时间戳转换为日期时间对象
from datetime import datetime
# timestamp1 = 9783599999
# print(datetime.fromtimestamp(timestamp1))
# print(datetime.utcfromtimestamp(timestamp1))
#
# time = datetime.now()
# print(datetime.timestamp(time))
# print(datetime.timetuple(time))
# # print(help(datetime))

# 4.日期时间对象到字符串的转换

# from datetime import datetime
# datetime1 = datetime.now()
# print(datetime.strftime(datetime1,'%Y/%m/%d %H:%M:%S'))
# print(datetime1.strftime('%Y/%m/%d %H:%M:%S'))

# 5.datetime的前减后加
# from datetime import datetime,timedelta
# datetime1 = datetime.now()
# print(type(datetime1))
# print(datetime1)
# datetime2 = datetime1+timedelta(days=1,hours=8)
# print(datetime2.strftime('%Y/%m/%d %H:%M:%S'))
# datetime3 = datetime1-timedelta(days=2,hours=8,minutes=20 )
# print(datetime3.strftime('%Y/%m/%d %H:%M:%S'))


# os包括操作系统相关操作,具体与平台无关
# import os
# print(os.name)
# print(os.stat('__init__.py'))
# print(os.getcwd())
# print(os.getenv('python', '没有这个系统变量'))
# os.environ['name'] = 'ben'
# print(os.getenv('name', '-1'))
# print(os.environ['name'])
# print(os.listdir('.'))
# print(os.listdir(r'D:/'))
# # os.mkdir('music')
# # os.makedirs('./game/wow')
# # os.removedirs('./game/wow')
# # os.removedirs('music')

# os module
import os
# os.removedirs('ben1.py')
# os.mkdir('ben.txt')
# os.rename('ben.txt', 'ben.py')
# os.mkdir('tom.txt')
# os.renames('tom.txt', 'ben1.py')
# os.rmdir('./game')
# os.system("ping www.baidu.com")
# os.system("print('我是中国人')")
# os.system('dir')
# 下面来看看os.path模块
from os import path
# print(dir(path))
# print(path.split(r'./'))  #
# print(path.isfile(r'f:/'), path.isdir(r'f:/'))
# print(path.exists(r'f:/'))
# print(path.getsize('ben.py'))  #
# print(path.abspath('ben.py'))  #
# print(path.normpath('f:/temp/ben.py'))
# print(path.splitext('f:/temp/ben.py'))

from os import path

# print(path.join(r'f:', '\game'))
# print(path.join(r'game', r'wow\ben.py'))
# print(path.basename('ben.py'))
# print(path.dirname(r'f:\game\wow\ben.py'))

# 1.平台与版本信息:
import sys
# print(sys.version)
# print(sys.platform)
# print(sys.maxsize)
# sys.exit()
# # 2.Python模块搜索路径,模块表
# print(sys.path)
# print(sys.modules)
# # 3.当前编码信息：
# print(sys.getdefaultencoding())
# print(sys.getfilesystemencoding())
# # # 4.其他信息：
# print(sys.exc_info())
# for arg in sys.argv:
#     print(arg)
# print(len(sys.argv) - 1)  #

# 再来看看另外一个sys的函数：

# sys.getrefcount()
# import sys
# class Student():
#     def __init__(self, name, age):
#         self.name = name
#         self.age = age
#
# student = Student('tom', 22)
# print(sys.getrefcount(student))
# # 此处的结果为2
# sys.getrefcount() - 1

# math模块
# 简单看看math模块,这里模块方法比较多
import math
# print(dir(math))
# print(help(math))
a = 3
# print(math.pow(a, 2))
# print(math.ceil(-4.88))
# print(math.floor(5.777))
# print(math.sqrt(9.0))
# print(math.factorial(5))  #阶乘
# print(math.hypot(2,3))
# print(math.log(100))
# # 看看负数求平方根：
# print(math.sqrt(-9))  #ValueError
# import cmath
# print(cmath.sqrt(-9))
# print(cmath.sqrt(9))
# print(cmath.log10(100))


# random模块中常用的函数：
# random函数：
import random
# print(random.random())
# # uniform函数:
# print(random.uniform(20,30))
# print(random.uniform(30,20))
# # randint函数：
# print(random.randint(1,10))
# print(random.randint(9,9))
# # 如果前面的参数大于后面的参数呢
# # rangrange函数：
# print(random.randrange(1,9))
# print(random.randrange(1,20,2))

# choice/choices
import random
print(random.choice('I love python'))
print(random.choices('I love python'))
print(random.choices(['my', 'name', 'is', 'ben']))
print(random.choices((1, 2, 3,'')))
# sample函数：返回的结果为list
print(random.sample('123456789',6))
print(random.sample([1,2,3,4,5,6],3))
print(random.sample((1,2,3,4,5),4))
# shuffle函数：
a_list = [1,2,3,4]
random.shuffle(a_list )
print(a_list)
