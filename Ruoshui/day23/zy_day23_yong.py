# !/usr/bin/env python
# coding:utf-8
"""
    Author = 上善若水(QQ:564667570)
    Affiliation = Institute of Subtropical Agriculture, Chinese Academy of Sciences
    Time = '2018-3-19, 22:41'
"""
# 1.datetime,获取你当前系统时间三天前的时间戳;并把当前时间戳增加
# 899999,然后转换为日期时间对象,把日期时间并按照%Y-%m-%d %H:%M:%S
# 输入打印这个日期对象,并获取date属性和time属性,最后判断当前日期时间
# 对象是星期几;
import datetime
from datetime import datetime, timedelta
timenow = datetime.now()
time_now = timenow - timedelta(days=3)
timestamp = time_now.timestamp() + 899999
time = datetime.fromtimestamp(timestamp)
print(time.strftime('%Y-%m-%d %H:%M:%S'))

# 2.使用os模块线获取当前工作目录的路径,并打印当前目录下所有目录和文件,
# 获取当前脚本的绝对路径, 分割当前脚本文件的目录和文件在tuple中并判断
# 当前文件所在的目录下存不存在文件game.py,如果不存在则创建该文件.
# 并往该文件里写入数据信息,然后判断当前目录下是否存在目录
# /wow/temp/,如果不存在则创建,最后把目录名称改为:/wow/map/


# 3.写函数，检查获取传入列表或元组对象的所有奇数位索引对应的元素，并将其作为新的列表返回给调用者