# !/usr/bin/env python
# coding:utf-8
"""
    Author = 上善若水(QQ:564667570)
    Affiliation = Institute of Subtropical Agriculture, Chinese Academy of Sciences
    Time = '2018-3-7, 23:05'
"""
# 1.计算range(1,99999)的总和,采取三种方式,
# 第一:重复10次计算总时间;
import time
from multiprocessing import Process
import os

def suma(num):
    # print('子进程开始运行,子进程id为:%s' % (os.getpid()))
    num = range(1,99999)
    print(sum(num))
    time.sleep(1)
    # print('子进程结束运行,子进程id为:%s' % (os.getpid()))

if __name__=='__main__':
    print('父进程id为% s' % os.getpid())
    time_start = time.time()
    process = Process(target=suma, args=(range(1,11),))
    process.start()
    process.join()
    time_end = time.time()
    print('processing is completed')
    print('the whole time used % s' % (time_end - time_start))

# 第二:开启十个子线程去计算;

from threading import Thread


# 第三:开启十个进程去计算,然后对比耗时
import time
from multiprocessing import Process, Pool
import os

def suma(num):
    # print('子进程开始运行,子进程id为:%s' % (os.getpid()))
    print(sum(num))
    time.sleep(1)
    # print('子进程结束运行,子进程id为:%s' % (os.getpid()))

if __name__=='__main__':
    print('父进程id为% s' % os.getpid())
    pool = Pool(10)
    time1 = time.time()
    for i in range(10):
        pool.apply_async(suma, args=(range(1,99999),))
    pool.close()
    pool.join()
    time2 = time.time()
    print('processing is completed')
    print('the whole time used % s' % (time2 - time1))


# 2.将第一题计算十次改为使用线程池去计算,线程池线程个数由你自己电脑cpu逻辑核数决定

#