# !/usr/bin/env python
# coding:utf-8
"""
    Author = 上善若水(QQ:564667570)
    Affiliation = Institute of Subtropical Agriculture, Chinese Academy of Sciences
    date = '2018-2-5'
"""

# python 异常的处理


# 1. 什么是错误，异常
# 2. 常见异常的种类
# 3. 异常是怎么处理/
# 4. 自定义异常类 --->内置异常类的扩展


# try:
#     f = open('yong.py')  # 发生了异常
#     print('没有异常发生')   # 代码没有被执行
# except Exception as e:   # 异常的基类匹配 Exception(异常的基类）   --->给未知的异常类型取了个别名
#     print(e)
#     print('发生异常')
# print(1111)

# try:
#     f = open('yong.py')
#     print('everything is fine')
# except Exception as e:
#     print(e)
#     print('something is wrong')
# else:
#     print('the code functions very well')   # try:  except:如果没有发生异常，会走else代码块内的代码
# finally:
#     print("99837")
#

# NameError
# ZeroDivisionError
# print(name)
# print(100/0)

# try:   #异常，提前预判
#     print(name)
# except NameError as e:
#     print(21893)
#
#
# try:
#     print(100/0)
# except ZeroDivisionError as e:
#     print(12456432)
#
# # # SyntaxError  语法错误
# # ：fsa
#
# # IndexError 索引异常
# list_a = [1, 2, 4, 5, 6]
# print(list_a[9])
#
# # IOError 输入输出异常
# try:
#     f = open('tom.py')
# except IOError as e:
#     print(12421)

# # AttributeError 属性错误
# class MyClass():
#     pass
# t = MyClass()
# print(t.age)

# try:
#     num = 100/0
#     print(1243435)
#     # raise ZeroDivisionError('division by zero')
# except Exception as e:
#     print(e)
#     print(565432)

# 自定义异常
# 内置异常不够用

import sys
class MyError(Exception):
    def __init__(self,message):   #初始化函数
        self.message = message
    def __str__(self):
        return self.message

def fun():
    try:
        if len(sys.argv) == 1:   # python 代码.py 9000
            raise MyError('参数个数少了')
    except Exception as e:
        print(e)
if __name__ == '__main__':
    fun()