# !/usr/bin/env python
# coding:utf-8
"""
    Author = 上善若水(QQ:564667570)
    Affiliation = Institute of Subtropical Agriculture, Chinese Academy of Sciences
    Time = '2018-3-23, 20:17'
"""
# 爬取
# url = 'http://tieba.baidu.com/p/2460150866'
# 前三页所有的图片


# 1. first step
import requests
# from urllib.request import urlretrieve # keep source
# import re
# import time
#
# # 2. second step
# def get_code(basic_url):
#     try:
#         for num in range(1, 4):
#             url = basic_url + '?pn=%d' % num
#             page = requests.get(url)
#             return page.text
#     except Exception as e:
#         print(e)
#
# # 3. third step
# def get_image(code):
#     try:
#         # 使用正则匹配图片链接地址
#         reg = r'src="(.+?\.jpg)" pic_ext'
#         image = re.compile(reg)
#         image_list = re.findall(image, code)
#         print(image_list)
#         # picture name
#         num = 1
#         for page in range(1,4):
#             for url in image_list:
#                 urlretrieve(url, './image/' + '%s %d.jpg' % page+'-page-'+num)
#                 num += 1
#                 time.sleep(2)
#     except Exception as e:
#         print(e)
#
# if __name__=='__main__':
#     # get the resource
#     basic_url = 'http://tieba.baidu.com/p/2460150866'
#     get_image(get_code(basic_url))
#     print("图片处理完毕")
#

#coding=utf-8
import requests
from bs4 import BeautifulSoup
import os
from multiprocessing import Pool
import sys

def find_MaxPage():
    all_url = 'http://tieba.baidu.com/p/2460150866'
    start_html = requests.get(all_url,headers=header)
    #找寻最大页数
    soup = BeautifulSoup(start_html.text, "html.parser")
    page = soup.find_all('a',class_='page-numbers')
    max_page = page[-2].text
    return max_page

def Download(href,header,title,path):
    html = requests.get(href,headers = header)
    soup = BeautifulSoup(html.text,'html.parser')
    pic_max = soup.find_all('span')
    pic_max = pic_max[10].text  # 最大页数
    if(os.path.exists(path+title.strip().replace('?','')) and len(os.listdir(path+title.strip().replace('?',''))) >= int(pic_max)):
        print('已完毕，跳过'+title)
        return 1
    print("开始扒取：" + title)
    os.makedirs(path+title.strip().replace('?',''))
    os.chdir(path + title.strip().replace('?',''))
    for num in range(1,int(pic_max)+1):
        pic = href+'/'+str(num)
        #print(pic)
        html = requests.get(pic,headers = header)
        mess = BeautifulSoup(html.text,"html.parser")
        pic_url = mess.find('img',alt = title)
        html = requests.get(pic_url['src'],headers = header)
        file_name = pic_url['src'].split(r'/')[-1]
        f = open(file_name,'wb')
        f.write(html.content)
        f.close()
    print('完成'+title)

def download(href,header,title):

    html = requests.get(href,headers = header)
    soup = BeautifulSoup(html.text,'html.parser')
    pic_max = soup.find_all('span')
    #for j in pic_max:
        #print(j.text)
    #print(len(pic_max))
    pic_max = pic_max[10].text  # 最大页数
    print(pic_max)


'''
#安卓端需要此语句
reload(sys)
sys.setdefaultencoding('utf-8')
'''


if __name__=='__main__':
    if (os.name == 'nt'):
        print(u'你正在使用win平台')
    else:
        print(u'你正在使用linux平台')
    header = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 UBrowser/6.1.2107.204 Safari/537.36'}
    # http请求头
    path = 'D:/mzitu/'
    max_page = find_MaxPage()
    same_url = 'http://www.mzitu.com/page/'

    #线程池中线程数
    pool = Pool(5)
    for n in range(1,int(max_page)+1):
        each_url = same_url+str(n)
        start_html = requests.get(each_url, headers=header)
        soup = BeautifulSoup(start_html.text, "html.parser")
        all_a = soup.find('div', class_='postlist').find_all('a', target='_blank')
        for a in all_a:
            title = a.get_text()  # 提取文本
            if (title != ''):
                href = a['href']
                pool.apply_async(Download,args=(href,header,title,path))
    pool.close()
    pool.join()
    print('所有图片已下完')

