# !/usr/bin/env python
# coding:utf-8
"""
    Author = 上善若水(QQ:564667570)
    Affiliation = Institute of Subtropical Agriculture, Chinese Academy of Sciences
    Time = '2018-3-21, 21:01'
"""

# 下载安装: pip
# install requests或者pycharm安装

import requests
# print(help(requests))
# response = requests.get(url='http://www.baidu.com')  # GET
# print(type(response))
# print(response.status_code)  # 响应码=200
# print(response.url)
# print(response.text)

# response = requests.get(url='http://dict.baidu.com/s', params={'wd': 'python'})
# print(response.url)
# print(response.text)

import requests
import json
# r = requests.post('https://api.github.com/some/endpoint', data=json.dumps({'some': 'data'}))
# print(r.json())
# print(r.text)

# data = {'some': 'data'}
# headers = {'content-type': 'application/json',
#            'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:22.0) Gecko/20100101 Firefox/22.0'}
#
# r = requests.post('https://api.github.com/some/endpoint', data=data, headers=headers)
# print(r.json())
# print(r.raw)
# print(r.content)


import urllib.request
with urllib.request.urlopen('http://www.hao123.com') as r:
    conn = r.read().decode('utf-8')
    print(conn)