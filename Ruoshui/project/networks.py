# !/usr/bin/env python
# coding:utf-8
"""
    Author = 上善若水(QQ:564667570)
    Affiliation = Institute of Subtropical Agriculture, Chinese Academy of Sciences
    Time = '2018-3-12, 10:01'
"""
# Erdos_renyi_graph
from networkx import *
import matplotlib.pyplot as plt
import sys

N = 10
G = nx.grid_2d_graph(N, N)
pos = dict((n, n) for n in G.nodes())
labels = dict(((i, j), i + (N-1-j) * N ) for i, j in G.nodes())
nx.relabel_nodes(G, labels, False)

Pos = {y: x for x, y in labels.items()}
G2 = nx.erdos_renyi_graph(100, 0.1)
nx.draw_networkx(G2, pos=Pos, with_labels=True, node_size=300)
print(G.nodes())
plt.axis('off')
plt.show()

