# !/usr/bin/env python
# coding:utf-8
"""
    Author = 上善若水(QQ:564667570)
    Affiliation = Institute of Subtropical Agriculture, Chinese Academy of Sciences
    Time = '2018-3-2, 9:54'
"""

# locals ---> enclosing ---> globals ---> builting
#
# globals()
# # max = 99
# def fun(x, y):
#     #max = 20
#     def func(z):
#         #max = 10
#         print(max)
#         return x+y+z
#     return func
#
# fun(1,2)(3)

"""nonlocal parameter"""
def outer():
    count = 0  #
    def inner():

        """first type in this case"""
        # call_count = count +1
        # print(call_count)

        """to differenciate the outer parameter, with nonlocal"""
        nonlocal count
        count = count + 1
        print(count)

    return inner
outer()()