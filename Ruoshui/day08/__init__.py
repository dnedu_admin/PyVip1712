# !/usr/bin/env python
# coding:utf-8
"""
    Author = 上善若水(QQ:564667570)
    Affiliation = Institute of Subtropical Agriculture, Chinese Academy of Sciences
    Time = '2018-3-2, 9:54'
"""

# python module
# """function is also the object"""
# import random
#
# def make_num():
#     return random.randint(1,9)
#
# def make_change(func,num):
#     return func() * num
#
# z = make_change(make_num, 30.0)
# print(z)

# """eval function"""
# import random
# from ast import literal_eval
#
# x = random.randint(1,9)
# print(x)
# print(eval('x+1'))
# b_tuple = '(2,3,45,52)'
# print(eval(b_tuple))
# print(literal_eval('1+2'))

# a_list = [1,2,104,65,64]
# # b_list = [2,3,3, 52,62]
# # print(list(zip(a_list, b_list)))
# print(sorted(a_list))
# print(a_list.sort())
# print(a_list.reverse())

"""to calculate the recursive_function values"""
# def recursive_func(num):
#     if not isinstance(num, int):
#         raise TypeError('input number is not int')
#     if num == 1 or num == 0:
#         return 1
#     if num < 0:
#         raise ValueError('number should be bigger or equal than 0')
#     return num * recursive_func(num - 1)
#
# result = recursive_func(100)
# print(result)

class Human():
    @property
    def name(self):
        return self._name # 私有变量/保护变量
    @name.setter
    def name(self,name):
        self._name = name

    @name.deleter
    def name(self):
        del self._name
    @name.getter
    def name(self):
        return "name is: {}".format(self._name)

h = Human()
h.name = 'ben'  # 递归调用
print(h.name)

