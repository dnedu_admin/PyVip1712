# !/usr/bin/env python
# coding:utf-8
"""
    Author = 上善若水(QQ:564667570)
    Affiliation = Institute of Subtropical Agriculture, Chinese Academy of Sciences
    Time = '2018-3-26, 10:38'
"""

# class Student(object):
#
#     @property
#     def birth(self):
#         return self._birth
#
#     @birth.setter
#     def birth(self, value):
#         self._birth = value
#
#     @property
#     def age(self):
#         return 2018 - self._birth
#
# tom = Student()
# tom.birth = 2002
# print(tom.age)
#
# class Screen(object):
#
#     @property
#     def width(self):
#         return self._width
#
#     @width.setter
#     def width(self, value):
#         self._width = value
#
#     @property
#     def height(self):
#         return self._height
#
#     @height.setter
#     def height(self, value):
#         self._height = value
#
#     @property
#     def resolution(self):
#         return self._width + self._height
#
# s = Screen()
# s.width = 1024
# s.height = 768
# print(s.resolution)


# 多重继承
# class Fib(object):
#     def __init__(self):
#         self.a, self.b = 0, 1
#
#     def __iter__(self):
#         return self
#
#     def __next__(self):
#         self.a, self.b = self.b, self.a + self.b  #计算下一个值
#         if self.a > 100000:   # 退出循环条件
#             raise StopIteration()
#         return self.a
#
# # 测试
# # for n in Fib():
# #     print(n)
#
#
# class Fib(object):
#     def __getitem__(self, item):
#         a, b = 1, 1
#         for x in range(1000):
#             a, b = b, a + b
#         return a
#
# f = Fib()
#
# print(f[100])


# class Chain():
#     def __init__(self, path=''):
#         self._path = path
#     def __getattr__(self, path):
#         return Chain('%s/%s' % (self._path, path))
#     def __str__(self):
#         return self._path
#     __repr__=__str__
#
# print(Chain().status.user.timeline.list)

# 任何类，只需要定义一个__call__()方法，就可以直接对实例进行调用。

# class Student(object):
#     def __init__(self, name):
#         """to set the initiate parameters"""
#         self.name = name
#     def __call__(self, *args, **kwargs):
#         """using __call__() can directly get the attr of example"""
#         print('My name is %s' % self.name)
#
# s = Student('Yong')
# s()
#
# """
# to judge object can be callable or not, using callable(),
# return true or false
# """
# print(callable(Student('Michal')))

# 使用枚举类
# from enum import Enum
# Month =
# month = Enum('Month', ('Jan', 'Feb', 'Mar'
#                         'May', 'Jun', 'Jul', 'Aug',
#                        'Sep', 'Oct', 'Nov', 'Dec'
#                        ))
# for name, member in Month.__members__.items():
#     print(name, '=>', member, ',', member.value)

# class Hello(object):
#     def hello(self, name = 'world'):
#         print('hello %s' % name)


# metaclass是类的模板，所以必须从`type`类型派生：
# class ListMetaclass(type):
#     def __new__(cls, name, bases, attrs):
#         attrs['add'] = lambda self, value: self.append(value)
#         return type.__new__(cls, name, bases, attrs)
#
# class Mylist(list, metaclass=ListMetaclass):
#     pass
# from enum import Enum
# class Color(Enum):
#     RED = 1
#     GREEN = 2
#     BLUE = 3
# print(repr(Color.GREEN))

# from enum import Enum, unique
# @unique
# class Mistake(Enum):
#     ONE = 1
#     TWO = 2
#     THREE = 3
#     FOUR = 3

from enum import Enum, auto
# class Color(Enum):
#     RED = auto()
#     BLUE = auto()
#     GREEN = auto()
#
# print(list(Color))
#
# class AutoName(Enum):
#     def _generate_next_value_(name, start, count, last_values):
#         return name
#
# class Ordinal(AutoName):
#     NORTH = auto()
#     SOUTH = auto()
#     EAST = auto()
#     WEST = auto()
#
# print(list(Ordinal))

# a = ['Google', 'Baidu', 'Runoob', 'Taobao', 'QQ']
# for i in range(len(a)):
#     print(i, a[i])
#
# for letter in 'Runoob':
#     if letter == 'o':
#         continue
#     print('the current letter : %s' % letter)
#
# var = 10
# while var > 0:
#     var = var - 1
#     if var == 5:
#         continue
#     print('the current value: % d' % var)
#
# print('Adios')

# i=1
# while i<=9:
#      #里面一层循环控制每一行中的列数
#      j=1
#      while j<=i:
#           mut =j*i
#           print("%d*%d=%d"%(j,i,mut), end="  ")
#           j+=1
#      print("")
#      i+=1

# import sys
# list = [1, 2,4, 52, 532, 5]
# it = iter(list)
#
# while True:
#     try:
#         print(next(it))
#     except StopIteration:
#         sys.exit()

# import sys
# def fibonacci(n):
#     a, b, counter = 0, 1, 0
#     while True:
#         if (counter > n):
#             return
#         # yield a
#         a, b = b, a + b
#         print('%d, %d' % (a, b))
#         counter += 1
#
# f = fibonacci(10)
#
# while True:
#     try:
#         print(next(f), end=' ')
#     except StopIteration:
#         sys.exit()


# def get():
#     m = 0
#     n = 2
#     l = ['s', 1, 3]
#     k = {1: 1, 2: 2}
#     p = ('2', 's', 't')
#     while True:
#         m += 1
#         yield m
#         yield m, n, l, k, p
#
#
# it = get()
# print(next(it))  # 1
# print(next(it))  # (1, 2, ['s', 1, 3], {1: 1, 2: 2}, ('2', 's', 't'))
#
# print(next(it))  # 2
# print(type(next(it)))  # <class 'tuple'>

# from collections import deque
# queue = deque(['eric', 'john', 'michael'])
# queue.append('terry')
# queue.append('graham')
# print(queue.popleft())
# print(queue)


matrix = [[1, 2, 3, 4],[5, 6, 7, 8], [9, 10, 11, 12]]
print(matrix)

# print([[row[i] for row in matrix] for i in range(4)])
# transposed = []
# for i in range(4):
#     transposed.append([row[i] for row in matrix])
# print(transposed)

transposed = []
for i in range(4):
    # the following 3 lines implement the nested listcomp
    transposed_row = []
    for row in matrix:
        transposed_row.append(row[i])
    transposed.append(transposed_row)

print(transposed)

t = 1234, 21334, 'hello'
print(t[0])
u = t, (1, 2, 3, 4, 5)
print(u)
basket = {'apple', 'orange', 'apple', 'pear', 'orange', 'banana'}
print(basket)

import pickle

