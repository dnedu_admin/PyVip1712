# !/usr/bin/env python
# coding:utf-8
"""
    Author = 上善若水(QQ:564667570)
    Affiliation = Institute of Subtropical Agriculture, Chinese Academy of Sciences
    Time = '2018-3-1, 15:37'
"""

# 1.range(),需要生成下面的列表,需要填入什么参数:
# 1.[3,4,5,6]
print(list(range(3,7)))
# 2.[3,6,9,12,15,18]
print(list(range(3,19,3)))
# 3.[-20,200,420,640,860]
print(list(range(-20,900,220)))

# 2.列表推导式或者生成器表达式(2选择1)完成1-100之间所有偶数和
# 列表推导式
from functools import reduce
print(reduce(lambda x, y : x + y, [x for x in range(2,101,2)]))
# 生成器表达式
print(reduce(lambda x, y: x + y, range(2,101,2)))

# 3.定义一个函数,使用:关键字yield创建一个生成器,求1,3,5,7,9各数
# 字的三次方,并把所有的结果放入到list里面打印输入
def make_generator(n):
    for num in range(1,n,2):
        yield pow(num, 3)
print(list(make_generator(10)))

# 4.写一个自定义的迭代器类(iter)

class MyIterator(object):
    def __init__(self, step):
        self.step = step

    def __get_item__(self):
        self.item = item

    def __next__(self):
        """Returns the next element."""
        if self.step <= 0:
            raise StopIteration('There is not include the parameters')
        self.step -= 1
        return pow(self.step, 3)

    def __iter__(self):
        """Returns the iterator itself."""
        return self

print(list(MyIterator(9)))