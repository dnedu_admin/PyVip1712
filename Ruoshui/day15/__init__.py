# !/usr/bin/env python
# coding:utf-8
"""
    Author = 上善若水(QQ:564667570)
    Affiliation = Institute of Subtropical Agriculture, Chinese Academy of Sciences
    Time = '2018-2-28 20:33'
"""
# def test_var_args(f_arg, *argv):
#     print("first normal arg:", f_arg)
#     for arg in argv:
#         print("another arg through *argv:", arg)
#
# test_var_args('yasoob', 'python', 'eggs', 'test')
#
# print(list(range(2,-6,-1)))
#
# def mk_range(y, x=0, z=1):
#     if not isinstance(y,int):
#         raise TypeError('传入的参数类型不正确')
#     for num in range(x, y, z):
#         print(num)
# mk_range(3,1,1)


# 可迭代对象？---->容器数据类型

# from collections import Iterable, Iterator
# a = 'abc'
# b = [1, 2, 3]
# c = (4, 5, 6)
# d = {'name': 'tom', 'age': 22}
# if isinstance(a, Iterable):
#     print('a是可迭代对象')
# if isinstance(b,Iterator):
#     print('b是可迭代对象')
# if isinstance(c,Iterable):
#     print('c是可迭代对象')
# if isinstance(d,Iterable):
#     print('d是可迭代对象')
# if not isinstance(88,Iterable):
#     print('88不是可迭代对象')


# def main():
#     function()
#     function2()
#
# if __name__ == '__main__':
#     main()

# a_dict = {'name': 'tom', 'age': 22}
# for key, value in a_dict.items():
#     print(key + ':' + str(value))  # 'age' + ':' + 22  字符串+整数

# 引申出一个函数，内置函数 enumerate 给每一个元素取一个对应的索引
# for index, item in enumerate('tom'):  # 索引默认是从0 开始
#     print(index, item)
# for index, item in enumerate([1,3,4,26,63], 8):  # 第二个参数是起始的索引， 第二个参数可以为负数或整数
#     print(index, item)
# for item in enumerate([3, 2, 1]):
#     print(item)

# 列表推导式
# a_list = list((1, 2, 3))
# print(a_list)
# b_list = list(range(1,11)) # [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
# print(b_list)
# c_list = list('python')  # ['p', 'y', 't', 'h', 'o', 'n']
# print(c_list)

# 列表推导:
# print([x+2 for x in range(10)])  # [2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
# print([x**9 for x in range(8)])  # [0, 1, 512, 19683, 262144, 1953125, 10077696, 40353607]

# # 复杂列表推导
# print([x - 3 for x in [1,2,3,4,5,6,6,7,7,9] if x > 3])
#
# # 循环遍历看看:
# for i in [x+4 for x in range(1,9) if x % 2 == 0]:
#     print(i)
# # 再看看下面这个列表推导式:
# print([x*y for x in range(1, 3) for y in range(3, 5)])
# print([x+y for x in range(1, 9) if x % 2 == 0 for y in range(1,9) if y % 2 != 0])

# 进入迭代器主题: 到底什么是迭代器?python2.2 版本引入迭代器
# Iterator:
# from collections import Iterator, Iterable
#
# a_iter = iter([1, 2, 3])
# print(list(a_iter))
# b_iter = iter([4, 5, 6])
# print(type(b_iter))
# print(isinstance(a_iter, Iterable))
# print(isinstance(b_iter, Iterator))
# # print(next(a_iter))
# # print(next(a_iter))
# for item in list(a_iter):
#     print(item)


# _str = ‘pythoner’,那么a_str是可迭代对象,为什么这么说:
# for letter in a_str:
#     print(letter)
# from collections import Iterable,Iterator
# print(isinstance(a_str,Iterable))     True  or  False
# print(isinstance(a_str,Iterator))      Yes  or   No
# b_iter = iter([1,2,3])
# for num in b_iter:
#     print(num)
# print(isinstance(b_iter,Iterable))     True  or  False
# print(isinstance(b_iter,Iterator))      Yes  or   No

# print(tuple(range(10)))
#
# a_tuple = (1, 2, 3, 4, 5, 6, 7, 8, 9)
# b_tuple = a_tuple[::-1]
# print(b_tuple)

# # 生成器和列表推到有什么联系吗？
# a_list = [x for x in range(9)]
# print(type(a_list))
# print(a_list)
# b_generator = (x for x in [1,2,3]) #（）生成器对象， []是列表对象，推导式
# print(type(b_generator))
# print(b_generator)
# # 好,看看next函数
# print(next(b_generator))
# print(next(b_generator))
# print(next(b_generator))
# # print(next(b_generator))

# 其它方式 ---》生成器
# 函数定义结合关键字yield ---》组合方式 ---》定义生成器
def make_generator(n):
    while n > 2:
        yield n  # 关键字 ---》 yield(): 可暂停代码
        n -= 1

for num in make_generator(15):
    print(num)
