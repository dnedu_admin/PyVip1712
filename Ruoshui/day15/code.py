# !/usr/bin/env python
# coding:utf-8
"""
    Author = 上善若水(QQ:564667570)
    Affiliation = Institute of Subtropical Agriculture, Chinese Academy of Sciences
    Time = '2018-2-28 20:33'
"""



# # make a gererator
# def make_generator(y, x = 0, z = 1):
#     """to judge the type of input variables is int?"""
#     if not isinstance(y, int):
#         raise TypeError('Incoming parameter type is incorrect.')
#     for num in range(y):
#         print(num)
#
# p = make_generator(2)
# print(p)

# class MyClass():
#     """ """
#     def __init__(self, max):
#         super().__init__()
#         self.max = max
#
#     def __iter__(self):
#         self.a = 0
#         self.b = 1
#         return self
#
#     def __next__(self):
#         MyClass = self.a
#         if MyClass > self.max:
#             raise StopIteration
#         self.a, self.b = self.b, self.a + self.b
#         return MyClass
#
# def main():
#     myClass = MyClass(1000)
#     for i in myClass:
#         print(i)
#
# if __name__ == '__main__':
#     main()

#
# def make_generator(y, x = 0, z = 1):
#     if not isinstance(y, int):
#         raise TypeError('Incoming parameter type is incorrect.')
#     for num in range():
#         print(num)
#
# make_generator(2)


# def reverse(data):
#     for index in range(len(data)-1, -1, -1):
#         yield data[index]
#
# for char in reverse('hello'):
#     print(char)

# unique_characters = {'E', 'D', 'M', 'O', 'N', 'S', 'R', 'Y'}
# gen = (ord(c) for c in unique_characters)
# print(gen)

def reduce(function, iterable, initializer=None):
    it = iter(iterable)
    if initializer is None:
        try:
            initializer = next(it)
        except StopIteration:
            raise TypeError('reduce() of empty sequence with no initial value')
    accum_value = initializer
    for x in it:
        accum_value = function(accum_value, x)
    return accum_value

print(reduce(lambda x, y: x + y, [1,2,4,5,62,9]))

from functools import reduce
print(reduce(lambda x, y: x*y, range(1,10)))
print(1*2*3*4*5*6*7*8*9)