# !/usr/bin/env python
# coding:utf-8
"""
    Author = 上善若水(QQ:564667570)
    Affiliation = Institute of Subtropical Agriculture, Chinese Academy of Sciences
    Time = '2018-3-26, 21:03'
"""

# class MyClass():
#     def __init__(self, a_list):
#         self.a_list = a_list
#         print("set the data!")

#     # 两个核心的协议方法
#     def __enter__(self):  # 进入上下文环境自动执行 可执行某些逻辑
#         print("execute the code")
#         self.a_list.append('ben')
#         return self   # 要返回当然实例对象
#
#     def __exit__(self, exc_type, exc_val, exc_tb): # 退出上下文环境自动执行
#         # 后面三个参数是异常信息， exc_type 异常类型， 异常值， 堆栈信息
#         self.a_list = None
#         if exc_type:  #异常触发了
#             print(exc_val)
#         else:  #没有异常
#             print('正常执行')
#
#     def work(self):  # 先初始化函数，再执行__enter__， 然后再执行function, __exit__
#         for item in self.a_list:
#             print(item)
#
# # 怎么用？
# # with open() as f:
# with MyClass([1,3,53,6,'a']) as f:
#     f.work()

# 上下文表达式 ---》创建类的实例
# 注意代码执行的顺序

# contextlib库 ---》来定义上下文管理器
# from contextlib import contextmanager  # 管理器对象
# class MyClass():
#     def __init__(self, a_list):
#         self.a_list = a_list
#         print("set the data!")
#
#     def work(self):  # 先初始化函数，再执行__enter__， 然后再执行function, __exit__
#         for item in self.a_list:
#             print(item)
#
# # 不遵循协议
# # 使用contextmanager
#
# @contextmanager     # 不用在类里面去定义两个核心的协议方法
# def mk_context(a_list):
#     print('开始处理')
#     tls = MyClass(a_list)  #拿到实例
#     yield tls    # __exit__
#     print('处理完毕')
#
# with mk_context([1,3,53,6,'a']) as mk:    # mk -->yield 抛出来的实例对象
#     mk.work()
#

# Closing
# from contextlib import closing
#
# class MyClass():
#     def __init__(self):
#         print('启动电源，初始化数据')
#
#     def work(self):
#         print('执行任务')
#
#     def close(self):    # 显示调用？还是隐式调用？
#         print('任务执行完毕')
#
# with closing(MyClass()) as f:   # closing (定义了close 的一个类的实例对象）
#     f.work()   # f.close()


# # colorama python
# from colorama import Fore, Back, Style
#
# print(Fore.GREEN + '我是刘勇')
# print(Back.RED + '我是刘勇')
# print(Fore.BLUE + "what color is")
# print(Style.RESET_ALL)
# print('the fault color')

from virtualenv import *


# hash ---> MD5算法， sha1算法
# MD5算法：密码的存储
# sha1（算法比较多）：数字签名 --->区别其他的标记

import hashlib     # 高度封装
#
# # MD5
# m = hashlib.md5()   # 构建对象
# m.update('pythonweb'.encode('utf-8'))    # 更新数据
# print(m.hexdigest())  # d10ad34fb87bc341d0e89571829beb7e
#
# # sha1
# n = hashlib.sha1()   # 构建对象
# n.update('pythonweb'.encode('utf-8'))    # 更新数据
# print(n.hexdigest())  # 728ddffea93b2c908fd98a7a3224062372067c19

# md5, sha1算法 --->安全性并不是很高，受到攻击
# 有些信息，对安全性要求非常高， 达不到要求, 通过更高级的算法

import hashlib, binascii

pk = hashlib.pbkdf2_hmac('sha512', b'ben', b'pythonweb', 9999, dklen=32)    # 高级算法
conn = binascii.hexlify(pk)
print(conn)
# 第一个参数：基础算法名称
# 第二个参数：安全性类似加密密码的处理
# 第三个参数：传入字符串，用来迭代的字符串
# 第四个参数：计算的轮次
# 第五个参数: 指定计算之后key的长度