# coding:utf-8

__author__ = 'Yong(QQ:564667570)'
__affiliation__ = 'Institute of Subtropical Agriculture, Chinese Academy of Sciences'
__date__ = 'date'

# python setup.py sdist

# 1. 数学计算内置函数
# print(abs(-5))   #求绝对值
# print(max(1, 3, 4, 5, 8))   #求最大值
# print(max(['bc', 'hf', 'o']))  # asck码, abc 这样往下面排
# print(max(['21', '2', '3', '100']))    # asc码表，字符串是比较第一位
#
# print(sorted(['21', '2', '3', '100']))   # 排列
# print(sum([1,3, 5]))  # 求和函数
#
# print(sum(range(1, 100, 2)))  # 求和函数 while --> for 1+3+5+....+99
# print(len('yong'))
#
# print(len({'name': 'ben', 'age':22}))  # 字典的数据项字数
# print(len((1,3,5)))
#
# print(pow(2, 3))  # a的b次方
# print(2**3)  # 效率更高一些比pow（2， 3）
# print(pow(2, 2, 3))  # 2**2%3

# 类型转换的功能函数
# print(list(range(9)))
# print(divmod(8, 2)) # (8%2, 余数=0）

# 四舍五入
# print(round(-4.4))  # 以6为进，5为舍
# print(complex(2,4))  # (2+4j)
# print(tuple(range(9)))  # (0, 1, 2, 3, 4, 5, 6, 7, 8)
# print(dict(name= 'ben', age=22))  # {'name': 'ben', 'age': 22}

# 切片函数
# print('python'[slice(1,4)])  # 切片函数
# print('python'[1:4])  # 切片函数
# print([1, 23, 4, 5, 543, 2][slice(1,4)])  # 切片函数

# 循环结构  ---->怎么样判断

# python 进制转换

# print(oct(20))  # 十进制转八进制
# print(bin(20))  # 十进制转二进制
# print(hex(20))  # 十进制转十六进制

# all(and)/any(or) ---> 逻辑判断
# print(all(['1',' ','ben']))
#
# # compile  --->函数把字符编译成字节代码（概念） --->提高执行的效率（目的）
# code = compile("print('hello, world')", '', 'exec')
# exec(code)
# eval(code)

import os
# print(__doc__)
# print(os.__all__)
# print(dir(os))
# print(help(os))

print(__file__)  # 当前脚本文件的全路径

