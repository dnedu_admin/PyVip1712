# !/usr/bin/env python
# coding:utf-8
"""
    Author = 上善若水(QQ:564667570)
    Affiliation = Institute of Subtropical Agriculture, Chinese Academy of Sciences
    Time = '2018-3-14, 21:04'
"""
import re

# pattern = re.compile('tom')
#
# print(type(pattern))
#
# match = pattern.match('tom is a good boy!')
# if match:
#     print(match.group())

# . 通配符
# print(re.match(r'.tom', r'$tom?89 is a not a good boy!'))  # r 原始字符串

import re
# pattern = re.compile(r'.haha', re.S)
# target = pattern.match('\nhaha')
# if target:
#     print(target.group())
# * 可以匹配0次或多次， + 匹配1次及多次， ？ 匹配0次或是1次 $:字符串的结尾（结尾符号）
# | : 从左边往右边匹配 --->
# print(re.findall('b+\ne?', 'b+\ne?'))
print(re.findall(r'^ben','ben'))
print(re.findall(r'^ben$','ben'))
