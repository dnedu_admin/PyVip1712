# !/usr/bin/env python
# coding:utf-8
"""
    Author = 上善若水(QQ:564667570)
    Affiliation = Institute of Subtropical Agriculture, Chinese Academy of Sciences
    Time = '2018-3-2, 20:52'
"""

"""
Decorator
1.函数也是对象
2. 装饰器的演变 
3. 函数的嵌套
4. 装饰器
5. 带参数的装饰器
"""

# 1. 函数也是对象， 也可以当做参数传递：
def sleep(name):
    print('% s is sleeping' % name)

def make_fun(fun, na):
    sleep('yong')
    print('% s wake up, time to work' % na)

a = sleep
print(id(sleep))
print(id(a))

make_fun('yong', 'time')

# 装饰器的演变
# 1. 加工函数

