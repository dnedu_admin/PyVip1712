# !/usr/bin/env python
# coding:utf-8
"""
    Author = 上善若水(QQ:564667570)
    Affiliation = Institute of Subtropical Agriculture, Chinese Academy of Sciences
    Time = '2018-3-2, 11:39'
"""


# def sleep(name):
#     print('% s is sleeping' % name)
#
#
# def make_fun(fun):
#     sleep('tom')
#     print('睡醒了，该起来了')
# make_fun('javen')
#
# f = sleep
# print(id(sleep))
# print(id(f))
# print(sleep.__name__)
# print(f.__name__)
#
# print('方法执行之前')
# make_fun(sleep)
# print('方法执行之后')
#
# f = sleep
# print(id(sleep))
# print(id(f))
# print(sleep.__name__)
# print(f.__name__)
#
# print('方法执行之前')
# make_fun(sleep)
# print('方法执行之后')

import pandas

print(callable(pandas))
