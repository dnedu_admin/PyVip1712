# coding:utf-8

__author__ = 'Yong'
__affiliation__ = 'Institute of Subtropical Agriculture, Chinese Academy of Sciences'

import random
import math

# zy02. 十进制数字2034的二进制表示，八进制表示，16进制表示
# num = 2034
# num_2 = bin(num)
# num_8 = oct(num)
# num_16 = hex(num)
#
# print(num_2,num_8,num_16)

# zy03. 生成一个100到300之间的随机整数n，
# 求其平方根，产生一个1到20之间的随机浮点数p，求大于p 的最小整数。

# random_num = random.randint(100, 300)
# sqrt_rn = math.sqrt(random_num)
# print(random_num, sqrt_rn)
#
# # random int
# p = random_float = random.uniform(1, 20)
# bigger_p =math.ceil(p)
# print(p, bigger_p)

# zy04. 运维系统中监控到磁盘信息，磁盘使用量为1 2106450611211bytes,
# 页面展示中需要展示单位为GB，保留2为小数。
# vol = 2106450611211
# gb_vol = vol/1024**3
# print(round(gb_vol, 2))

# zy05. 使用input函数,记录键盘输入的内容,打印输出该值的类型, 并转换为数值型。
string = input('input single letter in here: ')
print(type(string))
print(ord(string))

# 判断数值num大小,如果num>=90,输出打印:你的成绩为优秀
if ord(string) >= 90:
    print('Your score is excellent!')
# 如果num>=80 and num<90,输出打印:你的成绩为良好
if 90 > ord(string) >= 80:
    print('Your score is good!')
# 如果num>=60 and num <80,输出打印:你的成绩为一般
if 80 > ord(string) >= 60:
    print('Your score is passed!')
# 如果num<60,输出打印:你的成绩为不合格
else:
    print('you did not pass the exam!')
