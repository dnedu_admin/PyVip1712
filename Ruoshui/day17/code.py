# !/usr/bin/env python
# coding:utf-8
"""
    Author = 上善若水(QQ:564667570)
    Affiliation = Institute of Subtropical Agriculture, Chinese Academy of Sciences
    Time = '2018-3-5, 20:49'
"""

# Python IO programming, it's like the file operation
# 1. 复习文件操作
# 2. stringIO, BytesIO
# 3. 深浅copy
# 4. pickle module
# 5. json module


f = open('yong.txt', 'wb+')
conn = f.read(10)
print(conn)
f.close()

# 二进制数据的操作 'wb','rb' 'rb+',是针对二进制字符串

# pickle and json
# import pickle
#
# c_dict = {'name':'tom', 'age':22}
# f = open('ben', 'wb+')
# pickle.dump(c_dict, f)
# f.close()
#
# f = open('ben', 'rb+')
# r = pickle.load(f)
# print(r)
# f.close()

# import json
#
# c_dict = {'name': 'tom', 'age': 22}
# f = open('yong.py', 'w+')
# json.dump(c_dict, f)
# f.close()
#
# f = open('yong.py', 'r+')
# r = json.load(f)
# print(r)
# f.close()

