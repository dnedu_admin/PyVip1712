# !/usr/bin/env python
# coding:utf-8
"""
    Author = 上善若水(QQ:564667570)
    Affiliation = Institute of Subtropical Agriculture, Chinese Academy of Sciences
    Time = '2018-3-5, 22:59'
"""

# homework
"""
1.使用pickle,json(注意:两个模块)
把字典a_dict = [1,2,3,[4,5,6]]
序列化与反序列化,序列化,保存到文件名为serialize.txt文件里面,然后打开,并读取数据
反序列化为python对象加载到内存,并打印输出
尝试序列化/反序列在非文件中,直接操作

2.自己写代码对比深浅拷贝的区别,写完代码然后看看运行结果,然后分析原因
   比如：a_list=[1,2,3,4,[8,9,0]]

3.分别写代码完成内存缓冲区文本数据和二进制数据的读/写操作
"""
# 1.使用pickle,json(注意:两个模块)把字典a_dict = [1,2,3,[4,5,6]]
# 序列化与反序列化,序列化,保存到文件名为serialize.txt文件里面,然后打开,并读取数据
# 反序列化为python对象加载到内存,并打印输出尝试序列化/反序列在非文件中,直接操作

# import modules
import pickle

# set a list sequence
a_list = [1,2,3,[4,5,6]]
f = open('serialize.txt', 'wb+')
pickle.dump(a_list, f)
f.close()

f = open('serialize.txt','rb+')
content = pickle.load(f)
print(content)
f.close()

import json
# to use json for 序列化与反序列化
a_list = [1,2,3,[4,5,6]]
file = open('serialize2.txt', 'w+')
json.dump(a_list, file)
file.close()

f = open('serialize2.txt','r+')
content = json.load(f)
print(content)
f.close()

"""反序列化为python对象加载到内存,并打印输出尝试序列化/反序列在非文件中,直接操作"""
from io import BytesIO
f = BytesIO()
f.write('今天天气不好\n'.encode('utf-8'))
f.write('明天天气也不好\n'.encode('utf-8'))
f.write('后天天气就更不好了\n'.encode('utf-8'))
print(f.getvalue().decode('utf-8'))

# to get the data
from io import StringIO
f = StringIO('反序列化为python对象加载到内存\n并打印输出尝试序列化/反序列在非文件中\n直接操作\n')
while True:
    content = f.readline()
    if len(content) != 0:
        print(content.strip())
    else:
        break
# content = f.readlines()
# print(content)

# 2.自己写代码对比深浅拷贝的区别,写完代码然后看看运行结果,然后分析原因
# 比如：a_list=[1,2,3,4,[8,9,0]]

from copy import copy
a_list= [1, 2, 3, 4, [8, 9, 0]]

b_list = copy(a_list)
b_list[4][0] = 44 # 改变copy变量，原变量中的内层变量也随之变动
b_list[3] = 88
print(a_list, b_list)
a_list = [3, 4, 5, 6]
print(a_list, b_list) # 内层变量也随着变动

from copy import deepcopy

a_list= [1, 2, 3, 4, [8, 9, 0]]
b_list = deepcopy(a_list)  # 改变deepcopy变量，原变量中的内层变量不会随之变动
b_list[4][0] = 55
print(a_list, b_list)
b_list[3] = 88
a_list = [3, 4, 5, 6]
print(a_list, b_list)

"""3.分别写代码完成内存缓冲区文本数据和二进制数据的读/写操作"""

from io import StringIO
files = StringIO('反序列化为python对象加载到内存\n并打印输出尝试序列化/反序列在非文件中\n直接操作\n')
while True:
    content = files.readline()
    if len(content) != 0:
        print(content.strip())
    else:
        break

from io import BytesIO
f = BytesIO('反序列化为python对象加载到内存\n并打印输出尝试序列化/反序列在非文件中\n直接操作\n'.encode('utf-8'))
while True:
    conn = f.readline()
    if len(conn) != 0:
        print(conn.strip().decode())
    else:
        break
