# !/usr/bin/env python
# coding:utf-8
"""
    Author = Yong Liu (y.liu@isa.ac.cn)
    Affiliation = Institute of Subtropical Agriculture, Chinese Academy of Sciences
    Time = '2018-4-2, 9:50'
"""

# Load libraries
import pandas as pd
from pandas.plotting import scatter_matrix
import matplotlib.pyplot as plt
from sklearn import model_selection
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC

# Load dataset
dataset = pd.read_excel('SbowelSample.xlsx', encoding='utf-8')
df = pd.DataFrame(data=dataset)

columns = df.columns.values   # to get the column-title
print(columns)

"""
['No.' 'H/D' 'TRM5P(DA)' 'T1R1P(DA)' 'T1R2P(DA)' 'T1R3P(DA)' 'SGLT1P(DA)'
 'GPR40P(DA)' 'GPR41P(DA)' 'GPR43P(DA)' 'GPR119P(DA)' 'GPR120P(DA)'
 'GPRC6AP(DA)' 'GustducinP(DD)' 'TRM5P(DD)' 'T1R1P(DD)' 'T1R2P(DD)'
 'T1R3P(DD)' 'SGLT1P(DD)' 'GPR40P(DD)' 'GPR41P(DD)' 'GPR43P(DD)'
 'GPR119P(DD)' 'GPR120P(DD)' 'GPRC6AP(DD)' 'GustducinP(DF)' 'TRM5P(DF)'
 'T1R1P(DF)' 'T1R2P(DF)' 'T1R3P(DF)' 'SGLT1P(DF)' 'GPR40P(DF)'
 'GPR41P(DF)' 'GPR43P(DF)' 'GPR119P(DF)' 'GPR120P(DF)' 'GPRC6AP(DF)'
 'GustducinP(DT)' 'TRM5P(DT)' 'T1R1P(DT)' 'T1R2P(DT)' 'T1R3P(DT)'
 'SGLT1P(DT)' 'GPR40P(DT)' 'GPR41P(DT)' 'GPR43P(DT)' 'GPR119P(DT)'
 'GPR120P(DT)' 'GPRC6AP(DT)']
"""

# set X, Y dataset.
X = df[columns]
Y = df['H/D']

# to get the training and validation datasets
validation_size = 0.15  # set the validation size
seed = 10  # set the random_state
X_train, X_validation, Y_train, Y_validation = model_selection.train_test_split(X, Y,
                                                                                test_size=validation_size, random_state=seed)

# print(X_train.describe())
# print(X_validation.describe())

