# !/usr/bin/env python
# coding:utf-8
"""
    Author = 上善若水(QQ:564667570)
    Affiliation = Institute of Subtropical Agriculture, Chinese Academy of Sciences
    Time = '2018-3-12, 22:48'
"""

# 创建两个协程函数对象, 分别完成1, 3, 5…99, 2, 4, 6…, 100
# 然后让两个协程函数对象分别交替执行.在每一个协程函数对象的内部先打印输出
# 每一个数字, 然后执行完一轮之后打印提示信息, 已经交替执行完毕一轮
# 等所有的协程函数对象执行完毕之后, 分析一下数据信息

# from collections import Iterator, Iterable
#
# def get_num(num):
#     for i in range(1, num):
#         yield i
# print(get_num(9))
#
# gen = get_num(9)
# print(isinstance(gen, Iterable))
# print(isinstance(gen, Iterator))
# print(next(gen))
# print(next(gen))
# for num in get_num(9):
#     print(num)

# def one():
#     num = 101
#     while num >1:
#         num -= 1
#         if num % 2 != 0:
#             yield num
#
# def two():
#     for num in one():
#         three(num)
#
# def three(num):
#     if num < 20:
#         print('start the task:')
#         print('the result of the code execution')


# yield协程


# def get_result(content):
#     print('Looking for %s\n' % content)
#     while True:
#         line = (yield)
#         if content in line:
#             print(line)
#
# result = get_result('ben')
# next(result)
# result.send('i love ben')
# result.send('my name is ben')
# result.send('tom is good boy')
# result.close()
