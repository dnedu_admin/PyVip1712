# !/usr/bin/env python
# coding:utf-8
"""
    Author = 上善若水(QQ:564667570)
    Affiliation = Institute of Subtropical Agriculture, Chinese Academy of Sciences
    Time = '2018-3-30, 11:02'
"""

def function(num):
    if isinstance(num, (int, float)):
        return pow(num, 3)
    else:
        raise Exception('Input the wrong type parameter')

class Person():
    def __init__(self, name):
        self.name = name

    def func(self):
        print('%s is running' % self.name)

    def function(self, num):
        if isinstance(num, (int, float)):
            return pow(num, 3)
        else:
            raise Exception('Input the wrong type parameter')

