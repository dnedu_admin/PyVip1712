# !/usr/bin/env python
# coding:utf-8
"""
    Author = 上善若水(QQ:564667570)
    Affiliation = Institute of Subtropical Agriculture, Chinese Academy of Sciences
    Time = '2018-3-30, 9:47'
"""

# 自己写一个函数, 然后写单元测试用例



# def fun():
#     try:
#         r = input('Input the parameters: ')
#         num = int(r)
#         assert (num < 30)   # assert conditions > 30
#         conn = 10 * num
#         print(conn)
#     except Exception as e:
#         print(12343)
#
# fun()    # 如果断言调试不成立 ---> 触发AssertionError异常，try代码断言失败的地方之后的代码没有执行

def fun():
    try:
        r = input('Input the parameters: ')
        num = int(r)
        assert (num < 30)   # assert conditions > 30
        conn = 10 * num
        print(conn)
    except Exception as e:
        print(12343)

# 3. logging 日志器的使用

def initloger():  # 获取日志器对象，并设置参数信息
    import logging
    # 获取一个日志器对象（初始）
    logger = logging.getLogger()   # 初始日志器对象
    logging.basicConfig(level=logging.ERROR)   #设置正常级别
    # 设置相关属性
    # 文件处理器
    hd = logging.FileHandler('logfile')
    # 文件处理器添加给日志器对象
    logger.addHandler(hd)
    formater = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    # 文件信息的格式和文件处理器相关联
    hd.setFormatter(formater)
    # 设置日志器调试级别
    logger.setLevel(level=logging.NOTSET)
    return logger

# 日志器的使用
def run():
    num = int(input('input numbers: '))
    logger = initloger()
    # 使用日志器
    logger.info('num==%s' % num)
    conn = 100 * num
    print(conn)

if __name__=='__main__':
    run()

# logging 的级别，noset, debug, info, warning, error, critical
# 级别越高，输出的信息更少


import pdb

x = 20
y = x + 20
z = x + y
pdb.set_trace()  # 设置断点
print(x, y, z)


# 最核心的单元测试
# 使用unittest函数

import unittest