# !/usr/bin/env python
# coding:utf-8
"""
    Author = 上善若水(QQ:564667570)
    Affiliation = Institute of Subtropical Agriculture, Chinese Academy of Sciences
    Time = '2018-3-30, 16:21'
"""

# python version 2.7
# import unittest
# from HTMLTestRunner import HTMLTestRunner
#
# if __name__=='__main__':
#     dis = unittest.defaultTestLoader.discover('./', pattern='test*.py')
#     file = open('pyresult.html', 'wb')
#     runner = HTMLTestRunner(stream=file, title=u'Test Reports', description=u'python unit reports')
#     runner.run(dis)
#     file.close()

# python version 3.6
import unittest
from HtmlTestRunner import HTMLTestRunner

if __name__=='__main__':
    dis = unittest.defaultTestLoader.discover('./', pattern='test*.py')
    file = open('pyresult_python36.html', 'wb')
    runner = HTMLTestRunner(stream=file, report_title=b'Test Reports', descriptions=b'python unit reports', output=file)
    runner.run(dis)
    file.close()
