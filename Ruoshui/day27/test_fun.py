# !/usr/bin/env python
# coding:utf-8
"""
    Author = 上善若水(QQ:564667570)
    Affiliation = Institute of Subtropical Agriculture, Chinese Academy of Sciences
    Time = '2018-3-30, 11:03'
"""

import unittest
from my_fun import functions

class test_fun(unittest.TestCase):
    def setUp(self):
        pass
    def tearDown(self):
        pass
    def test_fun(self):
        self.assertEqual(functions(5), 25, 'something wrong')

if __name__=='__main__':
    unittest.main()


# import unittest
# from my_fun import Person
#
# class myClassTest(unittest.TestCase):
#     def setUp(self):
#         self.p = Person('tom')
#     # test class
#     def testrun(self):
#         self.p.run()
#
#     def tearDown(self):
#         self.p = None
#
# if __name__=='__main__':
#     unittest.main()

# HTMLTestRunner

# import HtmlTestRunner
# import unittest
#
# class TestStringMethods(unittest.TestCase):
#     """Example test for HtmlRunner."""
#
#     def test_upper(self):
#         self.assertEqual('foo'.upper(), 'FOO')
#
#     def test_isupper(self):
#         self.assertEqual('FOO'.isupper(),'FOO')
#         self.assertEqual('Foo'.isupper(),'FOO')
#
#     def test_split(self):
#         s = 'hello world'
#         self.assertEqual(s.split(), ['hello', 'world'])
#         # check that s.split fails when the separator is not a string
#         # with self.assertEqual(TypeError):
#         #     s.split()
#
#     def test_error(self):
#         """This test should be marked as error one."""
#         raise ValueError
#
#     def test_fail(self):
#         """This test should fail."""
#         self.assertEqual(1, 2)
#
#     @unittest.skip("This is a skipped test.")
#     def test_skip(self):
#         """This test should be skipped."""
#         pass
#
#
# if __name__ == '__main__':
#     unittest.main(testRunner=HtmlTestRunner.HTMLTestRunner(output='example_dir'))

