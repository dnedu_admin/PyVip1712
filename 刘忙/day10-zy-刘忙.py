# coding=utf-8
# dn-刘忙(QQ:734523893)

# 1.自定义一个异常类,当list内元素长度超过10的时候抛出异常
from random import randint

class MyException(Exception):
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return self.message

a_list = []
while 1:
    anum = randint(0, 100)
    a_list.append(anum)
    try:
        if len(a_list) >= 10:
            raise MyException('长度超过10')
    except Exception as e:
        print(e)
        break

print(a_list)

# 2.思考如果对于多种不同的代码异常情况都要处理, 又该如何去
# 处理, 自己写一个小例子

# try:
#     print(100 / 0)
#     print(name)
#     f = open('mar.py')
# except ZeroDivisionError as e:
#     print(e)
#     print('无法被零除！')
# except NameError as e:
#     print(e)
#     print('变量未命名！')
# except IOError as e:
#     print(e)
#     print('文件找不到！')
# except Exception as e:
#     print(e)

#  3.try-except和try-finally有什么不同,写例子理解区别
# try:
#     print(99/0)
# except ZeroDivisionError as e:
#     print(e)


# try:
#     print(99/0)
# except ZeroDivisionError as e:
#     print(e)
# finally:
#     print('无论什么情况都会执行！')

# 4.写函数，检查传入字典的每一个value的长度，如果大于2，
#      那么仅仅保留前两个长度的内容，并将新内容返回给调用者
#      dic = {“k1”: "v1v1","k2":[11,22,33}}

# from copy import deepcopy

# dic = {'k1':'v1v1','k2':[11,22,33],'k3':'mazda'}
# b_dic = deepcopy(dic)
# for k, val in b_dic.items():
#     if len(val) > 2:
#         b_dic[k] = val[:2]
#
# print(b_dic)