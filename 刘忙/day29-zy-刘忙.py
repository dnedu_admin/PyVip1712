# coding=utf-8
# dn_sulfur(刘忙 qq:734523893)

# 1.函数缓存的使用

# from time import time
# from functools import lru_cache
#
# @lru_cache(maxsize=10,typed=False)
# def fib(n):
#     if n < 2:
#         return n
#     return fib(n-1) + fib(n-2)
#
#
# def countTime(num):
#     t1 = time()
#     [fib(n) for n in range(num)]
#     print(time()-t1)
#
#
# countTime(35)
# print(fib.cache_info())


# 2.性能分析器的使用
# import cProfile
#
# def fib(n):
#     if n < 2:
#         return n
#     return fib(n-1) + fib(n-2)
#
#
# cProfile.run('fib(35)')




# 3.内存分析器的使用"""

from memory_profiler import profile

@profile
def fun_time():

    x = [8] * (10 ** 5)
    y = [9] * (10 ** 6 * 6)
    del x
    del y


if __name__ == '__main__':
    fun_time()
