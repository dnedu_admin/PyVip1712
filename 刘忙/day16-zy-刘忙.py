# coding=utf-8
# dn_sulfur(刘忙 qq:734523893)

'''
1.写一个函数计算1 + 3 + 5 +…+97 + 99的结果
      再写一个装饰器函数, 对其装饰, 在运算之前, 新建一个文件
      然后结算结果, 最后把计算的结果写入到文件里
'''

# def file_load(func):
#     content = str(func())
#     with open('records.txt','a',encoding='utf-8') as f:
#         f.write(content)
#
#     # return file_load
#
#
# @file_load
# def calcu():
#     return sum(range(1,100))




'''
2.写一个函数计算传入进来参数的平方, 并将结果.写一个带参数的装饰器, 
       将装饰器参数传入到被包装的函数,计算并输入结果

'''

def result(func):
    def inner(a,b):
        # re = func(a,b)**2
        print('计算结果:', func(a, b)**2)
    return inner


@result
def triboom(a,b):
    return a+b


triboom(11,12)