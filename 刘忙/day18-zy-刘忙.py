# coding=utf-8
# dn_sulfur(刘忙 qq:734523893)
'''
1.计算range(1,99999)的总和,采取三种方式,第一:重复10次计算总时间;
 第二:开启十个子线程去计算;第三:开启十个进程去计算,然后对比耗时
'''
import time
from threading import Thread
from multiprocessing import Pool

# def test1(t1):
#     for i in range(1,11):
#         sum(range(1,99999))
#     print('共耗时：',time.time()-t1)
#
#
# t1 = time.time()
# test1(t1)




# def son_process():
#     sum(range(1,99999))
#
# if __name__ == "__main__":
#     t2 = time.time()
#     pool = Pool(10)
#     for i in range(1,6):
#         pool.apply_async(son_process, args=(i,))
#     pool.close()
#     pool.join()
#     print('共耗时',time.time()-t2)


# def son_thread(i):
#     print(i)
#     sum(range(1,99999))
#
# threadList = [Thread(target=son_thread,args=(i,)) for i in range(1,11)]
# t3 = time.time()
# for thr in threadList:
#     thr.start()
#     thr.join()
# print('共耗时',time.time()-t3)


'''
2.将第一题计算十次改为使用线程池去计算,线程池线程个数由你自己电脑cpu逻辑核数决定
'''
import threadpool

def son_thread(i):
    print(i)
    sum(range(1,99999))

t4 = time.time()
pool = threadpool.ThreadPool(8)
reqs = threadpool.makeRequests(son_thread,range(1,11))
[pool.putRequest(req) for req in reqs]
pool.wait()
print("共耗时",time.time()-t4)