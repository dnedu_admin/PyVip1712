# coding=utf-8
# dn_sulfur(刘忙 qq:734523893)

# 1.基于tcp协议socket套接字编程半双工模式

import socket

# host = '127.0.0.1'
# port = 911
#
# soc = socket.socket()
# soc.bind((host, port))
# soc.listen(5)
# while 1:
#     conn, addr = soc.accept()
#     while 1:
#         text = conn.recv(1024)
#         if len(text) == 0:
#             print('数据为空！')
#         else:
#             print('接受客户端的数据为:%s' % text.decode('utf-8'))
#             res = input('输入回复客户端的数据:')
#             conn.send(res.encode('utf-8'))
#     conn.close()
# soc.close()


# 2.socketserver异步非阻塞编程,编写代码

import socketserver
from socketserver import StreamRequestHandler as srh

host = '127.0.0.1'
port = 9915
addr = (host, port)

class Server(srh):
    def handle(self):
        print('连接到服务器的客户端地址为:%s' % str(self.client_address))
        while 1:
            text = self.request.recv(1024)
            if not text:
                break
            print('服务器接受的数据为:%s' % text.decode('utf-8'))
            result = input('发送给客户端的数据为：')
            self.request.send(result.encode('utf-8'))

server = socketserver.ThreadingTCPServer(addr,Server)
server.serve_forever()
