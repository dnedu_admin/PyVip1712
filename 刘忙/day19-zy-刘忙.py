# coding=utf-8
# dn_sulfur(刘忙 qq:734523893)

'''
    使用装饰器增强完成1-999999所有奇数和计算10次
   1.使用普通方法
   2.使用多线程
   3.使用多进程
   4.采用线程池完成计算
   5.采用进程池完成计算
'''

import time

#计时函数
def decorator(func):
    def inner(*args, **kwargs):
        time1 = time.time()
        func(*args, **kwargs)
        time2 = time.time()
        print('总共耗时%s秒' % (time2-time1))
    return inner

#计算函数
def childCal(num):
    for i in range(num):
        sum(range(1, 999999))


@decorator
def caculator1(num):        #普通计算
    for i in range(num):
        sum(range(1,999999))

@decorator
def caculator2(num):        #多进程
    from multiprocessing import Process

    def masterProcess():
        pro = Process(target=childCal,args=(num,))
        pro.start()
        pro.join()
    return masterProcess


@decorator
def caculator3(num):           #进程池
    from multiprocessing import Pool

    def masterPool():
        pool = Pool(3)
        pool.apply_async(childCal,args=(num,))
        pool.close()
        pool.join()
    return masterPool

@decorator
def caculator4(num):        #多线程
    from threading import Thread

    def masterThread():
        threadList = [Thread(target=childCal,args=(num,)) for i in range(10)]
        for thd in threadList:
            thd.start()
            thd.join()
    return masterThread

@decorator
def caculator5(num):        #线程池
    from multiprocessing.dummy import Pool as threadPool

    def masterThreadPool():
        pool = threadPool(3)
        for i in range(10):
            pool.apply_async(childCal,args=(num,))
            pool.close()
            pool.join()
    return masterThreadPool


if __name__ == '__main__':
    # caculator1(10)    #普通计算
    # caculator2(10)    #多进程
    # caculator3(10)    #进程池
    # caculator4(10)    #多线程
    # caculator5(10)      #线程池
    pass