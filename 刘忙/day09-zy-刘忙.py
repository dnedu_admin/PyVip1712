# coding=utf-8
# dn_sulfur(刘忙 qq:734523893)

# 1.a_list = [1,2,3,2,2]    删除a_list中所有的2
a_list = [1,2,3,2,2]
print(list(set(a_list)-{2}))

# 2.用内置函数compile创建一个文件xx.py, 在文件里写你的名字, 然后用eval
# 函数读取文件中内容, 并打印输出到控制台

code = compile('print("Hello,my name is sulfur")', 'xx.py', 'eval')
eval(code)

# 3.写一个猜数字的游戏, 给5次机会, 每次随机生成一个整数, 然后由控制台输入一个
# 数字, 比较大小.大了提示猜大了, 小了提示猜小了.猜对了提示恭喜你, 猜对了.
# 退出程序, 如果猜错了, 一共给5次机会, 5
# 次机会用完程序退出.

n = 0
while n <= 4:
    guess = int(input('please:'))
    if guess > 15:
        print('猜大了!')
    elif guess < 15:
        print('猜小了!')
    else:
        print('猜中了!')
        break
    n += 1
