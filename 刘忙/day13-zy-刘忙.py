# coding=utf-8
# dn-刘忙(QQ:734523893)

#1.自己写代码对比2.x深度优先和3.x广度优先的继承顺序

# class A():
#     def eat(self):
#         print('this is A')
#
# class B():
#     def eat(self):
#         print('this is B')
#
# class C(A,B):
#     pass
#
# class D(A,B):
#     def eat(self):
#         print('this is D')
#
# class E(C,D):
#     pass
#
# et = E()
# et.eat()    # this is D,    python3广度优先。
              # this is A,    python2深度优先。

#2.写一个类MyClass,使用__slots__属性,定义属性(id,name),创建该类实例
#  后,动态附加属性age,然后执行代码看看.

# class Myclass():
#     __slots__ = ('id', 'name')
#
#
# my = Myclass()
# my.name = 'askone'
# print(my.name)
# my.age = 12     #AttributeError: 'Myclass' object has no attribute 'age'
#                 #无法绑定，限制了类的属性
# print(my.age)



# 3.使用type创建一个类,属性name,age,sex,方法sleep,run并创建此类的实例对象

def seeds(self):
    print('播种...')

def growup(self):
    print('成长...')

Vegtable = type('Vegtable',(),{'solid':True,'water':False,'seeds':seeds,'growup':growup})
# print(type(Vegtable))
veg = Vegtable()
veg.seeds()
veg.growup()
print(veg.solid)
print(veg.water)


# 4.写例子完成属性包装,包括获取属性,设置属性,删除属性

# class Person():
#     @property
#     def age(self):
#         return self._age
#     @age.setter
#     def age(self,age):
#         if not isinstance(age,int):
#             print('年龄必须是整数！')
#         if age < 0 or age > 150:
#             print('超出正常年龄段！')
#         self._age = age
#     @age.deleter
#     def age(self):
#         del self._age
#
# p = Person()
# p.age = 20
# del p.age
# print(p.age)


