# coding=utf-8
# dn_sulfur(刘忙 qq:734523893)

'''

1.
datetime, 获取你当前系统时间三天前的时间戳;
并把当前时间戳增加
899999, 然后转换为日期时间对象, 把日期时间并按照 % Y - % m - % d % H: % M: % S
输入打印这个日期对象, 并获取date属性和time属性, 最后判断当前日期时间
对象是星期几;
'''
from _datetime import datetime, timedelta
# dt1 = datetime.now()
# dt2 = dt1 + timedelta(days=-3)
# print(dt2)
# dt3 = dt1 + timedelta(days=899999)
# print(datetime.strftime(dt3, '%Y-%m-%d %H:%M:%S'))
# date1 = datetime.date(dt3)
# time1 = datetime.time(dt3)
# print(date1,time1)
# print(datetime.weekday(date1))
# time_stamp = dt2.timestamp()+899999
# tempdate = datetime.fromtimestamp(time_stamp)
# print(tempdate.strftime('%Y-%m-%d %H:%M:%S'))
# print(tempdate.date())
# print(tempdate.time())
#
# print(datetime.now().weekday())
# print(datetime.weekday(datetime.today()))

'''
2.
使用os模块线获取当前工作目录的路径, 并打印当前目录下所有目录和文件,
获取当前脚本的绝对路径, 分割当前脚本文件的目录和文件在tuple中并判断
当前文件所在的目录下存不存在文件game.py, 如果不存在则创建该文件.
并往该文件里写入数据信息, 然后判断当前目录下是否存在目录
/ wow / temp /, 如果不存在则创建, 最后把目录名称改为: / wow / map /
'''

import os
# road1 = os.getcwd()
# print(road1)
# print(os.listdir(road1))
# road2 = os.path.abspath(road1)
# print(road2)
# print(os.path.split(r'E:\python-project\seven.py'))
# print(os.path.exists('./game.py'))
# os.makedirs('E:\\python-project\\wow\\temp')
# with open(r'E:\\python-project\\wow\\temp\\game.py','w+') as f:
#     f.write(r'#hello the world!')
# os.renames('E:\\python-project\\wow\\temp\\','E:\\python-project\\wow\\map')

# print(os.listdir(os.getcwd()))
# pydirs = os.getcwd()
# print(pydirs)
# pyline = __file__
# print(pyline)
# pytuple = os.path.split(pyline)
# print(pyline)
# if os.path.exists('./game.py'):
#     print('yes,exists!')
# else:
#     os.makedirs('./wow/temp')
#     print('built')
# if os.path.exists('./wow/map'):
#     os.rmdir('./wow/map')
# os.renames('./wow/temp', './wow/map')

'''
3.
写函数，检查获取传入列表或元组对象的所有奇数位索引对应的元素，并将其作为新的列表返回给调用者
'''

# def reIndex(content):
#     if(isinstance(content,(list,tuple))):
#         ls1 = []
#         for index, val in enumerate(content,1):
#             if index%2 == 1:
#                 ls1.append(val)
#         return ls1
#
# result = reIndex((2,4,6,8,10))
# print(result)


def reIndex1(n):
    ls1 = []
    [ls1.append(val) for index,val in enumerate(n,1) if index%2==1]
    return ls1

result1 = reIndex1((2,4,6,8,10))
print(result1)


