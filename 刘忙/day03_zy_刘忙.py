# coding=utf8
# author:dn_刘忙(QQ:734523893)


#1. 格式化显示浮点数12.78，要求保留3位小数，输出占20字符，空位用0补齐
num = 12.78
print("%.3f" % num)
print("%020.3f" % num)


#2. 将字符串”动脑学院”转换为bytes类型。
str1 = '动脑学院'
byte1 = str1.encode("utf-8")
print(byte1)

#3. 将以下3个字符串合并为一个字符串，字符串之间用3个_分割	hello  动脑  pythonvip  	合并为“hello___动脑___pythonvip”
li1 = 'hello','动脑','pythonvip'
str_an1 = "__".join(li1)
print(str_an1)


#4. 删除字符串  “    你好，动脑eric     ”首尾的空格符号
str_an2 = '    你好，动脑eric     '
print(str_an2.strip())


'''5. 用户信息存在如下所示字符串中，包含信息依次为  姓名   学号  年龄，
不同信息之间的空格数不确定，请提取用户信息分别保存到 xxx_name,xxx_num, xxx_age。
eric=“    Eric   dnpy_001     28”        
提取后 eric_name=“Eric”eric_num=”dnpy_001”,eric_age=”28”
zhangsan = “ 张三         dnpy_100    22     ”
 '''
eric = '    Eric   dnpy_001     28'
li3 = eric.split()
print(li3)
name2, num2, age2 = li3
print('eric_name='+name2, 'eric_num='+num2, 'eric_age='+age2)


zhangsan = ' 张三         dnpy_100    22     '
li2 = zhangsan.split()
print(li2)
name1, num1, age1 = li2
print(name1, num1, age1)
