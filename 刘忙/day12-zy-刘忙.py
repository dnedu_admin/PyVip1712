# coding=utf-8
# dn-刘忙(QQ:734523893)

#1.自己动手写一个学生类,创建属性名字,年龄,性别,完成
#   __new__,__init__,__str__,__del__，__call__
import time


class Student():

    def __init__(self,name,age,gender):
        self.name = name
        self.age = age
        self.gender = gender
        print(self)

    def __new__(cls, *args, **kwargs):
        return super().__new__(cls)

    def __str__(self):
        return '\n(student:%s, %s, %s)' % (self.name, self.age, self.gender)

    def __call__(self, *args, **kwargs):
        print('当前方法正在被调用')
        return self

    def __del__(self):
        print('销毁当前对象%s' % self.name)

s1 = Student('mary', 22, 'female')
s1()

# 2.创建父类Person,属性name,函数eat,run,创建子类Student,学生类
#   继承Person,属性name,age,id,函数eat,study,全局定义一个函数
#   get_name,传入一个Person类的实例参数,分别传入Person和Student
#   两个类的实例对象进行调用.

class Person():

    def __init__(self,name):
        self.name = name

    def eat(self):
        print('person is eating')

    def run(self):
        print('person is running')


class Student(Person):

    def __init__(self,name,age,id):
        self.name = name
        self.age = age
        self.id = id

    def eat(self):
        print('student is eating')

    def run(self):
        print('student is running')

p1 = Person('mark')
s1 = Student('sonia',12,'9001')

def get_name(person):
    person.eat()
    person.run()

print(p1.name)
get_name(p1)
print(s1.name,s1.age,s1.id)
get_name(s1)



# 3.用代码完成python多态例子和鸭子模型
# 多态
class animal():
    def swim(self):
        print('animal can swim')
    def fly(self):
        print("animal bird can fly")

class fish(animal):
    def swim(self):
        print('fish is swimming')
    def fly(self):
        print("most fish can't fly")


p1 = animal()
p2 = fish()
def show(fish):
    fish.swim()
    fish.fly()

show(p1)
show(p2)

# 鸭子形态

class animal():
    def swim(self):
        print('animal can swim')
    def fly(self):
        print("animal bird can fly")

class fish(animal):
    def swim(self):
        print('fish is swimming')
    def fly(self):
        print("most fish can't fly")




p1 = animal()
p2 = fish()
def show(fish):
    fish.swim()
    # fish.fly()

show(p1)
show(p2)