# coding=utf8
# dn 劉忙suflur



# 1.自己写一个字典:
#     name_dict = {'name':'ben','age':22,'sex':'男'}
#     添加,删除,更新,清空操作


# name_dict = {'name':'ben','age':22,'sex':'男'}
# name_dict['addr'] = 'shanghai'
# print(name_dict)
#
# del name_dict['addr']
# print(name_dict)
#
# name_dict['sex'] = 'female'
# print(name_dict)
#
# name_dict.clear()
# print(name_dict)

# 2.写两个集合:
#     num1_set = {3,5,1,2,7}
#     num2_set = {1,2,3,11}
#     并分别进行&,|,^,-运算

# num1_set = {3,5,1,2,7}
# num2_set = {1,2,3,11}
#
# print('&', num1_set & num2_set)
# print('|', num1_set | num2_set)
# print('^', num1_set ^ num2_set)
# print('-', num1_set - num2_set)


# 3.整数字典:
#     num_dict = {'a':13,'b':22,'c':18,'d':24}
#     按照dict中value从小到大的顺序排序

num_dict = {'a':13,'b':22,'c':18,'d':24}

print(sorted(num_dict.values()))