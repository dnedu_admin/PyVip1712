# coding=utf-8
# dn_sulfur(刘忙 qq:734523893)


import re
# 1.使用正则表达式匹配电话号码：0713-xxxxxx(湖南省座机号码)
astr1 = '0713-247866'
pattern1 = re.compile('^0713-\d+')
res1 = re.findall(pattern1, astr1)
print(res1)






# 2.区号中可以包含()或者-,而且是可选的,就是说你写的正则表达式可以匹配800-555-1212,555-1212,(800)555-12123.
bstr1 = '800-555-1212,555-1212,(800)555-12123,(086)800-123-136564'
pattern2 = re.compile('[(]?\d*[)]?\d{3}[-]\d{3,}[-]?\d*')
res2 = re.findall(pattern2, bstr1)
print(res2)

