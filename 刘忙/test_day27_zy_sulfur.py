# coding=utf-8
# dn_sulfur(刘忙 qq:734523893)

import unittest

#功能模块,测试函数
# def run(num):
#
#     if isinstance(num,(int,float)):
#         return num**2
#     else:
#         raise TypeError('参数类型不对！')

#测试函数
# class myTest(unittest.TestCase):
#     def setUp(self):
#         pass
#
#     def tearDown(self):
#         pass
#
#     def testRun(self):
#         self.assertEqual(run(5), 25, '函数测试失败！')
#
#
# if __name__ == '__main__':
#     unittest.main()


class Person():
    def __init__(self,name):
        self.name = name

    def run(self):
        print('%s is installing'.format(self.name))


class myTest(unittest.TestCase):

    def setUp(self):
        self.p = Person('tom')      #准备测试阶段先实例化对象

    def testRun(self):      #这个是测试函数
        self.p.run()

    def tearDown(self):     #测试完成之后，对象置空
        self.p = None


if __name__ == '__main__':
    unittest.main()
