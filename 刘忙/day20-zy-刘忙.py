# coding=utf-8
# dn_sulfur(刘忙 qq:734523893)

'''
 创建两个协程函数对象,分别完成1,3,5…99,   2,4,6…,100
 然后让两个协程函数对象分别交替执行.在每一个协程函数对象的内部先打印输出
 每一个数字,然后执行完一轮之后打印提示信息,已经交替执行完毕一轮
 等所有的协程函数对象执行完毕之后,分析一下数据信息
'''

from types import coroutine

async def obj1():
    cal1 = 0
    for i in range(1,100,2):
        cal1 += i
        print('奇数',cal1)
        await do_next()

async def obj2():
    cal2 = 0
    for i in range(2,101,2):
        cal2 += i
        print('偶数',cal2)
        await do_next()

@coroutine
def do_next():
    yield



def workRun(alist):
    blist = list(alist)
    while blist:
        for item in blist:
            try:
                item.send(None)
            except StopIteration as e:
                print(e)
                blist.remove(item)

if __name__ == '__main__':
    ob1 = obj1()
    ob2 = obj2()
    clist = []
    clist.append(ob1)
    clist.append(ob2)
    workRun(clist)