#coding:utf8
#dn-zy-刘忙

#1.简述普通参数、默认参数、关键字参数、动态收集参数的区别,理解为主

'''
普通參數：就是可以傳參的參數；
默認參數：有默認值的參數
關鍵值參數：*args 收集參數以元組形式輸出，**kwargs收集參數以字典形式輸出
动态收集参数：比如空列表，空元組之類的參數，應盡量避免使用
'''


#2.

def foo(x, y, z, *args, **kw):
    sum = x + y + z
    print(sum)
    for i in args:
        print('*args',i)
    print('**kwargs1',kw)
    for j in kw.items():
        print('**kwargs2',j)

a_tuple = (1,2,3,)
#此处参数修改为2个看看会怎么样?
# 会报错，x,y,z是三个参数，给了两个数目不对,多给参数会进入到*args中
b_dict = {'name': 'jim', 'age': 28, 'add': '上海'}
foo(*a_tuple, **b_dict)
#分析这里会怎么样?
#a_tuple的三个参数正好送入x,y,z中，*args就为空了

'''3.题目:执行分析下代码
    def func(a, b, c=9, *args, **kw):
         print('a =', a, 'b =', b, 'c =', c, 'args =', args, 'kw =', kw)
    func(1,2)
    func(1,2,3)
    func(1,2,3,4)
    func(1,2,3,4,5)
    func(1,2,3,4,5,6,name='jim')
    func(1,2,3,4,5,6,name='tom',age=22)
    扩展: 如果把你的函数也定义成  def  get_sum(*args,**kw):pass 
    你的函数可以接受多少参数?
'''
# 可以接受以上全部的参数

def func(a, b, c=9, *args, **kw):
    print('a =', a, 'b =', b, 'c =', c, 'args =', args, 'kw =', kw)


func(1,2,3,4,5,6,name='tom',age=22)








#4.写一个函数函数，计算传入字符串中的数字、字母、空格和其他的个数分别为多少?

c1 = []
c2 = []
c3 = []
c4 = []

a_str = 'a1idjk4 56k jf/a?.as3467iop'

for i in a_str:

    if i.isdigit():
        c1.append(i)
    elif i.isalpha():
        c2.append(i)
    elif i.isspace():
        c3.append(i)
    else:
        c4.append(i)

print('數字共有：', len(c1))
print('字母共有：', len(c2))
print('空格共有：', len(c3))
print('其餘的:', len(c4))



