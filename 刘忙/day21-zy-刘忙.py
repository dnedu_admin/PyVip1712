# coding=utf-8
# dn-刘忙(QQ:734523893)

import re

# 1.识别下面字符串:’ben’,’hit’或者’ hut’
# patt1 = re.compile('\s?\w+')
# print(re.match(patt1,'ben').group())
# print(re.match(patt1,'hit').group())
# print(re.match(patt1,' hut').group())

# 2.匹配用一个空格分隔的任意一对单词,比如你的姓 名
# patt2 = re.compile('\w+\s\w+')
# res = re.match(patt2, 'Martin Sulfur').group()
# if(res):print(res)


# 3.匹配用一个逗号和一个空格分开的一个单词和一个字母
# patt3 = re.compile('\w+[,]\s\w')
# res = re.findall(patt3, 'dress, H')
# if(res):print(res)


# 4.匹配简单的以’www’开头,以’com’结尾的web域名,比如:www.baidu.com
# patt4 = re.compile('www[.]\w+[.]com[.]?.*')
# res = re.match(patt4, 'www.baidu1.com.cn').group()
# if(res):
#     print(res)