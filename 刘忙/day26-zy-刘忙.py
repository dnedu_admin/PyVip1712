# coding=utf-8
# dn-刘忙(QQ:734523893)

# 1. PKCS#5(选择一种hash算法)加密算法加密你的中文名字,写出代码

# def hashEncode(str1):
#     import hashlib
#     str0 = str1.encode('gbk')
#     h = hashlib.md5()
#     h.update(str0)
#     return h.hexdigest()


# 2. 创建文件test.py,写入一些数据,并计算该文件的md5值
# with open('./test.py', 'w+',encoding='utf-8') as f:
#     for i in range(1, 11):
#         f.write('#今天天氣真好{},出去玩！\n'.format(i))


# def hashEncode1():
#     import hashlib
#     hm5 = hashlib.md5()
#     with open('./test.py', 'r', encoding='utf-8') as f:
#         hm5.update(f.read().encode('utf-8'))
#     return hm5.hexdigest()
#
#
# print(hashEncode1())

# 3. 遵循上下文管理协议,自己编写一个类,去创建一个上下文管理器,然后自己使用看看
# 方式1:普通方式

# class MyOpen():
#
#     def __init__(self,alist):
#         print('initializing...')
#         self.alist = alist
#
#     def __enter__(self):
#         print('执行代码')
#         self.alist.append('hello')
#         return self
#
#     def __exit__(self, exc_type, exc_val, exc_tb):
#         self.alist = None
#         if exc_type:
#             print(exc_val)
#         else:
#             print('没有异常')
#
#     def work(self):
#         for item in self.alist:
#             print(item)
#
#
# with MyOpen(['a','b','c']) as f:
#     f.work()



#方式2:使用contextmanager
from contextlib import contextmanager


class myOpen1():

    def __init__(self,alist):
        print('initializing...')
        self.alist = alist

    def work(self):
        for item in self.alist:
            print(item)

@contextmanager
def mk_context(alist):
    print('开始处理')
    tls = myOpen1(alist)
    yield tls
    print('处理完毕')


with mk_context(['a','b','c']) as fmk:
    fmk.work()


# 4.使用colorama模块使用多颜色输出
from colorama import Fore,Back,Style
# print(Fore.RED + "a red text")
# print(Back.BLACK + Fore.RED + "red word and black background")
# print(Style.BRIGHT+'hello the world!')
# print(Style.RESET_ALL)
