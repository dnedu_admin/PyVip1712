# coding=utf-8
# dn_sulfur(刘忙 qq:734523893)

'''
1.使用pickle,json(注意:两个模块)
把字典a_dict = [1,2,3,[4,5,6]]
序列化与反序列化,序列化,保存到文件名为serialize.txt文件里面,然后大开,并读取数据
反序列化为python对象加载到内存,并打印输出
尝试序列化/反序列非文件中,直接操作

'''

import json,pickle
# a_dict = [1,2,3,[4,5,6]]
# with open('serialize.txt','w',encoding='utf-8') as f:
#     content = json.dumps(a_dict)    #json.dump(a_dict,f)
#     f.write(content)
#
# with open('serialize.txt','r',encoding='utf-8') as f:
#     fond = json.loads(f.read())       #fond = json.load(f)
#     print(fond)

# with open('serialize.txt','wb') as f:
#     pickle.dump(a_dict, f, 0)
#
#
# with open('serialize.txt','rb') as f:
#     fond = pickle.load(f)
#     print(fond)


'''
2.自己写代码对比深浅拷贝的区别,写完代码然后看看运行结果,然后分析原因
   比如：a_list=[1,2,3,4,[8,9,0]]
'''
# a_list=[1,2,3,4,[8,9,0]]
# blist = a_list.copy()
# blist[1] = 200
# print(a_list,blist)     #一层的内容是独立的
# blist[4][2] = 1000
# print(a_list,blist)     #深层内容仍是共享的

# import copy
# clist = copy.deepcopy(a_list)
# clist[4][2] = 1001
# print(a_list,clist)         #一层的内容也是共享的


''' 3.分别写代码完成内存缓冲区文本数据和二进制数据的读/写操作 '''

from io import StringIO,BytesIO
f = StringIO('为赋新词强说愁')
print(f.getvalue())
f1 = BytesIO('却道天凉好个秋'.encode('utf-8'))
print(f1.getvalue().decode('utf-8'))
