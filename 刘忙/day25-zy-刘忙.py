# coding=utf-8
# dn_sulfur(刘忙 qq:734523893)
'''
1.在当前项目下创建目录abc,并在abc目录下,使用uuid产生唯一码并写入到文件uuid.py中,并拷贝整个目录abc,新目录名为app,并将app目录下文件拷贝,新  文件名为:new.py文件
'''
import uuid,os
import shutil

# if not os.path.exists('./abc/'):
#     os.mkdir('./abc')
# content = uuid.uuid3(uuid.NAMESPACE_DNS, 'hello the world!')
#
# with open('./abc/uuid_test.py', 'w+', encoding='utf-8') as f:
#     f.write(str(content))
# shutil.copytree('./abc/','./app/')
# os.renames('./app/uuid_test.py','./app/new.py')



'''
2.创建本地文件’one.txt’,在里写入部分数据(‘今天天气不错,我们去动物园n玩吧’n从1-9),使用b64encode对每一行数据编码,然后写入到文件oen.txt中.每写  完一行之后写入换行符.  
打开刚才创建的文件one.txt,以读取多行的形式读取文件中的内容,并把每一行的  内容通过b64decode对每一行数据解码,解码之后每一行数据依然是二进制形式  
所以,继续对每一行数据通过utf-8的形式解码为可以普通字符串,然后判断每一行  字符串是否包含’动物园6’的子字符串,如果包含,先输出换行符,然后则打印输出  
当前行的字符串,最后创建一个文件,’two.txt’,并把帅选之后的数据写入到这个新  文件中.
'''
import base64
# with open('./one.txt', 'a+', encoding='utf-8') as f:
#     for i in range(1,10):
#         f.writelines('今天天气不错,我们去动物园%d玩吧\n' % i)
# with open('./one.txt', 'r',encoding='utf-8') as f:
#     context = f.read().encode('utf-8')


# result = base64.b85encode(context)
# conn = base64.b85decode(result).decode('utf-8')
# conn1 = conn.split('\n')[:-1]
# for word in conn1:
#     if '动物园6' in word:
#         print('\n', word)
#         result1 = word
# with open('two.txt','w+',encoding='utf-8') as f:
#     f.write(result1)



'''
3.准备一张.jpg图片,比如:mm.jpg,读取图片数据并通过b85encode加密之后写入到新文件mm.txt文件中,  然后读取mm.txt数据并解密之后然后写入到mmm.jpg文件中
'''
# with open('mm.jpg','rb') as f:
#     conne = f.read()
# print(conne)
# res1 = base64.b85encode(conne)
# with open('mm.txt','wb+') as f:
#     f.write(res1)
# with open('mm.txt','r') as f:
#     res2 = f.read()
#
# res3 = base64.b85decode(res2)
# print(res3)
#
# with open('mmm.jpg', 'wb+') as f:
#     f.write(res3)