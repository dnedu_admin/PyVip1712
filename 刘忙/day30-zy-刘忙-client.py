# coding=utf-8
# dn_sulfur(刘忙 qq:734523893)

# 1.基于tcp协议socket套接字编程半双工模式
import socket

# host = '127.0.0.1'
# port = 911
#
# soc = socket.socket()
# soc.connect((host, port))
# while 1:
#     text = input('请输入发送给服务器的数据:')
#     soc.send(text.encode('utf-8'))
#     result = soc.recv(1024)
#     print('接受服务器端的返回数据:%s'% result.decode('utf-8'))
#     print('end of transportation'.center(50, '*'))
# soc.close()




# 2.socketserver异步非阻塞编程,编写代码

host = '127.0.0.1'
port = 9915

addr =(host, port)
s = socket.socket()
s.connect(addr)

while 1:
    text = input('发送给服务器的数据:')
    if not text or text == 'exit':
        break
    s.send(text.encode('utf-8'))
    result = s.recv(1024)
    if not result:
        break
    print('接受自服务器端的数据:%s' % result.decode('utf-8'))
s.close()

