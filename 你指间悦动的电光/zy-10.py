# -*- coding=UTF-8 -*-
# @ File: zy-10.py.py
# @ Time: 2018/2/6 10:51

"""
动手完成:
  1.自定义一个异常类,当list内元素长度超过10的时候抛出异常
  2.思考如果对于多种不同的代码异常情况都要处理,又该如何去
     处理,自己写一个小例子
  3.try-except和try-finally有什么不同,写例子理解区别
  4.写函数，检查传入字典的每一个value的长度，如果大于2，
     那么仅仅保留前两个长度的内容，并将新内容返回给调用者
     dic = {“k1”: "v1v1","k2":[11,22,33}}
"""
"""
#  1
#class kako(Exception):
#    def __str__(self):
#        return '出现自定义异常了'

#上面的定义的异常类也可以不要
def fun(a):
    try:
        if len(a)>=10:
            raise IOError('自定义的异常')
        print("你的列表小于10")
    except Exception as e:
        print(e)
        print('大于十异常')
    else:
         print('小于十直接输出',a[:])


fun([1,2,3,4,5,6,7,8,4,3,2,3,3,])


#   2
try:
    u = 7 / int('jjjjj')
    m = 9 / 3
    print(open('hauuuu.txt'))
except NameError as l:
    print(l)
    print('变量名错误')
except ZeroDivisionError as o:
    print(o)
    print('被0除了')
except SyntaxError as p:
    print(p)
    print('语法错误')
except IndentationError as k:
    print(k)
    print('索引错误')
except IOError as h:
    print(h)
    print('输入输出错误')
except ArithmeticError as t:
    print(t)
    print('属性异常')
except Exception as g:
    print(g)
    print('这个错误不在计算内')
else:
    print('计算内:)')
finally:
    print('计算完毕')



#  3
#try-except  #这两个是必须都是成对出现的不能少其一 try是开始 except是结尾判定错误
#              根据解释器内置错误或自定义错误来判定错误的类型
#try-finally  #finally  写出后不管有没有错误都会显示
"""

#   4
import traceback
def kko_value(value):
    kks ={}
    count = 0
    for a,y in value.items():
        if count >= 2:
            break
        kks[a] = y
        count += 1
    return kks

def hh_dic(cc):
    if not isinstance(cc,dict):
        raise TypeError("不是字典,终止")
    rrr = {}
    for yes,value in cc.items():
        if len(value) <= 2:
            rrr[yes] = value
        elif isinstance(value,(str,list,tuple)):
            rrr[yes] = value[:2]
        elif isinstance(value,(dict,set)):
            rrr[yes] = kko_value(value)

    return rrr

def qqq():
    try:
        dic = {"k1": "v1v1", "k2": [11, 22, 33]}
        print(hh_dic(dic))
    except Exception as g:
        traceback.print_exc()

def kko():
    qqq()
if __name__ == '__main__':
    kko()
