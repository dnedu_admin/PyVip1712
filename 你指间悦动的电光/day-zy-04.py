# -*- coding=UTF-8 -*-
# @ File: day-zy-04.py.py
# @ Time: 2018/1/23 15:06
"""
第一题:
num_list = [[1,2],[‘tom’,’jim’],(3,4),[‘ben’]]
 1. 在’ben’后面添加’kity’
 2. 获取包含’ben’的list的元素
 3. 把’jim’修改为’lucy’
 4. 尝试修改3为5,看看
 5. 把[6,7]添加到[‘tom’,’jim’]中作为第三个元素
 6.把num_list切片操作:
      num_list[-1::-1]
"""


num_list = [[1,2],['tom','jim'],(3,4),['ben']]
num_list[3].append('kity')
print(num_list)                                              #第一题输出
print(num_list[3])                                           #第二题输出
num_list[1][1] = 'lucy'
print(num_list)                                              #第三题输出
num_lists = list(num_list[2])
num_lists[0] = "5"
print(num_lists)                                             #第四题输出
num_list[1].append('6,7')
print(num_list)                                              #第五题输出
print(num_list[1::1])             #[开头0始:尾数减一原则:步数 ]
print(num_list[-1::-1])           #开头-1是最后一位数,尾数是0(默认是0)步数是-1  因为步数是-1所以右往左算[1,2]到最后一位



"""
第二题:
numbers = [1,3,5,7,8,25,4,20,29];
1.对list所有的元素按从小到大的顺序排序
2.求list所有元素之和
3.将所有元素倒序排列
"""
numbers = [1,3,5,7,8,25,4,20,29]
numbers.sort()
print(numbers)                                                 #第一题输出
kk = 0
for mm in numbers:
    kk+=mm
print(kk)                                                      #第两题输出
numbers.sort(reverse=True)                                    #第三题输出
print(numbers)
