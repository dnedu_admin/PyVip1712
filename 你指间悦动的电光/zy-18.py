# -*- coding=UTF-8 -*-
# @ File: zy-18.py.py
# @ Time: 2018/3/7 23:30

"""
1.计算range(1,99999)的总和,采取三种方式,第一:重复10次计算总时间;
 第二:开启十个子线程去计算;第三:开启十个进程去计算,然后对比耗时

2.将第一题计算十次改为使用线程池去计算,线程池线程个数由你自己电脑cpu逻辑核数决定
"""

import time
from multiprocessing import Process
def sai():
     print(sum(range(1,99999)))

if __name__=='__main__':
    boss = Process(target=sai,args=())
    time_2 = time.time()
    boss.start()
    boss.join()
    time_1 = time.time()
    time_s = time_2+time_1
    for i in range(11):
        print('运算这次的总时间为%s' % (time_s))



# 多线程
from threading import Thread,Lock
import time
# king = Lock()
def skt_1():
#    try:
#        king.acquire()
    print(sum(range(1,99999)))
#    finally:
#        king.release()

zix_ian = [Thread(target=skt_1,args=()) for mai in range(10)]
time_4 = time.time()
for skt_2 in zix_ian:
    skt_2.start()
    skt_2.join()
time_5 = time.time()
time_m = time_4+time_5
print("总时间为%s" % (time_m))

# 多进程

from multiprocessing import Pool,Process
import time

def king_1():
    print(sum(range(1,99999)))

if __name__  ==  '__main__':
    pool = Pool(10)
    for i in range(10):
        pool.apply_async(king_1, args=())
    time_8 = time.time()
    pool.close()
    pool.join()
    time_7 = time.time()
    time_z = time_8+time_7
    print("计算总时间为%s" % (time_z))



from multiprocessing import cpu_count
print("电脑的线成是%s线程" % (cpu_count()))


import time

from threading import Thread
def skt_1():
    print(sum(range(1,99999)))

zix_ian = [Thread(target=skt_1,args=()) for mais in range(8)]
time_44 = time.time()
for skt_2 in zix_ian:
    skt_2.start()
    skt_2.join()
time_55 = time.time()
time_d = time_44+time_55
print("总时间为%s" % (time_d))