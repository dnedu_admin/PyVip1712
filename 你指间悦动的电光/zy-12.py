# -*- coding=UTF-8 -*-
# @ File: zy-12.py
# @ Time: 2018/2/12 16:09
"""
1.
自己动手写一个学生类, 创建属性名字, 年龄, 性别, 完成
__new__, __init__, __str__, __del__，__call__

2.
创建父类Person, 属性name, 函数eat, run, 创建子类Student, 学生类
继承Person, 属性name, age, id, 函数eat, study, 全局定义一个函数
get_name, 传入一个Person类的实例参数, 分别传入Person和Student
两个类的实例对象进行调用.

3.
用代码完成python多态例子和鸭子模型
"""
class XieShng():
    def __init__(seif,mingzi,nianling,xingming):
        seif.m=mingzi
        seif.n=nianling
        seif.x=xingming
        print('实例对象')
    def __str__(seif):
        return"__str__方法"
    def __del__(seif):
        print('已删除')
    def __call__(seif,*yuan,**zidian):
        print('call正在使用')
kk = XieShng("xiiao",88,"hai")
kk()


class Person():
    name = ""
    def eat(self):
        print("op")
    def trn(self):
        print("lll")
class Student(Person):
    age = 0
    id = 0
    def eat(self):
        print("子类eat")
    def run(self):
        print("子类run")
def haipi(Student):
    Student.eat()

p1 = Person()
p2 = Student()

haipi(p1)
haipi(p2)

# 3.用代码完成python多态例子和鸭子模型
class dog():
    def work(self):
        print("sss")
class cat():
    def work(self):
        print("aaa")
def fun(obj):
    obj.work()

a = dog()
b = cat()

fun(a)
fun(b)
