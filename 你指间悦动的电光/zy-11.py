# -*- coding=UTF-8 -*-
# @ File: zy-11.py.py
# @ Time: 2018/2/8 11:33

"""
1.文件名称:use.txt
   创建该文件,路径由你自己指定,打开该文件,往里面写入
   str_content = ['tom is boy','\n','you are cool','\n',
                       '今天你怎么还不回来']
   把数据写入到该文件中,注意操作的模式,关闭文件句柄后
   再次打开该文件句柄,然后读取文件中内容,用with代码块
   实现,然后打印输出信息到控制台,注意换行符
"""
p = open("use.txt","w",buffering="UTF-8")
str_content = ['tom is boy', '\n', 'you are cool', '\n',
                   '今天你怎么还不回来']
p.writelines(str_content)
p.close()

with open("use.txt",'r',buffering='UTF-8') as k:
    k.readlines()
    for line in k:
        print(line.strip())


"""
2.with创建一个文件homework.txt,尝试多种操作模式.进行写读操作,注意区别
"""
#with open("homework.txt",'r+',buffering="UTF-8") as l:
#    l.write('我是谁')
#    l.read()
# w 模式是从头写覆盖数据   写
# r 模式是读模式数据       读
# a 模式是末尾追加数据
# r+可读可写  文件不存在会报错
# w+可读可写   文件不存在会构建文件,会覆盖数据
# a+可读可写   文件不存在会构建文件,会在后面追加数据

"""
3.理解文件句柄,对比read(),readline(),readlines()写一个小例子
"""
#repr()没有传参的情况下读取所有数据 返回的是字符串类型,有参数时是读取多少的位置
#readline() 默认情况下读取第一行数据,第二行需要重复,有参数是读取多少位置
#readlines()  所有的行读取  读取后放入列表里返回的是列表类型
"""
4.准备一张jpg图片,把图片中数据读书出来,并写入到文件my_img.jpg文件中,
    操作完毕后尝试打开my_img.jpg文件看图片显示正不正常
"""
with open("timg.jpg",'rb') as kk:
    hha = kk.readlines()

with open("my_img.jpg",'wb') as h:
    h.writelines(hha)


