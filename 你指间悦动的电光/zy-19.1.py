# -*- coding=UTF-8 -*-
# @ File: zy-19.1.py.py
# @ Time: 2018/3/10 21:38

"""
主要是练习装饰器,和多线程,多进程的使用

使用装饰器增强完成1-999999所有奇数和计算10次
   1.使用普通方法
   2.使用多线程
   3.使用多进程
   4.采用线程池完成计算
   5.采用进程池完成计算
"""

from multiprocessing import Pool,cpu_count

def sai_1():
    return sum(range(1, 1000000, 2))


# 2多进程
def sai_2():
    kkin = [Thread(target=sai_1,args=()) for i in range(1,11)]
    for i in kkin:
        i.start()
    for i in kkin:
        i.join()

# 3进程池
def sai_3():
    shies = Pool(cpu_count())
    for i in range(10):
        shies.apply_async(sai_1,())
    shies.close()
    shies.join()


#4多线程
from threading import Thread

def sai_4():
    SKT = [Thread(target=sai_1,args=()) for i in range(10)]
    for kk in SKT:
        kk.clear()
        kk.join()

#5线程池计算
from multiprocessing.dummy import Pool as threadpool

def sai_5():
    xian = threadpool(cpu_count())
    for i in range(10):
        xian.apply_asyuc(sai_1,())
    xian.close()
    xian.join()


if __name__=='__main__':
    print("多进程计算")
    sai_2(999999)

    print("进程池计算")
    sai_3(999999)

    print("多线程计算")
    sai_4(999999)

    print("线程池计算")
    sai_5(999999)


