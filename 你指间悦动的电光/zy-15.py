# -*- coding=UTF-8 -*-
# @ File: zy-15.py
# @ Time: 2018/3/1 10:13
"""
1.range(),需要生成下面的列表,需要填入什么参数:
    1.[3,4,5,6]
    2.[3,6,9,12,15,18]
    3.[-20,200,420,640,860]

2.列表推导式或者生成器表达式(2选择1)完成1-100之间所有偶数和

3.定义一个函数,使用关键字yield创建一个生成器,求1,3,5,7,9各数
   字的三次方,并把所有的结果放入到list里面打印输入

4.写一个自定义的迭代器类(iter)

"""
#   1
print(list(range(3,7)))
print(list(range(3,19,3)))



# 2
kk = range(2,101,2)
print(sum(kk))



# 3

def xoooo():
    for sao in range(1, 10, 2):
        yield sao ** 3

p = xoooo()
for hai in p:
    print(hai)


class Iter():
    def __init__(self,skt):
        self.skt = skt
    def __next__(self):
        if self.skt == 0:
            raise StopIteration('完了')
        self.skt -=1
        return self.skt
    def __iter__(self):
        return self

op = Iter(9)
for h in op:
    print(h)