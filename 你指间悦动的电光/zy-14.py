# -*- coding=UTF-8 -*-
# @ File: zy-14.py
# @ Time: 2018/2/27 16:42
"""
1.filter判断[0,False,True,5,{},(2,3)]对应bool值

2.map实现1-10中所有奇数项的三次方,打印输出结果

3.reduce实现1-100所有偶数项的和

4.用递归函数实现斐波拉契数列
"""


# 1

print(list(filter(lambda x: x, [0, False, True, 5, {}, (2, 3)])))

# 2.

print(list(map(lambda x: x ** 3,range(1,10,2))))


# 3.
from functools import reduce

print(reduce(lambda x,y:x+y, range(2,101,2)))


# 4
skt = int(input('请输入一个整数:'))

def rng(sao):
    if sao == 1 or sao == 0:
        return sao
    else:
        return rng(sao-1) + rng(sao-2)

for o in range(skt):
    print(rng(o), end=' ')