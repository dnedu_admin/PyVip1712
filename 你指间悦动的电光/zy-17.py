# -*- coding=UTF-8 -*-
# @ File: zy-17.py
# @ Time: 2018/3/6 20:19

"""
1.使用pickle,json(注意:两个模块)
把字典a_dict = [1,2,3,[4,5,6]]
序列化与反序列化,序列化,保存到文件名为serialize.txt文件里面,然后大开,并读取数据
反序列化为python对象加载到内存,并打印输出
尝试序列化/反序列非文件中,直接操作

2.自己写代码对比深浅拷贝的区别,写完代码然后看看运行结果,然后分析原因
   比如：a_list=[1,2,3,4,[8,9,0]]

3.分别写代码完成内存缓冲区文本数据和二进制数据的读/写操作
"""

import pickle

a_dict = [1,2,3,[4,5,6]]
with open("serialize.txt","wb+") as lin:
    pickle.dump(a_dict,lin,0)

with open("serialize.txt","rb+") as kin:
    skts = pickle.load(kin)
    print(skts)


import json

a_dictk = [1,2,3,[4,5,6]]
with open("json.txt","w+") as sai:
    json.dump(a_dictk,sai)

with open("json.txt","r+") as shu:
    king = json.load(shu)
    print(king)




from copy import copy,deepcopy

a_list = [1,2,3,4,[8,9,0]]
b_list = copy(a_list)                # 复制了第一层以外的参数,改变不共享
#print(a_list is b_list)
a_list[3] = 60
# print(a_list)
# print(b_list)
a_list[4][1] = 90                    # 内层改变,则共享
print(a_list)
print(b_list)

# deepcopy
c_list = deepcopy(a_list)            #整体复制 整体不共享
c_list[3] = 40
c_list[4][1] = 300
print(c_list)



from io import StringIO,BytesIO

xiao = StringIO('你好\n我是贾克斯\n你是谁\n')
print(xiao.getvalue())

klo = BytesIO("你好\n我是贾克斯\n你是谁\n".encode("utf-8"))
print(klo.getvalue().decode("utf-8"))