# -*- coding=UTF-8 -*-


"""1.写一个函数,然后写单元测试

2.自定义一个类,完成单元测试"""


def run(num):
    return num**3

import unittest

class MyTest(unittest.TestCase):
    def setUp(self):
        pass
    def tearDown(self):
        pass
    def testrun(self):
        self.assertEqual(run(5),125,'测试NumpySum函数失败')

if __name__=='__main__':
    unittest.main()

# 2.
import unittest

class MyOffice():
    def __init__(self,name):
        self.name=name
    def learn(self):
        print('%s正在办公室学习'%self.name)

class MyClassTest(unittest.TestCase):
    def setUp(self):
        self.m=MyOffice('zym')
    def testlearn(self):
        self.m.learn()
    def tearDown(self):
        self.m=None

if __name__=='__main__':
    unittest.main()
