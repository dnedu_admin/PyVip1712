#! /usr/bin/env python
# -*- coding:utf-8 -*-

'''
author :ben
date : 2018-1-xx

sindy(qq):2771429254
QQ: 2835579534
Python学习交流群 602319259

'''

# python流程控制

# 1.程序块与作用域
# 2.if 条件语句
# 3.while/for
# 4.循环break,continue
# 5.语句的嵌套
# 6.三元运算符

# 1.程序块与作用域:
# 概念,理解为重
# 程序块---->程序结构一种的形式,强制缩进,使代码结构清晰,便于程序员的阅读,修改
# 作用域:变量,函数--->可见行(作用域)

#
# x = 200  # 全局变量
# def work():  # 定义函数
#     # x = 2   # 当前程序块可见   局部变量
#     # y = 10
#     print(x)    # 快捷键
#     print(2)  #
# # print(x)   # 不可见
# work()     #

# 程序块和作用域合二为一:简洁,统一使用缩进
# 相同缩进范围的代码在一个程序块和作用域
# :

# def work():
#     pass   #  程序块的占位符--->
#
# work()
# 最少有一行代码


# 2.if 条件语句

# 如果今天不下雨,我们去公园完,如果下雨,就留在家里
# if 今天下雨:   # 中文    # 如果
#     print('留在家里')
# else:                  # 否则
#     print('去公园完')

# name = input('请输入你的名字:') or '王八蛋'  # ben
# if name == '王八蛋':
#     print(1)
# else:
#     print(2)

# if {}:     # 111
#     print(111)

# 总结:
# [].(),{},'',None,0---->  False
# 1,'  '------->True,



# if 结构

# socre = int(input('请输入一个整数:'))
# if socre >= 90:
#     print('非常棒')
# elif socre < 90 and socre >= 80:   # 故意  and--->  80 <= socre < 90:
#     print('很好')
# elif socre < 80 and socre >= 60:
#     print('一般')
# else:
#     print('不及格')


# 这种情况?
# socre = int(input('请输入一个整数:'))
# if socre >= 70:
#     print('非常棒')
# elif socre >= 90:   # 故意  and--->  80 <= socre < 90:
#     print('很好')
# else:
#     print('000')


# 三元运算符
# x, y = 3, 6
# if x < y:
#     print(1)
# else:
#     print(2)

# x, y, z = 3, 6, 4  # 方便
# print(1) if x < y else print(2)  # 优雅

# while

# while 0:
#     print('要死了')
#     print(111)

# num = 3
# while num < 100:  ## 循环条件
#     num +=1
#     print(num)   # 4  17
#     num = pow(num,2)
#     print(num)    # 16  289


# 1+2+3+100所有的和   1,100
# result = 0
# num = 1
# while num <=100:
#     result += num  #   result = result + num = 0 + 1 =1
#     num += 1   # 2
# print(result)

# 1+3+5+97+99所有的和
# result = 0
# num = 99
# while num > 0:
#     result += num  # result = result + num = 0 + 1 =1
#     num -= 2  # 2
# print(result)

# range函数
# py2.x---> range函数(list),xrange函数(xrange对象)
# py3.x   3.6.4
# range--->相当于2.x版本里面xrange函数,名称改变了

# num_list = range(9)  # 内置函数
# num_list = range(0, 9, 1)
# print(list(num_list))


# for 循环--->  1+2+3+100所有的和   1,100
# result = 0
# for num in range(1,1100000000000000000000000000000000000000000000000000000):    # 包左不包右
#     result += num   # 简单,理解
# print(result)

# for 循环--->  1+3+5+97+99所有的和   1,100
# result = 0
# for num in range(1,100,2):    # 包左不包右   切片参数
#     result += num   # 简单,理解
# print(result)

# range(1,100,2)    # 第一:起始元素   第二:终止元素   第三:步长

# for 循环的魅力--->代码简单优雅

# a_dict = {'name': 'ben', 'age': 18}
# for item in a_dict.items():   # for
#     print(item)         # ('name', 'ben')

# a_dict = {'name': 'ben', 'age': 18}
# for key,value in a_dict.items():   # for
#     print(key,value)        #('name', 'ben')--->x,y


# 判断是否是迭代器
# from collections import Iterable,Iterator    # Iterable(可迭代对象) Iterator(迭代器)
# print(isinstance(range(9),Iterator))
# print(isinstance(range(9),Iterable))     # 可迭代对象,不是迭代器


# break跳出循环

# num = 1
# while 1:
#     print(num)    # num=12
#     num +=1       # num=13
#     if num > 12:   # 循环终止的条件
#         break    # break--->整个循环结构
# print(1111111)

# continue
# for i in range(1,20):   # 1,2,3...19
#     if i % 2 == 0:   # 判断偶数
#         continue   #   终止本次循环,进入下一次循环
#     print(i)  # 2,4,6,..18
#     print(1111)

# x,y = 1,2
# # 其他语言里面定义临时temp
# x,y = y,x
# print(x,y)   #

# 语句嵌套
# for i in range(10):
#     for j in range(9):


# 理解起来比较吃力---->
# num_list = [44, 34, 2, 9, 12, 46, 88, 76]
# print('排序之前的num_list为:%s' % num_list)
# # 求列表的长度
# len_list = len(num_list)    # 8
# # i,j只是索引和角标
# for x in range(0,len_list-1):   # 外层循环    # (0,7)  7        # 44这一次
#     for y in range(x+1,len_list):     #  被比较对象,包左不包右     # 34, 2, 9, 12, 46, 88, 76
#         if num_list[x] > num_list[y]:  # i= 0,j=1,i=0,2
#             num_list[i],num_list[j] = num_list[j],num_list[i]
# print('排序之后的的num_list为:%s' % num_list)



