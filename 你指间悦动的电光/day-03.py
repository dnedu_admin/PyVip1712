# coding=utf-8

# 1
p=12.78
print("%020.3f"%(p))

# 2
p = "动脑学院"
print( type (p . encode("utf-8")))

# 3
m = "___"
o = "hello"
l = "动脑"
k = "pythonvip"
ss = (o,l,k)
print(m.join( ss ))

# 4
m = "    你好，动脑eric     "
print(m.strip())

# 5
ll = "xxx_name{}，xxx_num{}，xxx_age{}".format(  "Eric",      "dnpy_001",     " 28 ")
print(ll)
