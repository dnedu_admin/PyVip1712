# coding utf-8
# 动脑—1712-茫路暗逢

"""1.a_list = [1,2,3,2,2]
   删除a_list中所有的2

 2.用内置函数compile创建一个文件xx.py,在文件里写你的名字,然后用eval
   函数读取文件中内容,并打印输出到控制台

 3.写一个猜数字的游戏,给5次机会,每次随机生成一个整数,然后由控制台输入一个
   数字,比较大小.大了提示猜大了,小了提示猜小了.猜对了提示恭喜你,猜对了.
    退出程序,如果猜错了,一共给5次机会,5次机会用完程序退出."""
# 第一题
a_list = [1,2,3,2,2]
a_list=list(set(a_list))
a_list.remove(2)
print(a_list)


# 第二题
code=compile("print('name','茫路暗逢')","myfile.py","eval")
print(eval(code))

# 第三题
import random
count=0;
times=5
num=0
def question3():
    global count,num
    if count==0:
        num=random.randint(1,100)

    print("num", num)

    in_num=input("\n\n猜数字游戏开始\n你还有 %d"%(times-count)+"次机会\n请输入1-100的整数")
    count = count + 1
    try:
        in_num=int(in_num.strip())
        if in_num==num:
            print("恭喜你猜对了!")
        else:
            if in_num<num:
                print("猜小了")
            if in_num>num:
                print("猜大了")
            if count==5:
                print("5次机会已用完，游戏结束")
            else:
                print("请再猜")
                question3()
    except Exception:
        print("\neee: ",Exception)
        if count==5:
           count=0
           print("输入错误，游戏结束")
           return
        else:
           print("输入错误！还有",(times - count), "次机会")
           question3()
question3()