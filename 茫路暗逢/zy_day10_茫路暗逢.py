# coding utf-8
# 动脑—1712-茫路暗逢
"""  1.自定义一个异常类,当list内元素长度超过10的时候抛出异常
  2.思考如果对于多种不同的代码异常情况都要处理,又该如何去
     处理,自己写一个小例子
  3.try-except和try-finally有什么不同,写例子理解区别
  4.写函数，检查传入字典的每一个value的长度，如果大于2，
     那么仅仅保留前两个长度的内容，并将新内容返回给调用者
     dic = {“k1”: "v1v1","k2":[11,22,33}}"""

# 第一题

class MyExceptiion(Exception):
    def __init__(self,msg):
        self.msg=msg

    def getMsg(self):
        return self.msg
def question1():
    num_list=[]
    try:
        for i in range(20):
            num_list.append(i)
            if len(num_list)>10:
                raise  MyExceptiion("列表长度超过10")
    except MyExceptiion as e:
        print(e)
# question1()

# 第二题 (可以把要处理异常的各个代码写成不同的方法，然后再各个方法中处理异常)
def fun1():
    try:
        num=20/0
    except ZeroDivisionError as e:
        print("ZeroDivisionError")

def fun2():
    try:
        open("nothing.py")
    except FileNotFoundError as e:
        print("FileNotFoundError")

def question2():
    fun1()
    fun2()
# question2()

# 第三题
# try-except和try-finally有什么不同
"""try中的逻辑代码在遇到异常时会进入except语句，最后再进入finally,在没遇到异常时会进到finally,不会进到except"""
def fun3():
    try:
        print("\n1.程序没有遇到异常的执行步骤")
        num=8/4
    except Exception as e:
        print("程序遇到异常,来到except")
    finally:
        print("程序没有遇到异常，我来到了这里finally")

def fun4():
    try:
        print("\n2.程序遇到异常的执行步骤")
        num = 8 / 0

    except Exception as e:
        print("程序遇到异常了，来到except")
    finally:
        print("虽然有异常抛出，但是我还是执行到这里finally")

def question3():
    fun3()
    fun4()
question3()

# 第四题
dict_1={"aaa":"aaaaa","bbb":"b1b1b1","ccc":236}
def question4(mDic):
    try:
        if type(mDic) is dict:
            for key,val in mDic.items():
                if len(val)>2:
                    mDic[key]=val[0:2]
            return mDic
        else:
            return mDic
    except Exception as e:
        return mDic
print(question4(dict_1))