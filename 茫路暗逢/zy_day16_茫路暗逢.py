# coding utf-8
# 动脑—1712-茫路暗逢

"""
1.写一个函数计算1 + 3 + 5 +…+97 + 99的结果
      再写一个装饰器函数, 对其装饰, 在运算之前, 新建一个文件
      然后结算结果, 最后把计算的结果写入到文件里


2.写一个函数计算传入进来参数的平方, 并将结果.写一个带参数的装饰器,
       将装饰器参数传入到被包装的函数,计算并输入结果
"""
def func1_1(fun):
    f=open("mf.txt","a+")
    num=fun()
    print("计算结果 : ",num)
    f.write("计算结果 : %d"%num)
    f.close()
    return fun


from functools import reduce
@func1_1
def fun1_2():
    num=reduce(lambda x,y : x+y,range(1,100,2))
    return num
# fun1_2()

def func2_1(arg):
    def fun_mid(fun):
        def fun_inner(a):
            a_list=[]
            num1=fun(arg)
            print("装饰器自带参数计算的结果是：%d"%num1)
            a_list.append(num1)

            num2=fun(a)
            print("传入实参计算的结果是 ： %d"%num2)
            a_list.append(num2)
            return fun
        return fun_inner
    return fun_mid

@func2_1(15)
def func2_2(a):
    num=a*a
    return num

func2_2(3)