# coding utf-8
# 动脑—1712-茫路暗逢

# 1.用while和for两种循环求计算1+3+5.....+45+47+49的值
def question1(cond):
    num_list=range(1,50)
    sum1 = 0
    if cond=="while":

        i=1
        while i<50:
            sum1+=i
            i+=2
        print(" 1.用while和for两种循环求计算1+3+5.....+45+47+49的值  while")
        print(sum1)
        sum1=0
    elif cond=="for":
        for i in range(1,50,2):
            sum1+=i
        print(" 1.用while和for两种循环求计算1+3+5.....+45+47+49的值  for")
        print(sum1)
        sum1=0
question1("while")
question1("for")


# 2.代码实现斐波那契数列(比如前30个项)
def question2(index):
    mlist=[1,1]
    i=0
    while i<index-2:
        mlist.append(mlist[len(mlist)-1]+mlist[len(mlist)-2])
        i+=1
    print("2.代码实现斐波那契数列(比如前30个项)")
    print(mlist)

question2(30)
