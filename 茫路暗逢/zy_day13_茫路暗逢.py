# coding utf-8
# 动脑—1712-茫路暗逢

"""
1.自己写代码对比2.x深度优先和3.x广度优先的继承顺序
2.写一个类MyClass,使用__slots__属性,定义属性(id,name),创建该类实例
    后,动态附加属性age,然后执行代码看看.
3.使用type创建一个类,属性name,age,sex,方法sleep,run并创建此类的
    实例对象
4.写例子完成属性包装,包括获取属性,设置属性,删除属性

"""

"""
#1
class A():
    def fun(self):
        print('A')
class B():
    def fun(self):
        print('B')
class C(A, B):
    pass
class D(A, B):
    def fun(self):
        print('D')
class E(C, D):
    pass
e = E()
e.fun() # python3.x   D(广度优先)          python2.7  A（深度优先）
"""

"""
#2
class Myclass():
    __slots__ = ('id','name')
    def get_name(self):
        print("name = ",self.name)
    def get_id(self):
        print("id = ",self.id)
c=Myclass()
c.name="茫路暗逢"
c.id=123456
# c.get_name()
# c.get_id()
def set_age(self, age):
    self.age = age
    print("age = ",age)
c.set_age=set_age
c.set_age(c,26)  #报错 AttributeError: 'Myclass' object has no attribute 'set_age'
"""

"""
#3
def sleep(self):
    print(self.name,"睡觉了")
def run(self):
    print(self.name,"起来工作了")
Students=type("Students",(),{"name":"mang","age":26,"sex":"男","sleep":sleep,"run":run})
s=Students()
s.sleep()
s.run()
"""

#4
class Person():
    @property
    def sex(self):
        return self._sex

    @sex.setter
    def sex(self, sex):
        if sex!="男"  and sex != "女":
            raise ValueError('性别必须是男/女')
        self._sex = sex

    @sex.deleter
    def sex(self):
        del self._sex

p=Person()
p.sex="男"
print(p.sex)
del p.sex
print(p.sex)  #报错(已删除)






