# coding utf-8
# 动脑—1712-茫路暗逢

"""
1.使用pickle,json(注意:两个模块)
把字典a_dict = [1,2,3,[4,5,6]]
序列化与反序列化,序列化,保存到文件名为serialize.txt文件里面,然后大开,并读取数据
反序列化为python对象加载到内存,并打印输出
尝试序列化/反序列非文件中,直接操作

2.自己写代码对比深浅拷贝的区别,写完代码然后看看运行结果,然后分析原因
   比如：a_list=[1,2,3,4,[8,9,0]]

3.分别写代码完成内存缓冲区文本数据和二进制数据的读/写操作
"""
import pickle
import json

def func1():
    # pickle
    a_dict = [1, 2, 3, [4, 5, 6]]

    f=open("serialize.txt","wb+")
    pickle.dumps(a_dict,f)
    f.close()

    f1=open("serialize.txt","rb+")
    print(pickle.load(f))
    f.close()

    f2=open("s2.txt","ab+")
    json.dump(a_dict,f)
    f2.close()

    f3=open("s2.txt","rb+")
    print(json.load(f))
# func1()

def func2():
    from copy import copy
    from copy import deepcopy
    a_list = [1, 2, 3, 4, [8, 9, 0]]

    b_list=copy(a_list)
    c_list=deepcopy(a_list)
    a_list[4][2]=5
    print("a_list的id是: " ,id(a_list))
    print("b_list的id是: " ,id(b_list))
    print("c_list的id是: " ,id(c_list))
    print(b_list is a_list)
    print(c_list is a_list)
    print(b_list[4][2]) #浅拷贝的值随原值的变化而变化
    print(c_list[4][2])
# func2()

def func3():
    from io import StringIO
    f=StringIO()
    f.write("文本数据")
    print(f.getvalue())

    f1=StringIO("这是一行，\n 这是下一行")
    while True:
        str=f1.readline()
        if str:
            print(str)
# func3()

def func4():
    from io import BytesIO
    b=BytesIO()
    b.write("二进制数据".encode("utf-8"))
    print(b.getvalue())
    print(b.getvalue().decode("utf-8"))
func4()