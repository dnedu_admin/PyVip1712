# coding utf-8
# 动脑—1712-茫路暗逢

"""第一题:
num_list = [[1,2],[‘tom’,’jim’],(3,4),[‘ben’]]
 1. 在’ben’后面添加’kity’
 2. 获取包含’ben’的list的元素
 3. 把’jim’修改为’lucy’
 4. 尝试修改3为5,看看
 5. 把[6,7]添加到[‘tom’,’jim’]中作为第三个元素
 6.把num_list切片操作:
      num_list[-1::-1]"""

def getObjIndex(mlist,obj):
    ind = -1;
    try:
        ind = mlist.index(obj)
    except Exception:
        ind=-1
    return ind


def question1():
    print("第一题：",end="\n")
    num_list = [[1, 2], ['tom','jim'], (3, 4), ['ben']]
    # 1. 在’ben’后面添加’kity’
    num_list[3].append('kity')
    print("1. 在’ben’后面添加’kity’")
    print(num_list,end="\n\n")

    # 2.获取包含’ben’的list的元素
    obj='ben'
    ind=getObjIndex(num_list,obj)
    if ind==-1:
       for index,item in enumerate(num_list):
        if type(item) is list:
            subind=getObjIndex(item,obj)
            if subind!=-1:
                ind=index
                break
    if ind==-1:
        print("2.获取包含’ben’的list的元素")
        print(num_list,"没有包含",obj,"的元素",end="\n\n")
    else:
        print("2.获取包含’ben’的list的元素")
        print(num_list,"中包含"+obj+"的元素为: ",num_list[index],end="\n\n")


    # 3.把’jim’修改为’lucy’
    obj1 = 'jim'
    ind1 = getObjIndex(num_list, obj1)
    if ind1 == -1:
        for index, item in enumerate(num_list):
            if type(item) is list:
                subind = getObjIndex(item, obj1)
                if subind != -1:
                    num_list[index][subind]='lucy'
                    break
    else:
        num_list[ind1]='lucy'
    print("3.把’jim’修改为’lucy’")
    print(num_list,end="\n\n")

    # 4.尝试修改3为5, 看看
    try:
        num_list[2][0]=5
        print("4.尝试修改3为5, 看看")
        print(num_list,end="\n\n")
    except Exception:
        print("4.尝试修改3为5, 看看")
        print(Exception,end="\n\n")

    # 5.把[6, 7]添加到[‘tom’, ’jim’]中作为第三个元素
    sublist=[6,7]
    num_list[1].append(sublist)
    print("5.把[6, 7]添加到[‘tom’, ’jim’]中作为第三个元素")
    print(num_list,end="\n\n")

    # 6.把num_list切片操作: num_list[-1::-1]"""
    ml=num_list[-1::-1]
    print("6.把num_list切片操作: num_list[-1::-1]")
    print(ml,end="\n\n\n")

question1()

"""第二题:
numbers = [1,3,5,7,8,25,4,20,29];
1.对list所有的元素按从小到大的顺序排序
2.求list所有元素之和
3.将所有元素倒序排列"""

def question2():
    print("第二题：",end="\n")
    numbers = [1, 3, 5, 7, 8, 25, 4, 20, 29];
    # 1.对list所有的元素按从小到大的顺序排序
    numbers.sort()
    print("1.对list所有的元素按从小到大的顺序排序")
    print(numbers,end="\n\n")

    # 2.求list所有元素之和
    sums=sum(numbers)
    print("2.求list所有元素之和")
    print(sums,end="\n\n")

    # 3.将所有元素倒序排列
    numbers.reverse()
    print("3.将所有元素倒序排列")
    print(numbers)
question2()