# coding utf-8
# 动脑—1712-茫路暗逢

"""
1.filter判断[0,False,True,5,{},(2,3)]对应bool值

2.map实现1-10中所有奇数项的三次方,打印输出结果

3.reduce实现1-100所有偶数项的和

4.用递归函数实现斐波拉契数列

"""
def question1():
    la=[0,False,True,5,{},(2,3)]
    print(list(filter(lambda x:bool(x),la)))
# question1()


def question2():
    print(list(map(lambda x:x**3 if x%2==1 else 0,range(1,10,2))))
# question2()

from functools import reduce
def question3():
    print(reduce(lambda x,y:x+y ,range(2,100,2)))
# question3()

def fbnq(num):
    if num<0:
        raise ValueError("你输入的项数不对")
    if num==0 or num==1:  #第0项和第一项
        return 1
    return fbnq(num-1)+fbnq(num-2)
def question4():
    try:
        cc = input("请输入要求的斐波那契数列的项数:")
        cc=int(cc)
        al=[]
        for i in range(cc):
            al.append(fbnq(i))
        print(al)
    except Exception as e:
        print("输入有误")
print(fbnq(3))
question4()