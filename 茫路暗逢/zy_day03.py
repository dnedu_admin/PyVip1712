# coding utf-8
# 动脑—1712-茫路暗逢

'''1. 格式化显示浮点数12.78，要求保留3位小数，输出占20字符，空位用0补齐'''

def question1(num):
    try:
        print(num,"格式化输出的结果是:%020f.3"%num)
    except Exception:
        print("参数错误！")
# question1(12.78)

"""2. 将字符串”动脑学院”转换为bytes类型。"""
def question2(str):
    print(str,"转为bytes类型后是：",str.encode("utf-8","strict"))
# question2("动脑学院")

"""3. 将以下3个字符串合并为一个字符串，字符串之间用3个_分割
	hello  动脑  pythonvip       
	合并为“hello___动脑___pythonvip”"""
def question3():
    strs=["hello","动脑","pythonvip"]
    print("合并后的字符串是:"+"___".join(strs))
# question3()

"""4. 删除字符串  “    你好，动脑eric     ”首尾的空格符号"""
str="    你好，动脑eric     "
# print("删除首尾空格后的字符串位为：",str.strip())

"""5. 用户信息存在如下所示字符串中，包含信息依次为  姓名   学号  年龄，
不同信息之间的空格数不确定，请提取用户信息分别保存到 xxx_name,xxx_num, xxx_age。
eric=“    Eric   dnpy_001     28”        
提取后 eric_name=“Eric”eric_num=”dnpy_001”,eric_age=”28”
zhangsan = “ 张三         dnpy_100    22     ”"""
zhangsan=" 张三         dnpy_100    22     "
def question5(str):
    str=str.strip()
    infos=str.partition(" ")
    name=infos[0]
    infos2=infos[2].rpartition(" ")
    age=infos2[2]
    num=infos2[0].strip()
    # print("name: %s,\nnum: %s,\nage: %s" %(name,num,age))
    # print("name: {0},num: {1},age: {2}".format(name,num,age))
question5(zhangsan)