# coding utf-8
# 动脑—1712-茫路暗逢

# 1.简述普通参数、默认参数、关键字参数、动态收集参数的区别,理解为主
'''
1)普通参数：是指没有默认值的参数，调用方法的时候必须传入的参数，否则会执行异常
2）默认参数：是指制定了默认值的参数，调用方法的时候可以不传入该参数，则使用的是声明方法时的默认值
    如果传入了对应的值，那么调用方法的时候将会用传入进来的参数值替代指定的默认值进行运算
3)关键字参数：是指调用函数的时候指定函数的各个参数对应的值，这样可以无视参数声明的顺序，但是参数名字要和函数中声明的名字一样
4）动态收集参数：是指可以不指定传入的参数的个数，如果是一个”*“指定的参数，当调用方法传入n个参数时，这n个参数将会被收集到一个元组里面；
    如果是两个星号"**"指定的收集参数，则其接受的单个参数为字典对象，函数会将传入的各个字典对象收集到一个字典中以供使用,有默认值()和{}
'''

"""2.def foo(x, y, z, *args, **kw):
    sum = x + y + z
    print(sum)
    for i in args:
        print(i)
    print(kw)
    for j in kw.items():
        print(j)
a_tuple = (1,2,3)     此处参数修改为2个看看会怎么样?
b_dict = {'name': 'jim', 'age': 28, 'add': '上海'}
foo(*a_tuple, **b_dict)    分析这里会怎么样?"""

def foo(x, y, z, *args, **kw):
    sum = x + y + z
    print(sum)
    for i in args:
        print(i)
    print("km: ",kw)
    for j in kw.items():
        print(j)
a_tuple = (1,2,3)     # 此处参数修改为2个看看会怎么样?
# 上一步如果将元组改为两个元素，在后面拆包后只有两个元素（参数值）传入到foo方法，但是foo方法有3个默认参数，少了一个，所以会报错
b_dict = {'name': 'jim', 'age': 28, 'add': '上海'}
# foo(*a_tuple, **b_dict)    # 分析这里会怎么样?
# 这里先将a_tuple拆包成 1,2,3；再将b_dict拆包成name='jim',age=28,add='上海',然后传入方法中执行
# 1,2,3分别对应x，y，z顺序传入，然后剩下的参数是字典对象，对应的传入动态收集参数km中

"""3.题目:执行分析下代码
    def func(a, b, c=9, *args, **kw):
         print('a =', a, 'b =', b, 'c =', c, 'args =', args, 'kw =', kw)
    func(1,2)
    func(1,2,3)
    func(1,2,3,4)
    func(1,2,3,4,5)
    func(1,2,3,4,5,6,name='jim')
    func(1,2,3,4,5,6,name='tom',age=22)
    扩展: 如果把你的函数也定义成  def  get_sum(*args,**kw):pass 
    你的函数可以接受多少参数?"""


def func(a, b, c=9, *args, **kw):
    print('a =', a, 'b =', b, 'c =', c, 'args =', args, 'kw =', kw)

# func(1, 2) # 分别传给对应的参数a,b
# func(1, 2, 3) # 分别传给对应的参a,b,c
# func(1, 2, 3, 4) # 前三个分别传给参数 a，b，c，第四个参数不是字典对象，收集到动态参数args
# func(1, 2, 3, 4, 5)# 前三个分别传给参数 a，b，c，第四、五个参数不是字典对象，收集到动态参数args
# func(1, 2, 3, 4, 5, 6, name='jim')# 前三个分别传给参数 a，b，c，第四、五、六个参数不是字典对象，收集到动态参数args,最后一个是字典对象，收集到参数kw
# func(1, 2, 3, 4, 5, 6, name='tom', age=22)# 前三个分别传给参数 a，b，c，第四、五、六个参数不是字典对象，收集到动态参数args,最后两个是字典对象，收集到参数kw

# 扩展: 如果把你的函数也定义成  def  get_sum(*args,**kw):pass
#     你的函数可以接受无穷多个参数

"""4.写一个函数函数，计算传入字符串中的数字、字母、空格和其他的个数分别为多少?"""
import re
def dealWithStr(strs):
    a_list=strs.split(" ")

    print("空格的个数 = ",len(a_list)-1)

    nums=re.findall("\d+",strs)
    sums=0
    for item in nums:
        sums+=len(item)
    print("数字的个数 = ",sums)

    alphas=re.findall("[a-zA-Z]",strs)
    print("字母的个数 = ",len(alphas))

    print("其他字符的个数 = ",len(strs)-(len(a_list)-1)-len(alphas)-sums)


dealWithStr(" dsfa1234sdf sd 3石佛寺大方 sdf 56 ")