# coding utf-8
# 动脑—1712-茫路暗逢

# 1.自己写一个字典:添加,删除,更新,清空操作
def question1():
    mDic={"pro":"gd","city":"sz"}
    mDic["eara"]="ft" # 添加
    mDic.pop("eara") # 删除
    mDic.popitem()
    mDic.update({"ishot":"false"}) # 更新
    mDic.clear() # 清空
    print(mDic)
# question1()

"""2.写两个集合:
    num1_set = {3,5,1,2,7}
    num2_set = {1,2,3,11}
    并分别进行&,|,^,-运算"""
def qestion2():
    set1={1,6,8,5,"a","n"}
    set2={"a","s",8,3,5,4}
    print("set1 & set2 : ",set1 & set2)
    print("set1 | set2 : ",set1 | set2)
    print("set1 ^ set2 : ",set1 ^ set2)
    print("set1 - set2 : ",set1 - set2)
# qestion2()

"""3.整数字典:
    num_dict = {'a':13,'b':22,'c':18,'d':24}
    按照dict中value从小到大的顺序排序"""
def qestion3():
    num_dict = {'a': 13, 'b': 22, 'c': 18, 'd': 24}
     # 字典是无序的，要排序课先转为列表对象
    num_list=num_dict.items()
    num_list_sorted=sorted(num_list,key=lambda val:val[1])
    # print(num_list_sorted)
    print("按照dict中value从小到大的顺序排序")
    print(dict(num_list_sorted))
qestion3()


