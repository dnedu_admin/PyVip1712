# coding utf-8
# 动脑—1712-茫路暗逢

"""
1.range(),需要生成下面的列表,需要填入什么参数:
    1.[3,4,5,6]
    2.[3,6,9,12,15,18]
    3.[-20,200,420,640,860]

2.列表推导式或者生成器表达式(2选择1)完成1-100之间所有偶数和

3.定义一个函数,使用关键字yield创建一个生成器,求1,3,5,7,9各数
   字的三次方,并把所有的结果放入到list里面打印输入

4.写一个自定义的迭代器类(iter)
"""
# 1.1 range(3,7)
#1.2 range(3,19,3)
#1.3 range(-20,900,200)

#第二题
def fun1():
    print(sum([x for x in range(101) if x%2==0]))
fun1()
def func2():
    a_generator = (x for x in range(101) if x%2==0)
    all=0
    for num in a_generator:
        all+=num
    print(all)
func2()

# 第三题
def get_generator():
      a_list=list(range(1,10,2))
      for num in a_list:
          yield pow(num,3)
g = get_generator()
print(list(g))

#第四题

class product_iter():
    def __init__(self,count):
        self.count=count
    def __next__(self):
        if self.count==0:
            raise StopIteration("商品已售完")
        self.count-=1
        return self.count
    def __iter__(self):
        return self
m_iter=product_iter(5)
print(next(m_iter))
print(next(m_iter))
print(next(m_iter))
print(next(m_iter))
print(next(m_iter))
print(next(m_iter))


