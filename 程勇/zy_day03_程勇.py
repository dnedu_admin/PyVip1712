#!/usr/bin/env python
#coding:utf-8
"""
  Author:  cocoforever --<>
  Purpose: 动脑学院第三天作业
  Created: 2018/1/20
"""

import re
import os

def question_01():
    #1. 格式化显示浮点数12.78，要求保留3位小数，输出占20字符，空位用0补齐
    print('浮点数{num},格式化后:{num:0>20.3f}'.format(num = 12.78))
    
def question_02():
    #2. 将字符串”动脑学院”转换为bytes类型
    print('原字符串：{}'.format('动脑学院'))
    print('bytes类型:{}'.format(bytes('动脑学院',encoding="UTF-8")))
    print('bytes类型:{}'.format(('动脑学院').encode()))

def question_03():
    '''
    将以下3个字符串合并为一个字符串，字符串之间用3个_分割
	hello  动脑  pythonvip       合并为“hello___动脑___pythonvip”
    '''
    print('___'.join(['hello','动脑','pythonvip']))

def question_04():
    #删除字符串  “    你好，动脑eric     ”首尾的空格符号
    print('原字符串：{},删除首位空格后{}'.format('    你好，动脑eric     ','    你好，动脑eric     '.strip()))

def question_05():
    '''
    用户信息存在如下所示字符串中，包含信息依次为  姓名   学号  年龄，
    不同信息之间的空格数不确定，请提取用户信息分别保存到 xxx_name,xxx_num, xxx_age
    '''
    #字典来记录数据
    info_dic = {
    'eric':"    Eric   dnpy_001     28",
    'zhangsan':' 张三         dnpy_100    22     ',
    }
    
    re_space = re.compile(r'(\S+)\s?')
    for nick,info in info_dic.items():
        del_space = re_space.findall(info)
        exec(nick + '_name = del_space[0]')
        exec(nick + '_num = del_space[1]')
        exec(nick + '_age = del_space[2]')
        print('{nick}_name:{name},{nick}_num:{num},{nick}_age:{age}'.format(nick=nick,
                                                                            name=eval(nick+'_name'),
                                                                            num=eval(nick+'_num'),
                                                                            age=eval(nick+'_age')))
    
def main():
    
    print('第一题作业：')
    question_01()
    
    print('\n\n第二题作业:')
    question_02()
    
    print('\n\n第三题作业:')
    question_03()
    
    print('\n\n第四题作业:')
    question_04()
    
    print('\n\n第五题作业:')
    question_05()
    
if __name__ == '__main__':
    main()