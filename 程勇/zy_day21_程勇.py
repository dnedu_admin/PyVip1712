#!/usr/bin/env python
#coding:utf-8
"""
  Author:  程勇 --<>
  Purpose: 动脑学院第21节作业
  Created: 2018/3/15
"""

import re
import os

'''1.识别下面字符串:’ben’,’hit’或者’ hut’'''
question_01 = re.compile(r'ben|hit| hut')
r = question_01.findall('djkfahbendfakjfdhitkdjfk hut kjkjdasf ')
print(r)

'''2.匹配用一个空格分隔的任意一对单词,比如你的姓 名'''
question_02 = re.search(r'\w+\s\w+','coco forever')
print(question_02.group())

'''3.匹配用一个逗号和一个空格分开的一个单词和一个字母'''
question_03 = re.compile(r'\w+,\s\w+')
r_3 = question_03.findall('dfaf kjakef, dfajk')
print(r_3)

'''4.匹配简单的以’www’开头,以’com’结尾的web域名,比如:www.baidu.com'''
question_04 = re.compile(r'^www\.[\w|\.]+\.com$')
r_4 = question_04.findall('www.dfaf.comdfafdfwww.dfkajk.cn.www.dafo.com')
print(r_4)