#!/usr/bin/env python
#coding:utf-8
"""
  Author:  cocoforever --<>
  Purpose: 动脑学院第9节课作业
  Created: 2018/2/3
"""

import os
import random
import re

def question_01():
    '''a_list = [1,2,3,2,2]删除a_list中所有的2'''
    
    a_list = [1,2,3,2,2]
    num_count = a_list.count(2)
    
    print('原列表:{}'.format(a_list))
    for i in range(num_count):
        a_list.remove(2)
    print('删除所有2后的结果{}'.format(a_list))
    
def question_02():
    '''用内置函数compile创建一个文件xx.py
    '''
    code = compile("open('xx.py','a',encoding='utf-8').write('程勇')", 'xx.py', 'exec')
    exec(code)
    print(eval("open('xx.py','r',encoding='utf-8').read()"))
        
def question_03():
    '''猜数字的游戏'''
    winning_number = random.randint(1,100)
    
    print('''
        猜数字游戏
    您有5次机会，数字为1-100之间的整数。
    当您猜的数字大时，提示猜大了；
    当您猜的数字小时，提示猜小了；
    当您猜对了，那就恭喜你啊
        ''')
    chance = 5
    while chance:
        guess = input('请输入1-100之间任意整数:')
        
        #判断输入的是否是整数
        if re.search(r'\D+',guess):
            print('请输入正确格式！整数！！！')
            continue
        else:
            guess = int(guess)
        
        chance -= 1
        if guess > winning_number:
            print('猜大了。你还有{}次机会'.format(chance)) if chance > 0 else print('猜大了。机会已用完，退出程序')
        elif guess < winning_number:
            print('猜小了。你还有{}次机会'.format(chance)) if chance > 0 else print('猜大了。机会已用完，退出程序')
        else:
            print('恭喜你猜对了')
            break
        
   
def main():
    
    '''第一题作业'''
    question_01()
    
    '''第二题作业'''
    question_02()
    
    '''第三题作业'''
    question_03()
    
if __name__ == '__main__':
    main()