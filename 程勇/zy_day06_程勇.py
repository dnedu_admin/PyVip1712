#!/usr/bin/env python
#coding:utf-8
"""
  Author:  cocoforever --<>
  Purpose: 程勇第六天作业
  Created: 2018/1/27
"""

import os

def question_01():
    ''' 1.用while和for两种循环求计算1+3+5.....+45+47+49的值'''
    
    #while计算
    n = 1
    sum = 0
    while n < 50:
        sum += n
        n += 2
    print('while循环计算结果：',sum)
    
    #for计算
    sum = 0
    for i in range(1,50,2):
        sum += i
    print('for循环计算结果',sum)
    
def Fib(n):
    if n <= 2:
        return 1
    else:
        return Fib(n-1) + Fib(n-2)
    
def question_02():
    '''2.代码实现斐波那契数列(比如前30个项)
    1 1 2 3 5 8 13 21
    '''
    fibon_list = []
    for i in range(0,30):
        if i < 2:
            fibon_list.append(1)
        else:
            fibon_list.append(fibon_list[i-1] + fibon_list[i-2])
    print('循环计算斐波那契数列:',fibon_list)
    
    #递归计算 时间 太长了
    #print(Fib(30))
    
def main():
    
    #第一题
    question_01()
    
    #第二题
    question_02()

if __name__ == '__main__':
    main()
        