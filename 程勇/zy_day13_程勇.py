#!/usr/bin/env python
#coding:utf-8
"""
  Author:  程勇 --<>
  Purpose: 动脑学院第13节课作业
  Created: 2018/2/24
"""

import os

'''对比2.x深度优先和3.x广度优先的继承顺序'''
class third_lv1():
    def answer(self):
        print('我是第三级1')

class third_lv2():
    def answer(self):
        print('我是第三级2')

class second_lv1(third_lv1,third_lv2):
    pass

class second_lv2(third_lv1,third_lv2):
    def answer(self):
        print('我是第二级2')
        
class first_lv1(second_lv1,second_lv2):
    pass

def question_1():
    first_lv1().answer()

'''写一个类MyClass'''
class MyClass():
    def __init__(self,id,name):
        self.id = id
        self.name = name
        
    __slots__ = ('id','name','age')
    
def question_2():
    my = MyClass('1','test')
    my.age = 20
    print(my.id,my.name,my.age)

'''使用type创建一个类,属性name,age,sex,方法sleep,run并创建此类的'''
def sleep(self):
    print('{}在睡觉'.format(self.name))
    
def run(self):
    print('{}在跑步'.format(self.name))

Student = type('Student',(),{'name':'xiaoming','age':18,'sex':'man','sleep':sleep,'run':run})
def question_3():
    xiaoming = Student()
    xiaoming.sleep()
    xiaoming.run()
    
'''写例子完成属性包装,包括获取属性,设置属性,删除属性'''
class Area():
    @property
    def width(self):
        return self._width
    
    @width.setter
    def width(self,width):
        if not isinstance(width,(int,float)):
            raise TypeError('宽度必须为整数或浮点数')
        if width <= 0:
            raise ValueError('宽度不能小于0')
        self._width = width
        
    @width.deleter
    def width(self):
        del self._width
    
    @property
    def length(self):
        return self._length
    
    @length.setter
    def length(self,length):
        if not isinstance(length,(int,float)):
            raise TypeError('宽度必须为整数或浮点数')
        if length <= 0:
            raise ValueError('长度不能小于0')
        self._length = length
    
    @length.deleter
    def length(self):
        del self._length
        
    def cal_area(self):
        return self._width * self._length
    
def question_4():
    area = Area()
    area.width = 50
    area.length = 60
    print('该面积为:',area.cal_area())
    del area.width
    del area.length

def main():
    '第一题'
    print('第一题')
    question_1()
    
    print('第二题')
    question_2()
    
    print('第三题')
    question_3()
    
    print('第四题')
    question_4()
    
if __name__ == '__main__':
    main()