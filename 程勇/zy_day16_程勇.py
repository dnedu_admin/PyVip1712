#!/usr/bin/env python
#coding:utf-8
"""
  Author:  程勇 --<>
  Purpose: 动脑学院第16节作业
  Created: 2018/3/3
"""

import os

'''写一个函数计算1 + 3 + 5 +…+97 + 99的结果
      再写一个装饰器函数, 对其装饰, 在运算之前, 新建一个文件
      然后结算结果, 最后把计算的结果写入到文件里'''
def log_result(func_sum):
    def record(solve_list):
        with open('day_16.txt','a+',encoding='utf-8') as txt:
            result = func_sum(solve_list)
            txt.write('1+3+5+...+97+99的结果为：{}'.format(result))
        
        return result
    return record

@log_result
def interger_list(org_list):
    if not isinstance(org_list,list):
        raise TypeError('只能计算列表的和')
    
    return sum(org_list)

def question_01():   
    sum_1 = interger_list(list(range(1,100,2)))
    print('结果为{}'.format(sum_1))
    
'''写一个函数计算传入进来参数的平方, 并将结果.写一个带参数的装饰器, 
       将装饰器参数传入到被包装的函数,计算并输入结果'''
def my_dec(a):
    def dec_pargam(main_func):
        def innter():
            print('开始计算参数的平方')
            num = main_func(a)
            print('结果为{}'.format(num))
            return num     
        return innter
    return dec_pargam

@my_dec(6)
def square_number(*args):
    if len(args) < 1:
        return '参数没有传入'
    
    return [num ** 2 for num in args]

def question_02():   
    sqr_2 = square_number()
    
def main():
    '''第一题'''
    question_01()
    
    '''第二题'''
    question_02()
    
if __name__ == '__main__':
    main()