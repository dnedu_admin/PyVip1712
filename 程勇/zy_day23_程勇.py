#!/usr/bin/env python
#coding:utf-8
"""
  Author:  程勇 --<>
  Purpose: 动脑学院第23节作业
  Created: 2018/3/20
"""

import os
import datetime
import time

'''1.datetime,获取你当前系统时间三天前的时间戳;并把当前时间戳增加
 899999,然后转换为日期时间对象,把日期时间并按照%Y-%m-%d %H:%M:%S
 输入打印这个日期对象,并获取date属性和time属性,最后判断当前日期时间
 对象是星期几'''
def question_01():
    now_time = datetime.datetime.now()
    three_days_before = (now_time - datetime.timedelta(days=3)).timestamp()
    
    after_time = datetime.datetime.utcfromtimestamp(time.time() + 899999)
    after_str = after_time.strftime('%Y-%m-%d %H:%M:%S')
    
    now_week = now_time.isoweekday()
    
    print('三天前的时间戳:{}'.format(three_days_before))
    print('增加899999后的日期:{}'.format(after_str))
    print('date属性:{}'.format(after_time.date()))
    print('time属性:{}'.format(after_time.time()))
    print('当前日期时间为{}'.format(now_week))    

'''2.使用os模块线获取当前工作目录的路径,并打印当前目录下所有目录和文件,
获取当前脚本的绝对路径, 分割当前脚本文件的目录和文件在tuple中并判断
当前文件所在的目录下存不存在文件game.py,如果不存在则创建该文件.
并往该文件里写入数据信息,然后判断当前目录下是否存在目录
/wow/temp/,如果不存在则创建,最后把目录名称改为:/wow/map/'''
def question_02():
    cwd = os.getcwd()
    
    lisdir = os.listdir(r'./')
    
    abspa = os.path.abspath(__file__)
    
    path_tuple = os.path.split(abspa)
    
    file_path = path_tuple[0] + '\game.py'
    if not os.path.exists(file_path):
        with open(file_path,'w',encoding='utf-8') as t:
            t.write('动脑学院第23节作业')
    
    dir_path = path_tuple[0] + '\wow\\temp'
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)
    os.renames(dir_path,path_tuple[0]+'\wow\map')   
    

'''3.写函数，检查获取传入列表或元组对象的所有奇数位索引对应的元素，并将其作为新的列表返回给调用者 '''
def question_03(a):
    
    #判断是否是列表或元组
    if not isinstance(a,(list,tuple)):
        return '传入的参数有误'
    
    return a[::2]
