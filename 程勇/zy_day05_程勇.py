#!/usr/bin/env python
#coding:utf-8
"""
  Author:  程勇 --<>
  Purpose: 动脑学院第五天作业
  Created: 2018/1/25
"""

import os

def question_01():
    '''
    对字典增 删 改 查 清空
    '''
    name_dict = {'name':'ben','age':22,'sex':'男'}
    
    #字典增加
    print("字典增加职业Occupation：老师teacher")
    name_dict['occupation'] = 'teacher'
    print(name_dict,'\n')
    
    #删除字典指定
    print('删除字典中age')
    if name_dict.pop('age','该数据不存在') == '该数据不存在':
        print('该数据不存在')
    else:
        print(name_dict,'\n')
        
    #对字典更新 1.原有字典更新value 2.将第二个字典更新到第一个字典中
    print('更新字典中sex，将男改成女')
    name_dict['sex'] = '女'
    print(name_dict,'\n')
    print('将字典{"addr":"台州"}更新到name_dict中')
    addr_dict = {'addr':'台州'}
    name_dict.update(addr_dict)
    print(name_dict,'\n')
    
    #清空字典
    print('对字典进行清空')
    name_dict.clear()
    print(name_dict,'\n')
    
def question_02():
    '''写两个集合:
    num1_set = {3,5,1,2,7}
    num2_set = {1,2,3,11}
    并分别进行&,|,^,-运算'''
    num1_set = {3,5,1,2,7}
    num2_set = {1,2,3,11}
    
    #&运算
    print('两集合与&运算',num1_set & num2_set)
    
    #|运算
    print('两集合或|运算',num1_set | num2_set)
    
    #^运算
    print('两集合与或^运算',num1_set ^ num2_set)
    
    #-运算
    print('两集合差-运算 num1_set - num2_set',num1_set - num2_set)
    print('两集合差-运算 num2_set - num1_set',num2_set - num1_set)
    
def question_03():
    '''num_dict = {'a':13,'b':22,'c':18,'d':24}
    按照dict中value从小到大的顺序排序'''
    num_dict = {'a':13,'b':22,'c':18,'d':24}
    print(dict(sorted(num_dict.items(),key=lambda num_list:num_list[1])))  #num_dict.items() = [('a', 13), ('b', 22), ('c', 18), ('d', 24)] lambda匿名函数获取value

def main():
    
    print('第一题作业：')
    question_01()
    
    print('第二题作业：')
    question_02()
    
    print('第三题作业：')
    question_03()
    
if __name__ == '__main__':
    main()