#!/usr/bin/env python
#coding:utf-8
"""
  Author:  程勇 --<>
  Purpose: 动脑学院第10节课作业
  Created: 2018/2/6
"""

import os
import random
import sys
import traceback  #可在记录到日志，来定位错误方便处理

'''自定义一个异常类,当list内元素长度超过10的时候抛出异常'''
class TestError(Exception):
    def __init__(self,msg):
        self.msg = msg
        
    def __str__(self):
        return self.msg

def question_01():
    a_list = [x for x in range(random.randint(5,20))]    
    try:
        if len(a_list) > 10:
            raise TestError('a_list的长度大于10')
    except TestError as e:
        print('第一题目的异常')
        print(e)
    else:
        print('没有异常，进行后续代码处理')
    finally:
        print('第一题目已做完')
    
'''思考如果对于多种不同的代码异常情况都要处理,又该如何去处理'''
#函数嵌套函数，可以在最外层函数捕获异常
def second_fun(s):
    return 10 / int(s)

def first_fun(s):
    return second_fun(s) * 2

def question_02():
    try:
        first_fun('0')
    except ZeroDivisionError as e:
        print('Error:', e)
    finally:
        print('第二题程序结束')    

'''try-except和try-finally有什么不同'''
def topic_three():
    return random.randint(10,50) / 0

def question_03():
    
    #try-except
    try:
        res = topic_three()
    except ZeroDivisionError as e:
        print('除法中除数为0，请修改')
        traceback.print_exc()
    
    #try-finally
    try:
        res = topic_three()
    except ZeroDivisionError as e:
        print('除法中除数为0，请修改')
        traceback.print_exc()
    finally:
        print('try-except和try-finally有什么不同?不管发不发生异常，都执行finall中的代码')

'''写函数，检查传入字典的每一个value的长度，如果大于2，
   那么仅仅保留前两个长度的内容，并将新内容返回给调用者'''
def get_two_value(value): 
    two_value = {}
    count = 0
    for i,v in value.items():
        if count >= 2:
            break
        two_value[i] = v
        count += 1
        
    return two_value
        
def check_dic(dic):
    
    #优先判断是否是字典
    if not isinstance(dic,dict):
        raise TypeError('输入的参数不是字典，程序终止')
    
    result = {}
    #判断长度，根据条件返回
    for index,value in dic.items():
        if len(value) <= 2:
            result[index] = value
        elif isinstance(value,(str,list,tuple)):
            result[index] = value[:2]
        elif isinstance(value,(dict,set)):
            result[index] = get_two_value(value)
            
    return result
            
def question_04():    
    try:
        dic = {"k1":"v1v1","k2":[11,22,33]}
        print(check_dic(dic))
    except Exception as e:
        traceback.print_exc()

def main():
    
    '''动脑学院第10节课作业'''
    
    '''第一题'''
    print('第一题')
    question_01()
    
    '''第二题'''
    print('\n第二题')
    question_02()
    
    '''第三题'''
    print('\n第三题')
    question_03()
    
    '''第四题'''
    print('\n第四题')
    question_04()
    
if __name__ == '__main__':
    main()