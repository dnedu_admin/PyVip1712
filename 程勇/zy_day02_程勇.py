#!/usr/bin/env python
#coding:utf-8
"""
  Author:  程勇 --<>
  Purpose: 动脑第二天作业
  Created: 2018/1/18
"""

import os
import re
from math import sqrt,ceil
from random import randint,uniform

def question_01():
    '''
    数字格式化输出
    输入数字，输出保留3位小数，输出占20字符，空位用0补齐的数字
    '''
    
    format_num = input('请输入要格式化的数字:')
    format_num.rstrip()
    
    if re.match(r'\d*\.?\d*',format_num):
        print("{ori_num}格式化后为{new_num:0>20.3f}".format(ori_num = format_num,new_num = float(format_num)))
    else:
        print('您输入的不是数字，不能按规定格式化')

def question_02():
    '''
    十进制数字 用二进制表示，八进制表示，16进制表示 
    '''
    
    radix_num = input('请输入要转化进制的数字:')
    radix_num.rstrip()
    
    if re.match(r'\d*',radix_num):
        radix_num = int(radix_num)
        print("{}二进制表示：{}，八进制表示:{},16进制表示:{}".format(radix_num,bin(radix_num),oct(radix_num),hex(radix_num)))
    else:
        print('您输入的不是数字，不能转化进制')

def question_03():
    '''
    生成一个100到300之间的随机整数n，求其平方根，产生一个1到20之间的随机浮点数p，求大于p 的最小整数
    '''
    
    flag = True
    while flag:
        between_num = input('随机整数的区间，以，分开（如100,300 取100到300之间随机整数）')
        if re.match(r'\d*,\d*',between_num):
            flag = False
        else:
            print('输入格式有误，重新输入')
            
    between_num = between_num.split(',')
    between_num.sort()
    rand_num = randint(int(between_num[0]),int(between_num[1]))
    print("区间{}到{}生成随机数{}其平方根，引用math解:{},**求解:{}".format(between_num[0],between_num[1],rand_num,sqrt(rand_num),rand_num**0.5))
    
    flag = True
    while flag:
        between_num = input('随机整数的区间，以，分开（如1,20 取1到20之间随机浮点数）')
        if re.match(r'\d*,\d*',between_num):
            flag = False
        else:
            print('输入格式有误，重新输入')
            
    between_num = between_num.split(',')
    between_num.sort()
    rand_float = uniform(int(between_num[0]),int(between_num[1]))
    print("区间{}到{}生成随机浮点数{}大于其最小整数{}".format(between_num[0],between_num[1],rand_float,ceil(rand_float)))

def question_04():
    '''
    运维系统中监控到磁盘信息，磁盘使用量为12106450611211bytes, 页面展示中需要展示单位为GB，保留2为小数
    1GB = 2**10 MB = 2**20 KB = 2*30 bytes
    '''
    
    disk_usage = input('请输入磁盘的使用量（bytes）:')
    disk_usage.rstrip()
    
    if re.match(r'\d*',disk_usage):
        disk_usage = int(disk_usage)
        print("磁盘使用量为{}bytes,其转成GB为{}GB".format(disk_usage,round(number=disk_usage/2**30,ndigits=2)))
    else:
        print('您输入的不是数字，计算不了其GB')

def question_05():
    '''
    使用input函数,记录键盘输入的内容,打印输出该值的类型, 并转换为数值型。 
    判断数值num大小,如果num>=90,输出打印:你的成绩为优秀    
    如果num>=80 and num<90,输出打印:你的成绩为良好
    如果num>=60 and num <80,输出打印:你的成绩为一般
    如果num<60,输出打印:你的成绩为不合格
    '''
    sorce_num = input('输入的分数：')
    print('该值类型:{}'.format(type(sorce_num)))
    if not re.match(r'\d*,\d*',sorce_num):
        print('输入带有字母，请重新输入')
    sorce_num = float(sorce_num)
    
    #分数对应的成绩
    if sorce_num >= 90:
        print('你的成绩为优秀')
    elif sorce_num >= 80 and sorce_num < 90:
        print('你的成绩为良好')
    elif sorce_num >= 60 and sorce_num < 80:
        print('你的成绩为一般')
    elif sorce_num < 60:
        print('你的成绩不合格')
    else:
        print('分数没有对应的评级')

def main():
    '''
    解答运行，输入对应题目标号，可以进行解答
    1:格式化显示浮点数
    2:十进制转八进制，二进制，十六进制
    3：随机整数，随机浮点数
    4:内存单位转换
    5:输入分数得到对应成绩
    '''
    handle_dic = {'1':question_01,'2':question_02,'3':question_03,'4':question_04,'5':question_05}
    
    while True:
        id = input('''
        动脑学院课程二作业
        输入对应标号，即可运行对应的题目
        1:格式化显示浮点数
        2:十进制转八进制，二进制，十六进制
        3:随机整数，随机浮点数
        4:内存单位转换
        5:输入分数得到对应成绩
        回车:退出程序
        ''')
        if not id in ['1','2','3','4','5','']:
            print('输入有误，重新输入')
            continue
        elif id == '':
            print('欢迎下次使用本程序')
            break;
        
        handle_dic[id]()

if __name__ == '__main__':
    main()