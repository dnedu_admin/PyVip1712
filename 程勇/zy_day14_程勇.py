#!/usr/bin/env python
#coding:utf-8
"""
  Author:  程勇 --<>
  Purpose: 动脑学院第14节课作业
  Created: 2018/2/27
"""

import os
import random
from functools import reduce

'''filter判断[0,False,True,5,{},(2,3)]对应bool值'''
def check_true(v):
    return v

def check_false(v):
    return not v

def question_01():
    check_data = [0,False,True,5,{},(2,3)]
    
    #删除false
    true_list = list(filter(check_true,check_data))
    print('列表中true类型:',true_list)
    
    #删除true
    false_list = list(filter(check_false,check_data))
    print('列表中false类型:',false_list)
    
'''map实现1-10中所有奇数项的三次方,打印输出结果'''
def odd_cubic(i):
    if i % 2 == 0:
        return i
    else:
        return i ** 3

def question_02():
    
    result = list(map(odd_cubic,range(1,11)))
    print('所有奇数项3次方:',result)
    
'''reduce实现1-100所有偶数项的和'''
def question_03():
    
    second_result = reduce(lambda x,y:x+y,list(filter(lambda x:x % 2 == 0,range(1,101))))
    print('1-100所有偶数项和为:',second_result)
    
'''用递归函数实现斐波拉契数列'''
def fib(n):
    if n <= 2:
        return 1
    else:
        return fib(n-1) + fib(n-2)

def question_04():
    
    flag = True
    while flag:
        
        n = input('请输入展示斐波那契数列前n项，n的值;无数据输入则结束程序').strip()
        
        if n == '':
            break
        elif not n.isdigit() or int(n) <= 0:
            print('请输入大于0的正整数')
            continue
        
        print('斐波那契前{}项为{}'.format(n,list(map(fib,range(1,int(n)+1)))))

def main():
    #第一题
    print('第一题作业:')
    question_01()
    
    #第二题
    print('第二题作业')
    question_02()
    
    #第三题
    print('第三题作业')
    question_03()
    
    #第四题
    print('第四题作业')
    question_04()
    
if __name__ == '__main__':
    main()