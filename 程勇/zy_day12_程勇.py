#!/usr/bin/env python
#coding:utf-8
"""
  Author:  cocoforever --<>
  Purpose: 动脑学院第12节课作业
  Created: 2018/2/10
"""

import os

'''自己动手写一个学生类,创建属性名字,年龄,性别,完成
     __new__,__init__,__str__,__del__，__call__'''
class MyStudent():
    def __new__(cls,*args,**kwargs):
        print('开始创建一位学生')
        return super().__new__(cls)
        
    def __init__(self,name,age,sex):
        self.name = name
        self.age = age
        self.sex = sex
        print('创建了一位学生')
    
    def __str__(self):
        return "__str__方法该学生姓名：{}，年龄：{},性别：{}".format(self.name,self.age,self.sex)
    
    def __call__(self):
        print("__call__方法该学生姓名：{}，年龄：{},性别：{}".format(self.name,self.age,self.sex))
    
    def __del__(self):
        print('__del__学生：{},年龄：{},性别:{}'.format(self.name,self.age,self.sex))
        
'''创建父类Person,属性name,函数eat,run,创建子类Student,学生类
  继承Person,属性name,age,id,函数eat,study,全局定义一个函数
  get_name,传入一个Person类的实例参数,分别传入Person和Student
  两个类的实例对象进行调用.'''
class Person():
    def __init__(self,name):
        self.name = name
        
    def eat(self):
        print('父类{}在家里吃饭'.format(self.name))
    
    def run(self):
        print('父类{}在公园跑步'.format(self.name))
        
class Student(Person):
    def __init__(self,name,age,id):
        super(Student,self).__init__(name)
        self.age = age
        self.id = id
    
    def eat(self):
        print('子类{}在食堂吃饭'.format(self.name))
    
    def study(self):
        print('子类{}在教室上课'.format(self.name))
        
def get_name(person):
    person.eat()
    person.run()
        
'''3.用代码完成python多态例子和鸭子模型'''  
class Animal():
    def __init__(self,name):
        self.name = name
        
    def cry(self):
        print('{}在叫'.format(self.name))
        
class Cat(Animal):
    def __init__(self,name,age):
        super(Cat,self).__init__(name)
        self.age = age
        
    def cry(self):
        print('{}的叫声为：喵喵喵'.format(self.name))
        
    def info(self):
        print('{}的年龄为{}'.format(self.name,self.age))
        
def animal_show(animal):
    animal.cry()
    
class Car():
    @staticmethod
    def run():
        print('车在路上跑')
        
    def info(self):
        print('这是四轮车')
        
class Bike():
    @staticmethod
    def run():
        print('自行车在路上跑')
    
    def info(self):
        print('这是自行车')

def show(car):
    car.run()
    car.info()
    
def main():
    
    '''第一题'''
    print('第一题:')
    s1 = MyStudent('coco',18,'男')
    print(s1)
    s1()
    
    '''第二题'''
    print('第二题')
    p1 = Person('路人甲')
    s2 = Student('学生甲',17,'女')
    get_name(p1)
    get_name(s2)
    
    '''第三题'''
    print('第三题')
    a1 = Animal('动物甲')
    c1 = Cat('猫甲',3)
    animal_show(a1)
    animal_show(c1)
    
    c2 = Car()
    b1 = Bike()
    show(c2)
    show(b1)
    
if __name__ == '__main__':
    main()