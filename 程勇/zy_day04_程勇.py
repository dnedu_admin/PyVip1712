#!/usr/bin/env python
#coding:utf-8
"""
  Author:  cocoforever --<>
  Purpose: 动脑学院第四天作业
  Created: 2018/1/23
"""

import os
from operator import eq

def question_01():
    '''
    num_list = [[1,2],[‘tom’,’jim’],(3,4),[‘ben’]]   
    1. 在’ben’后面添加’kity’
    2. 获取包含’ben’的list的元素
    3. 把’jim’修改为’lucy’
    4. 尝试修改3为5,看看
    5. 把[6,7]添加到[‘tom’,’jim’]中作为第三个元素
    6.把num_list切片操作:
    num_list[-1::-1]
    '''  
    
    #在’ben’后面添加’kity’
    num_list = [[1,2],['tom','jim'],(3,4),['ben']]
    num_list[3].append('kity')
    print('在’ben’后面添加’kity’后list:',num_list,'\n')
    
    #获取包含’ben’的list的元素
    num_list = [[1,2],['tom','jim'],(3,4),['ben']]
    for find_ben in num_list:
        if isinstance(find_ben,list) and 'ben' in find_ben:
            print('含有"ben"的list元素',find_ben,'\n')
    
    #把’jim’修改为’lucy’
    num_list = [[1,2],['tom','jim'],(3,4),['ben']]
    for k in range(len(num_list)):
        if 'jim' in num_list[k]:
            if isinstance(num_list[k],tuple):
                print('该元素为tuple，不可修改\n')
            else:
                num_list[k][num_list[k].index('jim')] = 'lucy'
    print('把’jim’修改为’lucy’后的list为:',num_list,'\n')
    
    #尝试修改3为5,看看
    num_list = [[1,2],['tom','jim'],(3,4),['ben']]
    for k in range(len(num_list)):
        if 3 in num_list[k]:
            if isinstance(num_list[k],tuple):
                try:
                    num_list[k][num_list[k].index(3)] = 5
                except TypeError as e:
                    print(e,'\n')
            else:
                num_list[k][num_list[k].index(3)] = 5
                
    #把[6,7]添加到[‘tom’,’jim’]中作为第三个元素
    num_list = [[1,2],['tom','jim'],(3,4),['ben']]
    add_list = [6,7]
    find_list = ['tom','jim']
    for k in range(len(num_list)):
        if eq(find_list,num_list[k]):
            num_list[k].append(add_list)
    print('把[6,7]添加到[‘tom’,’jim’]中作为第三个元素后list:',num_list,'\n')
    
    #把num_list切片操作:num_list[-1::-1]
    num_list = [[1,2],['tom','jim'],(3,4),['ben']]
    print('切片操作后list为:',num_list[-1::-1],'\n')
    
def question_02():
    '''
    numbers = [1,3,5,7,8,25,4,20,29];
    1.对list所有的元素按从小到大的顺序排序
    2.求list所有元素之和
    3.将所有元素倒序排列
    '''
    
    #对list所有的元素按从小到大的顺序排序
    numbers = [1,3,5,7,8,25,4,20,29]
    numbers.sort()
    print('list.sort排序:',numbers,'\n')
    
    numbers = [1,3,5,7,8,25,4,20,29]
    print('sorted排序:',sorted(numbers),'\n')
    
    #求list所有元素之和
    numbers = [1,3,5,7,8,25,4,20,29]
    print('所有元素之和:',sum(numbers),'\n')
    
    #先判断list里的数据类型，只有整数，浮点型可以求和 1.求列表中数字的和，其他类型跳过 2.列表有其他类型，返回错误信息
    sum_number = 0
    for v in numbers:
        if isinstance(v,(int,float)):
            sum_number += v
        #else:
            #print('列表中有非数字里类型，不能计算总和')
            #break
    print('该列表所有元素之和:',sum_number,'\n')
    
    #将所有元素倒序排列
    numbers = [1,3,5,7,8,25,4,20,29]
    numbers.sort(reverse=True)
    print('list.sort排序:',numbers,'\n')
    
    numbers = [1,3,5,7,8,25,4,20,29]
    print('sorted排序:',sorted(numbers,reverse=True),'\n')
    
def main():
    print('动脑学院第四天作业的答案:')
    
    print('第一题：')
    question_01()
    
    print('\n\n第二题:')
    question_02()
    
if __name__ == '__main__':
    main()
    