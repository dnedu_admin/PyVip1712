# coding=utf-8
# author:dn_eric(qq:2384363842)
# 动脑学院pythonVip课程
# 创建于: 2018/1/21 17:49

# 1. 格式化显示浮点数12.78，要求保留3位小数，输出占20字符，空位用0补齐
# f = 12.78
# str_f = "%020.3f" %(f)
# print(str_f)
# # str_format = "{:020.3f}".format(f)
# # print(str_format)

# 2. 将字符串"动脑学院"转换为bytes类型。
# dn = "动脑学院"
# dn_bytes = dn.encode('utf-8')
# print(dn_bytes)


# 3.将以下3个字符串合并为一个字符串，字符串之间用3个_分割
# l = ["hello", "动脑", "pythonvip"]
# strs = '___'.join(l)
# print(strs)

# 4. 删除字符串  "    你好，动脑eric     "首尾的空格符号
# str1 = "    你好，动脑eric     "
# str1_nospace = str1.strip(' ')
# print(str1_nospace)

"""
5. 用户信息存在如下所示字符串中，包含信息依次为  姓名   学号  年龄，
不同信息之间的空格数不确定，请提取用户信息分别保存到 xxx_name,xxx_num, xxx_age。
eric="    Eric   dnpy_001     28"        
提取后 eric_name="Eric" eric_num="dnpy_001",eric_age="28"
zhangsan = " 张三         dnpy_100    22     "
"""
# eric = "    Eric   dnpy_001     28"
# import re
#
# no_lrspace = eric.strip(' ')
# ret = re.split("\s+", no_lrspace)
# eric_name, eric_num, eric_age = ret
# print(eric_name, eric_num, eric_age)
#
# zhangsan = " 张三         dnpy_100    22     "
# zs_name, zs_num, zs_age = re.split('\s+', zhangsan.strip(' '))
# print(zs_name, zs_num, zs_age)
