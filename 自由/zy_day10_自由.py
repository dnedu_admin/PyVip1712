# coding=utf-8

class listerror(Exception):
    def __str__(self):
        return '出现listerror,list内的元素长度超过10了'



def list_test(nlist):
    try:
        if len(nlist) >10 :
            raise listerror()

    except Exception as e:
        print(e)

    else:
        print('list内的元素长度不超过10')

def mucherror():
    try:
        a = int(input('请输入数字'))
        c = 1/a

    except ValueError as e :
        print(e)
    except ZeroDivisionError as e :
        print(e)
    except OverflowError as e:
        print(e)
    except Exception as e :
        print(e)

def moderror1(num):
    try:
        print(100/num)

    except:
        print('除数不可以为0')


def moderror2(num):
    try:
        print(100 / num)

    finally:
        print('输入的除数为0，也会执行finally模块里面的语句')


def check_len(ndict):
    for k,v in ndict.items():

        if isinstance(v,(int,float,bool,complex)):
            pass

        elif isinstance(v,dict):
            if len(v.values()) > 2:
                ndict[k] = list(v.values())[:2]

        else:
            if len(v) > 2:
                ndict[k] = v[:2]

    return ndict

list_test(list(range(1,20)))

mucherror()

moderror1(2)
moderror1(0)
moderror2(2)
moderror2(0)

dic = {"k1": "v1v1","k2":253,'k3':[11,22,33]}
# dic = {"k1": "v1v1","k2":{'name':'3256','sdw':652,'efsd':8933}}
dic = check_len(dic)
print(dic)