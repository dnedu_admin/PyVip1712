# _*_ coding:UTF-8 _*_


class A():
    def test(self):
        print("我是初始之人A")

class B():
    def test(self):
        print("我是初始之人B")

class C(A,B):
    pass

class D(A,B):
    def test(self):
        print("我是继承之人D")

class E(C,D):
    pass

test = E()
test.test()         #py2 打印 A.test()     py3 打印 D.test()


class MyClass():
    __slots__ = ('id','name')
    pass


m = MyClass()
m.age = 26
print(m.age)


class game():
    @property
    def num(self):
        return self.size

    @num.setter
    def num(self,size):
        if not isinstance(size,int):
            raise TypeError('请输入整数，谢谢')
        if size < 0 or size > 4869 :
            raise TypeError("数值过大，产生溢出")

        self.size = size


    @num.deleter
    def num(self):
        del self.size

g = game()
g.num = 586
print(g.size)
del g.size
print(g.size)

def run(self):
    print("开始测试")

def sleep(self):
    print("学习完毕,可以休息了")

guess = type('freeze',(),{'age':63,'name':'jack','sex':'male','run':run,'sleep':sleep})
gu = guess()
print(gu.age,gu.name,gu.sex)
gu.run()
gu.sleep()