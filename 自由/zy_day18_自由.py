# _*_ coding:UTF-8 _*_

import  datetime,time
# from time import * #多线程代码跟其他代码一起运行，找不到sleep(n),若不想报错，改成这么导入time模块
#把time.sleep(n),改成sleep(n)

from threading  import Thread,current_thread

from multiprocessing import  Process,Pool

def add():
    s = sum(range(1, 100000))
    print(s)
    time.sleep(2)
    # sleep(2)
    print('test')

if __name__ == '__main__':
    starttime = datetime.datetime.now()
    for i in range(10):
        a = sum(range(1, 100000))
    endtime = datetime.datetime.now()
    time = (endtime - starttime).total_seconds()
    print("for 循环重复10次，总耗费时间为%s" % (time))

    starttime_t = datetime.datetime.now()
    t_list = [Thread(target=add) for i in range(10)]
    for t in t_list:
        t.start()
        # t.join()
    t_list[-1].join()

    endtime_t = datetime.datetime.now()
    time_t = (endtime_t - starttime_t).total_seconds()
    print("10个子线程所耗费的时间为%s"%(time_t))

    starttime_mul = datetime.datetime.now()
    process_l = [Process(target=add) for i in range(10)]
    for p in process_l:
        p.start()
        # p.join()
    process_l[-1].join()
    endtime_mul = datetime.datetime.now()
    time_mul = (endtime_mul - starttime_mul).total_seconds()
    print("10个进程所耗费的时间为%s" % (time_mul))


    n = 5
    starttime_pmul = datetime.datetime.now()
    pool = Pool(n)
    for i in range(10):
        pool.apply_async(add,)
    pool.close()
    pool.join()
    endtime_pmul = datetime.datetime.now()
    time_pmul = (endtime_pmul - starttime_pmul).total_seconds()
    print("线程池线程个数为%d,所耗费的时间为%s" % (n,time_pmul))




