# _*_ coding:UTF-8 _*_


#1
li=[6,1,2,3,4,5,1,2,3,6]
new_li=list(set(li))
print(new_li)
new_li.sort(key=li.index)
print(new_li)


#2
import re
p = re.compile('(blue|white|black)')
print(p.subn('freedom','blue hair and black jacket'))

#3
a,b= 1,2
print(a,b)
a,b=b,a
print(a,b)

#4
strli = 'abcdef'
def str_sort1(strs):
    new_str = "".join(sorted(strs,reverse = True))
    return  new_str

def str_sort2(strs):
    new_str = "".join(strs[::-1])
    return  new_str


def str_sort3(strs,new_l=[]):
    if len(strs):

        new_l.append(strs.pop())
        str_sort3(strs)
    return ''.join(new_l)

import collections
def str_sort4(strs):
    d = collections.deque()
    d.extend(list(strli))
    d.reverse()
    return ''.join(d)

def str_sort5(strs):
    strs = list(strs)
    new_l2 = [strs.pop() for i in range(len(strs))]
    return  ''.join(new_l2)

print(str_sort1(strli))
print(str_sort2(strli))
print(str_sort3(list(strli)))
print(str_sort4(strli))
print(str_sort5(strli))



#5

li2 = [i for i in range(1,20)]
li3 = [i for i in range(1,20,2)]
li2[0] = 100
li3[0] = 3662
def combine_list(l2,l3):
    li = list(l2+l3)
    new_li = list(set(li))

    return new_li

print(combine_list(li2,li3))