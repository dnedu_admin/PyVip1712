# coding=utf-8

from functools import  reduce
a = 999999
def work(a):
    def inner(func):
        def middle(n):
            print("1、使用装饰器普通方法，增强完成1-{}所有奇数和计算10次".format(a))
            for i in range(10):
                print(func(n))

        return middle

    return inner


@work(a)
def odd(n):
    s = reduce(lambda x,y:x+y,[i for i in range(1,n+1,2)])
    return s




from multiprocessing import Process,Pool,cpu_count

def work2(func):
    def inner(n):
        func(n)

    return inner

def odd2(n):
    s = reduce(lambda x,y:x+y,[i for i in range(1,n+1,2)])
    print(s)

@work2
def mul(n):
    print("2、使用装饰器多进程方法，增强完成1-{}所有奇数和计算10次".format(n))
    p_list = [Process(target=odd2, args=(n,)) for i in range(10)]
    for p in p_list:
        p.start()
    p_list[-1].join()

@work2
def mulp(n):
    print("3、使用装饰器进程池方法，增强完成1-{}所有奇数和计算10次".format(n))
    pool = Pool(cpu_count())
    for i in range(10):
        pool.apply_async(odd2,(n,))
    pool.close()
    pool.join()

from threading import Thread
from multiprocessing.dummy import Pool as Threadpool

@work2
def thre(n):
    print("4、使用装饰器多线程方法，增强完成1-{}所有奇数和计算10次".format(n))
    t_list = [Thread(target=odd2,args=(n,)) for i in range(10)]
    for t in t_list:
        t.start()
    t_list[-1].join()

@work2
def threadp(n):
    print("5、使用装饰器线程池方法，增强完成1-{}所有奇数和计算10次".format(n))
    threadpool = Threadpool(cpu_count())
    for i in range(10):
        threadpool.apply_async(odd2,(n,))
    threadpool.close()
    threadpool.join()


if __name__ == "__main__":
    n= 999999
    odd(n)
    mul(n)
    mulp(n)
    thre(n)
    threadp(n)



