# _*_ coding:UTF-8 _*_

#one
print(list(range(3,7)))
print(list(range(3,20,3)))
print(list(range(-20,900,220)))

#two
from functools import reduce
numsum = reduce(lambda x,y: x + y,(i for i in range(1,101) if i % 2 ==0))
print(numsum)

#three
def Odd_cube(num):

    for i in range(1, num):
        if i % 2 == 1:
            yield i ** 3

numlist = []
for i in Odd_cube(10):
    numlist.append(i)

print(numlist)

#four

class free_iter():
    def __init__(self, money):
        self.money = money


    def __next__(self):
        if self.money == 0:
            raise StopIteration('大哥真的没钱了')
        self.money -= 1
        return self.money

    def __iter__(self):
        return self

f_iter = free_iter(8)
for x in f_iter:
    print(x)




