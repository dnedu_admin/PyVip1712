# coding=utf-8

def question_one():
	print('第一题测试结果')
	name_dict = {'name': 'ben', 'age': 22, 'sex': '男'}
	# add value
	name_dict['name2'] = 'mike'
	name_dict.update(name3='john')
	name_dict.setdefault('name4', 'freedom')
	print(name_dict)

	# del
	print("del")
	name2_dict = name_dict.copy()
	del name2_dict['name3']
	print(name2_dict)
	name2_dict.pop('name4')
	print(name2_dict)
	name2_dict.popitem()
	print(name2_dict)
	del name2_dict
	try:
		print(name2_dict)

	except:
		print('name2_dict 整个字典删除了')

	# update
	print("update")
	name_dict.update(name='liberty')
	name_dict['age'] = 25
	print(name_dict)

	# clear
	print('clear')
	name_dict.clear()
	print(name_dict)


def question_two():
	print('\n第二题测试结果')
	num1_set = {3, 5, 1, 2, 7}
	num2_set = {1, 2, 3, 11}
	print('交集',num1_set & num2_set,'\n并集',num1_set | num2_set,'\n对称差集',\
		  num1_set ^ num2_set,'\n差集',num1_set - num2_set)

def question_three():
	print('\n第三题测试结果')
	num_dict = {'a': 13, 'b': 22, 'c': 18, 'd': 24}
	num2_dict = dict(sorted(num_dict.items(),key = lambda d:d[1]))
	print(num_dict)
	print(num2_dict)

def main():
	question_one()
	question_two()
	question_three()

if __name__ == '__main__':
	main()
