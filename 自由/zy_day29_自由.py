# _*_ coding:UTF-8 _*_


from functools import lru_cache
import time



@lru_cache(maxsize=12,typed=False)
def numjc(n):
    if n <2:
        return n
    
    else:
        return n*numjc(n-1)

def game(n):
    t1 = time.time()
    [numjc(i) for i in range(1,n)]
    t2 = time.time()
    print('运行消耗时间为{}'.format(t2-t1))

def game2(n):
    t1 = time.time()
    [numjc(i) for i in range(1,n)]
    t2 = time.time()





from line_profiler import LineProfiler

def lprof():
    Lprof =  LineProfiler(game2)
    Lprof.enable()
    game2(1000)
    Lprof.disable()
    Lprof.print_stats()

from memory_profiler import profile
@profile
def run():
    x = [i for i in range(3200,2)]
    y = [t for t in range(1,3200,2)]
    del x
    del y

if __name__ == '__main__':
    game(3000)
    lprof()
    run()