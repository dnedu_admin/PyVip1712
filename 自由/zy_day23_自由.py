# _*_ coding:UTF-8 _*_
from datetime import datetime,timedelta

t1 = datetime.now()
t2 = t1 - timedelta(days=3)
print(t2.timestamp())
t3 = t1.timestamp()+899999
t3 = datetime.fromtimestamp(t3)
print(t3.strftime('%Y-%m-%d %H:%M:%S'))
a = input("请输入日期和时间：y-m-d h:m:s")
a = datetime.strptime(a,'%Y-%m-%d %H:%M:%S')
print(a.date())
print(a.time())
print("当前日期时间对象是星期%s"%a.isoweekday())

import os
from os import path
absp = path.abspath(__file__)
path_dirf = os.listdir(r'.')
print(absp)
print(path_dirf)
path_tuple = path.split(absp)
print(path_tuple)
if not path.exists(r'.\game.py'):
    with open(r'.\game.py','w+',encoding='utf-8') as f:
        f.write("创建成功")

if not path.exists(r'./wow/temp/'):
    os.makedirs(r'./wow/temp/')
if path.exists(r'./wow/map/'):
    os.removedirs(r'./wow/map/')
os.renames(r'./wow/temp/',r'./wow/map/')

def odd(listtu):
    new_list = list([listtu[i] for i in range(1,len(listtu),2)])
    print(new_list)

odd([2,66,88,3612,41641,34586,6516])