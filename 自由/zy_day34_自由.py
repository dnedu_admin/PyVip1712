# _*_ coding:UTF-8 _*_

import requests,os
headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.278'
                  '5.104 Safari/537.36 Core/1.53.1708.400 QQBrowser/9.5.9635.400',
    "Referer": "http://www.mm131.com/mingxing"
}
def imgdownloader(img_href,headers,i,dir):
    imgs = requests.get(img_href, headers=headers)
    imgs.encoding = 'gbk'
    imgtree = html.fromstring(imgs.text)
    img = imgtree.xpath("//div[@class='content-pic']//img/@src")[0]
    with open('./img/{0}/{1}.jpg'.format(dir,i), 'wb') as f:
        f.write(requests.get(img,headers=headers).content)
    i += 1
    next_url = imgtree.xpath("//div[@class='content-page']/a[@class='page-ch'][last()]/@href")
    next_text = str(imgtree.xpath("//div[@class='content-page']/a[@class='page-ch'][last()]/text()")[0])
    if next_text == "下一页" :
        imgdownloader(parse.urljoin(imgs.url,next_url[0]),headers,i,dir)

url = 'http://www.mm131.com/mingxing'

from urllib import parse
from lxml import html
def imgspider(url,headers):
    response = requests.get(url, headers=headers)
    response.encoding = 'gbk'
    tree = html.fromstring(response.text)
    datas = tree.xpath("//dl[@class='list-left public-box']/dd")
    nexts = datas.pop()
    next_url = nexts.xpath("./a[last()-1]/@href")
    next_text = str(nexts.xpath("./a[last()-1]/text()")[0])
    for data in datas:
        filedir = data.xpath("./a//@alt")[0]

        if not os.path.exists('./img/%s'%filedir):
            os.makedirs('./img/%s'%filedir)
        img_href = data.xpath("./a/@href")[0]
        i = 1
        imgdownloader(img_href, headers,i,filedir)

    if next_text == "下一页":
        imgspider(parse.urljoin(response.url,next_url[0]),headers)

imgspider(url,headers)

