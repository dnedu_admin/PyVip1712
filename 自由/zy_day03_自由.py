#coding=utf-8

def question_one():
	a = 12.78
	print('%020.3f' % a)


def question_two():
	name = "动脑学院"
	byte_name = name.encode("utf-8")
	print(byte_name, '\n', type(byte_name))


def question_three():
	a = ['hello', '动脑', 'pythonvip']
	b = '___'.join(a)
	print(b)


def question_four():
	a = '    你好，动脑eric     '
	b = a.strip()
	print(b)


def question_five():
	zhangsan = ' 张三         dnpy_100    22     '
	a = zhangsan.split()
	zhangsan_age = a.pop()
	zhangsan_num = a.pop()
	zhangsan_name = a.pop()
	print('name:%s\nnum:%s\nage:%s' % (zhangsan_name, zhangsan_num, zhangsan_age))


def main():
	question_one()
	question_two()
	question_three()
	question_four()
	question_five()


if __name__ == '__main__':
	main()
