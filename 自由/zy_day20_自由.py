# _*_ coding:UTF-8 _*_
from types import coroutine

async def odd():
    for o in range(1,100,2):
        print(o)
        await num_next()


async def even():
    for e in range(2,101, 2):
        print(e)
        await num_next()

@coroutine
def num_next():
    yield


def run(n_list):
    num_list = list(n_list)
    while num_list:
        for i in num_list:
            try:
                i.send(None)
                print('数据交替打印中')

            except StopIteration as e:
                print(e)
                num_list.remove(i)

if __name__ == '__main__':

    num_list = []
    num_list.extend([odd(),even()])
    run(num_list)
    print('数据交替打印完成')




