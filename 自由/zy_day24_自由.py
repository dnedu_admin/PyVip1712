# _*_ coding:UTF-8 _*_
import  requests
from bs4 import BeautifulSoup as BS
import re,os
from urllib.request import urlretrieve
from os import path




def img_spider(url,headers):
    html = requests.get(url,headers = headers)
    print(html.encoding)
    html_str = html.text
    soup = BS(html_str,'lxml')
    img_urls = soup.select('.l_post.l_post_bright.j_l_post.clearfix cc img[src$=".jpg"]')
    global num
    for img in img_urls:
        p = re.compile('src="(.*?\.jpg)"')
        img = p.search(str(img)).group(1)
        if not path.exists(r'./image/'):
            os.makedirs(r'./image/')
        urlretrieve(img,'./image/'+'%s.jpg'%num)
        num +=1



headers = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.104 Safari/537.36 Core/1.53.1708.400 QQBrowser/9.5.9635.400'
}
next_urls=[]
num = 1
for i in range(1,4):
    next_url = 'https://tieba.baidu.com/p/2460150866?pn=%d' %i
    next_urls.append(next_url)


for url in next_urls:
    img_spider(url,headers)