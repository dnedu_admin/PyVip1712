# coding=utf-8


#1.简述普通参数、默认参数、关键字参数、动态收集参数的区别,理解为主
# python定义的函数中，参数有必选参数、默认参数、可变参数（动态收集参数）、关键字参数和命名关键字参数，这5种
#位置参数，调用函数时根据函数定义的参数位置来传递参数。
#默认参数，用于定义函数，为参数提供默认值，调用函数时可传可不传该默认参数的值
#关键字参数，允许你传入0个或任意个含参数名的参数，这些关键字参数在函数内部自动组装为一个dict
#可变参数（动态收集参数），允许你传入多个参数，函数内部会自动用一个tuple接收
'''
命名关键字参数，相对于关键字参数，它限制关键字参数的名字。相对于默认参数，命名关键字参数必须传入参数名，如
果没有传入参数名，调用将报错；用特殊分隔符*，来区分默认参数和命名关键字参数；命名关键字参数可以有默认值。
'''
#最后总结下
'''
这些参数可以组合使用，除了可变参数无法和命名关键字参数混合
参数定义的顺序必须是：必选参数、默认参数、动态收集参数/命名关键字参数和关键字参数。
默认参数一定要用不可变对象，如果是可变对象，程序运行时会有逻辑错误！
要注意定义可变参数和关键字参数的语法：
*args是可变参数，args接收的是一个tuple；
**kw是关键字参数，kw接收的是一个dict。

以及调用函数时如何传入可变参数和关键字参数的语法：
可变参数既可以直接传入：func(1, 2, 3)，又可以先组装list或tuple，再通过*args传入：func(*(1, 2, 3))；
关键字参数既可以直接传入：func(a=1, b=2)，又可以先组装dict，再通过**kw传入：func(**{'a': 1, 'b': 2})。

使用*args和**kw是Python的习惯写法，当然也可以用其他参数名，但最好使用习惯用法。
命名的关键字参数是为了限制调用者可以传入的参数名，同时可以提供默认值。
定义命名的关键字参数不要忘了写分隔符*，否则定义的将是位置参数。
'''


#题目二
def foo(x, y, z, *args, **kw):
    sum = x + y + z
    print(sum)
    for i in args:
        print(i)
    print(kw)
    for j in kw.items():
        print(j)
a_tuple = (1,2,3)     #会报错，参数写少了
b_dict = {'name': 'jim', 'age': 28, 'add': '上海'}
foo(*a_tuple, **b_dict)   #a_tuple的元素，会按顺序赋值给x,y,z；b_dict的元素会在函数内部自动组装为一个dict
			  #由于a_tuple只有三个元素，赋值给三个必选参数后,已经没了，所以args为空，b_dict赋值给kw



#题目三
def func(a, b, c=9, *args, **kw):
	print('a =', a, 'b =', b, 'c =', c, 'args =', args, 'kw =', kw)

def  get_sum(*args,**kw):
	print('args =', args, 'kw =', kw)


func(1, 2)           #a = 1 b = 2 c = 9 args = () kw = {}
func(1, 2, 3)        #a = 1 b = 2 c = 3 args = () kw = {}
func(1, 2, 3, 4)     #a = 1 b = 2 c = 3 args = (4,) kw = {}
func(1, 2, 3, 4, 5)  #a = 1 b = 2 c = 3 args = (4, 5) kw = {}
func(1, 2, 3, 4, 5, 6, name='jim')           #a = 1 b = 2 c = 3 args = (4, 5, 6) kw = {'name': 'jim'}
func(1, 2, 3, 4, 5, 6, name='tom', age=22)   #a = 1 b = 2 c = 3 args = (4, 5, 6) kw = {'name': 'tom', 'age': 22}
# def  get_sum(*args,**kw):pass
#函数get_sum（）可以接受任意个参数


def count_string(nchar):
	count_digital = 0
	count_alpha = 0
	count_space = 0
	count_other = 0
	for i in nchar:
		if i.isdigit() == True:
			count_digital += 1
		elif i.isalpha() == True:
			count_alpha += 1
		elif i.isspace() == True:
			count_space += 1
		else:
			count_other += 1

	print("该字符串数字有%d个"%count_digital)
	print("字母有%d个"%count_alpha)
	print("空格有%d个"%count_space)
	print("其他字符有%d个"%count_other)

count_string(input("请输入字符串"))
