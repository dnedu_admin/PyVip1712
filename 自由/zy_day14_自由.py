# _*_ coding:UTF-8 _*_
from functools import reduce

def bool_value(num):
    return bool(num)


test_fi = filter(bool_value,[0,False,True,5,{},(2,3)])
print(list(test_fi))

a = map(lambda x : x**3,[x for x in range(1,11) if x%2 == 1])
print(list(a))

b = reduce(lambda x,y: x+y ,[x for x in range(1,101) if x%2 == 1])
print(b)


def fbsl(num):
    if num == 0:
        return 0
    if num == 1:
        return 1

    b = fbsl(num-2) + fbsl(num-1)
    return b


n=28
flist = [fbsl(i) for i in range(1,n+1)]
print(flist)



