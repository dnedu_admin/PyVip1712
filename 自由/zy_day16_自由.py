# _*_ coding:UTF-8 _*_

from functools import reduce

def odd_file(func):
    def opfile():

        with open('freedom.txt','w',encoding='utf-8')  as f:
            d = func()
            f.write(str(d))
    return opfile

@odd_file
def odd():
    sum = reduce(lambda x,y:x+y,[x for x in range(1,100,2)])
    return sum



def test(func):
    def num_cube(*args,**kwargs):
        s = func(*args,**kwargs)
        return s

    return num_cube

@test
def cube(n):
    return n**3


odd()
print(cube(367))