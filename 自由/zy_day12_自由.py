#coding=utf8

class student2():
    def __new__(cls, *args, **kwargs):
        print("我比初始化函数先运行")
        return super().__new__(cls)

    def __init__(self):
        print("创建实例对象的时候，我会进行初始化")
    def run(self):
        print("student2 is run,too")

    def __str__(self):
        print("打印对象时，我会运行")
        return "my name is studeng2"
    def __call__(self, *args, **kwargs):
        print("把实例对象当函数使用时，我会运行")

    def __del__(self):
        print("我是析构函数，当对象删除或者垃圾回收机制启动时，我就会运行")





class person():
    def __init__(self,name):
        self.name = name

    def eat(self):
        print("person吃饭了")

    def run(self):
        print("person is run")

class student(person):

    def __init__(self,name,age,id):
        self.name = name
        self.age = age
        self.id = id

    def eat(self):
        print("student吃饭了")

    def run(self):
        print("student is run")

def get_name(object):
    object.eat()


def show_time(object):
    object.run()


a = person("john")
b = student("mike",22,6536)
c = student2()
c()
print(c)
get_name(a)
get_name(b)

#多态例子
show_time(a)
show_time(b)

#鸭子例子
show_time(a)
show_time(c)


