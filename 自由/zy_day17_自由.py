# _*_ coding:UTF-8 _*_


import pickle
import json
#pickle序列化
a_dict = [1,2,3,[4,5,6]]
with open('serialize.txt','wb+') as f:
    pickle.dump(a_dict,f)
    f.seek(0,0)
    print(f.read())

#pickle反序列化
with open('serialize.txt','rb+') as f:
    b = pickle.load(f)
    print(type(b))
    print(b)

print('--------'*20)
#json序列化
a_dict = [1,2,3,[4,5,6]]
with open('serialize.txt','w+') as f:
    json.dump(a_dict,f)
    f.seek(0,0)
    print(f.read())

#json反序列化
with open('serialize.txt','r+') as f:
    b = json.load(f)
    print(type(b))
    print(b)

from copy import copy
e = [5,6,16,[2,8,3]]
f = list(e)
g = copy(e)
g[3][1] = 25
g[2] = 58
f[2] = 63
print(e,f,g)
print(e is f)


from copy import deepcopy
e = [5,6,16,[2,8,3]]
f = e
g = deepcopy(e)
g[3][1] = 25
print(e,f,g)


from io import  StringIO
S = StringIO("啊啊，要争取时间是可以───\n不过把那家伙打倒也没关系吧？\n你知道吗，这可是个死亡率超高的FLAG\n")
for i in S.readlines():
    print(i)

from io import  BytesIO
B = BytesIO()
B.write("……吉普莉尔，请你去对付伊纲。\n".encode('utf-8'))
B.write("……这样好吗？\n".encode('utf-8'))
B.write("如果说有人能和那家伙的性能正面对决，那就只有你了——请你尽可能争取时间。\n".encode('utf-8'))
B.write("要我争取时间是没问题——\n".encode('utf-8'))
B.write("不过把那家伙打倒也没关系吧。\n".encode('utf-8'))
B.write("当然如果你能打倒她，那是再好不过，不过我要吐个槽，那可是死亡flag哦？\n".encode('utf-8'))
B.write("什么……那我就普通地打倒她就好了\n".encode('utf-8'))
print(B.getvalue().decode('utf-8'))





