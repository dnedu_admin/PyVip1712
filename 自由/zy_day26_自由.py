# _*_ coding:UTF-8 _*_
import  hashlib,binascii,os


name = hashlib.pbkdf2_hmac('sha256',b'freedom','自由君'.encode('utf-8'),1000,dklen=64)
namepw = binascii.hexlify(name)
print(namepw)

text = ['明明都是我先来的……接吻也好，拥抱也好，还是喜欢上那家伙也好',
        '第一次，有了喜欢的人，还得到了一生的挚友，两份喜悦相互重叠，这'
        '双重的喜悦又带来了更多更多的喜悦，本应已经得到了梦幻一般的幸福'
        '时光，然而,为什么,会变成这样',
        '我即使是死了，被钉在棺材里了，也要用着腐朽的声音喊出：冬马小三!',
        '不行 忍住 不能笑 但是…… 泽村·斯宾塞·英梨梨 霞之丘诗羽 碍事的'
        '家伙都全部消失了 接下来 接下来，其他人不管怎样都会完完全全相信我,'
        '照这个样子，支配安艺同学不过是迟早的问题 计画通り 我会成为新世界'
        '的女主角 只有我能做到，其他人能做到吗？能做到这一步吗？接下来还能'
        '继续吗？是的，能创造新GALGAME的，只有我！',
        ]
with open('123.txt','w+') as f:
    f.writelines(text)

def get_md5(file):
    if not os.path.exists(file):
        print("文件不存在")
        return
    mymd5 = hashlib.md5()
    with open(file,'rb') as f:
        for i in f.readlines():
        #     i = f.read()
            mymd5.update(i)

    return mymd5.hexdigest()

print(get_md5('123.txt'))

print('*****'*10)

class Mymanger(object):
    def __init__(self,text):
        print("开始吧，我们的战斗")
        self.text = text

    def __enter__(self):
        print('show time')
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        print('我永远喜欢圣人惠')
        if exc_type:
            print(exc_val)
        else:
            print("战斗已经结束了")

    def work(self):
        for i in self.text:
            print(i)




from contextlib import contextmanager

class Mymanger2(object):
    def __init__(self,text):
        print("开始吧，我们的战斗")
        self.text = text

    def work(self):
        for i in self.text:
            print(i)

with Mymanger(text) as f:
    f.work()

print('*****'*10)

@contextmanager
def manger_context(text):
    print('新的后宫已经出现')
    dj = Mymanger2(text)
    yield dj
    print('怎么能停止开后宫')

with manger_context(text) as f :
    f.work()
text2 = text.copy()

print('*****'*10)

from colorama import Fore,Back,Style
print(Fore.LIGHTRED_EX + text2.pop())
print(Back.BLUE + text2.pop())
print(text2.pop())

print(Style.RESET_ALL)
print(text2.pop())

print(Fore.LIGHTYELLOW_EX+text[0],Fore.GREEN+text[1])