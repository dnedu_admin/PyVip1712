# coding:utf-8
def question1():
	print('第一题运行测试')
	num_list = [[1, 2], ['tom', 'jim'], (3, 4), ['ben']]
	num_list.append('kity')
	print(num_list)
	for i in num_list:
		if 'ben' in i:
			print(i)

	num_list[1][1] = 'lucy'
	print(num_list)
	try:
		num_list[2][0] = 5

	except:
		print('num_list[2][0]的值无法修改')

	num_list[1].append([6, 7])
	print(num_list)
	print(num_list[-1::-1])


def question2():
	print('第二题运行测试')
	numbers = [1, 3, 5, 7, 8, 25, 4, 20, 29]
	numbers.sort()
	print(numbers)
	print(sum(numbers))
	numbers.sort(reverse=True)
	print(numbers)

def main():
	question1()
	question2()


if __name__ == '__main__':
	main()
