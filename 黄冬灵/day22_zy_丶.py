# coding:utf-8
# coder:DongLing
import re
# 1.
# 使用正则表达式匹配电话号码：0713 - xxxxxx(湖南省座机号码)
# phone = '0713 - 334562'
# p = re.compile('0713\s-\s\d{6}')
# r = p.findall(phone)
# if r:
#     print(r)
# else:
#     print('no match')


# 2.
# 区号中可以包含()
# 或者 -, 而且是可选的, 就是说你写的正则表达式可以匹配
# 800 - 555 - 1212, 555 - 1212, (800)
# 555 - 1212
# text1 = '800 - 555 - 1212  (800)'
# text2 = '(800)'
# text3 = '555 - 1212'
# text4 = '4444 - 11aa'
# p = re.compile('(\d{3}\s\-\s\d{3}\s\-\s\d{4})|(\d{3}\s\-\s\d{4})|(\(\d{3}\))')
# r = p.search(text1)
# if r:
#     print(r.group())
# else:
#     print('no match')

# 3.
# 选作题:
# 实现一个爬虫代码
import urllib.request
#
# url = "http://www.baidu.com"
# page_info = urllib.request.urlopen(url).read()
# page_info = page_info.decode('utf-8')
# print(page_info)

