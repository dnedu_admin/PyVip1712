# coding:utf-8
# coder:DongLing

import matplotlib.pyplot as plt
import matplotlib.image as mpimg

# 1.文件名称:use.txt
#    创建该文件,路径由你自己指定,打开该文件,往里面写入
#    str_content = ['tom is boy','\n','you are cool','\n',
#                        '今天你怎么还不回来']
#    把数据写入到该文件中,注意操作的模式,关闭文件句柄后
#    再次打开该文件句柄,然后读取文件中内容,用with代码块
#    实现,然后打印输出信息到控制台,注意换行符


# str_content = ['tom is boy', '\n', 'you are cool', '\n',
#                '今天你怎么还不回来']
# file = open('use.text', 'w+')
# for i in str_content:
#     file.write(i)
# file.close()
#
# with open('use.text', 'r') as f:
#     print(f.read())


#  2.with创建一个文件homework.txt,尝试多种操作模式.进行写读操作,注意区别

"""
w模式:只有写的权限
r模式:只有读的权限
w+、r+:具有读写权限
a+模式:具有读写权限，在文件末尾添加内容
"""
# with open('homework.txt', 'w') as f:
#     f.write('哈哈哈哈哈!')
#
# with open('homework.txt', 'r') as f2:
#     print(f2.read())
#
# with open('homework.txt', 'a+') as f3:
#     f3.write('\n' + '呵呵呵呵呵呵呵呵')
#
# with open('homework.txt', 'r+') as f4:
#     print(f4.read())

#  3.理解文件句柄,对比read(),readline(),readlines()写一个小例子

"""
read:读取整个文件，并以字符串变量形式存在
readline:每次读取一行，并以字符串对象存在
readlines:返回的是一个字符串对象，保存当前行的内容
"""
# str_content = ['tom is boy', '\n', 'you are cool', '\n',
#                '今天你怎么还不回来']
# file = open('use.text', 'w+')
# for i in str_content:
#     file.write(i)
# file.close()
#
# file2 = open('use.text', 'r+')
# try:
#     # print(file2.read())
#     # print(file2.readline())
#     # print(file2.readlines())
# except Exception as e:
#     print(e)
# finally:
#     file2.close()

#  4.准备一张jpg图片,把图片中数据读书出来,并写入到文件my_img.jpg文件中,
#     操作完毕后尝试打开my_img.jpg文件看图片显示正不正常
lean = mpimg.imread('trophy.png')

plt.imshow(lean)
plt.axis('off')
plt.savefig('my_img.png')

