# coding:utf-8
# coder:DongLing
import os
from os import path
import uuid
import shutil
import base64

# 1.
# 在当前项目下创建目录abc, 并在abc目录下, 使用uuid产生唯一码并写入到文件
# uuid.py中, 并e拷贝整个目录abc, 新目录名为app, 并将app目录下文件拷贝, 新
# 文件名为: new.py文件
# def ope_dir():
#     if path.exists('./abc'):
#         shutil.rmtree('abc')
#         os.makedirs('./abc')
#     else:
#         os.makedirs('./abc')
#     # print(os.getcwd())
#     # os.chdir('./abc')
#     # print(os.getcwd())
#     with open('./abc/uuid.py', 'w+') as f:
#         u = uuid.uuid1()
#         f.write(str(u))
#
# ope_dir()

# 2.
# 创建本地文件’one.txt’, 在里写入部分数据(‘今天天气不错, 我们去动物园n玩吧’
# n从1 - 9), 使用b64encode对每一行数据编码, 然后写入到文件oen.txt中.每写
# 完一行之后写入换行符.
# 打开刚才创建的文件one.txt, 以读取多行的形式读取文件中的内容, 并把每一行的
# 内容通过b64decode对每一行数据解码, 解码之后每一行数据依然是二进制形式
# 所以, 继续对每一行数据通过utf - 8
# 的形式解码为可以普通字符串, 然后判断每一行
# 字符串是否包含’动物园6’的子字符串, 如果包含, 先输出换行符, 然后则打印输出
# 当前行的字符串, 最后创建一个文件,’two.txt’, 并把帅选之后的数据写入到这个新
# 文件中.

# with open('open.txt', 'wb+') as f:
#     for item in range(1, 10):
#         content = ("今天天气不错, 我们去动物园" + str(item) + "玩吧\n").encode('utf-8')
#         f.write(content)
#         with open('oen.txt', 'ab+') as f2:
#             r = base64.b64encode(content)
#             f2.write(r)
#             f2.write("\n".encode('utf-8'))

# with open('oen.txt','rb+') as f3:
#     content_list = f3.readlines()
#     for item in content_list:
#         r = base64.b64decode(item)
#         r_str = bytes(r).decode('utf-8')
#         des_str = '动物园6'
#         if des_str in r_str:
#             print("\n")
#             print(r_str)
#             with open('two.txt','w+') as f4:
#                 f4.write(r_str)


# 3.
# 准备一张.jpg图片, 比如: mm.jpg, 读取图片数据并通过b85encode加密之后写入到新文件mm.txt文件中,
# 然后读取mm.txt数据并解密之后然后写入到mmm.jpg文件中
# with open('trophy.png', 'rb+') as f:
#     content = base64.b85encode(f.read())
#     with open('trophy.txt', 'wb+') as f2:
#         f2.write(content)
#
# with open('trophy.txt', 'rb+') as f3:
#     result = base64.b85decode(f3.read())
#     print(result)
#     with open('mmm.jpg', 'wb+') as f4:
#         f4.write(result)


