# coding:utf-8
# coder:DongLing

"""
Keys()类提供了键盘上几乎所有按键的方法。前面了解到，send_keys()方法可以用来模拟键盘输入，
除此之外，我们还可以用它来输入键盘的按键，甚至是组合键，如Ctrl+A、Ctrl+C 等
"""

import time
from selenium import webdriver
# 引入 Keys 模块
from selenium.webdriver.common.keys import Keys

driver = webdriver.Chrome()
driver.get("https://www.baidu.com")

# 输入框输入内容
driver.find_element_by_id('kw').send_keys("Android")
time.sleep(3)
# 删除最后有个字符
driver.find_element_by_id("kw").send_keys(Keys.BACK_SPACE)
time.sleep(3)

# 输入"安卓"
driver.find_element_by_id("kw").send_keys("安卓")
time.sleep(3)

# ctrl+a 全选输入框内容
driver.find_element_by_id("kw").send_keys(Keys.CONTROL,"a")
time.sleep(3)

# ctrl+x 剪切输入框内容
driver.find_element_by_id("kw").send_keys(Keys.CONTROL,"x")
time.sleep(3)

# ctrl+v 粘贴内容到输入框
driver.find_element_by_id("kw").send_keys(Keys.CONTROL,"v")
# 通过回车键代替单击操作
driver.find_element_by_id("su").send_keys(Keys.ENTER)
time.sleep(3)

driver.quit()
