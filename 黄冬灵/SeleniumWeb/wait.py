# coding:utf-8
# coder:DongLing

"""
显示等待:
    使WebDriver等待某个条件成立时继续执行
    否则在达到最大时长时抛出超时异常(TimeOutException)
隐式等待:
    WebDriver提供了implicitly_wait()方法实现隐式等待，默认设置为0
"""
# from selenium import webdriver
# from selenium.webdriver.common.by import By
# from selenium.webdriver.support.ui import WebDriverWait
# from selenium.webdriver.support import expected_conditions as EC
#
# driver = webdriver.Chrome()
# driver.get("http://www.baidu.com")
# # WebDriverWait类是由WebDriver 提供的等待方法。在设置时间内，默认每隔一段时间检测一次当前页面元素是否存在
# # 如果超过设置时间，检测不到则抛出异常
# element = WebDriverWait(driver, 5, 0.5).until(EC.presence_of_element_located((By.ID, "kw")))
#
# """
# WebDriverWait() 一般由until()或until_not()方法配合使用，下面是until()和until_not()方法的说明.
# 1.until(method,message='') 调用该方法提供的驱动程序作为一个参数，直到返回值为True
# 2.until_not(method,message='') 调用该方法提供的驱动程序作为一个参数，直到返回False
# """
# element.send_key('selenium')
# driver.quit()

"""
implicitly_wait()默认参数的单位为秒。本例中设置等待时长为10秒
首先这10秒并非一个固定的等待时间，它并不影响脚本的执行速度。
其次，它并不针对页面上的某一元素进行等待。当脚本执行到某个元素定位时，如果元素可以定位，则继续执行;
如果元素定位不到，则它将以轮询的方式不断地判断元素是否被定位到。
假设在第6秒定位到了元素则继续执行，若直到超出设置时长(10)秒还没有定位到元素，则抛出异常
"""

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from time import ctime

driver = webdriver.Chrome()

# 设置隐式等待为10秒
driver.implicitly_wait(10)
driver.get("http://www.baidu.com")

try:
    print(ctime())
    driver.find_element_by_id("kw22").send_keys('selenium')
except NoSuchElementException as e:
    print(e)
finally:
    print(ctime())
    driver.quit()

