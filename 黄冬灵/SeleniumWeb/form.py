# coding:utf-8
# coder:DongLing
# 多表单切换

"""
在Web应用中经常会遇到frame/iframe表单嵌套页面的应用
WebDriver只能在一个页面上对元素识别与定位
对于frame/iframe表单内嵌页面上的元素无法直接定位。
这时就需要通过switch_to.frame()方法将当前定位的主体切换为frame/iframe表单的内嵌页面中
"""

from selenium import webdriver

driver = webdriver.Chrome()
driver.get("http://www.163.com")

driver.switch_to.frame('x-URS-iframe')
driver.find_element_by_name("email").clear()
driver.find_element_by_name("email").send_keys("username")
driver.find_element_by_name("password").clear()
driver.find_element_by_name("password").send_keys("password")
driver.find_element_by_id("dologin").click()
driver.switch_to_default_content()

import time
time.sleep(5)
driver.quit()
