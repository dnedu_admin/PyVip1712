# coding:utf-8
# coder:DongLing

# 基本操作
from selenium import webdriver
from selenium.webdriver.common.by import By

dr = webdriver.Chrome()
dr.get("http://www.baidu.com")
# print(dr.title)

# 设置浏览器窗口大小
# dr.set_window_size(480,800)

# 最大化
# dr.maximize_window()

"""
# 控制浏览器后退，前进
# 访问百度首页
first_url = "http://www.baidu.com"
print("now access %s" % first_url)
dr.get(first_url)

# 访问新闻页面
second_url = "http://news.baidu.com"
print("now access %s" % second_url)
dr.get(second_url)

# 返回(后退)到百度首页
print("back to %s " % first_url)
dr.back()

# 前进到新闻页
print("forward to %s " % second_url)
dr.forward()

# 当前url
print(dr.current_url)
dr.refresh()

# 关闭当前窗口
dr.close()
"""

"""
# 定位元素8种方式，name,tag,xpath,id,class,css,link,partial_link
kw = dr.find_element(by=By.XPATH,value="//*[@id='kw']")

# 点击、输入、提交
# clear() 清除文本 send_keys (value) 模拟按键输入。click() 单击元素
# submit() 方法用于提交表单

# dr.find_element_by_id("kw").clear()
# dr.find_element_by_id("kw").send_keys("动脑学院")
# dr.find_element_by_id("su").click()

# search_text = dr.find_element(By.ID,"kw")
# search_text.send_keys("动脑学院")
# search_text.submit()


# 其他常用方法
# 获得输入框的尺寸
size = dr.find_element_by_id('kw').size
print(size)

# 返回百度页面底部备案信息
text = dr.find_element_by_id("cp").text
print(text)

# 返回元素的属性值，可以是id、name、type或其他任意属性
attribute = dr.find_element_by_id("kw").get_attribute('type')
print(attribute)

# 返回元素的结果是否可见，返回结果为 True 或 False
result = dr.find_element_by_id("kw").is_displayed()
print(result)

dr.quit()
"""

# dr.quit()
