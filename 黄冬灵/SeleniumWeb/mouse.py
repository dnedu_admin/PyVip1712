# coding:utf-8
# coder:DongLing
# 鼠标操作

"""
在WebDriver中，将这些关于鼠标操作的方法封装在 ActionChains 类提供
ActionChains 类提供了鼠标操作的常用方法
perform(): 执行所有 ActionChains 中存储的行为
content_click(): 右击
doubule
"""

from selenium import webdriver
# 引入 ActionChains 类
from selenium.webdriver.common.action_chains import ActionChains

driver = webdriver.Chrome()
driver.get("https://www.baidu.com")

# 定位到要悬停的元素
above = driver.find_element_by_link_text("设置")
# 对定位到的元素执行鼠标悬停操作
ActionChains(driver).move_to_element(above).perform()
ActionChains(driver).context_click().perform()