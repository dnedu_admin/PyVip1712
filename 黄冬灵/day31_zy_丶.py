# coding:utf-8
# coder:DongLing

# 1.请写出一段Python代码实现删除一个list里面的重复元素
# a_list = [22,42,13,12,42,22,13]
# b_list = list(set(a_list))
# print("a_list的列表元素:%s" % a_list)
# print("b_list的列表元素:%s" % b_list)


# 2.如何用Python来进行查询和替换一个文本字符串？
# import re
# t_str = 'I am bad'
# pattern = re.compile(r'bad')
# match = re.findall(pattern, t_str)
# print("查询字符串:%s" % match)
#
# new_str = t_str.split()
# new_str[2] = 'good'
# print(' '.join(new_str))

# 3.a=1, b=2, 不用中间变量交换a和b的值
# a = 1
# b = 2
# print("交换前 a的值:%d" % a)
# print("交换前 b的值:%d" % b)
# a,b = b,a
# print("=" * 42)
# print("交换后 a的值:%d" % a)
# print("交换前 b的值:%d" % b)


# 4.写一个函数, 输入一个字符串, 返回倒序排列的结果:?如: string_reverse(‘abcdef’), 返回: ‘fedcba’
# def my_reverse(u_str):
#     n_str = u_str[::-1]
#     return ''.join(n_str)
#
# my_str = my_reverse('abcdef')
# print(my_str)

# 5种方法的比较:
# 1. 简单的步长为-1, 即字符串的翻转;
# t_str = 'abcdef'
# n_str = t_str[::-1]
# print(n_str)

# 2. 交换前后字母的位置;
# t_str = 'abcdef'
#
# n_list = [i for i in reversed(t_str)]
# n_str = ''.join(n_list)
# print(n_str)


# 3. 递归的方式, 每次输出一个字符;
# t_str = 'abcdef'
#
#
# def recursive(c_str, n):
#     global m_list
#     if not isinstance(c_str, list):
#         m_list = list(c_str)
#     else:
#         m_list = c_str
#
#     while n <= len(m_list) - 1:
#         print(m_list[n])
#         n += 1
#         return recursive(c_str, n)
#
#
# recursive(t_str, 0)

# 4. 双端队列, 使用extendleft()函数;
# import collections
#
# l = collections.deque()
# l.append(1)
# l.extendleft([5, 6, 7, 8])
# print(l)

# 5. 使用for循环, 从左至右输出;
# t_str = 'bcdsfea'
#
# for i in list(t_str):
#     print(i)

# 5.请用自己的算法, 按升序合并如下两个list, 并去除重复的元素
# 合并链表, 递归的快速排序, 去重;
# a_list = [5, 62, 5, 1, 5, 6, 36, 44]
# b_list = [62, 3, 55, 666, 156, 44, 5]
# c_list = list(set(a_list + b_list))
# print("排序前:%s" % c_list)
#
#
# def q_sort(a):
#     if isinstance(a, list):
#
#         if len(a) <= 1:
#             return a
#         return q_sort([i for i in a[1:] if i < a[0]]) + a[0:1] + \
#                q_sort([j for j in a[1:] if j >= a[0]])
#     else:
#         print("参数错误!")
#
#
# print("排序后:%s" % q_sort(c_list))
