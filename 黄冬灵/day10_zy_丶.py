# coding:utf-8
# coder:DongLing

# 1.
# 自定义一个异常类, 当list内元素长度超过10的时候抛出异常
# a_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
#
#
# class MyError(Exception):
#
#     def __init__(self, message='元素长度超过10'):
#         self.message = message
#
#     def __str__(self):
#         return self.message
#
#
# def my_fun(lists):
#     try:
#         if len(lists) > 10:
#             raise MyError()
#     except Exception as e:
#         print(e)
#     else:
#         print('没有异常')
#
# my_fun(a_list)




# 2.
# 思考如果对于多种不同的代码异常情况都要处理, 又该如何去
# 处理, 自己写一个小例子

# try:
#     pass
# except NameError as n:
#     # 处理NameError异常
#     print(n)
# except ValueError as v:
#     # 处理ValueError异常
#     print(v)
# except Exception as e:
#     # 以上的没有，交给父类处理
#     print(e)

# 3.
# try-except和try-finally有什么不同, 写例子理解区别
# try:
#     i = 100/0
#     print(i)
# except Exception as e:
#     print(e)

# try:
#     i = 100/0
#     print(i)
# finally:
#     print('finally()')  # 始终会执行这一行代码

# 4.
# 写函数，检查传入字典的每一个value的长度，如果大于2，
# 那么仅仅保留前两个长度的内容，并将新内容返回给调用者
dic = {"k1": "v1v1", "k2": [11, 22, 33]}

# def fun(a_dict):
#     new_dict = a_dict.copy()
#     for k in new_dict.keys():
#         if isinstance(new_dict[k],(list,tuple)):
#             if len(new_dict[k]) > 2:
#                 values = new_dict[k][0:2]
#                 new_dict[k] = values
#     return new_dict
#
# print(fun(dic))
# print(dic)
