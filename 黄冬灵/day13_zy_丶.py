# coding:utf-8
# coder:DongLing

# 1.自己写代码对比2.x深度优先和3.x广度优先的继承顺序
# class A:
#     def run(self):
#         print("A")
#
#
# class B:
#     def run(self):
#         print("B")
#
#
# class C(A, B):
#     pass
#
#
# class D(A, B):
#     def run(self):
#         print("D")
#
#
# class E(C, D):
#     pass
#
# """
# 1.python2.x --> 输出 A 深度优先 --> 当运行到E时，没有run函数，往父类C找，C也没有，往C的父类A找，因此输出A
# 2.python3.x --> 输出 B 广度优先 --> 当运行到E时，没有run函数，往父类C找，C也没有，继续往E的同级父类D找，D有run函数，因此输出D
# """
# e = E()
# e.run()


# 2.写一个类MyClass,使用__slots__属性,定义属性(id,name),创建该类实例
#     后,动态附加属性age,然后执行代码看看.
# class MyClass:
#     def __init__(self, id, name):
#         self.id = id
#         self.name = name
#
# my = MyClass('1', 'Dong')
# my.age = 23
# print(my.age)

# 3.使用type创建一个类,属性name,age,sex,方法sleep,run并创建此类的
#     实例对象
# def sleep(self):
#     print("睡觉")
#
# def run(self):
#     print("奔跑")
#
# MyClass = type('A',(),{'name':'Dong','age':23,'sex':'男','sleep':sleep,'run':run})
# my = MyClass()
# my.sleep()
# my.run()
# print(my.name)
# print(my.age)


# 4.写例子完成属性包装,包括获取属性,设置属性,删除属性
# class DataBean:
#
#     @property
#     def name(self):
#         return self._name
#
#     @name.setter
#     def name(self, name):
#         self._name = name
#
#     @name.deleter
#     def name(self):
#         del self._name
#
#
# bean = DataBean()
# bean.name = 'Huang'
# print(bean.name)
