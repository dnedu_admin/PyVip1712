# coding:utf-8
# coder:DongLing

from reportlab.platypus import SimpleDocTemplate, Paragraph
from reportlab.lib.styles import getSampleStyleSheet

file_name = "demo1.pdf"
f = open(file_name, "wb")
doc = SimpleDocTemplate(f, title="title_demo")

# pdf中的内容都可以放在datas
datas = []

# 段落
para1 = Paragraph("hello", getSampleStyleSheet()['Heading1'])
para2 = Paragraph("word", getSampleStyleSheet()['Heading2'])
datas.append(para1)
datas.append(para2)
doc.multiBuild(datas)
f.close()
