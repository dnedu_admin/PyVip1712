# coding:utf-8
# coder:DongLing

import re

# 1.识别下面字符串:’ben’,’hit’或者’ hut’
# content1 = 'ben'
# content2 = 'hit'
# content3 = 'hut'
#
# print(re.match('ben', content1).group())
#
# print(re.match('hit', content2).group())
#
# print(re.match('hut', content3).group())

# 2.匹配用一个空格分隔的任意一对单词,比如你的姓 名
# content = '黄 冬 灵'
# print(re.findall('黄\s冬\s灵', content))

# 3.匹配用一个逗号和一个空格分开的一个单词和一个字母
# content = 'word, d'
#
# print(re.match(r'(.+?)\s\w', content).group())

# 4.匹配简单的以’www’开头,以’com’结尾的web域名,比如:www.baidu.com
# content = 'www.baidu.com'
# print(re.findall(r'(^www)(.+?)(com$)', content))
# print(re.match(r'^www(.+?)com$', content).group())
