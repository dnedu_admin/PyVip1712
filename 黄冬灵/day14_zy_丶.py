# coding:utf-8
# coder:DongLing

from functools import reduce
# 1.必须完成:
#    高阶函数:filter判断[0,False,True,5,{},(2,3)]对应bool值
#    map实现1-10中所有奇数项的三次方,打印输出结果
#    reduce实现1-100所有偶数项的和

"""
高阶函数:filter判断[0,False,True,5,{},(2,3)]对应bool值
"""
# def is_bool(n):
#     lists = []
#     if isinstance(n, bool):
#         lists.append(n)
#         return lists
#
# new_list = filter(is_bool, [0, False, True, 5, {}, (2, 3)])
# print(list(new_list))
"""
map实现1-10中所有奇数项的三次方,打印输出结果
"""
# def calculate(n):
#     if n % 2 != 0:
#         x = n ** 3
#         return x
#     else:
#         return n
# results = map(calculate, range(1, 11))
# print(list(results))

"""
reduce实现1-100所有偶数项的和
"""
# def calculate(x, y):
#     return x + y
#
# lists = range(1, 101)
# new_lists = []
# for x in lists:
#     if x % 2 == 0:
#         new_lists.append(x)
# new_num = reduce(calculate, new_lists)
# print(new_num)

# 2.用递归函数实现斐波拉契数列
# def fibo(n):
#     if n <= 1:
#         return n
#     else:
#         return fibo(n-1) + fibo(n-2)
#
# for i in range(5):
#     print(fibo(i))
