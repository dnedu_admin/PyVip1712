# coding:utf-8
# coder:DongLing

# 1.列表list_1 = [[1,2,3,4,5],[{“name":"张三","age":28},{“name":"James":"age:30}]] ,
# 获取James的年龄

# list_1 = [[1, 2, 3, 4, 5], [{"name": "张三", "age": 28}, {"name": "James", "age": 30}]]
#
# for index, item in enumerate(list_1):
#         for index2, item2 in enumerate(item):
#             if isinstance(item2,dict):
#                 for k,v in item2.items():
#                     if 'James' is v:
#                         print(item2["age"])


# 2.名词解释: 可迭代对象、迭代器、生成器、装饰器
"""
可迭代对象：
list,dict,set,str,generator(生成器)，iterator(迭代器)，这全部都是可迭代对象，
可迭代对象都可以用for循环遍历

迭代器:
可以被next()函数调用并不断返回下一个值的对象称为迭代器:Iterator。生成器(generator)都是迭代器对象(Iterator)

生成器:
函数中包含yied，就实现了生成器函数

装饰器:
本质是函数，用于装饰其他函数，为函数增加本来函数没有的功能
"""

# 3.名词解释：面向对象编程中“类”中有几种方法？分别解释各种方法的用途。
"""
__init__(self):初始化对象(实例)，在创建新对象时调用
__del__(self):释放对象，在对象被删除之前调用
__new__(cls,*args,**kwd):创建对象的实例
__str__(self):在使用print语句输出实例时被调用
__call__(self,*args):把实例作为函数调用
"""

# 4.输出以下代码运行结果，解释变量作用域LEGB.
x = 2
z = 3


def func_outer():
    y = 9
    x = 0
    global z

    def func_inner():
        nonlocal x
        x += 5
        z = 6
        print("func_inner", max(x, z), max(x, y))

    z += 3
    x += 2
    print(x,y,z)
    return func_inner()


func_outer()()

"""
local:函数内部作用域
enclosing:函数内部与内嵌函数之间 
global:全局作用域 
builtin:内置作用域

输出结果为7 9
x = 7: 在函数外定义了一个变量x = 2，在func_inner()函数内部使用关键字nonlocal，
       代表x变量是使用外部外层变量x，因此这里输出的是2 + 5 = 7
y = 9:函数内部变量y的作用于为locals，x的值与y相比，取大的值，所以输出y = 9
z:报错，z是在函数外定义的变量，在函数内无法访问函数外变量，除非是使用global变量声明，否则会报错，在函数内部无法看到函数外部的变量
"""

# 5.解释什么是GIL(全局解释器锁)
"""
Python全局解释锁（GIL）简单来说就是一个互斥体（或者说锁），这样的机制只允许一个线程来控制Python解释器
"""

# 6.什么是线程、协程、进程
"""
线程与进程之间的关系:线程是属于进程的，线程运行在进程空间内，同一进程所产生的线程共享同一内存空间，当进程退出时该进程所产生的线程
    都会被强制退出并清除。同一进程间的不同线程的资源可以共享(但基本上不拥有系统资源)。
线程:FÏ有时被称为轻量级进程(Lightweight Process，LWP），是程序执行流的最小单元
进程:进程是资源的分配和调度的一个独立单元
协程:线程和进程的操作是由程序触发系统接口，最后的执行者是系统，它本质上是操作系统提供的功能。而协程的操作则是程序员指定的，
在python中通过yield，人为的实现并发处理。
"""

# 7.什么时候该用多线程、什么情况该用多进程
"""
并行处理多任务时，推荐使用多进程，
python下多进程程序要比多线程程序要高效。并且会随着Core数不断的增加，性能也会得到提升。

在一些等待的任务实现上如用户输入、文件读写和网络收发数据等，推荐使用多线程
"""

# 8.多个进程间如何通信？（注意多进程知识点回顾！！！）
"""
使用分布式进程，各个进程之间依靠网络通信。Python中multiprocessing模块支持多进程,子模块managers还支持把多任务分布到多台计算机上
    一个服务进程负责分配和调度master,其他要执行的任务分配到其他的workers上
"""