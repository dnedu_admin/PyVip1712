# coding:utf-8
# coder:DongLing
"""
1.自己写一个字典:
    name_dict = {'name':'ben','age':22,'sex':'男'}
    添加,删除,更新,清空操作
"""
# name_dict = {'name': 'ben', 'age': 22, 'sex': '男'}

# 添加
# name_dict['address'] = '广东'
# print(name_dict)

# 删除
# del name_dict['sex']
# name_dict.pop('sex')
# print(name_dict)

# 更新
# name_dict2 = {'address': '广东'}
# name_dict.update(name_dict2)
# print(name_dict)

# 清除
# name_dict.clear()
# print(name_dict)


"""
2.写两个集合:
    num1_set = {3,5,1,2,7}
    num2_set = {1,2,3,11}
    并分别进行&,|,^,-运算
"""
# num1_set = {3, 5, 1, 2, 7}
# num2_set = {1, 2, 3, 11}

# num3_set = num1_set & num2_set # 交集
# num3_set = num1_set | num2_set   # 并集
# num3_set = num1_set ^ num2_set     # 交集以外的元素
# num3_set = num1_set - num2_set
# num3_set = num2_set - num1_set
# print(num3_set)


"""
3.整数字典:
    num_dict = {'a':13,'b':22,'c':18,'d':24}
    按照dict中value从小到大的顺序排序
"""
# num_dict = {'a': 13, 'b': 22, 'c': 18, 'd': 24}
# sortedList = sorted(num_dict.items(), key=lambda x: x[1], reverse=False)
# print(sortedList)
