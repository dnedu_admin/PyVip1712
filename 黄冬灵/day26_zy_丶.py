# coding:utf-8
# coder:DongLing

# 1. PKCS#5(选择一种hash算法)加密算法加密你的中文名字,写出代码
import hashlib, binascii,os


# md5
def md5_cal():
    m = hashlib.md5()
    m.update('dong'.encode('utf-8'))
    print(m.hexdigest())


# sha1
def sha1_cal():
    m = hashlib.sha1()
    m.update('dong'.encode('utf-8'))
    print(m.hexdigest())


# hmac
def hmac_cal():
    pk = hashlib.pbkdf2_hmac('sha256', b'ling', b'Huang', 9999, dklen=32)
    r = binascii.hexlify(pk)
    print(r)


# 2. 创建文件test.py,写入一些数据,并计算该文件的md5值
def md5_file():
    if not os.path.exists('test.py'):
        with open('test.py','w') as f:
            f.write('这是测试数据')

    if os.path.exists('test.py'):
        with open('test.py', 'r') as f2:
            content = f2.read()
            print(content)
            m = hashlib.md5()
            m.update(content.encode('utf-8'))
            print(m.hexdigest)

# 3. 遵循上下文管理协议,自己编写一个类,去创建一个上下文管理器,然后自己使用看看
#    方式1:普通方式
#    方式2:使用contextmanager
# 方式1:普通方式
# class CommMethod:
#     def __init__(self,a_list):
#         print('初始化')
#         self.a_list = a_list
#
#     def __enter__(self):
#         print('执行代码')
#         self.a_list.append('dong')
#         return self
#
#     def __exit__(self, exc_type, exc_val, exc_tb):
#         self.a_list = None
#         if exc_type:
#             print(exc_val)
#         else:
#             print('正常状态')
#
#     def work(self):
#         for item in self.a_list:
#             print(item)
#
#
# with CommMethod(['a','b','c','d']) as f:
#     f.work()

# 方式2:使用contextmanager
# from contextlib import contextmanager
#
# class CommMethod:
#     def __init__(self,a_list):
#         print('初始化')
#         self.a_list = a_list
#
#     def work(self):
#         for item in self.a_list:
#             print(item)
#
# @contextmanager
# def cm_test(a_list):
#     print('开始处理')   # __enter__
#     ct = CommMethod(a_list) # 拿到实例
#     yield ct
#     print('处理完毕')   # __exit__
#
#
# with cm_test(['a','b','c','d']) as w:
#     w.work

# 4.使用colorama模块使用多颜色输出
from colorama import Fore, Back, Style

def colorama_zy():
    print(Fore.LIGHTRED_EX + '呵呵')
    print(Fore.WHITE + Back.BLACK + '嘻嘻')
    print(Style.RESET_ALL)
    print('哈哈')


if __name__ == '__main__':
    md5_cal()
    # sha1_cal()
    # hmac_cal()
    # md5_file()
    # colorama_zy()
