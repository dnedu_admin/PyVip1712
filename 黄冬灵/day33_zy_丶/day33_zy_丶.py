# coding:utf-8
# coder:DongLing

# 1.xlsxwriter包中，往工作表中插入写数据方法  work_sheet.write()
# 查看write函数定义，解释其函数定义中 @convert_cell_args这个装饰器的具体用途？

import xlsxwriter

file_name = "zy_33.xlsx"

# 1.创建工作簿
work_book = xlsxwriter.Workbook(file_name)

# 2.创建工作表
work_sheet = work_book.add_worksheet('python')

# 3.写数据

work_sheet.write(0,0,'您很好')
work_sheet.write("C8","科比")

work_book.close()


"""
@convert_cell_args
这个函数是用来转换符号的作用。convert_cell_args函数还有一个内置函数cell_wrapper
cell_wrapper(self, *args, **kwargs) --> 这个函数的功能是判断我们用户传递过来的第一个参数是否是int值，
是int的情况下，就强转成int值，如果不是int值，就把这个值传递给一个函数方法xl_cell_to_rowcol()转换成符号
这个符号的意思应该是对应Excel表中单元格的符号
"""