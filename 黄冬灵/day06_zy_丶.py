# coding:utf-8
# coder:DongLing

# 1.用while和for两种循环求计算1+3+5.....+45+47+49的值
# for循环
# num = range(1, 50)
# sumNum = 0
# for i in num:
#     if i % 2 == 0:
#         continue
#     else:
#         sumNum += i
# print(sumNum)

# while循环
# num = 49
# sumNum = 0
# while num > 0:
#     sumNum += num
#     num -= 2
# print(sumNum)



# 2.代码实现斐波那契数列(比如前30个项)
# def fibo(n):
#     if n <= 1:
#         return n
#     else:
#         return fibo(n-1) + fibo(n-2)
#
# for i in range(30):
#     print(fibo(i))
