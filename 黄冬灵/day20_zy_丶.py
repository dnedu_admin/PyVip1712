# coding:utf-8
# coder:DongLing

from types import coroutine
# 创建两个协程函数对象, 分别完成1, 3, 5⋯99, 2, 4, 6⋯, 100
# 然后让两个协程函数对象分别交替执行.在每一个协程函数对象的内部先打印输出
# 每一个数字, 然后执行完一轮之后打印提示信息, 已经交替执行完毕一轮
# 等所有的协程函数对象执行完毕之后, 分析一下数据信息

async def cal_single():
    for nums in range(1, 100, 2):
        print(nums)
        await do_next()
    print('奇数执行完毕')


async def cal_double():
    for nums in range(2, 101, 2):
        print(nums)
        await do_next()
    print('偶数执行完毕')


@coroutine
def do_next():
    yield

def run(a_list):
    b_list = list(a_list)
    while b_list:
        for item in b_list:
            try:
                item.send(None)
            except StopIteration as e:
                print(e)
                b_list.remove(item)

if __name__ == '__main__':
    single = cal_single()
    double = cal_double()
    c_list = [single, double]
    run(c_list)

"""
总结：
cal_single() --> 协程1
cal_double（）--> 协程2
do_next() --> 协程暂停开关
run() --> 完成协程调度

首先，先执行协程1，打印奇数的数字，列如打印1后，会遇到协程暂停(do_next),然后协程进行切换，执行协程2，这个时候打印偶数，如2,然后
遇到协程暂停(do_next),又回到协程1。如此类推，最后退出循环，Print循环结束后的语句
"""