# coding:utf-8
# coder:DongLing

# 1.range(),需要生成下面的列表,需要填入什么参数:
#     1.[3,4,5,6]
#     2.[3,6,9,12,15,18]
#     3.[-20,200,420,640,860]

"""
1.[3,4,5,6]
"""
# lists = list(range(3, 7))
# print(lists)

"""
2.[3,6,9,12,15,18]
"""
# lists = list(range(3, 21, 3))
# print(lists)

"""
3.[-20,200,420,640,860]
"""
# lists = list(range(-20, 901, 220))
# print(lists)

# 2.列表推导式或者生成器表达式(2选择1)完成1-100之间所有偶数和
# lists = [x+2 for x in range(0, 100) if x % 2 == 0]
# print(sum(lists))

# 3.定义一个函数,使用关键字yield创建一个生成器,求1,3,5,7,9各数
#    字的三次方,并把所有的结果放入到list里面打印输入
# def gen(n):
#     while n > 0:
#         num = n ** 3
#         yield num
#         n = n - 2
#
# g = gen(9)
# print(list(g))

# 4.写一个自定义的迭代器(iter)

# class my_iter():
#     def __init__(self, my_id):
#         self.my_id = my_id
#
#     def __next__(self):
#         if self.my_id == 0:
#             raise StopIteration()
#         self.my_id -= 1
#         return self.my_id
#
#     def __iter__(self):
#         return self
#
#
# iters = my_iter(4)
# for items in iters:
#     print(items)
