# coding:utf-8
# coder:DongLing

import random
import sys
# 1.
# a_list = [1, 2, 3, 2, 2]
# 删除a_list中所有的2

# a_list = [1, 2, 3, 2, 2]
# b_list = {}.fromkeys(a_list).keys()
# c_list = list(b_list)
# del c_list[1]
# print(c_list)

# 2.
# 用内置函数compile创建一个文件xx.py, 在文件里写你的名字, 然后用eval
# 函数读取文件中内容, 并打印输出到控制台

# name = "print('黄冬灵')"
# a = compile(name, 'xx.py', 'exec')
# eval(a)

# 3.
# 写一个猜数字的游戏, 给5次机会, 每次随机生成一个整数, 然后由控制台输入一个
# 数字, 比较大小.大了提示猜大了, 小了提示猜小了.猜对了提示恭喜你, 猜对了.
# 退出程序, 如果猜错了, 一共给5次机会, 5
# 次机会用完程序退出.

print("猜数字游戏")
change = 0
random_num = random.randint(1, 101)
while change < 5:
    input_num = input("请输入您猜的数字")
    if input_num.isalnum():
        input_num_new = int(input_num)
        change += 1
        if change < 5:
            if input_num_new > random_num:
                print('猜大了')
            elif input_num_new < random_num:
                print('猜小了')
            else:
                change = 5
                print('恭喜你，猜对了')
        else:
            print("机会用光了，游戏结束")
    else:
        print('输入类型错误')
