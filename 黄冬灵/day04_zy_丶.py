# coding:utf-8
# coder:DongLing

"""
第一题:

"""
num_list = [[1, 2], ['tom', 'jim'], (3, 4), ['ben']]
# 1. 在’ben’后面添加’kity’
# num_list.append("kity")
# print(num_list)

# 2. 获取包含’ben’的list的元素
# for index, item in enumerate(num_list):
#     if isinstance(item, list):
#         for index2, item2 in enumerate(item):
#             if 'ben' in item:
#                 print(item[index2])

# 3. 把’jim’修改为’lucy’
# for index, item in enumerate(num_list):
#     if isinstance(item, list):
#         for index2, item2 in enumerate(item):
#             if 'jim' in item:
#                 saveIndex = item.index('jim')
#                 item[saveIndex] = 'lucy'
#                 break
#
# for results in num_list:
#     print(results)

# 4. 尝试修改3为5,看看
# for index, item in enumerate(num_list):
#     if isinstance(item, tuple):
#         num_list[index] = (5, 4)
#
# for results in num_list:
#     print(results)

# 5. 把[6,7]添加到[‘tom’,’jim’]中作为第三个元素
# new_list = [6, 7]
# for index, item in enumerate(num_list):
#     if isinstance(item, list):
#         if 'tom' in item:
#             item.append(new_list)
#
# for results in num_list:
#     print(results)

# 6.把num_list切片操作:
#       num_list[-1::-1]
# 将步长设为-1,得到一个反序的序列
# print(num_list[::-1])

"""
第二题:

"""
numbers = [1, 3, 5, 7, 8, 25, 4, 20, 29]
# 1.对list所有的元素按从小到大的顺序排序
# numbers.sort()
# print(numbers)

# 2.求list所有元素之和
# sum = 0
# for item in numbers:
#     sum += item
#
# print(sum)

# 3.将所有元素倒序排列
# numbers.reverse()
# print(numbers)

