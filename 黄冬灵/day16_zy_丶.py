# coding:utf-8
# coder:DongLing

# 1.
# 写一个函数计算1 + 3 + 5 +⋯+97 + 99
# 的结果
# 再写一个装饰器函数, 对其装饰, 在运算之前, 新建一个文件
# 然后结算结果, 最后把计算的结果写入到文件里

# def cal(func):
#     with open('results.txt', 'w+') as f:
#         nums = func()
#         f.write(str(nums))
#
# @cal
# def extend():
#     sum_num = sum(range(1, 101, 2))
#     return sum_num


# 2.
# 写一个函数计算传入进来参数的平方, 并将结果.写一个带参数的装饰器,
# 将装饰器参数传入到被包装的函数, 计算并输入结果

# def decorator(func):
#     results = func(4, 2)
#     print(results)
#
#
# @decorator
# def cal_square(a, b):
#     return a ** b
