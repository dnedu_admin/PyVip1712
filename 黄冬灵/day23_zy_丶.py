# coding:utf-8
# coder:DongLing

from datetime import datetime, timedelta,date,time

# 1.datetime,获取你当前系统时间三天前的时间戳;并把当前时间戳增加
#  899999,然后转换为日期时间对象,把日期时间并按照%Y-%m-%d %H:%M:%S
#  输入打印这个日期对象,并获取date属性和time属性,最后判断当前日期时间
#  对象是星期几;
# datatime1 = datetime.now() - timedelta(days=3)
# datatime2 = datatime1.timestamp() + 899999
# print(datatime2)
# t = datetime.utcfromtimestamp(datatime2)
#
# time_str = datetime.strftime(t, '%Y-%m-%d %H:%M:%S')
# print(time_str)
#
# my_date = date(t.year, t.month, t.day)
# my_time = time(t.hour, t.minute, t.second)
# date_time = datetime.combine(my_date, my_time)
#
# print(datetime.weekday(date_time) + 1)


# 2.使用os模块线获取当前工作目录的路径,并打印当前目录下所有目录和文件,
# 获取当前脚本的绝对路径, 分割当前脚本文件的目录和文件在tuple中并判断
# 当前文件所在的目录下存不存在文件game.py,如果不存在则创建该文件.
# 并往该文件里写入数据信息,然后判断当前目录下是否存在目录
# /wow/temp/,如果不存在则创建,最后把目录名称改为:/wow/map/
from os import path
import os
# path_dir = os.getcwd()
# print(path_dir)
# print(os.listdir(path_dir))
# for item in path.split(path_dir):
#     if path.isdir(item):
#         if path.exists(r'./game.py'):
#             print('该文件已经存在')
#         else:
#             with open('game.py', 'w+') as f:
#                 f.write('写入数据')
#         if path.exists(r'./wow/temp/'):
#             print('该文件已经存在')
#         else:
#             os.makedirs('./wow/temp/')
#             os.renames('./wow/temp/', './wow/map/')
#     else:
#         print('不是目录')

# 3.写函数，检查获取传入列表或元组对象的所有奇数位索引对应的元素，并将其作为新的列表返回给调用者
# test_list = (22,11,55,33,77,53,34) # 22,55,77,34
# def check_list(a_list):
#     if isinstance(a_list,(list,tuple)):
#         b_list = []
#         for index, value in enumerate(a_list):
#             if (index + 1) % 2 != 0:
#                 b_list.append(value)
#         return b_list
#     else:
#         raise Exception('必须传入列表或者元祖')
#
# new_list = check_list(test_list)
# print(new_list)
