# coding:utf-8
# coder:DongLing

# 合拼字符串函数
def spitStr(strOne, strTwo, strThird):
    list_str = [strOne, strTwo, strThird]
    mergeStr = "___".join(list_str)
    return mergeStr


# 1. 格式化显示浮点数12.78，要求保留3位小数，输出占20字符，空位用0补齐
num = 12.78
str_format = "{:020.3f}".format(num)
print(str_format)

print("----------------------------------------------")

# 2. 将字符串”动脑学院”转换为bytes类型。
strToBytes = bytes("动脑学院", 'utf-8')
print(strToBytes)

print("----------------------------------------------")

# 3.将以下3个字符串合并为一个字符串，字符串之间用3个_分割
# hello
# 动脑
# pythonvip
# 合并为“hello___动脑___pythonvip”
str1 = "hello"
str2 = "动脑"
str3 = "pythonvip"

newStr = spitStr(str1, str2, str3)
print("合拼字符串:" + newStr)

print("----------------------------------------------")

# 4. 删除字符串  “    你好，动脑eric     ”首尾的空格符号
opStr = "    你好，动脑eric     "
print(opStr.strip())

print("----------------------------------------------")

# 5. 用户信息存在如下所示字符串中，包含信息依次为  姓名   学号  年龄，
# 户信息存在如下所示字符串中，包含信息依次为  姓名   学号  年龄，
# 不同信息之间的空格数不确定，请提取用户信息分别保存到 xxx_name,xxx_num, xxx_age。
#
#
# 之间的空格数不确定，请提取用户信息分别保存到 xxx_name,xxx_num, xxx_age。
#
# eric=“    Eric   dnpy_001     28”
#
# 提取后 eric_name=“Eric”eric_num=”dnpy_001”,eric_age=”28”
# zhangsan = “ 张三         dnpy_100    22     ”


# eric = "    Eric   dnpy_001     28"
# newEric = eric.split()
# print("{0},{1},{2}".format("eric_name=\"" + newEric[0] + "\"",
#                            "eric_num=\"" + newEric[1] + "\"",
#                            "eric_age=\"" + newEric[2] + "\""))


zhangsan = " 张三         dnpy_100    22     "
newZhangsan = zhangsan.split()
print("{0},{1},{2}".format("zhangsan_name=\"" + newZhangsan[0] + "\"",
                           "zhangsan_num=\"" + newZhangsan[1] + "\"",
                           "zhangsan_age=\"" + newZhangsan[2] + "\""))


