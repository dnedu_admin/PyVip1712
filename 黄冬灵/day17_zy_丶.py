# coding:utf-8
# coder:DongLing

# 1.使用pickle,json(注意:两个模块)
# 把字典a_dict = [1,2,3,[4,5,6]]
# 序列化与反序列化,序列化,保存到文件名为serialize.txt文件里面,然后大开,并读取数据
# 反序列化为python对象加载到内存,并打印输出
# 尝试序列化/反序列非文件中,直接操作
"""
pickle模块序列化与反序列化(文件中操作)
"""
import pickle
import json
from io import StringIO, BytesIO

a_dict = [1, 2, 3, [4, 5, 6]]

# with open('serialize.txt', 'wb+') as f:
#     pickle.dump(a_dict, f)
#
# with open('serialize.txt','rb+') as f2:
#     print(pickle.loads(f2.read()))

"""
json模块序列化与反序列化(文件中操作)
"""
# with open('serialize.txt', 'w+') as f:
#     json.dump(a_dict, f)
#
# # 反序列化为python对象加载到内存,并打印输出
# with open('serialize.txt', 'r+') as f2:
#     b_dict = json.loads(f2.read())
#     print(b_dict)


"""
pickle模块序列化与反序列化(非文件中操作)
"""
# data1 = pickle.dumps(a_dict)
# print(data1)
#
# data2 = pickle.loads(data1)
# print(data2)

"""
json模块序列化与反序列化(非文件中操作)
"""
# data1 = json.dumps(a_dict)
# print(data1)
#
# data2 = json.loads(data1)
# print(data2)
# print(json.dumps(a_dict))


# 2.自己写代码对比深浅拷贝的区别,写完代码然后看看运行结果,然后分析原因
#    比如：a_list=[1,2,3,4,[8,9,0]]
import copy

"""
赋值方式:
1.如下:当list1 = [1, 2, 3, 4] 时，list1赋值给list2，这个时候，都是指向同一块内存地址，所以当我们修改list1[0]的值时，打印list1和list2
的值都会发生改变(打印他们的地址值以及用is方法判断，都可以验证出它们指向的都是同一块地址值)

2.如下:当list1 = [1, 2, 3, 4] 时，list1赋值给list2,然后给list1重新赋值(地址值发生改变)，这个时候，list1和list2指向的就不是同一块内存地址
因此，当我们修改list[0]的值时，会发现list1的值发生了改变，但是list2的值却不受影响(打印他们的地址值以及用is方法判断，都可以验证出它们指向的不是
同一块内存地址)
"""
# 1.
# list1 = [1, 2, 3, 4]
# list2 = list1
# list1[0] = 'test'
# print(list1)
# print(list2)
# print(list1 is list2)
# print(id(list1))
# print(id(list2))

# 2.
# list1 = [1, 2, 3, 4]
# list2 = list1
# list1 = [2, 3, 4, 5]
# list1[0] = 'test'
# print(list1)
# print(list2)
# print(list1 is list2)
# print(id(list1))
# print(id(list2))

"""
浅拷贝
"""

"""
 1.没有嵌套的list
 总结:内存地址不一样，修改其中一个，不会影响另外一个
"""
# origin_list = [1, 2, 3, 4, 5]
# copy_list = copy.deepcopy(origin_list)
#
# print(origin_list is copy_list)
# print(id(origin_list))
# print(id(copy_list))
#
# copy_list[0] = 'str'
# print(origin_list)
# print(copy_list)

"""
 2.有嵌套的list
 总结:嵌套list,分为两层，第一层为外层list,第二层为嵌套的内层list。浅拷贝出来的list与原来的list对比，它们之间外层list的地址是不一样，
 但是它们的内层地址却是共享的(地址相同),当我们修改list第一层的值时，并不会影响另外一个list的值，可修改内层list的时候，因为其地址是共享，
 所以两个list的内层list的值都发生了改变
"""
# origin_list = [1, 2, 3, 4, [5, 6]]
# copy_list = copy.copy(origin_list)
# origin_list[4].append(55)
# origin_list.append(11)
# # 地址值:4353737160 4353737352
# print(id(origin_list), id(origin_list[4]))
#
# # 地址值:4353737544 4353737352
# print(id(copy_list), id(copy_list[4]))
#
# # [1, 2, 3, 4, [5, 6, 55], 11]
# # [1, 2, 3, 4, [5, 6, 55]]
# print(origin_list)
# print(copy_list)

"""
深拷贝
"""
"""
深拷贝:无论list中有多少层，它们都是完全独立的，改变任何一层中的某个元素，都不会影响其它list的值
"""
# origin_list = [1, 2, 3, 4, [5, 6]]
# copy_list = copy.deepcopy(origin_list)
# origin_list[4].append(55)
# origin_list.append(11)
# # 地址值:4344545736 4344545928
# print(id(origin_list), id(origin_list[4]))
#
# # 地址值:4344546120 4344546184
# print(id(copy_list), id(copy_list[4]))
#
# # [1, 2, 3, 4, [5, 6, 55], 11]
# # [1, 2, 3, 4, [5, 6, 55]]
# print(origin_list)
# print(copy_list)

# 3.分别写代码完成内存缓冲区文本数据和二进制数据的读/写操作
# s = StringIO('哈哈哈哈哈')
# b = BytesIO('嘻嘻嘻嘻'.encode('utf-8'))
# print(s.getvalue())
# print(b.getvalue())
