# coding:utf-8
# coder:DongLing

import unittest
from math import sqrt
# 1.写一个函数,然后写单元测试

# 待测试的函数
# def run(num):
#     if isinstance(num, (int, float)):
#         return sqrt(num)
#     else:
#         raise TypeError('参数类型不正确')
#
# # 测试用例
# class MyTest(unittest.TestCase):
#     def setUp(self):
#         pass
#
#     def tearDown(self):
#         pass
#
#     def testrun(self):
#         self.assertEqual(run(4), 2, '测试run函数失败')



# 2.自定义一个类,完成单元测试
# class Student():
#     def __init__(self,name):
#         self.name = name
#
#     def run(self):
#         print('%s正在上课' % self.name)
#
# class MyClassTest(unittest.TestCase):
#
#     def setUp(self):
#         self.s = Student('Dong')
#
#     def testrun(self):
#         self.s.run()
#
#     def tearDown(self):
#         self.s = None

if __name__ == '__main__':
    unittest.main()
