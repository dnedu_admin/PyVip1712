# coding:utf-8
# coder:DongLing

# 1.计算range(1,99999)的总和,采取三种方式,第一:重复10次计算总时间;
#  第二:开启十个子线程去计算;第三:开启十个进程去计算,然后对比耗时
import time
from threading import Thread
from multiprocessing import Process,Pool
import multiprocessing
"""
1.重复10次计算总时间
"""
# time_start = time.time()
# for nums in range(1,11):
#     print(sum(range(1, 100000)))
# time_end = time.time() - time_start
# print(time_end) # 0.03371310234069824

"""
2.开启十个子线程去计算
"""
def cal(nums):
    print(sum(range(1, 100000)))

# 构建列表，创建了10个子线程对象
# thread_list = [Thread(target=cal, args=(nums,)) for nums in range(1, 11)]
# time_start = time.time()
# for tds in thread_list:
#     tds.start()
#     tds.join()
# time_end = time.time() - time_start
# print(time_end)  # 0.03573107719421387

"""
3.开启十个进程去计算,然后对比耗时
"""
# process_list = [Process(target=cal, args=(nums,)) for nums in range(1, 11)]
# time_start = time.time()
# for ps in process_list:
#     ps.start()
#     ps.join()
# time_end = time.time() - time_start
# print(time_end) # 0.07508206367492676

# 2.将第一题计算十次改为使用线程池去计算,线程池线程个数由你自己电脑cpu逻辑核数决定
# pool = Pool(multiprocessing.cpu_count())
# time_start = time.time()
# for nums in range(1, 11):
#     pool.apply_async(cal, args=(nums,))
# pool.close()
# pool.join()
# time_end = time.time() - time_start
# print(time_end) # 0.10468792915344238
