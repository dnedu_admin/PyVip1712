# coding:utf-8
# coder:DongLing

# 1.
# 自己动手写一个学生类, 创建属性名字, 年龄, 性别, 完成
# __new__, __init__, __str__, __del__，__call__
"""
先执行__new__，后执行__init__
"""
# class Student:
#     def __init__(self, name, age, gender):
#         self.name = name
#         self.age = age
#         self.gender = gender
#         print("__init__")
#
#     def __new__(cls, *args, **kwargs):
#         print('__new__')
#         return super(Student, cls).__new__(cls)
#
#     def __str__(self):
#         return '姓名:%s ,年龄:%s,性别:%s' % (self.name, self.age, self.gender)
#
#     def __del__(self):
#         print("销毁Student对象实例")
#
#     def __call__(self, *args, **kwargs):
#         print("__call__")
#
# s = Student('Jack', '23', '男')
# print(s)


# 2.
# 创建父类Person, 属性name, 函数eat, run, 创建子类Student, 学生类
# 继承Person, 属性name, age, id, 函数eat, study, 全局定义一个函数
# get_name, 传入一个Person类的实例参数, 分别传入Person和Student
# 两个类的实例对象进行调用.

# class Person:
#     def __init__(self, name):
#         self.name = name
#
#     def eat(self):
#         print("Person eat")
#
#     def run(self):
#         print("Person run")
#
#
# class Student(Person):
#
#     def __init__(self, name, age, id):
#         super().__init__(name)
#         self.name = name
#         self.age = age
#         self.id = id
#
#     def eat(self):
#         print("Student eat")
#
#     def study(self):
#         print("Student study")
#
#
# def get_name(person):
#     print('姓名:%s' % person.name)
#     person.eat()
#     person.run()
#     if isinstance(person, Student):
#         person.study()
#
# p = Person('父亲')
# s = Student('学生', '23', '123')
# get_name(p)
# get_name(s)

# 3.
# 用代码完成python多态例子和鸭子模型
"""
多态
"""
# class Animal:
#
#     def eat(self):
#         print("Animal eat")
#
#
# class Dog(Animal):
#
#     def eat(self):
#         print("Dog eat")
#
# class Cat(Animal):
#
#     def eat(self):
#         print("Cat eat")
#
# def show(animal):
#     animal.eat()
#
# a = Animal()
# d = Dog()
# c = Cat()
#
# show(a)
# show(d)
# show(c)

"""
鸭子模型
"""
# class Animal:
#
#     def eat(self):
#         print("Animal eat")
#
#
# class Dog:
#
#     def eat(self):
#         print("Dog eat")
#
# class Cat:
#
#     def eat(self):
#         print("Cat eat")
#
# def show(animal):
#     animal.eat()
#
# a = Animal()
# d = Dog()
# c = Cat()
#
# show(a)
# show(d)
# show(c)
