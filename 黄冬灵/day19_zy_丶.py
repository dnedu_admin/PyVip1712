# coding:utf-8
# coder:DongLing

from multiprocessing import Pool,Process,cpu_count  # 进程池
from multiprocessing.dummy import Pool as threadPool  # 线程池
from threading import Thread
# 使用装饰器增强完成1-999999所有奇数和计算10次
def cal(func):
    def inner(num):
        new_list = range(1, 1000000, 2)
        func(new_list)
    return inner

@cal
def cal2(new_list):
    print(sum(new_list))

#    1.使用普通方法

def cal_sum(new_list):
    for i in range(10):
        print(sum(new_list))

# cal_sum(range(1, 1000000, 2))

#    2.使用多线程
# thread_list = [Thread(target=cal2, args=(None,)) for nums in range(1, 11)]
# for tds in thread_list:
#     tds.start()
#     tds.join()
#
# #    3.使用多进程
# process_list = [Process(target=cal2, args=(None,)) for nums in range(1, 11)]
# for pls in process_list:
#     pls.start()
#     pls.join()


#    4.采用线程池完成计算
# @cal
# def thread_cal(nums):
#     pools = threadPool(cpu_count())
#     pools.apply_async(cal_sum, (nums,))
#     pools.close()
#     pools.join()

#    5.采用进程池完成计算
# @cal
# def process_cal(nums):
#     pools = Pool(cpu_count())
#     pools.apply_async(cal_sum, (nums,))
#     pools.close()
#     pools.join()

# if __name__ == '__main__':
#     thread_cal(999999)
#     process_cal(999999)
