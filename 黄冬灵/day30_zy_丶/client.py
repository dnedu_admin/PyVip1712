# coding:utf-8
# coder:DongLing

import socket

host = '127.0.0.1'
port = 9008

# 1.基于tcp协议socket套接字编程半双工模式客户端
# s = socket.socket()
# s.connect((host, port))
#
# while 1:
#     content = input('请输入发送给服务器的数据:')
#     s.send(content.encode('utf-8'))
#
#     result = s.recv(1024)
#     print('接受服务器返回的数据为%s' % result.decode('utf-8'))
#     print('*******' * 44)
# s.close()

# 2.socketserver异步非阻塞编程,编写代码 --> 客户端
addr = (host, port)
s = socket.socket()

s.connect(addr)

while 1:
    content = input('请输入发送给服务器的数据为%s')
    if not content or content is 'ex':
        print('主动与服务器端断开连接!!!')
        break
    s.send(content.encode('utf-8'))
    result = s.recv(1024)
    if not result:
        break
    print('服务器返回给客户端的数据为%s' % result.decode('utf-8'))

s.close()
