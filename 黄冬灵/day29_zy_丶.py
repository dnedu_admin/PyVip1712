# coding:utf-8
# coder:DongLing

"""
    1.函数缓存的使用
    2.性能分析器的使用
    3.内存分析器的使用
"""

# 1.函数缓存的使用
from functools import lru_cache
# @lru_cache(maxsize=10, typed=False)
# def fib(n):
#     if n < 2:
#         return n
#     return fib(n - 1) + fib(n - 2)
#
#
# r = [fib(n) for n in range(35)]
# print(r)
# print(fib.cache_info())

# 2.性能分析器的使用
import cProfile
from line_profiler import LineProfiler

# def fib(n):
#     if n < 2:
#         return n
#     return fib(n - 1) + fib(n - 2)

# cProfile.run('fib(35)')

# prof = LineProfiler(fib)
# prof.enable()
# fib(35)
# prof.disable()
# prof.print_stats()

# 3.内存分析器的使用
# from memory_profiler import profile
#
# @profile
# def test():
#     x = [9] * (10 ** 5 * 3)
#     y = [7] * (10 ** 6 * 3)
#     del y
#     del x
#
# if __name__ == '__main__':
#     test()
