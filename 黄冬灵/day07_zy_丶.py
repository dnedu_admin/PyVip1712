# coding:utf-8
# coder:DongLing

# 1.简述普通参数、默认参数、关键字参数、动态收集参数的区别,理解为主
"""
1.普通参数: 直接在函数后面加上形参。如 def fun(x,y)
2.默认参数: 在函数的形参中初始化默认值。如 def fun(x = 1,y)
3.关键字参数: 用于函数调用。如有一个已存在的函数def fun(z,x,y),调用该函数的时候fun(x = 1,z = 2,y =3)
            指定函数中形参的值，且顺序不一定是按照函数中声明参数顺序
4.动态收集参数: 在函数括号里面添加 * 或 ** 来声明该形参为动态收集参数，在调用该函数并传入参数的时候，
              会自动识别元组或字典
"""

# 2.def foo(x, y, z, *args, **kw):
#     sum = x + y + z
#     print(sum)
#     for i in args:
#         print(i)
#     print(kw)
#     for j in kw.items():
#         print(j)
# a_tuple = (1,2,3)     此处参数修改为2个看看会怎么样?
# b_dict = {'name': 'jim', 'age': 28, 'add': '上海'}
# foo(*a_tuple, **b_dict)    分析这里会怎么样?

# def foo(x, y, z, *args, **kw):
#     sum_num = x + y + z
#     print(sum_num)
#     for i in args:
#         print(i)
#     # print(kw)
#     for j in kw.items():
#         print(j)
#
#
# a_tuple = (1, 2, 3)  # 这里参数如果修改为2个，会报错 --> 参数不一致
# b_dict = {'name': 'jim', 'age': 28, 'add': '上海'}
"""
 a_tuple本身是一个元组，但是用过 * 声明，在调用函数处对该元组进行了解包，
 因此x = 1,y = 2,z = 3。而 *args为空
"""
# foo(*a_tuple, **b_dict)


# 3.题目:执行分析下代码
#     def func(a, b, c=9, *args, **kw):
#          print('a =', a, 'b =', b, 'c =', c, 'args =', args, 'kw =', kw)
#     func(1,2)
#     func(1,2,3)
#     func(1,2,3,4)
#     func(1,2,3,4,5)
#     func(1,2,3,4,5,6,name='jim')
#     func(1,2,3,4,5,6,name='tom',age=22)
#     扩展: 如果把你的函数也定义成  def  get_sum(*args,**kw):pass
#     你的函数可以接受多少参数?

# def func(a, b, c=9, *args, **kw):
#     print('a=', a, 'b=', b, 'c=', c, 'args=', args, 'kw=', kw)
#
#
# func(1, 2)
# func(1, 2, 3)
# func(1, 2, 3, 4)
# func(1, 2, 3, 4, 5)
# func(1, 2, 3, 4, 5, 6, name='jim')
# func(1, 2, 3, 4, 5, 6, name='tom', age=22)

"""
1.如果传入的是普通参数的情况。可以传入无限多的参数
2.如果传入的是关键字参数的情况。元组里面包含会元组，字典，同理
"""
# def get_sum(*args, **kw):
#     for i in args:
#         print('args:', i)
#     for j in kw.items():
#         print('kw:', j)


# a_tuple = (1, 2, 3)
# b_dict = {'name': 'Jack', 'age': 24}
# get_sum('name', 5, 4, 6, 6, 7, *a_tuple, **b_dict)
# get_sum(args=(1, 3, 4), kw={'name': 'jim'})


# 4.写一个函数函数，计算传入字符串中的数字、字母、空格和其他的个数分别为多少?
# s = '41245151asgjknw  &&'
# def judge_type(s):
#     num_sum = 0
#     letter_sum = 0
#     space_sum = 0
#     other_sum = 0
#     lists = list(s)
#     for i in lists:
#         if str(i).isdigit():
#             num_sum += 1
#         elif str(i).isalpha():
#             letter_sum += 1
#         elif str(i).isspace():
#             space_sum += 1
#         else:
#             other_sum += 1
#     print('数字:', num_sum, '字母:', letter_sum, '空格:', space_sum, '其他:', other_sum)
#
# judge_type(s)
