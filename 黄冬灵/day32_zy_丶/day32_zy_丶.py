# coding:utf-8
# coder:DongLing

import pymysql
import pandas as pd
from sqlalchemy import create_engine
from pandas.io.sql import read_sql
import numpy as np

score_dict = {
              "english": [80, 90, 100],
              "c": [88.8, 99.9, 75.0],
              "python": [100, 100, 100]}

df_dict = pd.DataFrame(score_dict, index=['%s' % i for i in range(1,4)],
                       columns=["english", "c", "python"])
# print(df_dict)
# engine = create_engine("mysql+pymysql://root:123456@localhost:3306/zyDB?charset=utf8",
#                        echo=False)
# df_dict.to_sql(name="score", con=engine, if_exists="append", index=False)


db = pymysql.connect("localhost","root","123456","zyDB")
query = "select english,c,python from score"
df = read_sql(query,db)
print(df)
