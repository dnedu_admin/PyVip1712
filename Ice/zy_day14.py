# 1.filter判断[0,False,True,5,{},(2,3)]对应bool值
# value = [0, False, True, 5, {}, (2, 3)]
# a_filter = filter(bool, value)
# print(list(a_filter))

# # 2.map实现1-10中所有奇数项的三次方,打印输出结果
# def abc1():
#     a_abc1 = map(lambda x:x**3, range(1, 10,2))
#     print(list(a_abc1))
# if __name__ == "__main__":
#     abc1()
# # 3.reduce实现1-100所有偶数项的和
# from functools import reduce
# print(reduce(lambda x, y: x + y, range(0, 101, 2)))

# 4.用递归函数实现斐波拉契数列
# def feibolaqie(num):
#     if isinstance(num,int):
#         if num <= 2:
#             return 1
#         else:
#             return feibolaqie(num-1)+feibolaqie(num-2)
#     else:
#         raise TypeError('参数类型错误')
# def fun(num1):
#     a_map = map(feibolaqie,range(1,num1+1))
#     num_list= list(a_map)
#     print(num_list)
# if __name__ == '__main__':
#     fun(10)