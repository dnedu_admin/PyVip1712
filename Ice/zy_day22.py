# 1.
# 使用正则表达式匹配电话号码：0713 - xxxxxx(湖南省座机号码)
#
# import re
# pattern = re.compile('^0713-\d{6,8}')
# hnzj1 = pattern.search('07##13-22222222')
# if hnzj1:
#     print(hnzj1.group())

# 2.
# 区号中可以包含()
# 或者 -, 而且是可选的可以, 就是说你写的正则表达式匹配
# 800 - 555 - 1212, 555 - 1212, (800)
# 555 - 1212
import re
pattern= re.compile('(^\d+-\d+-\d+)|\d+-\d+|\(\d+\)\d+-\d+')
hnzj1=pattern.search('800-22222222')
if hnzj1:
    print(hnzj1.group())
hnzj2=pattern.search('1212-22222222')
if hnzj2:
    print(hnzj2.group())


# 3.
# 选作题:
# 实现一个爬虫代码