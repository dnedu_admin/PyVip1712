# 1.自己动手写一个学生类, 创建属性名字, 年龄, 性别,
# 完成__new__, __init__, __str__, __del__，__call__
# class Student():
#     name = ''
#     age = 0
#     sex = ''
#     def __new__(cls, *args, **kwargs):#构造函数
#         print('构造函数在初始化函数之前执行')
#         return super().__new__(cls)
#     def __init__(self, name, age, sex):#初始化函数
#         self.name = name
#         self.age = age
#         self.gander = sex
#         print('初始化函数在构造函数之后执行')
#     def __str__(self):
#         return '返回对象的字符串表达式'
#     def __del__(self):
#         print('销毁当前对象%s'%self)
#     def __call__(self, *args, **kwargs):   #__call__：实例对象当做函数来调用的时候会自动执行
#         def ben():
#             print('我是猪')
#         return ben
# S1 = Student('mengmeng',16,'女')
# S1()()


# 2.创建父类Person, 属性name, 函数eat, run, 创建子类Student, 学生类
# 继承Person, 属性name, age, id, 函数eat, study, 全局定义一个函数
# get_name, 传入一个Person类的实例参数, 分别传入Person和Student
# 两个类的实例对象进行调用.
# class Person():
#     name = '人'
#     def eat(self):
#         print('吃饱了才有力气干活')
#     def run(self):
#         print('我可以跑到时速10km')
# class  Student(Person):
#     def __init__(self,name,age,id):
#         self.name = name
#         self.age = age
#         self.id = id
#     def eat(self):
#         print('吃饱了才能更好的学习')
#     def study(self):
#         print('我可以学习使自己强大')
# def get_name(Person):   #发生继承关系
#     Person.eat()
# P1 = Person()
# S1 = Student('ice',16,1)
# get_name(P1)
# get_name(S1)

# 3.用代码完成python多态例子和鸭子模型

#鸭子形态------>不存在继承关系---->(静态语言没有这个感念)
#python是动态解释型语言--->动态语言的特征--->鸭子形态
# class Person():
#     def eat(self):
#         print('吃饱了才有力气干活')
# class  Student():
#     def eat(self):
#         print('吃饱了才能更好的学习')
# def get_name(Person):   #发生继承关系
#     Person.eat()
# P1 = Person()
# S1 = Student()
# get_name(P1)
# get_name(S1)

#多态例子
# class Person():
#     def eat(self):
#         print('吃饱了才有力气干活')
# class  Student(Person): #有继承关系
#     def eat(self):
#         print('吃饱了才能更好的学习')
# def get_name(Person):
#     Person.eat()
# P1 = Person()
# S1 = Student()
# get_name(P1)
# get_name(S1)