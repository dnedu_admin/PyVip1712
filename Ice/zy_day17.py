# 1.使用pickle,json(注意:两个模块)
# 把字典a_dict = [1,2,3,[4,5,6]]
# 序列化与反序列化,序列化,保存到文件名为serialize.txt文件里面,然后打开,并读取数据
# 反序列化为python对象加载到内存,并打印输出
# 尝试序列化/反序列非文件中,直接操作
import pickle
# a_list = [1,2,3,[4,5,6]]             #内存中
# f = open('serialize.txt','wb+')
# pickle.dump(a_list,f,0)              #二进制数据处理
# f.close()

# f = open('serialize.txt','rb+')
# b_list= pickle.load(f)
# print(type(b_list))
# print(b_list)
# f.close()

import json
# a_list = [1,2,3,[4,5,6]]             #内存中
# f = open('serialize.txt','w+')
# json.dump(a_list,f)                #字符串
# f.close()

f = open('serialize.txt','r+')
b_list= json.load(f)
print(type(b_list))
print(b_list)
f.close()

# f = open('serialize.txt','rb+')
# b_list= pickle.load(f)
# print(type(b_list))
# print(b_list)
# f.close()

# 2.自己写代码对比深浅拷贝的区别,写完代码然后看看运行结果,然后分析原因
# 比如：a_list=[1,2,3,4,[8,9,0]]
# from copy import copy,deepcopy
# # a_list=[1,2,3,4,[8,9,0]]
# # b_list = copy(a_list) #copy 浅拷贝
# # b_list[4][2]= 10
# # b_list[0]=0
# # print(a_list)     #外层元素拷贝，内层元素共享
# # print(b_list)
#
# a_list=[1,2,3,4,[8,9,0]]
# b_list = deepcopy(a_list) #deepcopy 深拷贝
# b_list[4][0]= 7
# b_list[0]=5
# print(a_list)
# print(b_list)       #所有层的元素全部拷贝


# 3.分别写代码完 成内存缓冲区文本数据和二进制数据的读/写操作
# from io import StringIO
# f = StringIO()               写入缓冲区数据
# f.write('My name is Ice\n')
# f.write('age = 17\n')
# print(f.getvalue()) #查看缓冲区数据

# f = StringIO('My name is Ice\nage = 17')
# while 1 :
#     read = f.readline()
#     if len(read) != 0:
#         print(read.strip()) #去除两端空格
#     else:
#         break
