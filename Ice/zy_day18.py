# 1.计算range(1,99999)的总和,采取三种方式,第一:重复10次计算总时间;
#  第二:开启十个子线程去计算;第三:开启十个进程去计算,然后对比耗时,Process(进程对象)
# import time
# def count():
#     join = time.time()
#     print('开始时间%s'%join)
#     tally = 1
#     while tally <=10:
#         sum(range(1,99999))
#         tally =1
#     end = time.time()
#     print('结束时间%s'%end)           #开始时间1520474494.9315689
#     print('总耗时%s'%(end - join))    #结束时间1520474494.95413
# count()                               #总耗时0.022561073303222656


# 第二:开启十个子线程去计算
# import time
# from threading import Thread,current_thread
# def sum(num):
#     result=sum(range(1,101))
# thread_list=[Thread(target=sum,args=(num,)) for num in range(1,11)]
# time1=time.time()
# for thr in thread_list:
#     thr.start()
#     thr.join()
# time2=time.time()
# print('Time2：%s' % str(time2-time1)) # Time2：0.0010008811950683594

# 第三:开启十个进程去计算
# from multiprocessing import Process,Pool
# import time
# def fun(num):
#     result = sum(range(1,99999))
#     time.sleep(2)
# if __name__ == '__main__':
#     pool = Pool(10)
#     time1 = time.time()
#     for num in range(10):
#         pool.apply_async(fun,args=())
#     pool.close()
#     pool.join()
#     time2 = time.time()
#     print('Time3:'str(time2 - time1))      #Time3:0.1278393268585205



# 2.将第一题计算十次改为使用线程池去计算,线程池线程个数由你自己电脑cpu逻辑核数决定
# from threadpool import ThreadPool,makeRequests
# import time
# def sum_calculate(num):
#     result=sum(range(1,99999))
# pool=ThreadPool(8)  # 四核八线程
# a_list=list(range(1,11))
# request_list=makeRequests(sum_calculate,a_list)
# time1=time.time()
# for req in request_list:
#     pool.putRequest(req)
# pool.wait()
# time2=time.time()
# print('线程池花费时间%s' % str(time2-time1)) # 线程池花费时间0.02601766586303711