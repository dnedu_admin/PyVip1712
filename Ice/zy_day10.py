# 1.自定义一个异常类, 当list内元素长度超过10的时候抛出异常
import sys
# class   MylistError (Exception):
#     def __init__(self,message):
#         super().__init__(self)
#         self.message = message
#     def __str__(self):
#         return self.message
#
# try:
#     my_list = []
#     for item in range(1,100):
#         my_list.append(item)
#     if len(my_list) > 10:
#         t = MylistError('类表长度超过规定长度')
#         raise t
# except (MylistError,Exception) as e:
#     print(e)
#     print('Exception')
# 2.思考如果对于多种不同的代码异常情况都要处理,又该如何去
# 处理,自己写一个小例子
# def divission(a,b):
#     try:
#         return a/b
#     except ZeroDivisionError as e:
#         print('除数不能为零')
# def throw_exception(i):
#     try:
#         if i < 0:
#             raise   ValueError('数值不正确')
#     except  ValueError as e:
#         print('数值不能小于零')
# result = divission(3,0)
# throw_exception(-5)

# 3.try-except和try-finally有什么不同,写例子理解区别
# try:
#     print(9/0)
# except ZeroDivisionError as e:
#     print(e)
#     print('except出现异常才会出现')
# except Exception    as e:
#     print(e)
#     print('except出现异常才会出现')
# finally:
#     print('finally,不管是否出现异常都要执行')



# 4.写函数，检查传入字典的每一个value的长度，如果大于2，
     # 那么仅仅保留前两个长度的内容，并将新内容返回给调用者
     # dic = {“k1”: "v1v1","k2":[11,22,33}}
# def func(a_dict):
#     if isinstance(a_dict,dict):
#         for key,value in a_dict.items():
#             if  len(value) > 2:
#                 a_dict[key] = value[:2]
#         return a_dict
# new_dict = {'key1':'10','key2':(1,2,3,4)}
# func1 = func(new_dict)
# print(func1)