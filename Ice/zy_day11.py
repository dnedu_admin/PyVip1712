# 1.文件名称:use.txt
#    创建该文件,路径由你自己指定,打开该文件,往里面写入
#    str_content = ['tom is boy','\n','you are cool','\n',
#           '今天你怎么还不回来']
# 把数据写入到该文件中, 注意操作的模式, 关闭文件句柄后
# 再次打开该文件句柄, 然后读取文件中内容, 用with代码块
# 实现, 然后打印输出信息到控制台, 注意换行符

# with open('use.txt','w+',encoding='utf-8') as f:
#     str_content = ['tom is boy','\n','you are cool','\n']
#     f.writelines(str_content)
# with open('use.txt','r+',encoding='utf-8') as f:
#     for line in f:
#         print(line.strip())

# 2.with创建一个文件homework.txt,尝试多种操作模式.进行写读操作,注意区别
# with open('homework.txt','w+',encoding='utf-8') as f:
#     f.readlines()             #文件不存在时新建文件
#     f.writelines('11\n22')    #新写入数据覆盖原有数据
# with open('homework.txt','r+',encoding='utf-8') as f:
    # f.writelines('11') #文件不存在时出错
    # f.readlines()
# with open('homework.txt','a+',encoding='utf-8') as f:
#     f.writelines('11\n2\n3')    #文件不存在时新建文件
#     f.writelines('4\n5')        #继续在后面追加

# 3.理解文件句柄,对比read(),readline(),readlines()写一个小例子
# with open('Ice.txt','w+',encoding='utf-8') as f:
#     f.write('1234\n2\n3\n4')
#     f.seek(0)
#     print(f.tell())
#     # print(f.read(4))
#     # print(f.readline())   #返回第一行
#     # print(f.readline())   #返回第二行
#     print(f.readlines())    #返回一个列表


# 4.准备一张jpg图片,把图片中数据读书出来,并写入到文件my_img.jpg文件中,
    # 操作完毕后尝试打开my_img.jpg文件看图片显示正不正常
s = open('smfhb.jpg','rb')
m = open('my_img.jpg','wb')
s_r = s.read()
m.write(s_r)
print(s_r)
s.close()
m.close()
