# 作业:
# 1.datetime,获取你当前系统时间三天前的时间戳;并把当前时间戳增加
#  899999,然后转换为日期时间对象,把日期时间并按照%Y-%m-%d %H:%M:%S
#  输入打印这个日期对象,并获取date属性和time属性,最后判断当前日期时间
#  对象是星期几;
#

# from datetime import date,time,datetime,timedelta
# datetime1 =datetime.now() - timedelta(days=3)
# timestamp1 = datetime1.timestamp()  #当前系统时间三天前的时间戳
# print(type(timestamp1))
# timestamp2 = 899999.0
# timestamp3 = timestamp2 + timestamp1#fromtimestamp 将时间戳转化成datetime对象
# print(timestamp3)
# print(type(timestamp3))
# datetime2 = datetime.fromtimestamp(timestamp3)  #datetime---->时间搓   # 1970-0-0 ...时间标准-->参照物(时间搓)
# print(datetime2)
# content = datetime.strftime(datetime2,'%Y-%m-%d %H:%M:%S')  #原格式2018-03-28 03:18:52.354965 转换成指定格式
# print(content)
# print(datetime2.date())           #获取date,time属性
# print(datetime2.time())
#
# print('当前日期是星期:',datetime2.weekday())  #<built-in method date of datetime.datetime object at 0x00000030B7980468>




# 2.使用os模块线获取当前工作目录的路径,并打印当前目录下所有目录和文件 51231
# 获取当前脚本的绝对路径, 分割当前脚本文件的目录和文件在tuple中
# 判断当前文件所在的目录下存不存在文件game.py,如果不存在则创建该文件.并往该文件里写入数据信息,
# 然后判断当前目录下是否存在目录/wow/temp/,如果不存在则创建,最后把目录名称改为:/wow/map/
# import os
# from os import path
# print(os.getcwd())      #获取当前目录
# print(os.listdir('.')) #当前目录结构(目录下的文件)
#
# print(os.path.abspath(r'./zy_day23.py'))  #/r'/zy_day23.py 相对路径  r'./zy_day23.py决对路径
# print(path.split(r'./zy_day23.py'))     #文件切割(元组,第一个元素所处的目录,第二个元素就是文件名称)
# print(path.splitext(r'./zy_day23.py'))  #文件切割(元组,文件,后缀切割)
#
# if path.exists(r'./game.py'):
#     pass
# else:
#     open('game.py','w')
#     with open('game.py','w',encoding='utf-8') as f:
#         f.write('#very happy!')
#
# if path.exists(r'./wow/temp/'):
#     os.renames('./wow/temp/','./wow/map')
# else:
#     os.makedirs('./wow/temp/')
#     os.renames('./wow/temp/', './wow/map')




# 3.写函数，检查获取传入列表或元组对象的所有奇数位索引对应的元素，并将其作为新的列表返回给调用者 
# def func(args):
#     new_list = []
#     if isinstance(args,(list,tuple)):
#         for item in args:               #enumerate(容器的元素取一个索引)
#             if item%2 != 0:
#                 index1 = args.index(item)
#                 new_list.append(index1)
#             else:
#                 continue
#     else:
#         a= Exception('类型错误,list,tuple')
#         raise a
#     return new_list
# a = list(range(0,100))
# print(func(a))
