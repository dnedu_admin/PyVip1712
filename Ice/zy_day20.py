# 创建两个协程函数对象, 分别完成1, 3, 5…99, 2, 4, 6…, 100
# 然后让两个协程函数对象分别交替执行.在每一个协程函数对象的输出内部先打印
# 每一个数字, 然后执行完一轮之后打印提示信息, 已经交替执行完毕一轮
# 等所有的协程函数对象执行完毕之后, 分析一下数据信息
from types import coroutine
async def odd():
    for num in range(1,100,2):
        print(num)
        await do_next()
async def even():
    for num in range(2,101,2):
        print(num)
        await do_next()
@coroutine
def do_next():
    yield

def run(coroutine_list):
    a_list1 = list(coroutine_list)
    while  a_list1:
        for item in a_list1:
            try:
                item.send(None)
            except Exception as e:
                a_list1.remove(item)

if __name__ == '__main__':
    odd=odd()
    even=even()
    b_list=[]
    b_list.append(odd)
    b_list.append(even)
    run(b_list)