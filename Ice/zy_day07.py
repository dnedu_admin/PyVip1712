#1.简述普通参数、默认参数、关键字参数、动态收集参数的区别,理解为主
#普通参数：要以对应的顺序传入函数，调用时参数的数量必须和声明时一致
#默认参数：定义函数时可以给参数一个默认值，在调用时如果没有参数就使用默认值，有参数就替换默认值
#关键字参数：允许函数调用时参数的顺序与声明时不一样，python解释器可以用参数名匹配参数值
#动态收集参数：加了星号（*）的变量名会存放所有未命名的变量参数，如果没有指定参数，它就是一个空元组
#加了两个星号（**），和一个星号一样，不过是形成一个字典dict

# 2.def foo(x,y,z,*args, **kw):
#     sum = x + y + z
#     print(sum)
#     for i in args:
#         print(i)
#         print(0)
#     print(kw)
#     for j in kw.items():
#         print(1)
#         print(j)
# a_tuple = (1,2,3)     #此处参数修改为2个看看会怎么样?         会报错，缺少了一个Z的参数
# # b_dict = {'name': 'jim', 'age': 28, 'add': '上海'}
# # a = foo(*a_tuple, **b_dict)    #分析这里会怎么样?   a_tuple中的1,2,3 赋值给x,y,z  sum = 6
# a = foo(*a_tuple)                                 #b_dict 的值给了**kw 正常执行第二个for循环
# print(a)                                          # None由于函数定义里面没有return，所以默认返回None


# #3.题目:执行分析下代码
# def func(a, b, c=9, *args, **kw):
#     print('a =', a, 'b =', b, 'c =', c, 'args =', args, 'kw =', kw)
# func(1,2)
# func(1,2,3)
# func(1,2,3,4)
# func(1,2,3,4,5)
# a = 1,2
# func(1,2,3,4,5,6,name='jim')
# func(*a,5,name='tom',age=22)             #要满足了a,b,c,才可以给aegs值,也可以引用一个元组
# #扩展: 如果把你的函数也定义成  def  get_sum(*args,**kw):pass
# #你的函数可以接受多少参数?
# def get_sum(*kw,**args):
#     pass
#     print(args)
# get_sum(1,2,3,4,5,6,7,8)
# print(get_sum(1,2,3))
# get_sum(name='xiaomen')
# 参数个数没有限制


# 4.写一个函数
# 函数，计算传入字符串中的数字、字母、空格和其他的个数分别为多少?
def func1(s):
    alpha_num = 0
    space_num = 0
    digit_num = 0
    others_num = 0
    for i in s:
        if i.isdigit():    # isdigit 判断有没有数字
            digit_num += 1
        elif i.isspace():   # isspace 判断有没有空格
            space_num += 1
        elif i.isalpha():    #isalpha 判断有没有字母
            alpha_num += 1
        else:
            others_num += 1
    return ('数字的数量为:',digit_num,'字母的数量为:',alpha_num,'空格的数量为:',space_num,'其他的数量为:',others_num)
str = func1("lbb, 897 ")
print(str)




