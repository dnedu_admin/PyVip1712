# 1.识别下面字符串:’ben’,’hit’或者’ hut’
# 2.匹配用一个空格分隔的任意一对单词,比如你的姓 名
# 3.匹配用一个逗号和一个空格分开的一个单词和一个字母
# 4.匹配简单的以’www’开头,以’com’结尾的web域名,比如:www.baidu.com

# 1.识别下面字符串:’ben’,’hit’或者’ hut’
# import re
# print(re.findall('ben|hit|hut','ben, hit, hut'))


# 2.匹配用一个空格分隔的任意一对单词,比如你的姓 名
# import re
# print(re.search('I.ce','my name is I ce').span())
# print(re.search('I.ce','my name is I ce').group())
#......



# 3.匹配用一个逗号和一个空格分开的一个单词和一个字母
# import re
# print(re.search('Ice...','Ice, w').group())
# print(re.search('Ice\W\sw','Ice, w').group())
# print(re.search('Ice\W\Ww','Ice, w').group())
# print(re.search('Ice\D\sw','Ice, w').group())
# print(re.search('Ice\D\Dw','Ice, w').group())
#


# 4.匹配简单的以’www’开头,以’com’结尾的web域名,比如:www.baidu.com
import re
print(re.search('www\.[a-zA-Z]*\.top','www.amazingit.top').group())
print(re.search('www\.[a-zA-Z]*\.top','www.amazingit.top').group)
print(re.search('www\.[a-zA-Z]*\.top','www.amazingit.top'))