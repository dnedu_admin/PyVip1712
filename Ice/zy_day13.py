# 1.自己写代码对比2.x深度优先和3.x广度优先的继承顺序
# class  A():
#     def run(self):
#         print('A')
# class B():
#     age = 16
# class C(A,B):
#     pass
# class D(A,B):
#     def run(self):
#         print('D')
# class E(C,D):
#     pass
# class F(C,D):
#     pass
# f = F()
# f.run()
# 2.x版本,是深度优先     3.X版本广度优先
# F-->C--->A.run-->A      F-->C-->D.run-->D


# 2.写一个类MyClass,使用__slots__属性,定义属性(id,name),创建该类实例
# 后,动态附加属性age,然后执行代码看看.
# class MyClass():
#     __slots__ = ('id','name') #元组
# c1 = MyClass()
# c1.age = 10
# print(c1.age)
# 动态附加属性age不在__slots__限制范围内,所以不能附加成功
#python3.x 新特性

# 3.使用type创建一个类,属性name,age,sex,方法sleep,run并创建此类的实例对象
# def sleep(self):
#     print('请不要打扰我睡觉!')
# def run(self):
#     print('跑的飞起!')
# Person = type('Person',(),{'name':'ice','age':16,'sex':'男','run':run,'sleep':sleep})
# p = Person()
# p.run()
# p.sleep()
# print(p.name)
# print(p.age)
# print(p.sex)
# 4.写例子完成属性包装,包括获取属性,设置属性,删除属性
# class Ice():
#     @property   #装饰器把函数包装成属性来使用
#     def age(self):
#         return self._age
#     @age.setter
#     def age(self,age):
#         self._age = age
#     @age.deleter
#     def age(self):
#         del self._age
# i = Ice()
# i.age = 10
# print(i.age)