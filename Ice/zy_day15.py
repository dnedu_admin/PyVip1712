# 1.range(),需要生成下面的列表,需要填入什么参数:
#     1.[3,4,5,6]
#     2.[3,6,9,12,15,18]
#     3.[-20,200,420,640,860]
# print(list(range(3,7)))
# print(list(range(3,19,3)))
# print(list(range(-20,1000,220)))


# 2.列表推导式或者生成器表达式(2选择1)完成1-100之间所有偶数和
# print(sum([x for x in range(2,101,2)]))
# a_list = [x for x in [1,2,3]]
# print(type(a_list))
# b_generator = (x for x in range(1,101) if x%2==0)
# print(sum(b_generator),type(b_generator))


# 3.定义一个函数,使用关键字yield创建一个生成器,求1,3,5,7,9各数字的三次方,并把所有的结果放入到list里面打印输入
# def make_generator():
#     for i in range(1,10,2):
#         yield i
#         print(i**3)
#
#
# generator = make_generator()
# print(type(generator),list(generator))