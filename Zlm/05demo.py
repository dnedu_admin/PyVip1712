#coding:utf-8
#author:朱立明（Zlm）


# 第一题:
num_list=[[1,2],['tom','jim'],(3,4),['ben']]
#  1. 在’ben’后面添加’kity’
# num_list.append('kity')
# print(num_list)

#  2. 获取包含’ben’的list的元素
# print(num_list[-1])

#  3. 把’jim’修改为’lucy’
# num_list[1][1]='lucy'
# print(num_list)

#  4. 尝试修改3为5,看看
# print("不用尝试了，tuple不支持修改值。所以改不了")

#  5. 把[6,7]添加到[‘tom’,’jim’]中作为第三个元素
# num_list[1].append([6,7])
# print(num_list)

#  6.把num_list切片操作:
# num_list[1::2]
# print(num_list)
#
#
# 第二题:
numbers = [1,3,5,7,8,25,4,20,29]
# 1.对list所有的元素按从小到大的顺序排序
# numbers.sort()
# print(numbers)

# 2.求list所有元素之和
# num=0
# for item in numbers:
#     num=num+item
#     if(item==numbers[-1]):
#         print(num)  #全部计算之后，再进行打印

# 3.将所有元素倒序排列
# numbers.reverse()
# print(numbers)
